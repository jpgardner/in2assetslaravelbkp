<?php

namespace App\Helpers;

use App\Models\Property;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AppHelper
{
    //Image Helper Functions - Returns Full image path to correct sizes
    // Mini returns Slider Thumb Size
    public static function imgMini($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_143_83'.substr($image, $extension_pos);

        return $thumb;
    }

    // Thumb returns List View Size
    public static function imgThumb($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_265_163'.substr($image, $extension_pos);

        return $thumb;
    }

    // Map returns Property Map View Image
    public static function imgMap($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_400_161'.substr($image, $extension_pos);

        return $thumb;
    }

    // Full size Image for Slider
    public static function imgLarge($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_835_467'.substr($image, $extension_pos);

        return $thumb;
    }

    // Facebook OG Tag
    public static function imgFacebook($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_1200_630'.substr($image, $extension_pos);

        return $thumb;
    }

    // Landscape size Image for Slider
    public static function imgLandscape($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_1920_600'.substr($image, $extension_pos);

        return $thumb;
    }

    //manage Property Image

    public static function imgManProp($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_265_163'.substr($image, $extension_pos);

        return $thumb;
    }

    //blog Small Image

    public static function imgSmlBlog($image)
    {
        $extension_pos = strrpos($image, '.'); // find position of the last dot, so where the extension starts
        $thumb = substr($image, 0, $extension_pos).'_small'.substr($image, $extension_pos);

        return $thumb;
    }

    public static function checkMultipleWhichEmpty()
    {
        $returnArray = [];
        $count = 0;
        foreach (func_get_args() as $arg) {
            if (empty($arg)) {
                $returnArray[] = $count;
            }
            $count++;
        }

        return $returnArray;
    }

    //Formulates full URL for property by ID
    public static function propertyURL($id)
    {
        $property = Property::find($id);

        $url = url('property').'/'.$id.'/'.$property['slug'];

        return $url;
    }

    public static function propertyEditURL($id)
    {
        $url = url('admin/property/'.$id.'/edit');

        return $url;
    }

    public static function leadEditURL($id)
    {
        $url = url('leads/'.$id.'/edit');

        return $url;
    }

    public static function classifiedURL($id)
    {
        $url = url('classifieds').'/'.$id;

        return $url;
    }

    public static function propertyTypeFull($property)
    {
        if (is_array($property)) {
            $property2 = new stdClass();
            foreach ($property as $key => $value) {
                $property2->$key = $value;
            }
            $property = $property2;
        }

        if ($property->property_type == 'Residential') {
            if (strlen($property->dwelling_type) > 16 or strlen($property->suburb_town_city) > 16) {
                $type = $property->dwelling_type.' in<br>'.$property->suburb_town_city;
            } else {
                $type = $property->dwelling_type.' in '.$property->suburb_town_city;
            }
        } elseif ($property->property_type == 'Commercial') {
            if (strlen($property->commercial_type) > 16 or strlen($property->suburb_town_city) > 16) {
                $type = $property->commercial_type.' in<br>'.$property->suburb_town_city;
            } else {
                $type = $property->commercial_type.' in '.$property->suburb_town_city;
            }
        } elseif ($property->property_type == 'Farm') {
            if (strlen($property->farm_type) > 16 or strlen($property->suburb_town_city) > 16) {
                $type = $property->farm_type.' in<br>'.$property->suburb_town_city;
            } else {
                $type = $property->farm_type.' in '.$property->suburb_town_city;
            }
        } else {
            $type = 'property';
        }

        return $type;
    }

    public static function propertyAddress($property)
    {
        $address = '';
        if ($property->show_street_address == '1') {
            if (! empty($property->complex_number)) {
                $address .= $property->complex_number.' ';
            }
            if (! empty($property->complex_name)) {
                $address .= $property->complex_name.', ';
            }
            if (! empty($property->street_number)) {
                $address .= $property->street_number.' ';
            }
            if (! empty($property->street_address)) {
                $address .= $property->street_address.', ';
            }
        }

        $address .= $property->suburb_town_city.', ';
        if ($property->suburb_town_city == $property->area) {
            $address .= '';
        } else {
            $address .= $property->area.', ';
        }

        $address .= $property->province;

        return $address;
    }

    public static function propertyType($property)
    {
        if ($property->property_type === 'Residential') {
            $type = $property->dwelling_type;
        } elseif ($property->property_type === 'Commercial') {
            $type = $property->commercial_type;
        } elseif ($property->property_type === 'Farm') {
            $type = $property->farm_type;
        } else {
            $type = null;
        }

        return $type;
    }

    public static function getLastQuery()
    {
        $queries = DB::getQueryLog();

        return $queries;
    }

    public static function formatPhoneNumber($phoneNumber)
    {
        $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);
        if (strlen($phoneNumber) > 10) {
            $countryCode = substr($phoneNumber, 0, strlen($phoneNumber) - 10);
            $areaCode = substr($phoneNumber, -10, 3);
            $nextThree = substr($phoneNumber, -7, 3);
            $lastFour = substr($phoneNumber, -4, 4);
            $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
        } elseif (strlen($phoneNumber) == 10) {
            Log::alert('Number');
            $areaCode = substr($phoneNumber, 0, 3);
            $nextThree = substr($phoneNumber, 3, 3);
            $lastFour = substr($phoneNumber, 6, 4);
            $phoneNumber = $areaCode.' '.$nextThree.' '.$lastFour;
            Log::alert($phoneNumber);
        } elseif (strlen($phoneNumber) == 7) {
            $nextThree = substr($phoneNumber, 0, 3);
            $lastFour = substr($phoneNumber, 3, 4);
            $phoneNumber = $nextThree.'-'.$lastFour;
        }

        return $phoneNumber;
    }

    public static function getFormattedPrice($property)
    {
        if ($property->price > 1) {
            $formattedPrice = 'R'.number_format($property->price, 0, '', ' ');
        } else {
            $formattedPrice = 'POA';
        }
        if ($property->listing_type == 'in2rental') {
            if ($property->rental_terms == 1) {
                $formattedPrice .= ' pm';
            } elseif ($property->rental_terms == 2) {
                $formattedPrice .= ' per m&sup2';
            }
        }

        return $formattedPrice;
    }
}
