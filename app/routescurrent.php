<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
 *
 * Redirects for url name changes
 *
 */

Route::get('commercial_buy', function () {
    return redirect('buying-commercial-property');
});
Route::get('contact', function () {
    return redirect('contact-us');
});

Route::get('newdoc', function () {
    return redirect('newdoc');
});
Route::get('marketing', function () {
    return redirect('property-marketing');
});
Route::get('about', function () {
    return redirect('about-us');
});
Route::get('info_page', function () {
    return redirect('property-blog');
});
Route::get('network', function () {
    return redirect('broker-network');
});

Route::get('sell', function () {
    return redirect('sell-property');
});
Route::get('let', function () {
    return redirect('let-property');
});
Route::get('calendar', function () {
    return redirect('auction-calendar');
});
Route::get('physical_auctions/{id}', function ($id) {
    return redirect('physical-auction/'.$id);
});

Route::get('commercial_sell_explained', function () {
    return redirect('sell-property');
});
// ---------------------End--------------------------------------

Route::get('addData', 'FunctionController@addOldData');
Route::get('getActualCoords/{xcoord}', 'FunctionController@getActualCoords');
Route::get('transferAttributes', 'FunctionController@runFunction');

Route::get('loginfbnew', ['as' => 'loginfbnew', 'uses' => 'FacebookController@setupFacebook']);
Route::get('loginfb/fb/callback', ['as' => 'loginfb.fb.callback', 'uses' => 'FacebookController@loginFacebook']);

Route::get('loginGoogle', ['as' => '/loginGoogle', 'uses' => 'RegistrationController@loginGoogle']);

//--------------------------------------------------------------------------------------------------------------------------------------------
//--Static Pages-------------------------------------------------------------------------------------------------------------
Route::post('admin/vetting/updateProperty', ['uses' => 'VettingController@updateProperty']);
Route::post('admin/vetting/removeProperty', ['uses' => 'VettingController@removeProperty']);
Route::get('admin/vetting/updatePropertiesDaily', 'VettingController@updatePropertiesDaily');
//---Homepage
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home']);
Route::get('/compliance_certificates', 'PagesController@complianceCerts');

//Route::get('/wesell_landing', ['as' => 'wesell_landing', 'uses' => 'PagesController@wesell']);

Route::get('/uploads/update', ['uses' => 'UploadsController@update_All_positions']);

Route::get('mobile_adv_search/{property_type}/{listing_type}/{province}/{city}/{suburb}/{price_range?}/{rent_price_range?}/{floor_range?}/{land_range?}/{sub_type?}/{bedrooms?}/{bathrooms?}',
    ['uses' => 'PagesController@mobileAdvSearch']);
//---About
Route::group(['prefix' => 'about-us'], function () {
    Route::get('/', ['as' => 'about', 'uses' => 'PagesController@about']);
//    Route::get('/in2assets_overview', ['as' => 'in2assetsoverview', 'uses' => 'PagesController@in2assetsOverview']);
//    Route::get('/wesell_overview', ['as' => 'weselloverview', 'uses' => 'PagesController@wesellOverview']);
//    Route::get('/why', ['as' => 'why', 'uses' => 'PagesController@why']);
    Route::get('/careers', ['as' => 'careers', 'uses' => 'PagesController@careers']);
//    Route::get('/service-providers', ['as' => 'affiliates', 'uses' => 'PagesController@affiliates']);
//    Route::get('/shareholders', ['as' => 'board_of_directors', 'uses' => 'PagesController@board_of_directors']);
});

//---Auction Calendar

Route::get('auction-calendar', ['as' => 'calendar', 'uses' => 'PagesController@calendar']);
Route::get('auction-calendar/{id}/{auctionName?}', ['as' => 'calendarDetail', 'uses' => 'PagesController@calendarDetail']);
Route::get('physical-auction/{id}/{auctionName?}', ['as' => 'calendarDetail', 'uses' => 'PagesController@calendarDetail']);
Route::get('findauctions/{auctionDate}', ['as' => 'findauctions', 'uses' => 'PagesController@findauctions']);

//---Academy----------------------------------------------

Route::get('api/academy/{status}', ['as' => 'api.academy', 'uses' => 'AcademyController@getDatatable']);
Route::get('/academy/{category}', ['as' => 'academy_category_path', 'uses' => 'AcademyController@academyCategory']);
Route::get('academy/search/{category}', ['as' => 'academy.search', 'uses' => 'AcademyController@articleSearch']);

//--Blog-------------------------------------------------
Route::get('/property-blog', ['as' => 'blog_path', 'uses' => 'BlogController@index']);
Route::get('/blog/category/{category}', ['as' => 'blog_category_path', 'uses' => 'BlogController@blogCategory']);
Route::get('/blog', ['as' => 'blog', 'uses' => 'BlogController@index']);
Route::get('/blog-list', ['as' => 'blog_list', 'uses' => 'BlogController@index_list']);
Route::get('/blog/{slug}', ['as' => 'single_blog_path', 'uses' => 'BlogController@showBlog']);
Route::get('/api/blog', ['as' => 'blog_api_path', 'uses' => 'BlogController@homeSnippet']);
Route::get('api/blog/{status}', ['as' => 'api.blog', 'uses' => 'BlogController@getDatatable']);

Route::get('/search', ['as' => 'posts.search', 'uses' => 'BlogController@postSearch']);

//--Careers
Route::post('/careers/storecv', ['as' => 'save_cv_path', 'uses' => 'CareersController@saveCV']);
Route::get('/careers/{id}/uploadcv', ['as' => 'upload_cv_path', 'uses' => 'CareersController@uploadCV']);
Route::get('/admin/career/create', ['as' => 'admin.career.create', 'uses' => 'CareersController@create']);
Route::resource('career', 'CareersController', ['only' => ['show', 'store']]);
Route::resource('jobform', 'CareersController@getJobinfo');
Route::get('api/career/{status}', ['as' => 'api.career', 'uses' => 'CareersController@getDatatable']);

//Richards Bay Mall
Route::get('richards_bay_mall', ['as' => 'richards_bay_mall', 'uses' => 'PagesController@RichardsBayMall']);
Route::get('opus1', ['as' => 'network', 'uses' => 'PagesController@Opus1']);
Route::get('midlands_hotel', ['as' => 'network', 'uses' => 'PagesController@MidlandsHotel']);
Route::post('opus-offer', ['as' => 'opus-offer', 'uses' => 'PagesController@OpusOffer']);

//---Contact
Route::get('contact-us', ['as' => 'contact', 'uses' => 'PagesController@contact']);
Route::get('newdoc', ['as' => 'contact', 'uses' => 'PagesController@newdoc']);
Route::post('contact', ['as' => 'makecontact', 'uses' => 'PagesController@makeContact']);

Route::post('save_number', ['as' => 'save_number', 'uses' => 'UsersController@saveNumber']);

//---Classified
//Route::post('/classifieds/search', ['as' => 'classifieds.search', 'uses' => 'ClassifiedsController@search']);
//Route::post('/classifieds/contact', ['as' => 'classifieds.contact', 'uses' => 'ClassifiedsController@contact']);
//Route::get('/classifieds/manage_classifieds', ['as' => 'classifieds.manage_classifieds', 'uses' => 'ClassifiedsController@manageClassifieds']);
//Route::resource('classifieds', 'ClassifiedsController');
Route::get('classifieds/create', ['as' => 'classifieds.create', 'uses' => 'ClassifiedsController@create']);
Route::get('/property-marketing', ['as' => 'marketing', 'uses' => 'PagesController@Marketing']);

//Route::get('/marketing/packages', ['as' => 'marketing.packages', 'uses' => 'MarketingController@index']);
//Route::get('marketing/sell-commercial-package', ['as' => 'commercial_package_sell', 'uses' => 'MarketingController@CommercialPackageSell']);
//---Academy
//Route::group(array('prefix' => 'academy'), function () {
//    Route::get('/', ['as' => 'helpadvice', 'uses' => 'PagesController@helpadvice']);
//    Route::get('/property-library', ['as' => 'helpadvice_PropertyLibrary', 'uses' => 'PagesController@helpadvicePropertyLibrary']);
//    Route::get('/propertyglossary', ['as' => 'helpadvice_PropertyGlossary', 'uses' => 'PagesController@helpadvicePropertyGlossary']);
//    Route::get('/property-library/cparegulations', ['as' => 'helpadvice_PropertyLibrary_CPARegulations', 'uses' => 'PagesController@helpadvicePropertyLibraryCPARegulations']);
//    Route::get('/property-library/ficaregulations', ['as' => 'helpadvice_PropertyLibrary_FICARegulations', 'uses' => 'PagesController@helpadvicePropertyLibraryFICARegulations']);
//    Route::get('/property-library/municipalityregulations', ['as' => 'helpadvice_PropertyLibrary_MunicipalityRegulations', 'uses' => 'PagesController@helpadvicePropertyLibraryMunicipalityRegulations']);
//    Route::get('/property-library/propertyvaluation', ['as' => 'helpadvice_PropertyLibrary_PropertyValuation', 'uses' => 'PagesController@helpadvicePropertyLibraryPropertyValuation']);
//    Route::get('/property-library/realestateagents', ['as' => 'helpadvice_PropertyLibrary_realEstateAgents', 'uses' => 'PagesController@helpadvicePropertyLibraryRealEstateAgents']);
//    Route::get('/property-library/realestatelaw', ['as' => 'helpadvice_PropertyLibrary_realEstateLaw', 'uses' => 'PagesController@helpadvicePropertyLibraryRealEstateLaw']);
//});

//---How it Works
Route::group(['prefix' => 'howitworks'], function () {
//    Route::get('/', ['as' => 'howitworks', 'uses' => 'PagesController@howitworks']);
//    Route::get('/residential', ['as' => 'howitworks_residential', 'uses' => 'PagesController@howitworksResidential']);
    Route::get('/commercial', ['as' => 'howitworks_commercial', 'uses' => 'PagesController@CommercialSellExplained']);
    Route::get('/auction', ['as' => 'howitworks_auction', 'uses' => 'PagesController@howitworksAuction']);
});

//Route::get('/pages/buyer_calculator/{price}', ['as' => 'buyer_calculator', 'uses' => 'PagesController@BuyerCalculator']);
//Route::get('api/buyer_calc', ['as' => 'api.buyer_calc', 'uses' => 'PagesController@getBuyerFigures']);

//--Properties
Route::get('/property/removed/{id}', ['as' => 'property.removed', 'uses' => 'PropertyController@propertyRemoved']);
Route::get('/property/getCoords/{address}', 'PropertyController@getCoordinates');
Route::get('property/ajax', ['as' => 'property.ajax', 'uses' => 'PropertyController@ajax']);
Route::get('property/ajaxmap', ['as' => 'property.ajax', 'uses' => 'PropertyController@ajaxMap']);
Route::post('property/createedit', ['as' => 'property.createEdit', 'uses' => 'PropertyController@createEdit']);
Route::get('property/createcont/{id}', ['as' => 'property.createcont', 'uses' => 'PropertyController@createcont']);
Route::get('property/getarea', 'PropertyController@getArea');
Route::get('property/calendar/{property_id}', ['as' => 'property.calendar', 'uses' => 'PropertyController@calendar']);
Route::get('property/getsuburbtown', 'PropertyController@getSuburbTown');
Route::get('property/vettinginfo', ['as' => 'property.vettingInfo', 'uses' => 'PropertyController@vettingInfo']);
Route::get('property/list_type/{listing_type}', ['as' => 'property.listtype', 'uses' => 'PropertyController@listType']);

Route::post('advsearchresults/', ['as' => 'property.search', 'uses' => 'PropertyController@search']);
//Route::get('search/{area}/{type}', ['as' => 'property.searchByArea', 'uses' => 'PagesController@searchByArea']);
Route::post('search-properties', ['as' => 'property.searchHome', 'uses' => 'PropertyController@searchHome']);
Route::get('search-multiple-areas', ['as' => 'property.indexMulti', 'uses' => 'PropertyController@indexMultiple']);
Route::post('search-by-suburb', ['as' => 'property.searchSuburbs', 'uses' => 'PropertyController@searchBySuburb']);
Route::get('get_agent_properties/{id}', ['as' => 'property.getAgentProperties', 'uses' => 'PropertyController@getAgentProperties']);
Route::get('searchresults', ['as' => 'property.searchresults', 'uses' => 'PropertyController@searchResults']);
Route::get('property/{listing_type}/create', ['as' => 'property.create', 'uses' => 'PropertyController@create']);
Route::get('property/{id}/{slug}', ['as' => 'property.show', 'uses' => 'PropertyController@show']);
Route::get('{property_type}/property/{listing_type}/{province}/{city}/{suburb}',
    ['as' => 'property.index', 'uses' => 'PropertyController@index']);
Route::resource('property', 'PropertyController', ['only' => ['show', 'update', 'edit', 'destroy', 'store']]);
Route::post('bidder-details', ['as' => 'update.bidder.details', 'uses' => 'AuctionController@updateBidderDetails']);

Route::get('setproperty/{count}', ['as' => 'property.setproperty', 'uses' => 'PropertyController@setc']);
Route::get('getproperty', ['as' => 'property.getproperty', 'uses' => 'PropertyController@getc']);
Route::get('api/area-search', ['as' => 'api.area_search', 'uses' => 'PropertyController@setAreaSearchSession']);
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------

Route::get('/api/saved_searches/{user_id}', ['as' => 'api.saved_searches', 'uses' => 'PortalController@getSavedSearches']);
Route::get('/api/user_enquiries/{user_id}', ['as' => 'api.user.enquiries', 'uses' => 'UsersController@getUserEnquiries']);
Route::get('/api/property_enquiries/{property_id}', ['as' => 'api.property.enquiries', 'uses' => 'PropertyController@getPropertyEnquiries']);

Route::get('/api/saved_searches_user/{user_id}', ['as' => 'api.saved_searches_user', 'uses' => 'UsersController@getSavedSearches']);
//--------------------------------------------------------------------------------------------------------------------------------------------
//--User Registration & Sessions & Uploads & Roles -------------------------------------------------------------------------------------------

//--Registration-----------------------------------------
Route::get('register', ['as' => 'register_path', 'uses' => 'RegistrationController@create']);
Route::get('unsubscribe/{email}', ['as' => 'unsubscribe', 'uses' => 'UsersController@unsubscribe']);
Route::get('registerfb', ['as' => 'registerFB_path', 'uses' => 'RegistrationController@loginWithFacebook']);
Route::post('register', ['as' => 'register_path', 'uses' => 'RegistrationController@store']);
Route::post('register-wesell', ['as' => 'register_wesell', 'uses' => 'RegistrationController@storeWeSell']);

//--Sessions---------------------------------------------
Route::get('login', ['as' => 'login_path', 'uses' => 'SessionsController@create']);
Route::post('login', ['as' => 'login_path', 'uses' => 'SessionsController@store']);
Route::get('loginfb', ['as' => 'loginFB_path', 'uses' => 'SessionsController@loginFB']);
Route::get('logout', ['as' => 'logout_path', 'uses' => 'SessionsController@destroy']);
Route::resource('sessions', 'SessionsController', ['only' => ['index', 'create', 'destroy', 'store']]);

//--Terms & Conditions---------------------------------------
Route::get('terms_conditions', ['as' => 'termsconditions_path', 'uses' => 'PagesController@termsConditions']);
//Route::get('terms_conditions_wesell', ['as' => 'termsconditions_wesell_path', 'uses' => 'PagesController@termsConditionsWesell']);
Route::get('terms_conditions_in2assets', ['as' => 'termsconditions_in2assets_path', 'uses' => 'PagesController@termsConditionsIn2assets']);
Route::get('terms_conditions_letting', ['as' => 'termsconditions_letting_path', 'uses' => 'PagesController@termsConditionsLetting']);
Route::get('terms_conditions_letting_in2assets', ['as' => 'termsconditions_lettingin2assets_path', 'uses' => 'PagesController@termsConditionsLettingIn2assets']);
//Sample code
//Route::get('wesell', ['as' => 'wesell', 'uses' => 'PagesController@wesell']);
//Route::get('wesell-login', ['as' => 'wesell-login', 'uses' => 'PagesController@WeSellLogin']);
//Route::get('wesell-how-it-works', ['as' => 'wesell-how-it-works', 'uses' => 'PagesController@WesellHowItWorks']);

//--Users------------------------------------------------
Route::get('/users/setup', ['as' => 'users.setup', 'uses' => 'UsersController@setup']);
Route::get('/users/edit', ['as' => 'users.edit', 'uses' => 'UsersController@edit']);
Route::get('/users/authfb', ['as' => 'users.authfb', 'uses' => 'UsersController@authFacebook']);
Route::resource('users', 'UsersController');

//--Uploads------------------------------------------------
Route::group(['prefix' => 'upload'], function () {
    Route::post('/ckeditor/image', 'UploadsController@ckEditor');
    Route::get('/image/move/{id}/{start}/{new}', ['as' => 'upload_image_order', 'uses' => 'UploadsController@changeImageOrder']);
    Route::post('/image/{type}/{id}/{class}/{listing_title?}', ['as' => 'upload_image_path', 'uses' => 'UploadsController@storeImage']);
    Route::post('/file/{fileId}/{class}', ['as' => 'upload_file_path', 'uses' => 'UploadsController@storeFile']);
    Route::get('/image/delete/{id}', ['as' => 'upload_deleteimage_path', 'uses' => 'UploadsController@deleteImage']);
    Route::post('/avatar', ['as' => 'upload_avatar_path', 'uses' => 'UploadsController@storeAvatar']);
    Route::post('/agent_photo', ['as' => 'upload_agent_photo_path', 'uses' => 'UploadsController@storeAgentPhoto']);
    Route::get('/multipleimages/{id}', ['as' => 'upload_multiimage_path', 'uses' => 'UploadsController@indexMulti']);
    Route::get('/image', ['as' => 'upload_image_path', 'uses' => 'UploadsController@index']);
    Route::get('/file/{fileId}', ['as' => 'view_upload_file_path', 'uses' => 'UploadsController@indexFileList']);
    Route::post('remove/file', ['as' => 'remove_file_path', 'uses' => 'UploadsController@removeFile']);
    Route::get('/buyer/{userId}', ['as' => 'view_buyer_file_path', 'uses' => 'UploadsController@indexBuyerFile']);
    Route::get('/all/{userId}', ['as' => 'allfileforadmin', 'uses' => 'UploadsController@allFilesList']);
    Route::get('/property/{userId}/{propertyId}', ['as' => 'fileforproperty', 'uses' => 'UploadsController@propertyFilesList']);
    Route::get('/auction/{userId}/{propertyId}', ['as' => 'fileforauction', 'uses' => 'UploadsController@auctionFilesList']);
    Route::get('/tenant/{userId}', ['as' => 'view_tenant_file_path', 'uses' => 'UploadsController@indexTenantFile']);
    Route::get('/file', ['as' => 'upload_file_path', 'uses' => 'UploadsController@indexFile']);
});

//Password Reset

Route::controller('password', 'RemindersController');

//Route::get('/portal/buyersclub', ['as' => 'buyersclub_path', 'uses' => 'BuyersclubController@index']);
Route::get('buying-commercial-property', ['as' => 'commercial_buy', 'uses' => 'PagesController@CommercialBuy']);

Route::get('broker-network', ['as' => 'network', 'uses' => 'PagesController@Network']);
Route::post('property-enquiry', ['as' => 'leads.property_enquiry', 'uses' => 'LeadsController@propertyEnquiry']);

Route::get('sell-property', ['as' => 'commercial_sell', 'uses' => 'PagesController@CommercialSell']);

Route::get('let-property', ['as' => 'commercial_let', 'uses' => 'PagesController@CommercialLet']);
//Route::get('commercial_sell_explained', ['as' => 'commercial_sell_explained', 'uses' => 'PagesController@CommercialSellExplained']);
Route::post('property_submit', ['as' => 'property_submit', 'uses' => 'LeadsController@propertySubmit']);
Route::post('property_submit_package', ['as' => 'property_submit_package', 'uses' => 'LeadsController@propertySubmitPackage']);
Route::post('network_submit', ['as' => 'network_submit', 'uses' => 'LeadsController@networkSubmit']);

Route::get('print-lead-notes/{lead_id}', ['as' => 'print.lead.notes', 'uses' => 'LeadsController@printLeadNotes']);
Route::get('excel-lead-notes/{lead_id}', ['as' => 'excel.lead.notes', 'uses' => 'LeadsController@excelLeadNotes']);
Route::get('print-property-notes/{property_id}', ['as' => 'print.property.notes', 'uses' => 'PropertyController@printPropertyNotes']);

//Route::group(array('prefix' => 'portal'), function () {
////--Payments---------------------------------------
//    Route::get('payment-index', ['as' => 'payment.index', 'uses' => 'PaymentsController@index']);
//    Route::get('payment', ['as' => 'payment_path', 'uses' => 'PaymentsController@payfastSubmit']);
//    Route::post('payment/addcart', ['as' => 'addcart_path', 'uses' => 'PaymentsController@addCart']);
//    Route::get('payment/getcart', ['as' => 'getcart_path', 'uses' => 'PaymentsController@getCart']);
//    Route::post('payment/deletecart', ['as' => 'deletecart_path', 'uses' => 'PaymentsController@deleteCart']);
//    Route::get('payment/updatecart', ['as' => 'updatecart_path', 'uses' => 'PaymentsController@updateCart']);
//    Route::get('payment/invoices', ['as' => 'payment_thankyou_path', 'uses' => 'PaymentsController@payfastResponse']);
//    Route::get('payment/order/{id}', ['as' => 'payment_order_path', 'uses' => 'PaymentsController@orderList']);
//    Route::post('payment/itn', ['as' => 'payment_itn_path', 'uses' => 'PaymentsController@payfastITN']);
//    Route::get('payment/datatable', ['as' => 'payment_datatabe_path', 'uses' => 'PaymentsController@getDatatable']);
//    Route::get('payment/datatableorder/{id}', ['as' => 'payment_datatabeorder_path', 'uses' => 'PaymentsController@getDatatableOrder']);
//    Route::get('payment/cancelled-transaction', ['as' => 'payment.cancel', 'uses' => 'PaymentsController@cancel']);
//
//});
Route::post('auction-my-property', ['as' => 'auction_my_property', 'uses' => 'LeadsController@auctionMyProperty']);

Route::group(['before' => 'role:User'], function () {
    Route::post('property-search/store', ['as' => 'preferences.store', 'uses' => 'PreferencesController@store']);
    Route::get('property-search/delete/{id}', ['as' => 'preferences.delete', 'uses' => 'PreferencesController@delete']);
    Route::get('property-search/edit/{id}/{users?}', ['as' => 'preferences.edit', 'uses' => 'PreferencesController@edit']);
    Route::get('property-search/create', ['as' => 'preferences.create', 'uses' => 'PreferencesController@create']);
    Route::patch('property-search/update/{id}', ['as' => 'preferences.update', 'uses' => 'PreferencesController@update']);
    Route::post('preference-email-alert', ['as' => 'preferences.email-alert', 'uses' => 'PreferencesController@emailAlert']);
    //----------------------------------------------------------------------------------------------------------------------
    //--WeSell -------------------------------------------------------------------------------------------------------------

//    Route::get('wesell-property-info', ['as' => 'wesell-property-info', 'uses' => 'PagesController@WeSellPropertyInfo']);
//    Route::post('add-wesell-property', ['as' => 'add-wesell-property', 'uses' => 'PropertyController@AddWeSellProperty']);
//    Route::get('wesell-congratulations', ['as' => 'wesell-congratulations', 'uses' => 'PagesController@WeSellConGrats']);

    Route::post('admin/auctions/register', ['as' => 'admin.auctions.register', 'uses' => 'AuctionController@register']);
    Route::post('admin/physical_auctions/register', ['as' => 'admin.physical_auctions.register', 'uses' => 'AuctionController@physicalRegister']);
    //--------------------------------------------------------------------------------------------------------------------------------------------
    //--Portal------------------------------------------------------------------------------------------------------------------------------------
    Route::group(['prefix' => 'portal'], function () {

        //--Buyers Club-------------------------------------------------

//        Route::get('/buyersclub/category/{category}', ['as' => 'buyersclub_category_path', 'uses' => 'BuyersclubController@buyersclubCategory']);
//        Route::get('/buyersclub/register', ['as' => 'buyersclub.register', 'uses' => 'BuyersclubController@register']);
//        Route::get('/buyersclub/{slug}', ['as' => 'single_buyersclub_path', 'uses' => 'BuyersclubController@showBuyersclub']);
//        Route::get('/api/buyersclub', ['as' => 'buyersclub_api_path', 'uses' => 'BuyersclubController@homeSnippet']);
//        Route::get('api/buyersclub/{status}', array('as' => 'api.buyersclub', 'uses' => 'BuyersclubController@getDatatable'));
//
//        Route::post('/buyersclub/search', array('as' => 'buyersclub.search', 'uses' => 'BuyersclubController@postSearch'));

        //--Buyers------------------------------------------------------
//        Route::resource('buyers', 'BuyersController');

        //--Tenants------------------------------------------------------
//        Route::resource('tenants', 'TenantsController');

        //--Messages------------------------------------------------
        Route::group(['prefix' => 'messages'], function () {
            Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
            Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
            Route::get('/api/listMessages', ['as' => 'messages.listAll', 'uses' => 'MessagesController@listMessages']);
            Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
            Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
            Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
        });

        //--Notes------------------------------------------------
        Route::group(['prefix' => 'notes'], function () {
            Route::get('/', ['as' => 'notes_path', 'uses' => 'NoteController@index']);
            Route::post('/', ['as' => 'notes_path', 'uses' => 'NoteController@store']);
            Route::delete('/{id}', ['as' => 'note_path', 'uses' => 'NoteController@destroy']);
            Route::get('/{id}', ['as' => 'note_path', 'uses' => 'NoteController@protect']);
        });

        //--Portal Routes------------------------------------------------
        Route::get('/', ['as' => 'portal_path', 'uses' => 'PortalController@index']);
//        Route::get('/mailbox', ['as' => 'portalBuyersMailbox_path', 'uses' => 'PortalController@userMailbox']);
//        Route::get('/calendar', ['as' => 'portalCalendar_path', 'uses' => 'PortalController@userCalendar']);
//        Route::get('/buyers', ['as' => 'portalBuyers_path', 'uses' => 'PortalController@buyer']);
//        Route::get('/buyers-club', ['as' => 'portalBuyersClub_path', 'uses' => 'PortalController@buyerClub']);
//        Route::get('/tenants', ['as' => 'portalTenants_path', 'uses' => 'PortalController@tenant']);
//        Route::get('/sellers', ['as' => 'portalSellers_path', 'uses' => 'PortalController@seller']);
        Route::get('/property-needs/{role}', ['as' => 'portalProperty_needs_path', 'uses' => 'PortalController@property_needs']);
//        Route::get('/registeredserviceproviders/{province}/{area}/{city}/{category}', ['as' => 'portalRegisteredServiceProvider_path', 'uses' => 'PortalController@registeredServiceProvider']);
//        Route::get('/propertyRaceTrack', ['as' => 'propertyRaceTrack_path', 'uses' => 'PortalController@propertyRaceTrack']);
//        Route::get('/how-to-guide', ['as' => 'howToGuide_path', 'uses' => 'PortalController@howToGuide']);
//        Route::get('/serviceProvider', ['as' => 'portalServiceProvider_path', 'uses' => 'PortalController@ServiceProvider']);
//        Route::get('/landlords', ['as' => 'portalLandlords_path', 'uses' => 'PortalController@Landlords']);
//        Route::get('/lease_agreements', ['as' => 'portalLease_agreements_path', 'uses' => 'PortalController@Leases']);
//        Route::get('/deed_of_sale', ['as' => 'portalDeed_of_sale_path', 'uses' => 'PortalController@Deeds']);
//        Route::get('/api/hasDeedorLease', 'PortalController@hasDeedorLease');
//        Route::get('/api/regserviceprovider/{province}/{area}/{city}/{category}', array('as' => 'api.registeredServiceProviders', 'uses' => 'ServiceProviderController@getRegisteredServiceProviders'));
//        Route::post('/searchServiceProviders', array('as' => 'searchServiceProviders', 'uses' => 'ServiceProviderController@searchServiceProviders'));

        //--Portal Properties
        Route::get('/properties', ['as' => 'property.manage_properties', 'uses' => 'PropertyController@manageProperties']);
        Route::get('/favorites', ['as' => 'property.favorites', 'uses' => 'PropertyController@searchFavorites']);
        Route::get('/{listing_type}', ['as' => 'property.portal', 'uses' => 'PropertyController@portal']);

        //--make contact
        Route::post('makecontact', 'MakeContactController@makecontact');
        Route::get('makecontact/show/{id}', ['as' => 'makecontact.show', 'uses' => 'MakeContactController@show']);
        Route::get('/makecontact/delete/{id}', ['as' => 'makecontact.delete', 'uses' => 'MakeContactController@delete']);
        Route::get('api/makecontact/{id}', ['as' => 'api.makecontact', 'uses' => 'MakeContactController@getMakeContactTable']);

        //registered user tables
//        Route::group(['prefix' => 'registeredusers'], function () {
//            Route::get('/agents', 'RegisteredUsersController@showRegisteredAgents');
//            Route::get('/brokers', 'RegisteredUsersController@showRegisteredBrokers');
//            Route::get('/buyers', 'RegisteredUsersController@showRegisteredBuyers');
//            Route::get('/landlords', 'RegisteredUsersController@showRegisteredLandlords');
//            Route::get('/sellers', 'RegisteredUsersController@showRegisteredSellers');
//            Route::get('/tenants', 'RegisteredUsersController@showRegisteredTenants');
//        });

        //--Auction Bids

        Route::post('auctions/bid', ['as' => 'auctions.bid', 'uses' => 'AuctionController@placeBid']);

        Route::get('api/getcurrentprice/{id}', 'AuctionController@getCurrentPrice');

        //--viewing
        Route::post('/portal/viewing/booking', ['as' => 'portal.viewing.booking', 'uses' => 'ViewingController@booking']);
        Route::resource('viewing', 'ViewingController');
    });

    //---------------------------------------------------------------------------------------------------------------------------------------
});

Route::post('email-alerts', ['as' => 'email_alerts', 'uses' => 'AuctionController@emailAlerts']);
Route::get('brochure/{id}', ['as' => 'print_brochure', 'uses' => 'PropertyController@brochureHTML']);

Route::get('brochure/{id}/print', ['as' => 'print_brochure_pdf', 'uses' => 'PropertyController@PrintBrochure']);

//--------------------------------------------------------------------------------------------------------------------------------------------
// Admin--------------------------------------------------------------------------------------------------------------------------------------
// Everything inside this group requires the Admin Role!!!!!----------------------------------------------------------------------------------
Route::group(['before' => 'role:Admin'], function () {

    //--Leads --------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------

    Route::get('api/ViewingSessionBooked/GetViewingSessionsByViewingId/{id}', ['as' => 'api/ViewingSessionBooked/GetViewingSessionsByViewingId', 'uses' => 'ViewingSessionBookingController@getViewingSessionsForViewing']);
    Route::post('api/ViewingSessionBooked/SaveSession', ['as' => 'api/ViewingSessionBooked/SaveSession', 'uses' => 'ViewingSessionBookingController@store']);
    Route::resource('ViewingSessionBooked', 'ViewingSessionBookingController');
    Route::get('wesell/calendar/property/{id}', ['as' => 'admin.property.calendar', 'uses' => 'PortalController@adminPropertyCalendar']);
    Route::get('api/convert_to_auction', ['as' => 'api.convertToAuction', 'uses' => 'AuctionController@convertToAuction']);
    Route::get('api/convert_to_sale', ['as' => 'api.convertToSale', 'uses' => 'AuctionController@convertToSale']);

    //--Admin Auctions
    Route::get('admin/auctions/new', ['as' => 'admin.auctions.new', 'uses' => 'AuctionController@newAuction']);
    Route::post('admin/auctions/storeauction', ['as' => 'admin.auctions.storeauction', 'uses' => 'AuctionController@storeAuction']);
    Route::patch('admin/auctions/{id}/updateauction', ['as' => 'admin.auctions.updateauction', 'uses' => 'AuctionController@updateAuction']);
    Route::get('admin/auctions/{id}/editauction', ['as' => 'admin.auctions.editauction', 'uses' => 'AuctionController@editAuction']);
    Route::get('admin/auctions/{status}', ['as' => 'admin.auctions', 'uses' => 'AuctionController@listAuctionProperty']);
    Route::get('admin/auctions/setup/{id}', ['as' => 'admin.auctions.setup', 'uses' => 'AuctionController@setup']);
    Route::get('admin/auctions/update/{id}', ['as' => 'admin.auctions.update', 'uses' => 'AuctionController@update']);
    Route::get('admin/auctions/bids/{id}', ['as' => 'admin.auctions.bids', 'uses' => 'AuctionController@showbids']);
    Route::get('admin/physicalauctions', ['as' => 'admin.physicalauctions', 'uses' => 'AuctionController@physicalAuctions']);
    Route::get('admin/auctions/listAuction/{id}', ['as' => 'admin.auctions.listAuction', 'uses' => 'AuctionController@listAuction']);
    Route::get('admin/auctions/propertyauctionreg/{id}', ['as' => 'admin.auctions.propertyauctionreg', 'uses' => 'AuctionController@PropertyAuctionReg']);
    Route::get('admin/auctions/physicalauctionreg/{id}', ['as' => 'admin.auctions.physicalauctionreg', 'uses' => 'AuctionController@PhysicalAuctionReg']);
    Route::get('admin/physical_auctions/add_property/{id}', ['as' => 'admin.physical_auctions.add_property', 'uses' => 'AuctionController@addProperties']);
    Route::get('admin/physical_auctions/add_to_auction/{property_id}/{auction_id}', 'AuctionController@addProperty');
    Route::get('admin/physical_auctions/remove_from_auction/{property_id}/{auction_id}', 'AuctionController@removeProperty');
    Route::get('admin/physical_auctions/{id}/delete', ['as' => 'physical.auction.delete', 'uses' => 'AuctionController@deletePhysicalAuction']);

    Route::resource('admin/auctions', 'AuctionController', ['only' => ['update']]);
    Route::get('admin/auctions/create/{property_id}', ['as' => 'admin.auctions.create', 'uses' => 'AuctionController@create']);
    Route::get('/admin/auctions/approve_user/{user_id}/{auction_id}', 'AuctionController@approveUser');
    Route::get('/admin/auctions/reject_user/{user_id}/{auction_id}', 'AuctionController@rejectUser');
    Route::get('/admin/auctions/approve_user_physical/{user_id}/{auction_id}', 'AuctionController@approveUserPhysical');
    Route::get('/admin/auctions/reject_user_physical/{user_id}/{auction_id}', 'AuctionController@rejectUserPhysical');
    Route::post('/admin/auctions/changeincrement', 'AuctionController@changeIncrement');
    Route::post('/admin/auctions/finalprice', 'AuctionController@setfinalprice');
    Route::get('api/admin-auction-property/{status}', ['as' => 'api.auction_properties', 'uses' => 'AuctionController@getAuctionProperty']);
    Route::get('api/physicalauctions', ['as' => 'api.physicalauctions', 'uses' => 'AuctionController@getPhysicalAuctions']);
    Route::get('api/physicalauctions/addproperties/{id}', ['as' => 'api.physicalauctions.addproperties', 'uses' => 'AuctionController@getAddPropertyTable']);
    Route::get('api/auctions/propertyauctionreg/{id}', ['as' => 'api.auctions.propertyauctionreg', 'uses' => 'AuctionController@getPropAuctRegTable']);
    Route::get('api/auctions/physicalauctionreg/{id}', ['as' => 'api.auctions.physicalauctionreg', 'uses' => 'AuctionController@getPhysAuctRegTable']);

    //--Messages------------------------------------------------
    Route::group(['prefix' => 'admin/messages'], function () {
        Route::get('/', ['as' => 'adminmessages', 'uses' => 'MessagesController@indexadmin']);
        Route::get('/create', ['as' => 'messages.admingeneralcreate', 'uses' => 'MessagesController@adminCreateGeneral']);
        Route::get('{user}/{referenceid}/{reference}/create', ['as' => 'messages.admincreate', 'uses' => 'MessagesController@adminCreate']);
        Route::get('/api/listMessages', ['as' => 'messages.listAll', 'uses' => 'MessagesController@create']);
        Route::post('/store', ['as' => 'messages.adminstore', 'uses' => 'MessagesController@adminStore']);
        Route::get('{id}', ['as' => 'messages.adminshow', 'uses' => 'MessagesController@adminshow']);
        Route::put('{id}', ['as' => 'messages.adminupdate', 'uses' => 'MessagesController@adminupdate']);
        Route::get('/api/listMessages', ['as' => 'messages.adminlistAll', 'uses' => 'MessagesController@adminlistMessages']);
    });

    //--Admin buyer registration
    Route::get('admin/buyers/{role}', ['as' => 'admin.buyers', 'uses' => 'BuyersController@adminBuyers']);
    Route::get('admin/buyers/show/{id}', ['as' => 'admin.buyers.show', 'uses' => 'BuyersController@adminBuyersShow']);
    Route::get('admin/buyers/status/{status}/{id}', ['as' => 'admin.buyers.status', 'uses' => 'BuyersController@adminBuyersStatus']);
    Route::get('api/adminbuyers/{role}', ['as' => 'api.adminbuyers', 'uses' => 'BuyersController@getAdminBuyersTable']);
    //--------------------------

    //--Admin tenant registration
    Route::get('admin/tenants/{role}', ['as' => 'admin.tenants', 'uses' => 'TenantsController@adminTenants']);
    Route::get('admin/tenants/show/{id}', ['as' => 'admin.tenants.show', 'uses' => 'TenantsController@adminTenantsShow']);
    Route::get('admin/tenants/status/{status}/{id}', ['as' => 'admin.tenants.status', 'uses' => 'TenantsController@adminTenantsStatus']);
    Route::get('api/admintenants/{role}', ['as' => 'api.admintenants', 'uses' => 'TenantsController@getAdminTenantsTable']);
    //--------------------------

    Route::get('api/createnewtag/', ['as' => 'users.createnewtag', 'uses' => 'UsersController@createnewtag']);
    Route::post('api/addrole/{user}/{role}', ['as' => 'users.addrole', 'uses' => 'UsersController@addRole']);
    Route::delete('api/removerole/{user}/{role}', ['as' => 'users.removerole', 'uses' => 'UsersController@removeRole']);
    Route::post('api/addtag/{user}/{tag}', ['as' => 'users.addtag', 'uses' => 'UsersController@addtag']);
    Route::delete('api/removetag/{user}/{tag}', ['as' => 'users.removetag', 'uses' => 'UsersController@removetag']);
    Route::get('api/gettags/{user}', ['as' => 'users.gettags', 'uses' => 'UsersController@gettags']);

    Route::get('admin/users/add', ['as' => 'users.add', 'uses' => 'UsersController@addUser']);
    Route::get('admin/users/search', ['as' => 'users.search', 'uses' => 'UsersController@searchUser']);
    Route::get('admin/users/search/return', ['as' => 'users.search.return', 'uses' => 'UsersController@searchReturnUser']);
    Route::post('admin/users/search/result', ['as' => 'users.search.result', 'uses' => 'UsersController@searchUserResult']);
    Route::get('admin/users/{role}', ['as' => 'users.list', 'uses' => 'UsersController@adminList']);
    Route::post('property-enquiry-save', ['as' => 'save.enquiry', 'uses' => 'LeadsController@saveUserEnquiry']);
    Route::post('property-enquiry-save-user', ['as' => 'save.enquiry.property', 'uses' => 'LeadsController@savePropertyEnquiry']);
    Route::get('admin/users/edit/{id}', ['as' => 'users.adminlist', 'uses' => 'UsersController@adminEdit']);
    Route::get('api/users', ['as' => 'users.api', 'uses' => 'UsersController@homeSnippet']);
    Route::get('api/users/{role}', ['as' => 'api.users', 'uses' => 'UsersController@getDatatable']);
    Route::get('/api/users_pref_result/{input}', ['as' => 'api.userPref', 'uses' => 'UsersController@getUserbyPrefList']);
    Route::get('api/users/property/{user_id}', ['as' => 'api.users.property', 'uses' => 'UsersController@getUserProperties']);
    Route::get('admin/users/update/{id}', ['as' => 'users.adminupdate', 'uses' => 'UsersController@adminUpdate']);

    //--Academy-------------------------------------------------
    Route::get('/admin/academy', ['as' => 'list_academy_path', 'uses' => 'AcademyController@listAcademy']);
    Route::get('/admin/academy/create', ['as' => 'create_academy_path', 'uses' => 'AcademyController@createAcademy']);
    Route::post('/admin/academy/create/new', ['as' => 'post_academy_path', 'uses' => 'AcademyController@storeAcademy']);
    Route::get('/admin/academy/close/{id}', ['as' => 'delete_academy_path', 'uses' => 'AcademyController@close']);
    Route::get('/admin/academy/edit/{slug}', ['as' => 'edit_academy_path', 'uses' => 'AcademyController@edit']);
    Route::patch('/admin/academy/update/{slug}', ['as' => 'academy.update', 'uses' => 'AcademyController@update']);

    //--Blog-------------------------------------------------
    Route::get('/admin/blog', ['as' => 'list_blog_path', 'uses' => 'BlogController@listBlog']);
    Route::get('/admin/blog/create', ['as' => 'create_blog_path', 'uses' => 'BlogController@createBlog']);
    Route::post('/admin/blog/create/new', ['as' => 'post_blog_path', 'uses' => 'BlogController@storeBlog']);
    Route::get('/admin/blog/close/{id}', ['as' => 'delete_blog_path', 'uses' => 'BlogController@close']);
    Route::get('/admin/blog/edit/{slug}', ['as' => 'edit_blog_path', 'uses' => 'BlogController@edit']);
    Route::patch('/admin/blog/update/{slug}', ['as' => 'blog.update', 'uses' => 'BlogController@update']);

    //--Buyers Club-------------------------------------------------
    Route::get('/admin/buyersclub', ['as' => 'list_buyersclub_path', 'uses' => 'BuyersclubController@listBuyersclub']);
    Route::get('/admin/buyersclub/create', ['as' => 'create_buyersclub_path', 'uses' => 'BuyersclubController@createBuyersclub']);
    Route::post('/admin/buyersclub/create/new', ['as' => 'post_buyersclub_path', 'uses' => 'BuyersclubController@storeBuyersclub']);
    Route::get('/admin/buyersclub/close/{id}', ['as' => 'delete_buyersclub_path', 'uses' => 'BuyersclubController@close']);
    Route::get('/admin/buyersclub/edit/{slug}', ['as' => 'edit_buyersclub_path', 'uses' => 'BuyersclubController@edit']);
    Route::patch('/admin/buyersclub/update/{slug}', ['as' => 'buyersclub.update', 'uses' => 'BuyersclubController@update']);

    //--Careers
    Route::get('/admin/career/create', ['as' => 'admin.career.create', 'uses' => 'CareersController@create']);
    Route::get('/admin/career/delete/{id}', ['as' => 'admin.career.delete', 'uses' => 'CareersController@destroy']);
    Route::get('/admin/career/{id}/edit', ['as' => 'admin.career.edit', 'uses' => 'CareersController@edit']);
    Route::patch('/admin/career/{id}/update', ['as' => 'admin.career.update', 'uses' => 'CareersController@update']);
    Route::get('/admin/career/{id}', ['as' => 'admin.career.delete', 'uses' => 'CareersController@showAdmin']);
    Route::get('/admin/career/', ['as' => 'admin.career', 'uses' => 'CareersController@listAdmin']);

    //--Reports
    Route::get('admin/report/home', ['as' => 'admin.reports', 'uses' => 'ReportsController@reportHome']);
    Route::get('admin/report/create', ['as' => 'reports.create', 'uses' => 'ReportsController@create']);
    Route::get('admin/report/index', ['as' => 'reports.index', 'uses' => 'ReportsController@index']);
    Route::post('admin/report/store', ['as' => 'reports.store', 'uses' => 'ReportsController@store']);
    Route::get('admin/report/{id}/edit', ['as' => 'reports.edit', 'uses' => 'ReportsController@edit']);
    Route::patch('admin/report/{id}/update', ['as' => 'reports.update', 'uses' => 'ReportsController@update']);
    Route::get('admin/report/{id}/delete', ['as' => 'reports.delete', 'uses' => 'ReportsController@delete']);
    Route::post('admin/report/run', ['as' => 'run.reports', 'uses' => 'ReportsController@reportRun']);
    Route::get('api/get_report', ['as' => 'api.get.report', 'uses' => 'ReportsController@getReport']);
    Route::get('api/test/script', 'ReportsController@testScript');
    Route::get('api/get-reports', ['as' => 'api.list.reports', 'uses' => 'ReportsController@getReports']);
    Route::post('excel/report', ['as' => 'export.report', 'uses' => 'ReportsController@exportReport']);

    //-- Broker Logos
    Route::get('admin/broker_logos/{id}/edit', ['as' => 'admin.broker_logo.edit', 'uses' => 'BrokerController@editBrokerLogo']);
    Route::get('admin/broker_logos/create', ['as' => 'broker_logo.create', 'uses' => 'BrokerController@createBrokerLogo']);
    Route::post('admin/broker_logos', ['as' => 'broker_logo.store', 'uses' => 'BrokerController@storeBrokerLogo']);
    Route::delete('admin/broker_logos/{id}', ['as' => 'broker_logo.delete', 'uses' => 'BrokerController@deleteBrokerLogo']);
    Route::patch('admin/broker_logos/{id}', ['as' => 'broker_logo.update', 'uses' => 'BrokerController@updateBrokerLogo']);
    Route::get('admin/broker_logos/{status}', ['as' => 'admin.other_brokers', 'uses' => 'BrokerController@otherBrokers']);

    //-- Zone Types
    Route::get('admin/zone-types/{id}/edit', ['as' => 'admin.zone_type.edit', 'uses' => 'ZoneTypeController@edit']);
    Route::get('admin/zone-types/create', ['as' => 'zone_type.create', 'uses' => 'ZoneTypeController@create']);
    Route::post('admin/zone-types', ['as' => 'zone_type.store', 'uses' => 'ZoneTypeController@store']);
    Route::patch('admin/zone-types/{id}', ['as' => 'zone_type.update', 'uses' => 'ZoneTypeController@update']);
    Route::get('admin/zone-types/{status}', ['as' => 'admin.zone_types', 'uses' => 'ZoneTypeController@index']);

    // --Admin ServicProviders
    Route::get('admin/{status}/serviceprovider', ['as' => 'ServiceProvider.index', 'uses' => 'ServiceProviderController@index']);
    Route::get('admin/serviceprovider/categoryAdd/{category}', 'ServiceProviderController@categoryAdd');
    Route::get('admin/serviceprovider/create', ['as' => 'ServiceProvider.create', 'uses' => 'ServiceProviderController@create']);
    Route::post('admin/serviceprovider/store', ['as' => 'ServiceProvider.store', 'uses' => 'ServiceProviderController@store']);
    Route::get('admin/serviceprovider/{id}/edit', ['as' => 'ServiceProvider.edit', 'uses' => 'ServiceProviderController@edit']);
    Route::patch('admin/serviceprovider/update/{id}', ['as' => 'ServiceProvider.update', 'uses' => 'ServiceProviderController@update']);
    Route::delete('admin/serviceprovider/destroy/{id}', ['as' => 'ServiceProvider.destroy', 'uses' => 'ServiceProviderController@destroy']);
    Route::get('api/serviceprovider/{status}', ['as' => 'api.serviceprovider', 'uses' => 'ServiceProviderController@getDatatable']);

    //--Admin Classifieds
    Route::get('admin/classifieds/index', ['as' => 'admin.classifieds', 'uses' => 'ClassifiedsController@AdminClassifieds']);
    Route::get('admin/classifieds/create', ['as' => 'admin.classifieds.create', 'uses' => 'ClassifiedsController@AdminCreate']);
    Route::get('admin/classifieds/inactive', ['as' => 'admin.classifieds.inactive', 'uses' => 'ClassifiedsController@AdminInactive']);
    Route::get('admin/classifieds/preview/{id}', ['as' => 'admin.classifieds.preview', 'uses' => 'ClassifiedsController@AdminPreview']);
    Route::get('admin/classifieds/approve/{id}', ['as' => 'admin.classifieds.approve', 'uses' => 'ClassifiedsController@AdminApprove']);
    Route::get('admin/classifieds/delete/{id}', ['as' => 'admin.classifieds.delete', 'uses' => 'ClassifiedsController@destroy']);
    Route::get('admin/classifieds/inactive/{id}', ['as' => 'admin.classifieds.inactive', 'uses' => 'ClassifiedsController@markInactive']);
    Route::get('admin/classifieds/activate/{id}', ['as' => 'admin.classifieds.activate', 'uses' => 'ClassifiedsController@AdminApprove']);
    Route::get('admin/classifieds/sold/{id}', ['as' => 'admin.classifieds.sold', 'uses' => 'ClassifiedsController@AdminSold']);
    Route::get('api/adminclassified/{status}', ['as' => 'api.adminclassified', 'uses' => 'ClassifiedsController@getAdminClassifieds']);
    Route::get('api/admininactive', ['as' => 'api.admininactive', 'uses' => 'ClassifiedsController@getAdminInactive']);

    // --Admin Vetting (must be situated before property routes)

    Route::post('admin/vetting/statuschange/{id}', ['as' => 'vetting.statuschange', 'uses' => 'VettingController@statuschange']);
    Route::resource('admin/vetting', 'VettingController', ['only' => ['show', 'update', 'index', 'edit']]);
    Route::post('admin/vetting/mark_as', 'VettingController@mark_as');

    //--Admin Properties
    Route::get('admin/property/advanced_search', ['as' => 'admin.property.advanced_search', 'uses' => 'PropertyController@advancedPropertySearch']);
    Route::post('admin/property/search', ['as' => 'admin.property.search', 'uses' => 'PropertyController@adminSearch']);
    Route::get('admin/property/close_property/{id}', ['as' => 'admin.property.closeProp', 'uses' => 'PropertyController@adminCloseProp']);
    Route::post('admin/property/close', ['as' => 'admin.property.close', 'uses' => 'PropertyController@adminClose']);
    Route::get('admin/property/{id}/edit', ['as' => 'admin.property.edit', 'uses' => 'PropertyController@adminPropertyEdit']);
    Route::post('admin/property/{id}/check', ['as' => 'property.checklist', 'uses' => 'PropertyController@adminPropertyCheck']);
    Route::post('admin/property/{id}/auctioneer', ['as' => 'property.auctioneer', 'uses' => 'PropertyController@adminAuctioneerNotes']);
    Route::put('admin/property/{id}/advert', ['as' => 'property.advert', 'uses' => 'PropertyController@adminPropertyAdvert']);
    Route::get('{id}/print-property-advert', ['as' => 'property.print_advert', 'uses' => 'PropertyController@adminPrint_advert']);
    Route::get('{id}/print-property-file', ['as' => 'property.print_file', 'uses' => 'PropertyController@adminPrint_file']);
    Route::get('{id}/print-auctioneer-notes', ['as' => 'property.print_auctioneer', 'uses' => 'PropertyController@adminPrint_auctioneer']);
    Route::get('{id}/new-property-file', ['as' => 'property.print_file', 'uses' => 'PropertyController@newProperty_file']);
    Route::get('admin/properties/{status}', ['as' => 'property.admin', 'uses' => 'PropertyController@adminProperty']);
    Route::get('admin/property/cont/{id}', ['as' => 'admin.property.createcont', 'uses' => 'PropertyController@adminCreatecont']);
    Route::post('admin/property/storecont', ['as' => 'admin.property.storecont', 'uses' => 'PropertyController@adminContStore']);
    Route::post('admin/property/store', ['as' => 'admin.property.store', 'uses' => 'PropertyController@adminPropertyStore']);
    Route::get('admin/property/archive', ['as' => 'admin.property.archive', 'uses' => 'PropertyController@archive']);
    Route::get('admin/property/restore/{id}', ['as' => 'admin.property.restore', 'uses' => 'PropertyController@restore']);
    Route::get('admin/property/history/{id}', ['as' => 'admin.property.history', 'uses' => 'PropertyController@history']);
    Route::get('admin/property/{type}/{vetting}', ['as' => 'admin.listing', 'uses' => 'PropertyController@admin']);
    Route::get('api/archived_properties', ['as' => 'api.archived_properties', 'uses' => 'PropertyController@getArchivedTable']);
    Route::get('admin/property_map/{type}', ['as' => 'admin.propertymap', 'uses' => 'PropertyController@showMap']);
    Route::get('admin/offers/{listing_type}', ['as' => 'admin.offers', 'uses' => 'PropertyController@viewOffers']);

    Route::get('admin/payment', ['as' => 'admin_payment_path', 'uses' => 'PaymentsController@adminListpackages']);
    Route::get('admin/invoices', ['as' => 'admin_invoices_path', 'uses' => 'PaymentsController@adminInvoices']);
    Route::get('admin/invoices/items/{id}', ['as' => 'admin_invoicesitems_path', 'uses' => 'PaymentsController@adminInvoiceItems']);
    Route::get('admin/payment/order/inprogress/{id}', ['as' => 'admin_paymentst_path', 'uses' => 'PaymentsController@adminSetProgress']);
    Route::get('admin/payment/order/complete/{id}', ['as' => 'admin_invoicessta_path', 'uses' => 'PaymentsController@adminSetCompleted']);
    Route::get('admin/payment/datatable', ['as' => 'admin_payment_datatable_path', 'uses' => 'PaymentsController@getadminDatatableOrder']);
    Route::get('admin/invoices/datatable', ['as' => 'admin_invoices_datatable_path', 'uses' => 'PaymentsController@getadminDatatable']);

    //--Admin Lease and Sale Agreements
    Route::get('admin/leaseAgree', ['as' => 'lease_agreement.admin', 'uses' => 'LeaseAgreementsController@adminLease']);
    Route::get('admin/saleAgree', ['as' => 'deed_of_sale.admin', 'uses' => 'DeedOfSaleController@adminDeed']);
    Route::resource('admin', 'AdminsController');

    Route::get('api/adminoffers/{listing_type}', ['as' => 'api.adminoffers', 'uses' => 'PropertyController@getAdminOffersTable']);
    Route::get('api/adminlease', ['as' => 'api.adminlease', 'uses' => 'LeaseAgreementsController@getAdminLease']);
    Route::get('api/admindeed', ['as' => 'api.admindeed', 'uses' => 'DeedOfSaleController@getAdminDeed']);
    Route::get('api/getBrokerLogos/{status}', ['as' => 'api.broker_logos', 'uses' => 'BrokerController@getBrokerLogos']);
    Route::get('api/getZoneTypes/{status}', ['as' => 'api.zone_types', 'uses' => 'ZoneTypeController@getZoneTypes']);
    //--Reports

    Route::get('admin/reports/users-by-month', ['as' => 'admin.report.users-by-month', 'uses' => 'ReportsController@usersByMonth']);
    Route::get('admin/reports/property-views', ['as' => 'admin.report.property-views', 'uses' => 'ReportsController@PropertyViews']);
    Route::post('admin/reports/print-users-by-month', ['as' => 'admin.report.print-users-by-month', 'uses' => 'ReportsController@printUsersByMonth']);
    Route::post('admin/reports/print-property-views', ['as' => 'admin.report.print-property-views', 'uses' => 'ReportsController@printPropertyViews']);
});

// Allows Letting Agents and Admin Access to these routes
Route::group(['before' => 'role:Admin;LettingAgent'], function () {
    //--Admin Properties
    Route::resource('admin', 'AdminsController');
    Route::get('api/adminproperty/{status}', ['as' => 'api.adminproperty', 'uses' => 'PropertyController@getAdminPropertyTable']);
    Route::get('api/adminproperty_search', ['as' => 'api.adminproperty_search', 'uses' => 'PropertyController@getAdminPropertySearchTable']);
    Route::get('admin/property/advanced_search', ['as' => 'admin.property.advanced_search', 'uses' => 'PropertyController@advancedPropertySearch']);
    Route::post('admin/property/search', ['as' => 'admin.property.search', 'uses' => 'PropertyController@adminSearch']);
    Route::get('admin/property/close_property/{id}', ['as' => 'admin.property.closeProp', 'uses' => 'PropertyController@adminCloseProp']);
    Route::post('admin/property/close', ['as' => 'admin.property.close', 'uses' => 'PropertyController@adminClose']);
    Route::get('admin/clone/property/{id}', ['as' => 'admin.property.clone', 'uses' => 'PropertyController@cloneProperty']);
    Route::get('admin/property/{id}/edit', ['as' => 'admin.property.edit', 'uses' => 'PropertyController@adminPropertyEdit']);
    Route::get('admin/properties/{status}', ['as' => 'property.admin', 'uses' => 'PropertyController@adminProperty']);
    Route::get('admin/property/cont/{id}', ['as' => 'admin.property.createcont', 'uses' => 'PropertyController@adminCreatecont']);
    Route::post('admin/property/storecont', ['as' => 'admin.property.storecont', 'uses' => 'PropertyController@adminContStore']);
    Route::post('admin/property/store', ['as' => 'admin.property.store', 'uses' => 'PropertyController@adminPropertyStore']);
    Route::get('admin/property/archive', ['as' => 'admin.property.archive', 'uses' => 'PropertyController@archive']);
    Route::get('admin/property/restore/{id}', ['as' => 'admin.property.restore', 'uses' => 'PropertyController@restore']);
    Route::get('admin/property/history/{id}', ['as' => 'admin.property.history', 'uses' => 'PropertyController@history']);
    Route::get('admin/property/{type}/{vetting}', ['as' => 'admin.listing', 'uses' => 'PropertyController@admin']);
    Route::get('api/archived_properties', ['as' => 'api.archived_properties', 'uses' => 'PropertyController@getArchivedTable']);
    Route::get('admin/property_map/{type}', ['as' => 'admin.propertymap', 'uses' => 'PropertyController@showMap']);
    Route::get('admin/offers/{listing_type}', ['as' => 'admin.offers', 'uses' => 'PropertyController@viewOffers']);
    Route::get('api/getAgentDets', ['as' => 'api.getagentdets', 'uses' => 'PropertyController@getAgentDetails']);
});
//--------------------------------------------------------------------------------------------------------------------------------------------
Route::group(['before' => 'role:Admin;Agent'], function () {
    Route::post('leads/print', ['as' => 'print.leads', 'uses' => 'ReportsController@printLeads']);
    Route::get('/leads/archived', ['as' => 'leads.archived', 'uses' => 'LeadsController@getArchivedLeads']);
    Route::get('/leads/{lead_id}/restore', ['as' => 'leads.archived.restore', 'uses' => 'LeadsController@restoreArchivedLead']);
    Route::get('/leads/{lead_id}/check_lead', ['as' => 'leads.check_lead', 'uses' => 'LeadsController@checkLead']);

    Route::resource('admin', 'AdminsController');
    Route::get('/leads/{user_id}/user', ['as' => 'user.leads', 'uses' => 'LeadsController@getUserLeads']);
    Route::get('/leads/user/{lead_id}/{user_id}/{role_id}/delete', ['as' => 'lead.user.delete', 'uses' => 'LeadsController@deleteLeadUser']);
    Route::get('api/leads/{user}/user_details', ['as' => 'leads.user_details', 'uses' => 'LeadsController@getUserDetails']);
    Route::get('api/leads/connect_property', ['as' => 'leads.connect_property', 'uses' => 'LeadsController@connectProperty']);
    Route::get('api/leads/disconnect_property', ['as' => 'leads.disconnect_property', 'uses' => 'LeadsController@disconnectProperty']);
    Route::get('api/leads/search_note/{search_note}/{agent}/{property_type}/{lead_ref_type}/{contact_first}/{contact_last}/{current_tags}', ['as' => 'api.leads.search_notes', 'uses' => 'LeadsController@getLeadsByNote']);
    Route::get('api/leads/search_note/{search_note}/{agent}/{property_type}/{lead_ref_type}/{contact_first}/{contact_last}/{current_tags}/archived', ['as' => 'api.leads.search_notes.archived', 'uses' => 'LeadsController@getLeadsByNoteArchived']);
    Route::get('api/leads/followup/{user_id}', ['as' => 'api.leads.followup', 'uses' => 'LeadsController@getLeadFollowUp']);
    Route::get('/leads/{lead_id}/delete', ['as' => 'leads.delete', 'uses' => 'LeadsController@deleteLead']);
    Route::get('leads/check_notes/{search_note}', ['as' => 'search.lead.page', 'uses' => 'LeadsController@searchLeadNotesPage']);
    Route::get('leads/check_notes/{search_note}/archived', ['as' => 'search.lead.page.archived', 'uses' => 'LeadsController@searchLeadNotesPageArchived']);
    Route::resource('leads', 'LeadsController', ['only' => ['create', 'show', 'update', 'edit', 'store']]);
    Route::get('api/{lead_id}/end-lead-session', ['as' => 'lead.end.session', 'uses' => 'LeadsController@endLeadEdit']);
    Route::post('leads/search/notes', ['as' => 'search.lead.notes', 'uses' => 'LeadsController@searchLeadNotes']);
    Route::post('leads/search/notes/archived', ['as' => 'search.lead.notes.archived', 'uses' => 'LeadsController@searchLeadNotesArchived']);
    //--Lead Notes
    Route::get('/api/save_note', 'LeadsController@saveNote');
    Route::get('/api/reset_activity', 'LeadsController@resetActivity');
    //--Property Notes
    Route::get('/api/save_property_note', 'PropertyController@saveNote');
    //--Lead Contacts
    Route::get('/api/save_contact', 'LeadsController@saveContact');
    Route::get('/api/update_contact/{id}', 'LeadsController@updateContact');
    Route::get('/api/contact/delete', 'LeadsController@apiDeleteContact');
    Route::get('api/leads/contacts/{lead_id}', ['as' => 'api.leads.contacts', 'uses' => 'LeadsController@getLeadContacts']);
    Route::get('/contact/{id}/edit', ['as' => 'contacts.edit', 'uses' => 'LeadsController@editContact']);
    Route::get('/contact/{id}/delete', ['as' => 'contact.delete', 'uses' => 'LeadsController@deleteContact']);
    //Lead Commissions
    Route::get('/api/add_commission', 'LeadsController@addCommission');
    Route::get('api/leads/commission/{lead_id}', ['as' => 'api.leads.commission', 'uses' => 'LeadsController@getLeadCommissions']);
    Route::get('/commission/{id}/approve', 'LeadsController@approveCommissions');
    Route::get('/commission/{id}/delete', 'LeadsController@deleteCommissions');
    //Convert Lead to Property
    Route::post('/api/convert_lead', 'LeadsController@convertLead');
    //Agent Reports
    Route::get('/leads/agent/{user_id}/cumulative', ['as' => 'leads.bdm.cumulative', 'uses' => 'LeadsController@getCumulativeReport']);
    Route::post('/leads/agent/{user_id}/cumulative', ['as' => 'post.agent.cumulative', 'uses' => 'LeadsController@getCumulativeReport']);
    Route::get('/leads/agent/report', ['as' => 'leads.bdm.report', 'uses' => 'LeadsController@getAgentReport']);
    Route::post('/leads/agent/report', ['as' => 'leads.agent.report', 'uses' => 'LeadsController@getAgentReport']);
    Route::get('/api/leads/{date}/agent', ['as' => 'api.leads.agents', 'uses' => 'LeadsController@getAgentTable']);
    Route::get('/leads/{date}/agent/{user_id}', ['as' => 'api.agent.charts', 'uses' => 'LeadsController@getAgentCharts']);
    Route::post('/save/agent/{user_id}/target', ['as' => 'agent.targets.save', 'uses' => 'LeadsController@saveAgentTargets']);
    Route::get('/api/agent/{agent_id}/{start_date}/{end_date}/cumulative', ['as' => 'api.agent.cumulative', 'uses' => 'LeadsController@getCumulativeTable']);
});

//--------------------------------------------------------------------------------------------------------------------------------------------
// Admin and Portal--------------------------------------------------------------------------------------------------------------------------------------

//lease_agreements
Route::get('lease_agreements/{id}/{tenant_id}', ['as' => 'lease_agreements.show', 'uses' => 'LeaseAgreementsController@show']);
Route::get('lease_agreements/{id}/{tenant_id}/edit', ['as' => 'lease_agreements.edit', 'uses' => 'LeaseAgreementsController@edit']);
Route::get('lease_agreements/{id}/{tenant_id}/approve/{status}', ['as' => 'lease_agreements.approve', 'uses' => 'LeaseAgreementsController@approve']);
Route::get('lease_agreements/{id}/{tenant_id}/newlease', ['as' => 'lease_agreements.new', 'uses' => 'LeaseAgreementsController@newLease']);
Route::patch('lease_agreements/{id}/{tenant_id}', ['as' => 'lease_agreements.update', 'uses' => 'LeaseAgreementsController@update']);

//deed of sale
Route::get('deed_of_sale/{id}/{buyer_id}', ['as' => 'deed_of_sale.show', 'uses' => 'DeedOfSaleController@show']);
Route::get('deed_of_sale/{id}/{buyer_id}/edit', ['as' => 'deed_of_sale.edit', 'uses' => 'DeedOfSaleController@edit']);
Route::get('deed_of_sale/{id}/{buyer_id}/approve/{status}', ['as' => 'deed_of_sale.approve', 'uses' => 'DeedOfSaleController@approve']);
Route::get('deed_of_sale/{id}/{buyer_id}/newDeed', ['as' => 'deed_of_sale.new', 'uses' => 'DeedOfSaleController@newDeed']);
Route::patch('deed_of_sale/{id}/{buyer_id}', ['as' => 'deed_of_sale.update', 'uses' => 'DeedOfSaleController@update']);

//--------------------------------------------------------------------------------------------------------------------------------------------

// *************************************************************************************************
//Has no code as yet

//agents
// Route::resource('agent', 'AgentController');

//brokers
// Route::resource('broker', 'BrokerController');

// *************************************************************************************************

//favourites
Route::resource('favourites', 'FavouritesController', ['only' => ['index', 'store']]);
Route::resource('findfavourites', 'FavouritesController@findFavourites');

//api
Route::get('api/registeredusers/{type}', ['as' => 'api.registeredusers', 'uses' => 'RegisteredUsersController@getRegisteredUsersTable']);
Route::get('api/getPreferences', ['as' => 'api.getPreferences', 'uses' => 'PreferencesController@getPreferences']);
Route::post('api/userViewing', ['as' => 'api.userViewing', 'uses' => 'ViewingController@getUserViewing']);
Route::post('api/buyerViewing', ['as' => 'api.buyerViewing', 'uses' => 'ViewingController@getBuyerViewing']);
Route::post('api/propertyViewing', ['as' => 'api.propertyViewing', 'uses' => 'ViewingController@getPropertyViewing']);
Route::post('api/getfullarea', ['as' => 'api.getfullarea', 'uses' => 'PortalController@getFullArea']);

Route::get('api/get-report-description', ['uses' => 'ReportsController@getFullArea']);

Route::get('api/send-welcome-email', ['uses' => 'UsersController@sendWelcomeEmail']);

// XnGo api calls
Route::get('api/updatePropertiesDaily', 'VettingController@updatePropertiesDaily');
Route::get('api/register/{email}/{password}/{firstname}/{lastname}', ['uses' => 'APIController@register']);
Route::get('api/user/{email}/{id}', ['uses' => 'APIController@authenticate']);
Route::get('api/forgotpassword/{email}', ['uses' => 'APIController@forgotPass']);
Route::get('api/{type}/{id}', ['uses' => 'APIController@api']);
Route::get('api/upass/{email}/{password}/{newpassword}/{newpasswordconfirm}', ['uses' => 'APIController@changePass']);

//competition
//Route::get('/competition/show', ['uses' => 'CompetitionController@show']);
//Route::post('/competition/store', ['as' => 'competition.store', 'uses' => 'CompetitionController@store']);

//minify

Route::get('/minifycss', ['uses' => 'MinifyController@minifyCSS']);
Route::get('/minifyjs', ['uses' => 'MinifyController@minifyJS']);

//Chronjobs

Route::get('api/send-auction-alerts', ['uses' => 'AlertController@checkAuctionEmailAlerts']);

//sitemap function

Route::get('sitemap.xml', function () {

    // create new sitemap object
    $sitemap = App::make('sitemap');

    // set cache (key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean))
    // by default cache is disabled
//    $sitemap->setCache('laravel.sitemap', 3600);

    // check if there is cached sitemap and build new only if is not

    $blogs = DB::table('blogs')->get();

    if (! $sitemap->isCached()) {
        // add item to the sitemap (url, date, priority, freq)

        $sitemap->add(URL::to('/'), date('Y-m-dTH:i:s'), '1.0', 'daily');

        foreach ($blogs as $blog) {
            $images = [];

            $title_lead = substr($blog->lead_image, 0, -17);
            $title_lead = substr($title_lead, strrpos($title_lead, '/') + 1);
            $images[] = [
                'url' => URL::to($blog->lead_image),
                'title' => $title_lead,
            ];

            $image_list = DB::table('uploads')
                ->whereLink($blog->gallery_id)
                ->whereType('blog_image')
                ->whereClass('blog')
                ->get();

            foreach ($image_list as $image) {
                $title = substr($image->file, 0, -17);
                $title = substr($title, strrpos($title, '/') + 1);
                $images[] = [
                    'url' => URL::to($image->file),
                    'title' => $title,
                ];
            }

            $sitemap->add(URL::to('blog/'.$blog->slug), date('Y-m-dTH:i:s'), '0.6', 'weekly', $images);
        }
        $sitemap->add(URL::to('property-blog'), date('Y-m-dTH:i:s'), '0.6', 'weekly');
        $sitemap->add(URL::to('about-us'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('howitworks/auction'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('commercial_sell_explained'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('sell-property'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('let'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('broker-network'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('property-marketing'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('buying-commercial-property'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('auction-calendar'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('contact-us'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('register'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
        $sitemap->add(URL::to('login'), date('Y-m-dTH:i:s'), '0.5', 'monthly');
    }

    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $sitemap->render('xml');
});

Route::get('auctions_sitemap.xml', function () {

    // create new sitemap object
    $auction_sitemap = App::make('sitemap');

    $auctions = DB::table('physical_auctions')->whereNull('deleted_at')
        ->where('start_date', '>', date('Y/m/d'))->get();

    if (! $auction_sitemap->isCached()) {
        foreach ($auctions as $auction) {
            $auction_properties_query = DB::table('auction')->select('property_id')->wherePhysical_auction($auction->id)->get();

            $image_list = DB::table('uploads')
                ->whereLink($auction_properties_query[0]->property_id)
                ->whereType('image')
                ->whereClass('property')
                ->get();

            $images = [];

            foreach ($image_list as $image) {
                $title = substr($image->file, 0, -17);
                $title = substr($title, strrpos($title, '/') + 1);
                $images[] = [
                    'url' => URL::to($image->file),
                    'title' => $title,
                ];
            }

            $auction_sitemap->add(URL::to('physical-auction/'.$auction->id.'/'.strtolower(str_replace(' ', '-', $auction->auction_name))), date('Y-m-dTH:i:s'), '0.9', 'weekly', $images);
        }
    }
    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $auction_sitemap->render('xml');
});

Route::get('agricultural_sitemap.xml', function () {

    // create new sitemap object
    $farm_sitemap = App::make('sitemap');

    $farms = DB::table('farm')->join('property', 'property.id', '=', 'farm.property_id')
        ->whereNull('property.deleted_at')->get();

    if (! $farm_sitemap->isCached()) {
        foreach ($farms as $farm) {
            $farm_properties_query = DB::table('property')->whereId($farm->property_id)->first();

            $image_list = DB::table('uploads')
                ->whereLink($farm_properties_query->id)
                ->whereType('image')
                ->whereClass('property')
                ->get();

            $images = [];

            foreach ($image_list as $image) {
                $title = substr($image->file, 0, -17);
                $title = substr($title, strrpos($title, '/') + 1);
                $images[] = [
                    'url' => URL::to($image->file),
                    'title' => $title,
                ];
            }

            $farm_sitemap->add(URL::to('property/'.$farm_properties_query->id.'/'.$farm_properties_query->slug), date('Y-m-dTH:i:s'), '0.9', 'daily', $images);
        }
    }
    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $farm_sitemap->render('xml');
});

Route::get('commercial_sitemap.xml', function () {

    // create new sitemap object
    $commercial_sitemap = App::make('sitemap');

    $commercials = DB::table('commercial')->join('property', 'property.id', '=', 'commercial.property_id')
        ->whereNull('property.deleted_at')->get();

    if (! $commercial_sitemap->isCached()) {
        foreach ($commercials as $commercial) {
//            try {
            $commercial_properties_query = DB::table('property')->whereId($commercial->property_id)->first();

            $image_list = DB::table('uploads')
                    ->whereLink($commercial_properties_query->id)
                    ->whereType('image')
                    ->whereClass('property')
                    ->get();

            $images = [];

            foreach ($image_list as $image) {
                $title = substr($image->file, 0, -17);
                $title = substr($title, strrpos($title, '/') + 1);
                $images[] = [
                        'url' => URL::to($image->file),
                        'title' => $title,
                    ];
            }

            $commercial_sitemap->add(URL::to('property/'.$commercial_properties_query->id.'/'.$commercial_properties_query->slug), date('Y-m-dTH:i:s'), '0.9', 'daily', $images);
//            }
//            catch(exception $e){
//
//                dd($e->getMessage() . ' ' . $commercial->property_id);
//            }
        }
    }
    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $commercial_sitemap->render('xml');
});

Route::get('residential_sitemap.xml', function () {

    // create new sitemap object
    $residential_sitemap = App::make('sitemap');

    $residentials = DB::table('residential')->join('property', 'property.id', '=', 'residential.property_id')
        ->whereNull('property.deleted_at')->get();

    if (! $residential_sitemap->isCached()) {
        $count = 0;
        foreach ($residentials as $residential) {
//            try {
            $residential_properties_query = DB::table('property')->whereId($residential->property_id)->first();

            $image_list = DB::table('uploads')
                ->whereLink($residential_properties_query->id)
                ->whereType('image')
                ->whereClass('property')
                ->get();

            $count++;
            $images = [];

            foreach ($image_list as $image) {
                $title = substr($image->file, 0, -17);
                $title = substr($title, strrpos($title, '/') + 1);
                $images[] = [
                    'url' => URL::to($image->file),
                    'title' => $title,
                ];
            }

            $residential_sitemap->add(URL::to('property/'.$residential_properties_query->id.'/'.$residential_properties_query->slug), date('Y-m-dTH:i:s'), '0.9', 'daily', $images);
            //            }
//            catch(exception $e){
//
//                dd($e->getMessage() . ' ' . $commercial->property_id);
//            }
        }
    }
    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $residential_sitemap->render('xml');
});

Route::get('archived_sitemap.xml', function () {

    // create new sitemap object
    $archived_sitemap = App::make('sitemap');

    $properties = DB::table('property')
        ->join('vetting', 'vetting.property_id', '=', 'property.id')
        ->whereNotNull('property.deleted_at')
        ->where(function ($q) {
            $q->where('vetting_status', '=', 6)
                ->orWhere('vetting_status', '=', 7);
        })
        ->get();

    if (! $archived_sitemap->isCached()) {
        foreach ($properties as $property) {
//            try {
            $property_properties_query = DB::table('property')->whereId($property->property_id)->first();

            $image_list = DB::table('uploads')
                ->whereLink($property_properties_query->id)
                ->whereType('image')
                ->whereClass('property')
                ->get();

            $images = [];

            foreach ($image_list as $image) {
                $title = substr($image->file, 0, -17);
                $title = substr($title, strrpos($title, '/') + 1);
                $images[] = [
                    'url' => URL::to($image->file),
                    'title' => $title,
                ];
            }

            $archived_sitemap->add(URL::to('property/'.$property_properties_query->id.'/'.$property_properties_query->slug), date('Y-m-dTH:i:s'), '0.4', 'yearly', $images);
//            }
//            catch(exception $e){
//
//                dd($e->getMessage() . ' ' . $commercial->property_id);
//            }
        }
    }
    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $archived_sitemap->render('xml');
});
