<?php

namespace In2Assets\Notify;

use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use In2Assets\Users\User;

class Notify
{
    public function sendMessage($to, $from, $ref, $refid, $subject, $message, $type)
    {
        switch ($type) {
            case 'vettingcorrection':
                $completeMessage = '
                    <p>Unfortunately there were issues with your property submission.</p>
                    <hr>
                    <p>'.$message.'</p>
                    <hr>
                    <p>Please go to the sellers dashboard, update the required details and resubmit your property.</p>
                    <p>The WeSell Property Race Team<br>
                    Your Formula One Property Champions</p>';

                break;
            case 'vettingapproved':
                $completeMessage = '
                    <p>We are pleased to inform you that you are now only one step away from participating in the exciting WeSell Property Race.</p>
                    <p>WeSell is the latest and exciting way of selling Real Estate online and provides you total independence, to sell your property without the assistance of an Estate Agent, by using a unique "State of the Art" and innovative online technology. WeSell saves you up to 5% in commission and only charges 2% excl.</p>
                    <p>We have developed a unique Online Estate Office that puts you in the driving seat, offering you step by step guidance and simplifying the process, ensuring a seamless, stress-free sale.</p>
                    <p>You now have access to your online WeSell property portal, which empowers you to drive your own sales process in a quick and exciting way, always guided by our interactive Property Race dashboard.</p>
                    <p>Your WeSell property portal will be your “Racing Box” or online Real Estate office which offers you the opportunity, to not only communicate with buyers and service providers directly, but will also assist you in promoting your property online and driving traffic to your own webpage.</p>
                    <p>The marketing process is fairly simple and easy to handle and is supported by a comprehensive How To section.</p>
                    <p>However, should your time be limited and you are not a very computer savvy person, the WeSell back office team will assist you at a nominal fee.</p>
                    <p>Firstly, you will need to collect your “For Sale” Board from our accredited attorney near you, which you will find on your sales portal under "service providers".</p>
                    <p>From now on you will receive frequent communication from us with hints and tips, which will guide you through the process.</p>
                    <p>The first step for you will be to log on to your property portal, get to the Race Track and start the Property Race.</p>
                    <p>Good luck for Round ONE !</p><br>
                    <p>The WeSell Property Race Team<br>
                    Your Formula One Property Champions</p>';

                break;
            case 'vettingrejected':
                $completeMessage = '
                    <p>Unfortunately your property submission was rejected.</p>
                    <hr>
                    <p>'.$message.'</p>
                    <hr>
                    <p>The WeSell Property Race Team<br>
                    Your Formula One Property Champions</p>';

                break;
            default:
                $completeMessage = $message;

                break;
        }

        $thread = Thread::create(
            [
                'subject' => $subject,
                'reference' => $ref,
                'reference_id' => $refid,
            ]
        );
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id' => $from,
                'body' => $completeMessage,
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id' => $from,
                'last_read' => new Carbon,
            ]
        );
        // Recipients

        $recipients = ['1'];

        $thread->addParticipants($recipients);
    }
}
