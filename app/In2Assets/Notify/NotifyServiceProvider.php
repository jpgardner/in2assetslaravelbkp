<?php

namespace In2Assets\Notify;

use Illuminate\Support\ServiceProvider;

class NotifyServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('notify', 'In2Assets\Notify\Notify');
    }
}
