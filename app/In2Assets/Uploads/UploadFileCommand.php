<?php

namespace In2Assets\Uploads;

class UploadFileCommand
{
    public $title;

    public $file;

    public $type;

    public $link;

    public $extension;

    public $userId;

    public $class;

    public $position;

    public function __construct($title, $file, $type, $link, $extension, $userId, $class, $position = null)
    {
        $this->title = $title;
        $this->file = $file;
        $this->type = $type;
        $this->link = $link;
        $this->extension = $extension;
        $this->userId = $userId;
        $this->class = $class;
        $this->position = $position;
    }
}
