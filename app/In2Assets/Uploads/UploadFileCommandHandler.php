<?php

namespace In2Assets\Uploads;

use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

class UploadFileCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    protected $fileRepository;

    public function __construct(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * Handle the command.
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $file = Upload::publish($command->title, $command->file, $command->type, $command->link, $command->extension, $command->class, $command->position);

        $file = $this->fileRepository->save($file, $command->userId);

        $this->dispatchEventsFor($file);
    }
}
