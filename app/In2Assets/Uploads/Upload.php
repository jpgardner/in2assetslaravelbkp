<?php

namespace In2Assets\Uploads;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use In2Assets\Uploads\Events\FileWasUploaded;
use Laracasts\Commander\Events\EventGenerator;

class Upload extends Model
{
    use EventGenerator;

    protected $fillable = ['title', 'file', 'type', 'link', 'extension', 'class', 'position'];

    //Files belong to a user
    public function user()
    {
        return $this->belongsTo('In2Assets\Users\User');
    }

    public static function publish($title, $file, $type, $link, $extension, $class, $position)
    {
        $upload = new static(compact('title', 'file', 'type', 'link', 'extension', 'class', 'position'));

        $upload->raise(new FileWasUploaded($title));

        return $upload;
    }
}
