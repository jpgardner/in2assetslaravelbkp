<?php

namespace In2Assets\Uploads\Events;

class FileWasUploaded
{
    public $body;

    public function __construct($body)
    {
        $this->body = $body;
    }
}
