<?php

namespace In2Assets\Uploads;

use In2Assets\Users\User;

class FileRepository
{
    public function getAllForUser(User $user, $id)
    {
        return $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')->get();
    }

    public function getImageGallery(User $user, $property_id)
    {
        return $user->files()->where('link', '=', $property_id)->where('class', '=', 'property')
            ->where('type', '=', 'image')->orderBy('uploads.position')->get();
    }

    public function save(Upload $file, $userId)
    {
        return User::findOrFail($userId)
            ->files()
            ->save($file);
    }

    public function findById($id)
    {
        return Upload::whereId($id)->first();
    }
}
