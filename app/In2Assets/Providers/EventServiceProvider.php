<?php

namespace In2Assets\Providers;

use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    //register In2Assets event listeners

    public function register()
    {
        $this->app['events']->listen('In2Assets.*', 'In2Assets\Handlers\EmailNotifier');
    }
}
