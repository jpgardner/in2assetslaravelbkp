<?php

namespace In2Assets\Mailers;

use DB;
use Illuminate\Support\Facades\App;
use In2Assets\Users\User;

class UserMailer extends Mailer
{
    /**
     * @param User $user
     * Send Welcome Email on Registration
     * Triggerd by RegisterUserCommandHandler
     * Queued by EmailNotifier Handler
     */
    public function sendWelcomeMessageTo(User $user)
    {
        $view = 'emails.registration.confirm';
        $subject = 'Welcome to In2assets online!';
        $name = 'Dear '.$user->firstname.' '.$user->lastname.',<br>';
        $messages = '<p>Thank you for registering on our website and now that you’ve signed up, what’s next?  Being a member gives you access to; </p>
                    <ol>
                        <li>Details on the properties currently listed on our various auction platforms;</li>
                        <li>Property related documents: - Rules of Auction / Conditions of Sale, Comprehensive Property information pack;</li>
                        <li>View our upcoming auctions on the calendar;</li>
                        <li>Regular E-Newsletters with updated news and information about the property industry.</li>
                    </ol>
                    <p>We look forward to being your property partner.</p>
                    <p>Sincerely</p>
                    <p><strong>The In2assets Team</strong></p>
                    <p><strong>0861 444 769</strong></p>';

        $data = ['name' => $name, 'subject' => $subject, 'messages' => $messages];

        return $this->sendTo($user, $subject, $view, $data);
    }

    /**
     * @param User $user
     * Send Welcome Email on Registration
     * Triggerd by RegisterUserCommandHandler
     * Queued by EmailNotifier Handler
     */
    public function sendWelcomeMessageToAuctionBidder(User $user)
    {
        if ($encrypted = DB::table('user_pass')->select('encrypted_pass')->whereUser_id($user->id)->first()) {
            $password = App::make('UsersController')->decryptPassword($encrypted->encrypted_pass);
            $view = 'emails.registration.confirm';
            $subject = 'Welcome to In2assets online!';
            $name = 'Dear '.$user->firstname.' '.$user->lastname.',<br>';
            $messages = '<p>Thank you for participating in our recent Auction, and for supplying us with your contact information.
                        To assist you with direct access to our latest property stock, For Sale or To Let, we have created a
                        profile with a relevant password to our website. Please keep the following information on file
                        and in a secure place.</p>
                        <p><strong>Username: </strong>'.$user->email.'</p>
                        <p><strong>Password: </strong>'.$password.'</p>
                    <p>With your profile, you can now set up your preferences, which includes the type of property, area and
                    budget. Our system will then categorise your profile so that you will receive information in the future
                    on property that suits your specific requirements.</p>
                    <p>We hope that this service is beneficial to you and we look forward to being of service to you.</p>
                    <p>Kind Regards</p>
                    <p><strong>The In2assets Team</strong></p>
                    <p><strong>0861 444 769</strong></p>';

            $data = ['name' => $name, 'subject' => $subject, 'messages' => $messages];

            $this->sendTo($user, $subject, $view, $data);
        }
    }

    /**
     * @param User $user
     * Send Welcome Email on Registration
     * Triggerd by RegisterUserCommandHandler
     * Queued by EmailNotifier Handler
     */
    public function sendWelcomeMessageToAddedUser(User $user)
    {
        if ($encrypted = DB::table('user_pass')->select('encrypted_pass')->whereUser_id($user->id)->first()) {
            $password = App::make('UsersController')->decryptPassword($encrypted->encrypted_pass);
            $view = 'emails.registration.confirm';
            $subject = 'Welcome to In2assets online!';
            $name = 'Dear '.$user->firstname.' '.$user->lastname.',<br>';
            $messages = '<p>We have given you a profile our <a href="https://www.in2assets.co.za">website</a>, your login details are as follows</p>
                        <p><strong>Username: </strong>'.$user->email.'</p>
                        <p><strong>Password: </strong>'.$password.'</p>
                    <p>With your profile, you can now set up your preferences, which includes the type of property, area and
                    budget. Our system will then categorise your profile so that you will receive information in the future
                    on property that suits your specific requirements.</p>
                    <p>We hope that this service is beneficial to you and we look forward to being of service to you.</p>
                    <p>Kind Regards</p>
                    <p><strong>The In2assets Team</strong></p>
                    <p><strong>0861 444 769</strong></p>';

            $data = ['name' => $name, 'subject' => $subject, 'messages' => $messages];

            $this->sendTo($user, $subject, $view, $data);
        }
    }

    /**
     * @param User $user
     * Send Admin Notification on Registration of User
     * Triggerd by RegisterUserCommandHandler
     * Queued by EmailNotifier Handler
     */
    public function sendAdminNewUser(User $user)
    {
        $preferences = DB::select('SELECT * FROM user_interest WHERE users_id = '.$user->id);
        $userPref = '';
        if ($preferences['interest_res'] = 1) {
            $userPref .= 'Residential, ';
        }
        if ($preferences['interest_com'] = 1) {
            $userPref .= 'Commercial, ';
        }
        if ($preferences['interest_farm'] = 1) {
            $userPref .= 'Farms, ';
        }
        if ($preferences['interest_auc'] = 1) {
            $userPref .= 'Auctions';
        }

        $admin = User::whereId(1)->first();
        $view = 'emails.registration.confirm';
        $subject = 'In2Assets – New Website Registration';
        $name = 'In2Assets Administrator';
        $messages = '<p>A client has registered on the website, details as follows:-</p>
                <ul>
                <li>Client Name: '.$user->firstname.' '.$user->lastname.'</li>
                <li>Client Contact: '.$user->contact.'</li>
                <li>Client Email: '.$user->email.'</li>
                <li>Preferences: '.$userPref.'</li>
                </ul>
            <p>Kind Regards,<br>
            The In2assets Team.<br>
            The One-Stop Shop in the Real Estate Industry</p>';

        $data = ['name' => $name, 'subject' => $subject, 'messages' => $messages];

        return $this->sendTo($admin, $subject, $view, $data);
    }
}
