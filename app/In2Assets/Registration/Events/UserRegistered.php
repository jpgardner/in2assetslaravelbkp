<?php

namespace In2Assets\Registration\Events;

use In2Assets\Users\User;

class UserRegistered
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
