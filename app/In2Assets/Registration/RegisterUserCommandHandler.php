<?php

namespace In2Assets\Registration;

use In2Assets\Users\User;
use In2Assets\Users\UserRepository;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

class RegisterUserCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle($command)
    {
        $user = User::register(

            $command->title,
            $command->firstname,
            $command->lastname,
            $command->companyname,
            $command->street_address,
            $command->suburb_town_city,
            $command->area,
            $command->province,
            $command->country,
            $command->postcode,
            $command->email,
            $command->password,
            $command->phonenumber,
            $command->cellnumber,
            $command->communicationpref,
            $command->newsletter,
            $command->avatar,
            $command->allow_contact
        );

        $this->repository->save($user);

        $this->dispatchEventsFor($user);

        return $user;
    }
}
