<?php

namespace In2Assets\Registration;

class RegisterUserCommand
{
    public $title;

    public $firstname;

    public $lastname;

    public $companyname;

    public $street_address;

    public $suburb_town_city;

    public $area;

    public $province;

    public $country;

    public $postcode;

    public $email;

    public $password;

    public $phonenumber;

    public $cellnumber;

    public $communicationpref;

    public $newsletter;

    public $avatar;

    public $allow_contact;

    public function __construct($title, $firstname, $lastname, $companyname, $street_address, $suburb_town_city, $area,
                         $province, $country, $postcode, $email, $password, $phonenumber, $cellnumber, $communicationpref,
                         $newsletter, $avatar, $allow_contact)
    {
        $this->email = $email;
        $this->password = $password;
        $this->avatar = $avatar;
        $this->title = $title;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->companyname = $companyname;
        $this->street_address = $street_address;
        $this->suburb_town_city = $suburb_town_city;
        $this->area = $area;
        $this->province = $province;
        $this->country = $country;
        $this->postcode = $postcode;
        $this->phonenumber = $phonenumber;
        $this->cellnumber = $cellnumber;
        $this->communicationpref = $communicationpref;
        $this->newsletter = $newsletter;
        $this->avatar = $avatar;
        $this->allow_contact = $allow_contact;
    }
}
