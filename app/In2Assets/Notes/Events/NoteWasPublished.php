<?php

namespace In2Assets\Notes\Events;

class NoteWasPublished
{
    public $body;

    public function __construct($body)
    {
        $this->body = $body;
    }
}
