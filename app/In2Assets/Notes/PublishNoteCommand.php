<?php

namespace In2Assets\Notes;

class PublishNoteCommand
{
    public $title;

    public $body;

    public $userId;

    public function __construct($title, $body, $userId)
    {
        $this->title = $title;
        $this->body = $body;
        $this->userId = $userId;
    }
}
