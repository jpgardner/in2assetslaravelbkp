<?php

namespace In2Assets\Notes;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use In2Assets\Notes\Events\NoteWasPublished;
use Laracasts\Commander\Events\EventGenerator;

class Note extends Model
{
    use EventGenerator;

    protected $fillable = ['title', 'body'];

    //Notes belong to a user
    public function user()
    {
        return $this->belongsTo('In2Assets\Users\User');
    }

    public static function publish($title, $body)
    {
        $note = new static(compact('title', 'body'));

        $note->raise(new NoteWasPublished($body));

        return $note;
    }
}
