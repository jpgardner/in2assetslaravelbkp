<?php

namespace In2Assets\Users;

class UserRepository
{
    public function save(User $user)
    {
        return $user->save();
    }

    //paginated list of all users;
    public function getPaginated($howMany = 3)
    {
        return User::orderBy('email', 'asc')->simplePaginate($howMany);
    }

    public function findbyId($id)
    {
        return User::whereId($id)->first();
    }

    public function findAll()
    {
        return User::where('role', '=', 3);
    }
}
