<?php

namespace In2Assets\Users;

use Cmgmyr\Messenger\Traits\Messagable;
use Eloquent;
use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use In2Assets\Registration\Events\UserHasRegistered;
use Laracasts\Commander\Events\EventGenerator;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Notifiable;
    use Authenticatable, Authorizable, CanResetPassword, EventGenerator, Messagable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    //Fields that can be mass assigned
    protected $fillable = ['title', 'firstname', 'lastname', 'companyname', 'street_address', 'suburb_town_city', 'area',
        'province', 'country', 'postcode', 'email', 'password', 'phonenumber', 'cellnumber', 'communicationpref', 'newsletter',
        'avatar', 'socialid', 'allow_contact', 'agent_photo', 'id_number', 'fax', ];

    public static function register($title, $firstname, $lastname, $companyname, $street_address, $suburb_town_city, $area,
                                    $province, $country, $postcode, $email, $password, $phonenumber, $cellnumber, $communicationpref,
                                    $newsletter, $avatar)
    {
        $user = new static(compact('title', 'firstname', 'lastname', 'companyname', 'street_address', 'suburb_town_city', 'area',
            'province', 'country', 'postcode', 'email', 'password', 'phonenumber', 'cellnumber', 'communicationpref', 'newsletter',
            'avatar'));

        //Raise Event
        $user->raise(new UserHasRegistered($user));

        return $user;
    }

    public function avatarLink()
    {
        $avatar = $this->avatar;

        return "/{$avatar}";
    }

    //A user has many notes
    public function notes()
    {
        return $this->hasMany('In2Assets\Notes\Note');
    }

    public function blogs()
    {
        return $this->hasMany('In2Assets\Blogs\Blog');
    }

    public function buyersclub()
    {
        return $this->hasMany('In2Assets\Buyersclub\Buyersclub');
    }

    public function files()
    {
        return $this->hasMany('In2Assets\Uploads\Upload');
    }

    public function preferences()
    {
        return $this->hasMany(\App\Models\Preferences::class);
    }

    public function payment()
    {
        return $this->hasMany('App\Models\payment');
    }

    //Check if User has a Preferences

    public function hasPreferences()
    {
        if ($this->preferences) {
            return true;
        }

        return false;
    }

    public function agent()
    {
        return $this->hasOne(\App\Models\Agent::class);
    }

    public function broker()
    {
        return $this->hasOne(\App\Models\Broker::class);
    }

    //Role Relationship
    //-----------------

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Role::class)->withTimestamps();
    }

    //Check if User has a Role
    public function hasRole($name)
    {
        foreach ($this->roles as $role) {
            if ($role->name == $name) {
                return true;
            }
        }

        return false;
    }

    //Assign a Role to A User
    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    //Remove a Role from a User
    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    //Check if user has only user role

    public function onlyRole($name)
    {
        foreach ($this->roles as $role) {
            if ($role->name != $name) {
                return false;
            }
        }

        return true;
    }

    //Tag Relationship
    //-----------------

    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class)->withTimestamps();
    }

    //Check if User has a Tag
    public function hasTag($name)
    {
        foreach ($this->tags as $tag) {
            if ($tag->name == $name) {
                return true;
            }
        }

        return false;
    }

    //Assign a Tag to A User
    public function assignTag($tag)
    {
        return $this->tags()->attach($tag);
    }

    //Remove a Tag from a User
    public function removeTag($tag)
    {
        return $this->tags()->detach($tag);
    }

    //Check if user has only user tag

    public function onlyTag($name)
    {
        foreach ($this->tags as $tag) {
            if ($tag->name != $name) {
                return false;
            }
        }

        return true;
    }

    //Property Relationship
    //-----------------

    public function property()
    {
        return $this->belongsToMany(\App\Models\Property::class)->withTimestamps();
    }

    //Assign a Property to A User
    public function assignProperty($property, $role)
    {
        return $this->property()->attach($property, $role);
    }

    //Remove a Property from a User
    public function removeProperty($property)
    {
        return $this->property()->detach($property);
    }

    //Auction Bids

    public function auction_bids()
    {
        return $this->belongsToMany(\App\Models\Auctionbids::class)->withTimestamps();
    }
}
