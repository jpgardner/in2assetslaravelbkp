<?php

namespace In2Assets\Blogs;

class PublishBlogCommand
{
    public $gallery_id;

    public $title;

    public $body;

    public $short_body;

    public $lead_image;

    public $category;

    public $status;

    public $slug;

    public $userId;

    public $site;

    public function __construct($gallery_id, $title, $body, $short_body, $lead_image, $category, $status, $slug, $userId, $site)
    {
        $this->gallery_id = $gallery_id;
        $this->title = $title;
        $this->body = $body;
        $this->short_body = $short_body;
        $this->lead_image = $lead_image;
        $this->category = $category;
        $this->status = $status;
        $this->slug = $slug;
        $this->userId = $userId;
        $this->site = $site;
    }
}
