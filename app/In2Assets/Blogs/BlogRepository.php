<?php

namespace In2Assets\Blogs;

use In2Assets\Users\User;

class BlogRepository
{
    public function getAllForUser(User $user)
    {
        return $user->blogs()->with('user')->latest()->get();
    }

    public function save(Blog $blog, $userId)
    {
        return User::findOrFail($userId)
            ->blogs()
            ->save($blog);
    }

    public function findBySlug($slug)
    {
        return Blog::whereStatus('published')->whereSlug($slug)->first();
    }

    public function findAll()
    {
        return Blog::orderBy('created_at', 'desc');
    }

    //paginated list of all users;
    public function getPaginated($howMany = 10)
    {
        return Blog::whereStatus('published')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })
            ->orderBy('created_at', 'desc')
            ->simplePaginate($howMany);
    }

    public function getHome()
    {
        return Blog::whereStatus('published')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })
            ->take(4)->orderBy('created_at', 'desc')->get();
    }

    //paginated list of all users;
    public function getCategoryPaginated($category, $howMany = 10)
    {
        return Blog::whereStatus('published')->whereCategory($category)
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })
            ->orderBy('created_at', 'desc')->simplePaginate($howMany);
    }

    public function getSearch($q)
    {
        return Blog::whereRaw('MATCH(title,body) AGAINST(? IN BOOLEAN MODE)', [$q])
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            });
    }
}
