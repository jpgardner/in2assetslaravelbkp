<?php

namespace In2Assets\Blogs;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use In2Assets\Blogs\Events\BlogWasPublished;
use Laracasts\Commander\Events\EventGenerator;

class Blog extends Model
{
    use EventGenerator;

    protected $fillable = ['gallery_id', 'title', 'body', 'short_body', 'lead_image', 'category', 'status', 'slug', 'site'];

    //Notes belong to a user
    public function user()
    {
        return $this->belongsTo('In2Assets\Users\User');
    }

    public static function publish($gallery_id, $title, $body, $short_body, $lead_image, $category, $status, $slug, $site)
    {
        $blog = new static(compact('gallery_id', 'title', 'body', 'short_body', 'lead_image', 'category', 'status', 'slug', 'site'));
        $blog->raise(new BlogWasPublished($body));

        return $blog;
    }
}
