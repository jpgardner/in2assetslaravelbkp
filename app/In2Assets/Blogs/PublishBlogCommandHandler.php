<?php

namespace In2Assets\Blogs;

use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

class PublishBlogCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    protected $BlogRepository;

    public function __construct(BlogRepository $BlogRepository)
    {
        $this->BlogRepository = $BlogRepository;
    }

    /**
     * Handle the command.
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $blog = Blog::publish($command->gallery_id, $command->title, $command->body, $command->short_body, $command->lead_image, $command->category, $command->status, $command->slug, $command->site);
        $blog = $this->BlogRepository->save($blog, $command->userId);
        $this->dispatchEventsFor($blog);

        return $blog;
    }
}
