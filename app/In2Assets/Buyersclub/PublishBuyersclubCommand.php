<?php

namespace In2Assets\Buyersclub;

class PublishBuyersclubCommand
{
    public $gallery_id;

    public $title;

    public $body;

    public $short_body;

    public $lead_image;

    public $category;

    public $status;

    public $file;

    public $slug;

    public $userId;

    public function __construct($gallery_id, $title, $body, $short_body, $lead_image, $category, $status, $file, $slug, $userId)
    {
        $this->gallery_id = $gallery_id;
        $this->title = $title;
        $this->body = $body;
        $this->short_body = $short_body;
        $this->lead_image = $lead_image;
        $this->category = $category;
        $this->status = $status;
        $this->file = $file;
        $this->slug = $slug;
        $this->userId = $userId;
    }
}
