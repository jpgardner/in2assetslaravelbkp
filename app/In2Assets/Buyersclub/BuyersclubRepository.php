<?php

namespace In2Assets\Buyersclub;

use In2Assets\Users\User;

class BuyersclubRepository
{
    public function getAllForUser(User $user)
    {
        return $user->buyersclub()->with('user')->latest()->get();
    }

    public function save(Buyersclub $buyersclub, $userId)
    {
        return User::findOrFail($userId)
            ->buyersclub()
            ->save($buyersclub);
    }

    public function findBySlug($slug)
    {
        return Buyersclub::whereSlug($slug)->first();
    }

    public function findAll()
    {
        return Buyersclub::orderBy('created_at', 'desc');
    }

    //paginated list of all users;
    public function getPaginated($howMany = 10)
    {
        return Buyersclub::orderBy('created_at', 'desc')->simplePaginate($howMany);
    }

    public function getHome()
    {
        return Buyersclub::take(4)->orderBy('created_at', 'desc')->get();
    }

    //paginated list of all users;
    public function getCategoryPaginated($category)
    {
        return Buyersclub::whereCategory($category)->orderBy('created_at', 'desc')->simplePaginate(10);
    }

    public function getSearch($q)
    {
        return Buyersclub::whereRaw('MATCH(title,body) AGAINST(? IN BOOLEAN MODE)', [$q]);
    }
}
