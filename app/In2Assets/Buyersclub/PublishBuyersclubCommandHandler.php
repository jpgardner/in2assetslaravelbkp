<?php

namespace In2Assets\Buyersclub;

use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

class PublishBuyersclubCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    protected $BuyersclubRepository;

    public function __construct(BuyersclubRepository $BuyersclubRepository)
    {
        $this->BuyersclubRepository = $BuyersclubRepository;
    }

    /**
     * Handle the command.
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $buyersclub = Buyersclub::publish($command->gallery_id, $command->title, $command->body, $command->short_body, $command->lead_image, $command->category, $command->status, $command->file, $command->slug);
        $buyersclub = $this->BuyersclubRepository->save($buyersclub, $command->userId);
        $this->dispatchEventsFor($buyersclub);

        return $buyersclub;
    }
}
