<?php

namespace In2Assets\Buyersclub\Events;

class BuyersclubWasPublished
{
    public $body;

    public function __construct($body)
    {
        $this->body = $body;
    }
}
