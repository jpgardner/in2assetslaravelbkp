<?php

namespace In2Assets\Forms;

use Laracasts\Validation\FormValidator;

class EditForm extends FormValidator
{
    //Validation rules for registration form
    protected $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
    ];
}
