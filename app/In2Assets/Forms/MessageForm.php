<?php

namespace In2Assets\Forms;

use Laracasts\Validation\FormValidator;

class MessageForm extends FormValidator
{
    /**
     * Validation rules for the publish status form.
     *
     * @var array
     */
    protected $rules = [
        'subject' => 'required',
        'message' => 'required',
    ];
}
