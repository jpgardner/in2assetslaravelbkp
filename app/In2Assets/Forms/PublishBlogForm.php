<?php

namespace In2Assets\Forms;

use Laracasts\Validation\FormValidator;

class PublishBlogForm extends FormValidator
{
    /**
     * Validation rules for the publish status form.
     *
     * @var array
     */
    protected $rules = [
        'title' => 'required',
        'body' => 'required',
    ];
}
