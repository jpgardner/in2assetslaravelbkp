<?php

namespace In2Assets\Blogs\Events;

class BlogWasPublished
{
    public $body;

    public function __construct($body)
    {
        $this->body = $body;
    }
}
