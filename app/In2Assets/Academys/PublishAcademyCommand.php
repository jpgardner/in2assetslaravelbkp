<?php

namespace In2Assets\Academys;

class PublishAcademyCommand
{
    public $title;

    public $body;

    public $category;

    public $status;

    public $slug;

    public function __construct($title, $body, $category, $status, $slug)
    {
        $this->title = $title;
        $this->body = $body;
        $this->category = $category;
        $this->status = $status;
        $this->slug = $slug;
    }
}
