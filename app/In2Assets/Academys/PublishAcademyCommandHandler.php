<?php

namespace In2Assets\Academys;

use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

class PublishAcademyCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    protected $AcademyRepository;

    public function __construct(AcademyRepository $AcademyRepository)
    {
        $this->AcademyRepository = $AcademyRepository;
    }

    /**
     * Handle the command.
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $academy = Academy::publish($command->title, $command->body, $command->category, $command->status, $command->slug);
        $academy = $this->AcademyRepository->save($academy);

        return $academy;
    }
}
