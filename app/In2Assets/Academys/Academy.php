<?php

namespace In2Assets\Academys;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use In2Assets\Academys\Events\AcademyWasPublished;
use Laracasts\Commander\Events\EventGenerator;

class Academy extends Model
{
    use EventGenerator;

    protected $fillable = ['title', 'body', 'category', 'status', 'slug'];

    public static function publish($title, $body, $category, $status, $slug)
    {
        $academy = new static(compact('title', 'body', 'category', 'status', 'slug'));

        return $academy;
    }
}
