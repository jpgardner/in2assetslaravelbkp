<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use In2Assets\Mailers\UserMailer;
use In2Assets\Users\User;
use Laracasts\Commander\Events\EventListener;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class NotifyCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:notification_command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email Notification Cron Jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();
        $sendMessages = DB::table('email_notifications')->where('send_date', $today)->get(); //Fetch Stored Notifications for Today

        // Registration Email
        $registration_messages = 'This is a registration messsage';
        $registration_subject = 'This is a registration subject';

        // Sales Email
        $sales_messages = 'This is a sales messsage';
        $sales_subject = 'This is a sales subject';

        //Loop through each and send message to user based on which notification was requested.
        foreach ($sendMessages as $message) {

            // Get the User
            $user = User::whereId($message->userid)->first();
            $userName = $user->firstname.' '.$user->lastname;

            // Set the right message
            $messages = $message->notification;
            $subject = $message->notification;

            if ($message->notification == 'registration') {
                $messages = $registration_messages;
                $subject = $registration_subject;
            } elseif ($message->notification == 'sales') {
                $messages = $sales_messages;
                $subject = $sales_subject;
            }

            // Combine to array before sending
            $data = ['name' => $userName, 'subject' => $subject, 'messages' => $messages];

            // Queue email
            Mail::queue('emails.registration.confirm', $data, function ($message) use ($user) {
                $message->to($user->email, $user->firstname)
                         ->subject('subject');
            });

            // Remove Record from DB
            $id = $message->id;
            DB::table('email_notifications')->delete($id);
        }
    }
}
