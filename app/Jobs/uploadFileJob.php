<?php

namespace App\Jobs;

use App\In2Assets\Uploads\FileRepository;
use App\In2Assets\Uploads\Upload;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class uploadFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $title;

    public $file;

    public $type;

    public $link;

    public $extension;

    public $userId;

    public $class;

    public $position;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title, $file, $link, $fileId, $extension, $userId, $class, $position = null)
    {
        $this->title = $title;
        $this->file = $file;
        $this->link = $link;
        $this->fileId = $fileId;
        $this->extension = $extension;
        $this->userId = $userId;
        $this->class = $class;
        $this->position = $position;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = Upload::publish($this->title, $this->file, $this->link, $this->extension, $this->userId, $this->class, $this->position);

        $fileRep = new FileRepository;
        $fileRep->save($file, $this->userId);

        dd('Uploading files');
    }
}
