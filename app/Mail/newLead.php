<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newLead extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $name, $emailData, $userMail)
    {
        //
        $this->subject = $subject;
        $this->name = $name;
        $this->emailData = $emailData;
        $this->userMail = $userMail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.registration.adminnotice', ['name' => $this->name,
            'emailData' => $this->emailData, 'userMail' => $this->userMail, ])->subject($this->subject);
    }
}
