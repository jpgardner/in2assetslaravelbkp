<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyAdvertBoards extends Model
{
    protected $table = 'advertising_boards';

    protected $guarded = ['id'];

    use SoftDeletes;
}
