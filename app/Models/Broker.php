<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Broker extends Model
{
    protected $table = 'broker';

    protected $fillable = ['user_id', 'agency', 'certificate_number', 'office_number'];

    use SoftDeletes;

    public static $rules =
        [
            'user_id' => 'required',
            'agency' => 'required',
            'certificate_number' => 'required',
            'office_number' => 'required',

        ];

    public function user()
    {
        return $this->belongsTo('App\Models\In2Assets/Users/User');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
