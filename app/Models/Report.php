<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Report extends Model
{
    protected $table = 'reports';

    protected $guarded = ['id'];

    use SoftDeletes;

    public static $rules =
        [
            'name' => 'required|unique:reports',
            'description' => 'required',
            'script' => 'required',
        ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
