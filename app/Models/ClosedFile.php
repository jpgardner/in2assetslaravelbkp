<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Created by PhpStorm.
 * User: Erich
 * Date: 2017/01/27
 * Time: 9:26 AM.
 */
class ClosedFile extends Model
{
    protected $table = 'closed_file';

    protected $guarded = ['id'];

    use SoftDeletes;
}
