<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// auction_type key
// 1. Physical Auction
// 2. Bid to Buy Auction (Conditional)
// 3. Bid to Buy Auction (Un-Conditional)
// 4. Online\live Auction
// 5. On Site Auction

class Auction extends Model
{
    protected $table = 'auction';

    protected $guarded = ['id'];

    use SoftDeletes;

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class);
    }

    public function auctionRegister()
    {
        return $this->hasMany(\App\Models\AuctionRegister::class);
    }
}
