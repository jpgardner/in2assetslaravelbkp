<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Preferences extends Model
{
    protected $table = 'preferences';

    protected $guarded = ['id', 'user_id'];

    public function user()
    {
        return $this->hasOne(\App\Models\User::class);
    }
}
