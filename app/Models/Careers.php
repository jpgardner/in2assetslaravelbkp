<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Careers extends Model
{
    protected $table = 'careers';

    protected $guarded = ['id'];

    use SoftDeletes;

    public static $rules =
        [
            'title' => 'required',
            'city' => 'required',
            'description' => 'required',
            'category_id' => 'required',
        ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
