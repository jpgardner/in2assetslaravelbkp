<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Targets extends Model
{
    protected $table = 'agent_targets';

    protected $guarded = ['id', 'mandate_actual', 'sale_actual', 'db_growth_actual', 'sla_actual', 'letting_actual'];
}
