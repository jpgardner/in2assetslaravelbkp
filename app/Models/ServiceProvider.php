<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class ServiceProvider extends Model
{
    protected $table = 'service_provider';

    protected $fillable = ['company', 'contact_firstname', 'contact_lastname', 'division', 'acc_number', 'cell_number', 'office_number', 'email', 'active', 'category', 'province', 'area', 'suburb_town_city'];

    use SoftDeletes;

    public static $rules =
        [
            'company' => 'required',
            'division' => 'required',
            'contact_firstname' => 'required_without_all:contact_lastname',
            'contact_lastname' => 'required_without_all:contact_firstname',
            'division' => 'required',
            'acc_number' => 'required',
            'cell_number' => 'required_without_all:office_number',
            'office_number' => 'required_without_all:cell_number',
            'email' => 'required',
            'category' => 'required',
            'province' => 'required',
            'area' => 'required',
            'suburb_town_city' => 'required',

        ];

    public $errors;

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
