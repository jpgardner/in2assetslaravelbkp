<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Featured extends Model
{
    protected $table = 'featured_property';

    protected $guarded = ['id'];

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class);
    }
}
