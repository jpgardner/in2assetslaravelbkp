<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commission extends Model
{
    protected $table = 'commissions';

    protected $guarded = ['id'];

    use SoftDeletes;
}
