<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

// business_types:-
// 1: Sole Proprietor
// 2: (Pty) Ltd
// 3: cc
// 4: Partnership
// 5: Limited

class LeaseAgreement extends Model
{
    protected $table = 'lease_agreements';

    protected $guarded = ['id', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class);
    }

    public function amendments()
    {
        return $this->hasOne(\App\Models\LeaseAmendment::class);
    }
}
