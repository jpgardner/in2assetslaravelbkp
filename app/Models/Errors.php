<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Errors extends Model
{
    protected $table = 'errors_log';

    protected $fillable = [];

    public static $rules =
        [
            'error' => 'required',
            'error_type' => 'required',
        ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
