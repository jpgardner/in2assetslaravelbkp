<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Buyers extends Model
{
    protected $table = 'buyers';

    protected $guarded = ['id'];

    use SoftDeletes;

    public static $rules =
        [
            'private_buyer' => 'required',
            'multiple_buyer' => 'required',
            'ownership_type' => 'required',
            'rep_full_name' => 'required_if:multiple_buyer,==,1',
            'rep_id_number' => 'required_if:multiple_buyer,==,1',
            'contact_name' => 'required_if:multiple_buyer,==,1',
            'contact_cell' => 'required_if:multiple_buyer,==,1',
            'contact_email' => 'required_if:multiple_buyer,==,1',
            'married' => 'required',
            'id_number' => 'required',
            'married_in_com' => 'required_if:married,==,1',
            'spouse_id_number' => 'required_if:married,==,1',
            'spouse_full_name' => 'required_if:married,==,1',
            'self_employed' => 'required',
            'pre_approval' => 'required',
        ];

    public function users()
    {
        return $this->hasOne('App\Models\Users');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
