<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Ownattourney extends Model
{
    protected $table = 'own_attourney';

    protected $guarded = ['id', 'property_id'];

    public static $rules =
        [
            'company_name' => 'required',
            'contact_name' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
        ];

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class, 'property_id');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
