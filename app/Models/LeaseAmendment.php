<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class LeaseAmendment extends Model
{
    protected $table = 'lease_amendments';

    protected $guarded = ['lease_agreement_id', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public function LeaseAgreement()
    {
        return $this->belongsTo(\App\Models\LeaseAgreement::class);
    }

    public static $rules =
        [

        ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
