<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auctionbids extends Model
{
    protected $table = 'auction_bids';

    protected $fillable = [];

    public function user()
    {
        return $this->belongsTo('App\Models\In2Assets/Users/User');
    }
}
