<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

/*
  * vetting_status key
  * 1. Pending
  * 2. Approved
  * 3. Vetting
  * 4. Sold < 60 Days
  * 5. Rented < 14 Days
  * 6. Sold > 60 Days
  * 7. Rented > 14 Days
  * 8. Rejected
  * 9. Auction
  * 10. Sold on Auction
  * 11. Not Sold
  * 12. Sold Private Treaty Pre Auction
  * 13. Sold Private Treaty Post Auction
  * 14. Withdrawn
  * 15. Postponed
  * 16. S.T.C. (Subject to Confirmation)
  * 17. Leads

*/

class Vetting extends Model
{
    protected $table = 'vetting';

    protected $fillable = ['property_id', 'owner_title', 'owner_firstname', 'owner_surname', 'ownership_type', 'rep_title',
        'rep_firstname', 'rep_surname', 'rep_id_number', 'rep_cell_num', 'rep_email', 'marriage_stat', 'mar_in_com', 'spouse_title',
        'spouse_firstname', 'spouse_surname', 'spouse_id_number', 'extra_owner_title1', 'extra_owner_firstname1',
        'extra_owner_surname1', 'extra_owner_title2', 'extra_owner_firstname2', 'extra_owner_surname2',
        'extra_owner_title3', 'extra_owner_firstname3', 'extra_owner_surname3', 'id_photos', 'resolution', 'mandate', 'in2assets_only', ];

    use SoftDeletes;

    public static $rules =
        [
            'owner_title' => 'required',
            'owner_firstname' => 'required',
            'owner_surname' => 'required',
            'ownership_type' => 'required',
            'rep_title' => 'required',
            'rep_firstname' => 'required',
            'rep_surname' => 'required',
            'rep_id_number' => 'required',
            'rep_cell_num' => 'required',
            'rep_email' => 'required',
            'marriage_stat' => 'required',
            'mar_in_com' => 'required_if:rep_marriage_stat,==,Married',
            'spouse_title' => 'required_if:mar_in_com,==,Yes',
            'spouse_firstname' => 'required_if:mar_in_com,==,Yes',
            'spouse_surname' => 'required_if:mar_in_com,==,Yes',
            'spouse_id_number' => 'required_if:mar_in_com,==,Yes',

        ];

    public static $rules2 =
        [
            'id_photos' => 'required',
            'resolution' => 'required_if:ownership_type,==,Closed Corporation|required_if:ownership_type,==,Company (Pty) Limited|required_if:ownership_type,==,Trust',
        ];

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class);
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }

    public function isValid2()
    {
        $validation = Validator::make($this->attributes, static::$rules2);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
