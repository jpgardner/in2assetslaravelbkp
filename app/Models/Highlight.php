<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: Erich
 * Date: 2017/01/27
 * Time: 9:26 AM.
 */
class Highlight extends Model
{
    protected $table = 'highlight_list';

    protected $guarded = ['id'];
}
