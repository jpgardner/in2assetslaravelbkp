<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Classified extends Model
{
    protected $guarded = ['id', 'status', 'user_id'];

    use SoftDeletes;

    public static $rules =
        [

            'title' => 'required',
            'price' => 'required',
            'category' => 'required',
            'sub_category' => 'required',
            'province' => 'required',
            'area' => 'required',
            'suburb_town_city' => 'required',

        ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
