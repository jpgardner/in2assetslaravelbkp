<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class DeedAmendment extends Model
{
    protected $table = 'deed_amendment';

    protected $guarded = ['deed_of_sale_id', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public function DeedOfSale()
    {
        return $this->belongsTo(\App\Models\DeedOfSale::class);
    }

    public static $rules =
        [

        ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
