<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailAlert extends Model
{
    protected $table = 'email_alert';

    protected $guarded = ['id'];

    use SoftDeletes;
}
