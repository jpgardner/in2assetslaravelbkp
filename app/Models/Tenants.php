<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Tenants extends Model
{
    protected $table = 'tenants';

    protected $guarded = ['id'];

    use SoftDeletes;

    public static $rules =
        [
            'id_number' => 'required',
            'itc_agree' => 'required',
            'cell' => 'required',
            'email' => 'required',
        ];

    public function users()
    {
        return $this->hasOne('App\Models\Users');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
