<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//Roles table legend
//1.Broker
// 2.Agent
// 3.Seller
// 4.Landlord
// 5.Buyer
// 6.ServiceProvider
// 7.Tenant
// 8.Admin

class Role extends Model
{
    protected $fillable = ['name'];
}
