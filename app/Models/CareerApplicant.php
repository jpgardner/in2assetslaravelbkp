<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class CareerApplicant extends Model
{
    protected $table = 'career_applicant';

    protected $guarded = ['id'];

    public static $rules =
        [
            'g-recaptcha-response' => 'required|captcha',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'career_id' => 'required',
            'file' => 'required',
        ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
