<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Commercial extends Model
{
    protected $table = 'commercial';

    protected $fillable = ['property_id', 'commercial_type', 'garages'];

    use SoftDeletes;

    public static $rules =
        [
            'commercial_type' => 'required',
        ];

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class, 'property_id');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
