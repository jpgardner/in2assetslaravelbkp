<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeedOfSale extends Model
{
    protected $table = 'deed_of_sale';

    protected $guarded = ['id', 'deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class);
    }

    public function amendments()
    {
        return $this->hasOne(\App\Models\DeedAmendment::class);
    }
}
