<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyAdvertSocialMedia extends Model
{
    protected $table = 'advertising_social_media';

    protected $guarded = ['id'];

    use SoftDeletes;
}
