<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class PhysicalAuction extends Model
{
    protected $table = 'physical_auctions';

    protected $guarded = ['id'];

    use SoftDeletes;

    public static $rules =
        [
            'auction_name' => 'required',
            'province' => 'not_in:all',
            'suburb_town_city' => 'not_in:all',
            'area' => 'not_in:all',
            'street_address' => 'required',
            'start_date' => 'required',
        ];

    public function Auction()
    {
        return $this->hasMany(\App\Models\Auction::class);
    }

    public function PhysicalAuctionRegister()
    {
        return $this->hasMany(\App\Models\PhysicalAuctionRegister::class);
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
