<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actual extends Model
{
    protected $table = 'agent_actuals';

    protected $guarded = ['id'];

    use SoftDeletes;
}
