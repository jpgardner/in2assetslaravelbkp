<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewingSessionsBooked extends Model
{
    protected $table = 'viewing_sessions_booked';

    protected $fillable = [];
}
