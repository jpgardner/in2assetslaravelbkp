<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Property extends Model
{
    protected $table = 'property';

    protected $guarded = ['id'];

    use SoftDeletes;

    public static $rules =
        [
            'country' => 'required',
            'province' => 'required',
            'suburb_town_city' => 'required',
            'area' => 'required',
            'property_type' => 'required',
            'listing_type' => 'required',
            'land_measurement' => 'required_if:land_size,==,true',
        ];

    public static $rules2 =
        [
            'lead_image' => 'required',
            'image_gallery' => 'required',
            'short_descrip' => 'required',
            'long_descrip' => 'required',
            'attourney' => 'required',
            'price' => 'required',
        ];

    public $errors;

    public function farm()
    {
        return $this->hasOne(\App\Models\Farm::class);
    }

    public function featured()
    {
        return $this->hasOne(\App\Models\Featured::class);
    }

    public function residential()
    {
        return $this->hasOne(\App\Models\Residential::class);
    }

    public function commercial()
    {
        return $this->hasOne(\App\Models\Commercial::class);
    }

    public function own_attourney()
    {
        return $this->hasOne(\App\Models\Ownattourney::class);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\In2Assets/Users/User');
    }

    public function vetting()
    {
        return $this->hasOne(\App\Models\Vetting::class);
    }

    public function auction()
    {
        return $this->hasOne(\App\Models\Auction::class);
    }

    public function favourites()
    {
        return $this->hasMany('App\Models\Favourites');
    }

    public function features()
    {
        return $this->belongsToMany(\App\Models\Attributes::class)->withTimestamps()->withPivot('value');
    }

    //Check if Property has Attribute
    public function hasAttribute($name)
    {
        foreach ($this->features as $attribute) {
            if ($attribute->name == $name) {
                return true;
            }
        }

        return false;
    }

    public function getFeatureValue($name, $property_id)
    {
        $query = 'SELECT value FROM attributes
                    INNER JOIN attributes_property
                    ON attributes.id = attributes_id
                    WHERE name = "'.$name.'"
                    AND property_id = "'.$property_id.'"';

        return DB::select($query);
    }

    public function getFeatureValuebyType($name, $property_id, $type)
    {
        $query = 'SELECT value FROM attributes
                    INNER JOIN attributes_property
                    ON attributes.id = attributes_id
                    WHERE name = "'.$name.'"
                    AND property_id = "'.$property_id.'"
                    AND type = "'.$type.'"';

        return DB::select($query);
    }

    //Assign a Attribute to A Property
    public function assignAttribute($attribute, $value)
    {
        //dd($value);
        if (! is_numeric($attribute)) {
            $attributeDetails = DB::select('SELECT id FROM attributes WHERE attributes.name = "'.$attribute.'"');
            $attribute = $attributeDetails[0];
        }

        return $this->features()->attach($attribute, ['value' => $value]);
    }

    public function ammendAttribute($attributes_id, $property_id, $value)
    {
        $query = 'UPDATE attributes_property SET value = "'.$value.'" WHERE attributes_id = "'.$attributes_id.'"
        AND property_id = "'.$property_id.'"';

        return DB::update($query);
    }

    //Remove a Role from a User
    public function removeAttribute($attribute)
    {
        if (! is_numeric($attribute)) {
            $attributeDetails = DB::select('SELECT id FROM attributes WHERE attributes.name = "'.$attribute.'"');
            $attribute = $attributeDetails[0];
        }

        return $this->features()->detach($attribute);
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }

    public function isValid2()
    {
        $validation = Validator::make($this->attributes, static::$rules2);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
