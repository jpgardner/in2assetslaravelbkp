<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Viewing extends Model
{
    protected $table = 'viewing';

    protected $guarded = ['id', 'user_id', 'booked_by'];

    public static $rules =
        [
            'property_id' => 'required',
            'event_type' => 'required',
            'event_start' => 'required',
            'event_end' => 'required',
        ];

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class, 'property_id');
    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }
}
