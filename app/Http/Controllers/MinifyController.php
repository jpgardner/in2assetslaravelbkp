<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use MatthiasMullie\Minify;

class MinifyController extends Controller
{
    public function minifyCSS()
    {
//        chmod("../css/publishCSS/style", 0600);
        $sourcePath = public_path().'/css/style1.css';
//        $minifier = new Minify\CSS($sourcePath);
        // we can even add another file, they'll then be
        // joined in 1 output file
        $sourcePath2 = public_path().'/css/style2.css';
//        $minifier->add($sourcePath2);
        // save minified file to disk

        $minifier = new Minify\CSS($sourcePath, $sourcePath2);

        $newPath = $this->saveExt('css');

        $minifiedPath = public_path().$newPath;

        $minifier->minify($minifiedPath);
    }

    public function minifyJS()
    {
//        chmod("../css/publishCSS/style", 0600);
        $sourcePath = public_path().'/js/ajax.js';
//        $minifier = new Minify\CSS($sourcePath);
        $minifier = new Minify\JS($sourcePath);

        $newPath = $this->saveExt('js');

        $minifiedPath = public_path().$newPath;

        $minifier->minify($minifiedPath);
    }

    //version dating for css and js minify
    public function saveExt($type)
    {
        $old_date = DB::table('minify_version')->select($type)->whereId(1)->first();

        $dateExt = date('YMd');

        if ($type == 'css') {
            rename(public_path().'/css/publishCSS/style'.$old_date->css.'.css', public_path().'/css/publishCSS/style'.$dateExt.'.css');
        } elseif ($type == 'js') {
            rename(public_path().'/js/publishJS/ajax'.$old_date->js.'.min.js', public_path().'/js/publishJS/ajax'.$dateExt.'.min.js');
        }

        DB::table('minify_version')
            ->whereId(1)
            ->update([$type => $dateExt]);

        if ($type == 'css') {
            return '/css/publishCSS/style'.$dateExt.'.css';
        } else {
            return '/js/publishJS/ajax'.$dateExt.'.min.js';
        }
    }
}
