<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class AgentController extends Controller
{
    public function __construct(Agent $agent)
    {
        $this->agent = $agent;
    }

    public function index()
    {
        //
        $noForm = false;

        if (! DB::table('agent')->select('id')->whereUser_id(Auth::user()->id)->first()) {
            $noForm = true;
        }

        return view('agent.index', ['noform' => $noForm]);
    }

    public function store()
    {
        $input = Request::all();
        $input['user_id'] = Auth::user()->id;

        if (! $this->agent->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->agent->errors);
        }

        $this->agent->save();

        return Redirect::route('agent.index');
    }
}
