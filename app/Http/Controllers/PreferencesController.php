<?php

namespace App\Http\Controllers;

use App\Models\Preferences;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use Laracasts\Flash\Flash;

class PreferencesController extends Controller
{
    public function __construct(Preferences $preferences, Property $property, EmailAlert $email_alert)
    {
        $this->preferences = $preferences;
        $this->property = $property;
        $this->email_alert = $email_alert;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function create()
    {
        $areas = App::make('PropertyController')->get_adv_areas();

        $preferences = new Preferences();

        $preferences->price = 'R0-R15000000';
        $preferences->land_size = '0-20000+';
        $preferences->floor_space = '0-2000+';
        $preferences->province = 'south-africa';

        return view('portal.property_search_create', ['preferences' => $preferences, 'provinces_adv' => $areas['provinces'],
            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], ]);
    }

    public function store()
    {
        $input = Request::all();

        $receive_email = 0;
        if (Request::get('receive_email')) {
            $receive_email = 1;
            unset($input['receive_email']);
        }

        $prov_area_sub_array = explode(',', $input['prov_area_sub']);

        switch (count($prov_area_sub_array)) {
            case 0:
                $prov_area_sub_array[0] = '';

                break;
            case 1:
                $prov_area_sub_array[1] = '';

                break;
            case 2:
                $prov_area_sub_array[2] = '';

                break;
        }

        $input['country'] = 'South Africa';
        if (trim($prov_area_sub_array[0]) == '') {
            $input['province'] = 'south-africa';
            $input['area'] = 'all-cities';
            $input['suburb_town_city'] = 'all-suburbs';
        } else {
            $input['province'] = trim($prov_area_sub_array[0]);
            if (trim($prov_area_sub_array[1]) == '') {
                $input['area'] = 'all-cities';
                $input['suburb_town_city'] = 'all-suburbs';
            } else {
                $input['area'] = trim($prov_area_sub_array[1]);
                if (trim($prov_area_sub_array[2]) == '') {
                    $input['suburb_town_city'] = 'all-suburbs';
                } else {
                    $input['suburb_town_city'] = trim($prov_area_sub_array[2]);
                }
            }
        }

        unset($input['prov_area_sub']);

        if (isset($input['price_range_rent']) || isset($input['price_range'])) {
            if ($input['listing_type'] == 'welet') {
                $input['price'] = $input['price_range_rent'];
            } else {
                $input['price'] = $input['price_range'];
            }
            $input['land_size'] = $input['land_size_range'];
            $input['floor_space'] = $input['floor_space_range'];

            $price_range = $input['price_range'];
            $price_range_rent = $input['price_range_rent'];
            $floor_space_range = $input['floor_space_range'];
            $land_size_range = $input['land_size_range'];

            unset($input['price_range_rent']);
            unset($input['price_range']);
            unset($input['land_size_range']);
            unset($input['floor_space_range']);

            $referrer = URL::previous();

            if (strpos($referrer, 'users/edit')) {
                $id = substr($referrer, strrpos($referrer, '/') + 1);
                $this->preferences->user_id = $id;
            } else {
                $this->preferences->user_id = Auth::user()->id;
            }

            $this->preferences->fill($input);

            $this->preferences->save();

            switch ($input['property_type']) {
                case 'commercial':
                    $sub_type = $input['commercial_type'];

                    break;
                case 'residential':
                    $sub_type = $input['dwelling_type'];

                    break;
                case 'farm':
                    $sub_type = $input['farm_type'];

                    break;
                default:
                    $sub_type = 'None';

                    break;
            }

            $slug = new Slugify();

            if ($receive_email == 1) {
                App::make('AlertController')->addPreferenceAlert($this->preferences->id);
            }
            if (strpos($referrer, 'users/edit')) {
                return Redirect::back();
            }

            return Redirect::route('property.index', [$input['property_type'], $input['listing_type'], $slug->slugify($input['province']),
                $slug->slugify($input['area']), $slug->slugify($input['suburb_town_city']), 'default', 'grid_view', $price_range, $price_range_rent,
                $floor_space_range, $land_size_range, $sub_type, $input['bedrooms'], $input['bathrooms'], ]);
        }
        $this->preferences->fill($input);
        $this->preferences->user_id = Auth::user()->id;
        $this->preferences->save();

        if ($receive_email == 1) {
            App::make('AlertController')->addPreferenceAlert($this->preferences->id);
        }

        return 'Done';
    }

    public function emailAlert()
    {
        $input = Request::all();

        $prov_area_sub_array = explode(',', $input['prefSuburb']);

        switch (count($prov_area_sub_array)) {
            case 0:
                $prov_area_sub_array[0] = '';

                break;
            case 1:
                $prov_area_sub_array[1] = '';

                break;
            case 2:
                $prov_area_sub_array[2] = '';

                break;
        }
        $input['area'] = $input['city'];
        $input['suburb_town_city'] = $input['suburb'];

        $input['country'] = 'South Africa';
        if (trim($prov_area_sub_array[0]) == '') {
            $input['province'] = 'south-africa';
            $input['area'] = 'all-cities';
            $input['suburb_town_city'] = 'all-suburbs';
        } else {
            $input['province'] = trim($prov_area_sub_array[0]);
            if (trim($prov_area_sub_array[1]) == '') {
                $input['area'] = 'all-cities';
                $input['suburb_town_city'] = 'all-suburbs';
            } else {
                $input['area'] = trim($prov_area_sub_array[1]);
                if (trim($prov_area_sub_array[2]) == '') {
                    $input['suburb_town_city'] = 'all-suburbs';
                } else {
                    $input['suburb_town_city'] = trim($prov_area_sub_array[2]);
                }
            }
        }

        if ($input['listing_type_pref'] == 'welet') {
            $input['price'] = $input['price_range_rent_pref'];
        } else {
            $input['price'] = $input['price_pref_range'];
        }

        $input['land_size'] = $input['land_size_pref_range'];
        $input['floor_space'] = $input['floor_space_pref_range'];

        $input['property_type'] = $input['property_type_pref'];
        $input['listing_type'] = $input['listing_type_pref'];
        $input['commercial_type'] = $input['commercial_type_pref'];
        $input['dwelling_type'] = $input['dwelling_type_pref'];
        $input['farm_type'] = $input['farm_type_pref'];
        $input['bedrooms'] = $input['bedrooms_pref'];
        $input['bathrooms'] = $input['bathrooms_pref'];

        unset($input['property_type_pref'], $input['listing_type_pref'], $input['commercial_type_pref'],
            $input['dwelling_type_pref'], $input['farm_type_pref'], $input['price_range_rent_pref'], $input['price_pref_range'],
            $input['land_size_pref_range'], $input['floor_space_pref_range'], $input['prefSuburb'], $input['bedrooms_pref'],
            $input['bathrooms_pref'], $input['city'], $input['suburb']);

        $this->preferences->fill($input);
        $this->preferences->user_id = Auth::user()->id;
        $this->preferences->save();

        App::make('AlertController')->addPreferenceAlert($this->preferences->id);

        return Redirect::back();
    }

    public function update($id)
    {
        $input = Request::all();

        if (isset($input['users'])) {
            $users = 1;
            unset($input['users']);
        }

        $receive_email = 0;
        if (Request::get('receive_email')) {
            $receive_email = 1;
            unset($input['receive_email']);
        }

        $prov_area_sub_array = explode(',', $input['prov_area_sub']);

        $input['country'] = 'South Africa';
        if (count($prov_area_sub_array) == 0) {
            $input['province'] = 'south-africa';
            $input['area'] = 'all-cities';
            $input['suburb_town_city'] = 'all-suburbs';
        } else {
            $input['province'] = trim($prov_area_sub_array[0]);
            if (count($prov_area_sub_array) == 1) {
                $input['area'] = 'all-cities';
                $input['suburb_town_city'] = 'all-suburbs';
            } else {
                $input['area'] = trim($prov_area_sub_array[1]);
                if (count($prov_area_sub_array) == 2) {
                    $input['suburb_town_city'] = 'all-suburbs';
                } else {
                    $input['suburb_town_city'] = trim($prov_area_sub_array[2]);
                }
            }
        }

        unset($input['prov_area_sub']);
        if ($input['listing_type'] == 'welet') {
            $input['price'] = $input['price_range_rent'];
        } else {
            $input['price'] = $input['price_range'];
        }
        $input['land_size'] = $input['land_size_range'];
        $input['floor_space'] = $input['floor_space_range'];

        unset($input['price_range_rent']);
        unset($input['price_range']);
        unset($input['land_size_range']);
        unset($input['floor_space_range']);

        $this->preferences = $this->preferences->whereId($id)->first();

        $this->preferences->fill($input)->update();

        if ($receive_email == 1) {
            App::make('AlertController')->addPreferenceAlert($this->preferences->id);
        } else {
            $type = DB::table('email_alert_type')->select('id')->whereType('preference')->first();
            $this->email_alert->where('type_id', '=', $id)->where('type', '=', $type->id)->delete();
        }

        if (isset($users)) {
            return Redirect::route('users.adminlist', ['id' => $this->preferences->user_id]);
        }

        return Redirect::route('portalProperty_needs_path', ['role' => 'buyer']);
    }

    public function edit($id, $users = 0)
    {
        $preferences = $this->preferences->whereId($id)->first();

        $areas = App::make('PropertyController')->get_adv_areas();

        return view('portal.property_search_edit', ['preferences' => $preferences, 'provinces_adv' => $areas['provinces'],
                'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'users' => $users, ]);
    }

    public function delete($id)
    {
        $this->preferences->whereId($id)->delete();

        $type = DB::table('email_alert_type')->select('id')->whereType('preference')->first();
        $this->email_alert->where('type_id', '=', $id)->where('type', '=', $type->id)->delete();

        $referrer = URL::previous();

        if (strpos($referrer, 'users/edit')) {
            return Redirect::back();
        }

        Flash::message('Your saved search has been deleted.');

        return Redirect::route('portalProperty_needs_path', ['role' => 'buyer']);
    }

    public function getPreferencesResults($preference_id)
    {
        $preference = $this->preferences->whereId($preference_id)->first();

        $listing_type = $preference->listing_type;

        $price_range = str_replace('+', '', (str_replace('R', '', $preference->price)));

        $price_range_array = explode('-', $price_range);

        $min = $price_range_array[0];
        $max = $price_range_array[1];

        $query_min = "AND (price >= '".$min."'";
        if ($max == '15000000') {
            $query_max = 'OR price <= 1)';
        } else {
            $query_max = "AND price <= '".$max."' OR price <= 1)";
        }

        if ($listing_type !== 'all') {
            if ($listing_type == 'wesell') {
                $listing = '%';
                $reference = "'%SALE%' OR property.reference LIKE '%AUCT%'";
            } elseif ($listing_type == 'welet') {
                if ($max == '30000') {
                    $query_max = 'OR price <= 1)';
                } else {
                    $query_max = "AND price <= '".$max."' OR price <= 1)";
                }
                $listing = '%';
                $reference = "'%RENT%'";
            } else {
                $listing = '%'.$listing_type;
                $reference = "'%'";
            }
        } else {
            $listing = '%';
            $reference = "'%'";
        }

        $property_type = $preference->property_type;

        $bed_query = 'AND 1=1';
        $bath_query = 'AND 1=1';
        if ($property_type != 'property') {
            $property = 'AND property.property_type LIKE "%'.$property_type.'"';
            if ($property_type == 'commercial') {
                $property = 'AND (property.property_type LIKE "%'.$property_type.'" OR property.property_type LIKE "%farm")';
            }
            switch ($property_type) {

                    case 'residential':
                        if ($preference->dwelling_type != 'None') {
                            $sub_type = '%'.str_replace('_', '-', str_replace('-', ' ', $preference->dwelling_type));
                        } else {
                            $sub_type = '%';
                        }

                        if ($preference->bedrooms != 'None') {
                            $bedrooms = $preference->bedrooms;
                            if ($bedrooms == '0') {
                                $bed_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                    ON ap.attributes_id = a.id  WHERE a.name = 'bedrooms' AND value = 0");
                                $bedIds = $this->ObjectToString($bed_ids);
                                $bed_query = "AND property.id IN ('".$bedIds."')";
                            } else {
                                $bed_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                    ON ap.attributes_id = a.id  WHERE a.name = 'bedrooms' AND value >= '".$bedrooms."'");
                                $bedIds = $this->ObjectToString($bed_ids);
                                $bed_query = "AND property.id IN ('".$bedIds."')";
                            }
                        }
                        if ($preference->bathrooms != 'None') {
                            $bathrooms = $preference->bathrooms;
                            $bath_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                    ON ap.attributes_id = a.id  WHERE a.name = 'bathrooms' AND value >= '".$bathrooms."'");
                            $bathIds = $this->ObjectToString($bath_ids);
                            $bath_query = "AND property.id IN ('".$bathIds."')";
                        }

                        break;
                    case 'commercial':
                        if ($preference->commercial_type != 'None') {
                            $sub_type = '%'.str_replace('_', '-', str_replace('-', ' ', $preference->commercial_type));
                            if ($sub_type == '%industrial space') {
                                $sub_type = '%industrial%';
                            }
                        } else {
                            $sub_type = '%';
                        }

                        break;
                    case 'farm':
                        if ($preference->farm_type != 'None') {
                            $sub_type = '%'.str_replace('_', '-', str_replace('-', ' ', $preference->farm_type));
                        } else {
                            $sub_type = '%';
                        }

                        break;
                }
        } else {
            $property = 'AND property.property_type LIKE "%"';
            $sub_type = '%';
        }

        $floor_range = str_replace('+', '', $preference->floor_space);

        $floor_range_array = explode('-', $floor_range);

        $floor_min = $floor_range_array[0];
        $floor_max = $floor_range_array[1];

        if ($floor_max == '2000') {
            $floor_query = 'AND 1=1';
        } else {
            $floor_query_max = 'AND value < '.$floor_max;
            $floor_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                    ON ap.attributes_id = a.id  WHERE a.name = 'floor_space' AND value > $floor_min
                                    $floor_query_max GROUP BY ap.property_id");
            $floorIds = $this->ObjectToString($floor_ids);
            $floor_query = 'AND property.id IN ("'.$floorIds.'")';
        }

        $land_range = str_replace('+', '', $preference->land_size);

        $land_range_array = explode('-', $land_range);

        $land_min = $land_range_array[0];
        $land_max = $land_range_array[1];

        if ($land_max == '20000') {
            $land_query = 'AND 1=1';
        } else {
            $land_query_max = 'AND value < '.$land_max;
            $land_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                    ON ap.attributes_id = a.id  WHERE a.name = 'land_size' AND value > $land_min
                                    $land_query_max GROUP BY ap.property_id");
            $landIds = $this->ObjectToString($land_ids);
            $land_query = 'AND property.id IN ("'.$landIds.'")';
        }

        $provinceInput = $preference->province;
        if ($provinceInput != 'south-africa') {
            $province = '%'.$provinceInput;
        } else {
            $province = '%';
        }

        $city_Input = $preference->city;
        if ($city_Input != 'all-cities') {
            $city = '%'.$city_Input;
        } else {
            $city = '%';
        }

        $suburb_Input = $preference->suburb;

        if ($suburb_Input != '') {
            if ($suburb_Input[0] == '0') {
                parse_str($suburb_Input, $suburbs);
                $suburb = '(';
                foreach ($suburbs as $suburbInputMultiple) {
                    if ($suburbInputMultiple === end($suburbs)) {
                        $suburb .= 'property.suburb_town_city LIKE "%'.str_replace('-', ' ', $suburbInputMultiple).'"';
                    } else {
                        $suburb .= 'property.suburb_town_city LIKE "%'.str_replace('-', ' ', $suburbInputMultiple).'" OR ';
                    }
                }
                $suburb .= ')';
            } else {
                $suburb = '(property.suburb_town_city LIKE "%'.str_replace('-', ' ', $suburb_Input).'"
                    OR property.area LIKE "%'.str_replace('-', ' ', $suburb_Input).'")';
            }
        } else {
            $suburb = '1=1';
        }

        $properties = $this->property->
            select('*', 'property.id as actual_id')->
            join('vetting', 'vetting.property_id', '=', 'property.id')->
            leftJoin('residential', 'residential.property_id', '=', 'property.id')->
            leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
            leftJoin('farm', 'farm.property_id', '=', 'property.id')->
            leftJoin('auction', 'auction.property_id', '=', 'property.id')->
            whereRaw('(property.reference LIKE '.$reference.")
                AND CASE '".$listing."'
                        WHEN '%welet'
                        THEN (property.listing_type = 'welet'
                            OR property.listing_type = 'in2rental')
                        WHEN '%wesell'
                        THEN  (property.listing_type = 'wesell'
                            OR property.listing_type = 'in2assets'
                            OR property.listing_type = 'weauction')
                        WHEN '%weauction'
                        THEN  property.listing_type = 'weauction'
                        WHEN '%'
                        THEN  property.listing_type LIKE '%'
                        ELSE '1=1'
                        END
                AND CASE  '".$suburb."'
                        WHEN '1=1'
                        THEN (property.suburb_town_city LIKE '".$city."'
                OR property.area LIKE '".$city."')
                        ELSE ".$suburb.'
                       END
                       '.$property."
                AND property.province  LIKE '".$province."'
                AND ( vetting.vetting_status = 2 )
                AND CASE '".$property_type."'
                        WHEN 'residential'
                        THEN residential.dwelling_type LIKE '".$sub_type."' $bed_query $bath_query
                        WHEN 'commercial'
                        THEN (commercial.commercial_type LIKE '".$sub_type."' OR farm.farm_type LIKE '".$sub_type."')
                        WHEN 'farm'
                        THEN farm.farm_type LIKE '".$sub_type."'
                        ELSE '1=1'
                        END
                        $floor_query
                        $land_query
                        $query_min
                        $query_max
                        AND MONTH(vetting.updated_at) = MONTH(CURRENT_DATE())")->get();

        return $properties;
    }
}
