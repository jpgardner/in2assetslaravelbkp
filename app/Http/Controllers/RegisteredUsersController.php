<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegisteredUsersController extends Controller
{
    public function showRegisteredAgents()
    {
        return view::make('registeredusers.agents');
    }

    public function showRegisteredBrokers()
    {
        return view::make('registeredusers.brokers');
    }

    public function showRegisteredBuyers()
    {
        return view::make('registeredusers.buyers');
    }

    public function showRegisteredLandlords()
    {
        $user = Auth::user();

        if (Auth::user()->hasRole('Tenant')) {
            return view::make('registeredusers.landlords');
        } elseif (Auth::user()->hasRole('TenantPending')) {
            return view('portal.tenantsPending')->withUser($user);
        } elseif ($tenant = DB::table('tenants')->whereUser_id($user->id)->first()) {
            if ($tenant->itc_agree == 1) {
                return view('portal.tenantsrejected', ['itc' => 1])->withUser($user);
            } else {
                return view('portal.tenantsrejected', ['itc' => 0])->withUser($user);
            }
        } else {
            return view('portal.tenantsNo_dets')->withUser($user);
        }
    }

    public function showRegisteredSellers()
    {
        $user = Auth::user();
        if (Auth::user()->hasRole('Buyer')) {
            return view::make('registeredusers.sellers');
        } elseif (Auth::user()->hasRole('BuyerPending')) {
            return view('portal.buyersPending')->withUser($user);
        } else {
            return view('portal.buyersNo_dets')->withUser($user);
        }
    }

    public function showRegisteredTenants()
    {
        return view::make('registeredusers.tenants');
    }

    public function getRegisteredUsersTable($type)
    {
        $type_id = DB::table('roles')->select('id')->whereName($type)->first();

        $users = DB::table('users')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->where('role_user.role_id', '=', $type_id->id)
        ->where('users.allow_contact', '=', '1')
        ->get();

        return Datatable::collection(new Collection($users))
            ->showColumns('title', 'firstname', 'lastname')
            ->addColumn('action', function ($User) {
                return '<a href="/property/'.$User->user_id.'/message" title="Message Landlord" ><i class="i-circled i-light i-small icon-comments"></i></a>';
            })
            ->searchColumns('title', 'firstname', 'lastname')
            ->orderColumns('title', 'firstname', 'lastname')
            ->make();
    }
}
