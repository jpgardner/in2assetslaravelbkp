<?php

namespace App\Http\Controllers;

//use App\In2Assets\Core\CommandBus;
use App\helpers\AppHelper;
use App\In2Assets\Forms\uploadForm;
use App\In2Assets\Uploads\FileRepository;
use App\In2Assets\Uploads\Upload;
use App\In2Assets\Uploads\UploadFileCommand;
use App\In2Assets\Users\User;
use App\Jobs\uploadFileJob;
use App\Models\Property;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Intervention\Image\Response;
use Request;

class UploadsController extends Controller
{
    /**
     * Display a listing of the resource.
     * GET /uploads.
     *
     * @return Response
     */

//    use CommandBus;
    protected $fileRepository;

    protected $uploadForm;

    public function __construct(uploadForm $uploadForm, FileRepository $fileRepository, Property $property)
    {
        $this->uploadForm = $uploadForm;
        $this->FileRepository = $fileRepository;
        $this->property = $property;
    }

    //Temp Views for Uploads ---------------------------

    public function indexMulti($id)
    {
        $files = $this->FileRepository->getAllForUser(Auth::user(), $id);

        return view('uploads.multipleimage', compact('files'));
    }

    public function indexFile()
    {
        return view('uploads.indexfile');
    }

    public function index()
    {
        return view('uploads.index');
    }

    //--------------------------------------------------

    public function allFilesList($userId)
    {
        if (Request::ajax()) {
            $user = User::whereId($userId)->first();
            $files = $user->files()->latest()->get();

            return $files;
        }
    }

    public function propertyFilesList($userId, $propertyId)
    {
        $propertyId = $propertyId.'%';
        if (Request::ajax()) {
            $user = User::whereId($userId)->first();
            $files = $user->files()->whereClass('property')->where('link', 'LIKE', $propertyId)->where('type', '!=', 'image')->where('type', '!=', 'lead_image')->latest()->get();

            return $files;
        }
    }

    public function auctionFilesList($userId, $propertyId)
    {
        if (Request::ajax()) {
            $user = User::whereId($userId)->first();
            $files = $user->files()->whereClass('property')->where('link', '=', $propertyId)->latest()->get();

            return $files;
        }
    }

    public function indexFileList($fileId)
    {
        if (Request::ajax()) {
            $user = Auth::user();
            $files = $user->files()->where('link', '=', $fileId)->latest()->get();

            return $files;
        }
    }

    public function indexBuyerFile($userId)
    {
        if (Request::ajax()) {
            $user = User::whereId($userId)->first();
            $files = $user->files()->where('link', '=', $userId)->whereClass('buyer')->latest()->get();

            return $files;
        }
    }

    public function indexTenantFile($userId)
    {
        if (Request::ajax()) {
            $user = User::whereId($userId)->first();
            $files = $user->files()->where('link', '=', $userId)->whereClass('tenant')->latest()->get();

            return $files;
        }
    }

    // URL Sets Type & File ID
    public function storeImage($type, $fileId, $class, $listing_title = '')
    {
        $file = Request::file('file');

        $extension = File::extension($file->getClientOriginalName());
        $extension = strtolower($extension);
        if ($class == 'property') {
            $property = $this->property
                ->leftJoin('residential', 'property.id', '=', 'residential.property_id')
                ->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
                ->leftJoin('farm', 'property.id', '=', 'farm.property_id')
                ->where('property.id', '=', $fileId)
                ->first();

            $userId = $property->user_id;

            if ($listing_title == '') {
                $image_append = strtolower(str_replace(' ', '-', str_replace('<br>', '', AppHelper::propertyTypeFull($property))));
            } else {
                $image_append = strtolower(str_replace(' ', '-', str_replace('<br>', '', $listing_title)));
            }
        } elseif ($class == 'lead') {
            $userId = Auth::user()->id;
        } elseif ($class == 'blog') {
            if ($listing_title == '') {
                $image_append = 'blog-image';
            } else {
                $image_append = strtolower(str_replace(' ', '-', str_replace('<br>', '', $listing_title)));
            }
            $userId = Auth::user()->id;
        } else {
            $userId = Auth::user()->id;
        }

        if ($type == 'auction') {
            $lastImage = DB::table('uploads')->select('file', 'extension')->whereType('auction')
                ->orderBy('created_at', 'desc')->first();

            if (! empty($lastImage)) {
                $fileId = str_replace('.'.$lastImage->extension, '', str_replace('uploads/auctions/auction_', '', $lastImage->file));
                $fileId++;
            } else {
                $fileId = 1;
            }
        }

        switch ($type) {
            case 'avatar':      //Naming Scheme for User Avatar
                $directory = 'uploads/users/avatar/';
                $filename = 'userAvatar_'.Str::random(12);

                break;
            case 'auction':      //Naming Scheme for User Avatar
                $directory = 'uploads/auctions/';
                $filename = 'auction_'.$fileId;

                break;
            case 'agent_photo':      //Naming Scheme for User agent_photo
                $directory = 'uploads/users/agent_photo/';
                $filename = 'userAgent_photo_'.Str::random(12);

                break;
            case 'lead_image':  //Naming Scheme for Lead Image
                $directory = 'uploads/users/'.$userId.'/'.$fileId.'/';
                $filename = 'lead_image_'.$image_append.'_'.Str::random(12);

                break;
            case 'image':  //Naming Scheme for Multiple Image Uploads
                if ($class == 'lead') {
                    $directory = 'uploads/users/'.$userId.'/lead_'.$fileId.'/';
                    $filename = 'image_'.Str::random(12);
                } else {
                    $directory = 'uploads/users/'.$userId.'/'.$fileId.'/';
                    $filename = 'image_'.$image_append.'_'.Str::random(12);
                }

                break;
            case 'blog_lead_image':  //Naming Scheme for Multiple Image Uploads
                $directory = 'uploads/blogs/'.$fileId.'/';
                $filename = 'image_'.$image_append.'_'.Str::random(12);

                break;
            case 'classified_lead_image':  //Naming Scheme for Multiple Image Uploads
                $directory = 'uploads/classifieds/'.$fileId.'/';
                $filename = 'image_'.Str::random(12);

                break;
            case 'blog_image':  //Naming Scheme for Multiple Image Uploads
                $directory = 'uploads/blogs/'.$fileId.'/';
                $filename = 'image_'.$image_append.'_'.Str::random(12);

                break;
            case 'buyersclub_lead_image':  //Naming Scheme for Multiple Image Uploads
                $directory = 'uploads/buyersclub/'.$fileId.'/';
                $filename = 'image_'.Str::random(12);

                break;
            case 'buyersclub_image':  //Naming Scheme for Multiple Image Uploads
                $directory = 'uploads/buyersclub/'.$fileId.'/';
                $filename = 'image_'.Str::random(12);

                break;
            case 'logo_url':  //Naming Scheme for Multiple Image Uploads
                $directory = 'uploads/logo_url/'.$fileId.'/';
                $filename = 'image_'.Str::random(12);

                break;

        }

        $title = 'User Image Upload';
        $file_location = $directory.$filename.'.'.$extension;    //Full File Path
        $file_type = $type;

        $upload_success = Request::file('file')->move($directory, $filename.'.'.$extension);

        //Resize Image, Save to DB, Send JSON Response
        if ($upload_success) {
            switch ($type) {
                case 'avatar':              //Sizes for Avatar
                    Image::make($directory.$filename.'.'.$extension)->fit(200, 200)->save($directory.$filename.'.'.$extension);

                    break;
                case 'auction':              //Sizes for Avatar
                    Image::make($directory.$filename.'.'.$extension)->save($directory.$filename.'.'.$extension);

                    break;
                case 'lead_image':          //Sizes for Lead Property Image
                    Image::make($directory.$filename.'.'.$extension)->fit(265, 163)->save($directory.$filename.'_265_163.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(400, 161)->save($directory.$filename.'_400_161.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(143, 83)->save($directory.$filename.'_143_83.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(835, 467)->save($directory.$filename.'_835_467.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(1920, 600)->save($directory.$filename.'_1920_600.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->save($directory.$filename.'.'.$extension);

                    break;
                case 'image':               //Sizes for Multiple Property Image Uploads
                    Image::make($directory.$filename.'.'.$extension)->fit(143, 83)->save($directory.$filename.'_143_83.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(835, 467)->save($directory.$filename.'_835_467.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->save($directory.$filename.'.'.$extension);

                    break;
                case 'blog_lead_image':     //Sizes for Blog Uploads
                    Image::make($directory.$filename.'.'.$extension)->fit(400, 300)->save($directory.$filename.'_small.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->save($directory.$filename.'.'.$extension);

                    break;
                case 'blog_image':          //Sizes for Multiple Blog Uploads
                    Image::make($directory.$filename.'.'.$extension)->fit(200, 200)->save($directory.$filename.'_small.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(600, 600)->save($directory.$filename.'.'.$extension);

                    break;
                case 'buyersclub_lead_image':     //Sizes for Blog Uploads
                    Image::make($directory.$filename.'.'.$extension)->fit(400, 300)->save($directory.$filename.'_small.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(861, 300)->save($directory.$filename.'.'.$extension);

                    break;
                case 'buyersclub_image':          //Sizes for Multiple Blog Uploads
                    Image::make($directory.$filename.'.'.$extension)->fit(200, 200)->save($directory.$filename.'_small.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->fit(600, 600)->save($directory.$filename.'.'.$extension);

                    break;
                case 'classified_lead_image':     //Sizes for Classified Uploads
                    Image::make($directory.$filename.'.'.$extension)->fit(200, 200)->save($directory.$filename.'_small.'.$extension);
                    Image::make($directory.$filename.'.'.$extension)->resize(null, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($directory.$filename.'.'.$extension);

                    break;
                case 'logo_url':
                    Image::make($directory.$filename.'.'.$extension)->save($directory.$filename.'_nrml.'.$extension);

                    break;
            }

            if ($file_type == 'image') {
                $positionDet = DB::table('uploads')->select('position')->whereType('image')->whereLink($fileId)->orderBy('position', 'DESC')->first();

                if (! $positionDet) {
                    $position = 0;
                } else {
                    $position = $positionDet->position;
                }

                $position = $position + 1;
            } else {
                $position = null;
            }

            $job = new uploadFileJob($title, $file_location, $file_type, $fileId, $extension, $userId, $class, $position);

            $this->dispatch($job);
//            $this->execute(
//                new UploadFileCommand($title, $file_location, $file_type, $fileId, $extension, $userId, $class, $position)
//            );

            $file = Upload::where('file', '=', $file_location)->firstOrFail();

            if ($type == 'lead_image' || $type == 'blog_lead_image' || $type == 'classified_lead_image' ||
                $type == 'buyersclub_lead_image' || $type == 'logo_url' || $type == 'auction'
            ) {
                return Response::json($file->file);
            } else {
                return Response::json($file->id);
            }
        } else {
            return Response::json('error', 400);
        }
    }

    public function deleteImage($id)
    {
        $file = Upload::find($id);

        $filePos = $file->position;
        $fileLink = $file->link;

        if ($file->class == 'lead') {
            $imageUploads = DB::table('uploads')->whereType('image')->where('type', '=', 'lead')->whereLink($fileLink)->get();
        } else {
            $imageUploads = DB::table('uploads')->whereType('image')->whereLink($fileLink)->get();
        }

        if ($file->user_id == Auth::user()->id || Auth::user()->hasRole('Admin')) {
            $delete_success = $file->delete();
            $filename = $file->file;

            if (File::exists(str_replace('.jpg', '_143_83.jpg', $filename))) {
                File::delete(str_replace('.jpg', '_143_83.jpg', $filename));
            }
            if (File::exists(str_replace('.jpg', '_835_467.jpg', $filename))) {
                File::delete(str_replace('.jpg', '_835_467.jpg', $filename));
            }
            if (File::exists($filename)) {
                File::delete($filename);
                $path = $_SERVER['DOCUMENT_ROOT'].$filename;
                unlink('image_lhPR4uGLXjVc.jpg');
                dd('Hi');
            }
            if ($delete_success) {
                foreach ($imageUploads as $imageUpload) {
                    if ($imageUpload->position > $filePos) {
                        $newPosition = $imageUpload->position - 1;
                        DB::table('uploads')->whereId($imageUpload->id)->update(['position' => $newPosition]);
                    }
                }

                return Response::json($filename, 200);
            } else {
                return Response::json('error', 400);
            }
        } else {
            return Response::json('error', 400);
        }
    }

    public function storeFile($fileId, $class)
    {
        if ($class == 'cv') {
            $userId = 1;
        } elseif (Auth::user()->hasRole('Admin')) {
            $userId = 1;
        } else {
            $userId = Auth::user()->id;
        }
        $file = Request::file('file');

        $extension = File::extension($file->getClientOriginalName());
        $extension = strtolower($extension);

        $this->uploadForm->validate(Request::all());

        $title = Request::get('title');
        $file_type = Request::get('type');

        switch ($class) {
            case 'cv':
                $directory = 'uploads/careers/'.$fileId.'/';
                $filename = $title.'.'.$extension;

                break;
            case 'buyer':
                $directory = 'uploads/buyer/'.$fileId.'/';
                $filename = $title.'.'.$extension;

                break;
            case 'tenant':
                $directory = 'uploads/tenant/'.$fileId.'/';
                $filename = $title.'.'.$extension;

                break;
            default:
                if ($file_type == 'property_info_pack') {
                    $directory = 'brochure/';
                    $property = Property::find($fileId);
                    $filename = $property->street_number.'-'.str_replace(' ', '-', $property->street_address).'.'.$extension;
                } else {
                    $directory = 'uploads/users/'.$userId.'/'.$fileId.'/';
                    $filename = $title.'.'.$extension;
                }

                break;
        }

        $upload_success = Request::file('file')->move($directory, $filename);
        $file_location = $directory.$filename;

        if ($file_type == 'rules_of_auction_conditions_of_sale') {
            $rule = DB::table('auction_rule_change')->whereProperty_id($fileId)->first();
            if ($rule == null) {
                DB::table('auction_rule_change')->insert(['property_id' => $fileId, 'version_date' => date('Y/m/d'), 'comment' => 'First Version']);
            }
        }

        if ($upload_success) {
            if ($class === 'cv') {
                return Response::json($file_location, 200);
            } else {
                $this->execute(
                    new UploadFileCommand(Request::get('title'), $file_location, $file_type, $fileId, $extension, $userId, $class)
                );
            }

            return Response::json($file_type, 200);
        } else {
            return Response::json('error', 400);
        }
    }

    public function removeFile()
    {
        $new_rules = Request::get('new_rules');

        $item_type = Request::get('item_type');
        $propertyId = Request::get('propertyId');
        $created_at = Request::get('created_at');

//        $filelocation = DB::table('uploads')->select('file')->whereType($item_type)->whereLink($propertyId)->whereCreated_at($created_at)->first();

        if ($new_rules) {
            DB::table('auction_rule_change')->insert(['property_id' => $propertyId, 'version_date' => $created_at]);
        }

        DB::table('uploads')->whereType($item_type)->whereLink($propertyId)->whereCreated_at($created_at)->delete();
    }

    public function storeAvatar()  //Separate Logged Out function for Avatar Image.
    {
        $file = Request::file('file');
        $extension = File::extension($file->getClientOriginalName());
        $extension = strtolower($extension);

        $directory = 'uploads/users/avatar/';
        $filename = 'userAvatar_'.Str::random(12).'.'.$extension;

        $title = 'Avatar';
        $file_location = $directory.$filename;    //Full File Path
        $upload_success = Request::file('file')->move($directory, $filename);
        //Resize Image, Send JSON Response
        if ($upload_success) {
            Image::make($directory.$filename)->fit(200, 200)->save($directory.'200x200_'.$filename);

            return Response::json($directory.'200x200_'.$filename, 200);
        } else {
            return Response::json('error', 400);
        }
    }

    public function storeAgentPhoto()  //Separate Logged Out function for Avatar Image.
    {
        $file = Request::file('file');
        $extension = File::extension($file->getClientOriginalName());
        $extension = strtolower($extension);

        $directory = 'uploads/users/agent_photo/';
        $filename = 'userAgent_photo_'.Str::random(12).'.'.$extension;

        $title = 'AgentPhoto';
        $file_location = $directory.$filename;    //Full File Path
        $upload_success = Request::file('file')->move($directory, $filename);
        //Resize Image, Send JSON Response
        if ($upload_success) {
            Image::make($directory.$filename)->save($directory.'200x270_'.$filename);

            return Response::json($directory.'200x270_'.$filename, 200);
        } else {
            return Response::json('error', 400);
        }
    }

    public function update_All_positions()
    {
        $x = 1;
        while ($x <= 150) {
            $imageUploads = DB::table('uploads')->whereType('image')->whereLink($x)->get();
            $y = 1;
            foreach ($imageUploads as $imageUpload) {
                DB::table('uploads')->whereId($imageUpload->id)->update(['position' => $y]);
                $y++;
            }
            $x++;
        }

        return 'Done';
    }

    public function changeImageOrder($id, $start, $new)
    {
        DB::table('uploads')->whereType('image')->whereLink($id)->wherePosition($start)->update(['position' => '0']);
        $imageUploads = DB::table('uploads')->whereType('image')->whereLink($id)->get();

        if ($start > $new) {
            $y = $new;
            while ($y < $start) {
                foreach ($imageUploads as $imageUpload) {
                    if ($imageUpload->position == $y) {
                        $y++;
                        DB::table('uploads')->whereId($imageUpload->id)->update(['position' => $y]);
                    }
                }
            }
        }

        if ($start < $new) {
            $y = $new;
            while ($y > $start) {
                foreach ($imageUploads as $imageUpload) {
                    if ($imageUpload->position == $y) {
                        $y--;
                        DB::table('uploads')->whereId($imageUpload->id)->update(['position' => $y]);
                    }
                }
            }
        }

        DB::table('uploads')->whereType('image')->whereLink($id)->wherePosition('0')->update(['position' => $new]);

        return 'Done';
    }

    public function ckEditor()
    {
        if (file_exists('uploads/highlight/'.$_FILES['upload']['name'])) {
            echo $_FILES['upload']['name'].' already exists. ';
        } else {
            move_uploaded_file($_FILES['upload']['tmp_name'],
                'uploads/highlight/'.$_FILES['upload']['name']);
            echo 'Stored in: '.'http://'.$_SERVER['SERVER_NAME'].'/uploads/highlight/'.$_FILES['upload']['name'];
        }
    }
}
