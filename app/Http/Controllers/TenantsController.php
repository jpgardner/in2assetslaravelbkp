<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class TenantsController extends Controller
{
    protected $tenants;

    public function __construct(Tenants $tenants)
    {
        $this->tenants = $tenants;
    }

    /**
     * Display a listing of the resource.
     * GET /tenant.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /tenant/create.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view('tenants.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /tenant.
     *
     * @return Response
     */
    public function store()
    {
        //

        $input = Request::all();
        $input['user_id'] = Auth::user()->id;

        if ($input['itc_agree'] == '') {
            $input['itc_agree'] = 0;
            $this->tenants->fill($input)->save();

            return Redirect::route('portalTenants_path');
        }

        if (! $this->tenants->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->tenants->errors);
        }

        $this->tenants->save();

        if (Auth::user()->hasRole('TenantPending')) {
        } else {
            Auth::user()->assignRole(10);
        }

        return Redirect::route('portalTenants_path');
    }

    /**
     * Display the specified resource.
     * GET /tenant/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /tenant/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //

        $tenant = $this->tenants->whereUser_id($id)->first();

        return view('tenants.edit', ['tenant' => $tenant]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /tenant/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $tenant = $this->tenants->whereId($id)->first();

        $input = Request::all();
        $input['user_id'] = Auth::user()->id;

        if ($input['itc_agree'] == '') {
            $input['itc_agree'] = 0;
            $tenant->fill($input)->update();

            return Redirect::route('portalTenants_path');
        }

        if (! $tenant->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->tenants->errors);
        }

        $tenant->update();

        if (Auth::user()->hasRole('TenantPending')) {
        } else {
            Auth::user()->assignRole(10);
        }

        return Redirect::route('portalTenants_path');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /tenant/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function adminTenants($role)
    {
        return view('tenants.admin.tenants', ['role' => $role]);
    }

    public function getAdminTenantsTable($role)
    {
        if ($role === 'all') {
            $users = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 7)->orWhere('role_user.role_id', '=', 10)->get();
        // $queries = DB::getQueryLog();
            // $query = end($queries);
            // dd($query);
        } elseif ($role === 'tenant') {
            $users = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 7)->get();
        } elseif ($role === 'tenantpending') {
            $users = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_user.role_id', '=', 10)->get();
        }

        return Datatable::collection(new Collection($users))
            ->showColumns('firstname', 'lastname', 'suburb_town_city')
            ->addColumn('role', function ($User) {
                if ($User->role_id == 7) {
                    return 'Tenant';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('action', function ($User) {
                if ($User->role_id == 10) {
                    return '<a href="/admin/users/edit/'.$User->user_id.'/" title="View User" ><i class="i-circled i-light i-alt i-small icon-user4"></i></a>
				<a href="/admin/tenants/show/'.$User->user_id.'" title="Preview Tenant" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
				<a href="/admin/messages/'.$User->user_id.'/tenants" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>';
                } else {
                    return '<a href="/admin/users/edit/'.$User->user_id.'/" title="View User" ><i class="i-circled i-light i-alt i-small icon-user4"></i></a>
				<a href="/admin/messages/'.$User->user_id.'/tenants" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>';
                }
            })
            ->searchColumns('firstname', 'lastname', 'suburb_town_city', 'role')
            ->orderColumns('firstname', 'lastname', 'suburb_town_city', 'role')
            ->make();
    }

    public function adminTenantsShow($id)
    {
        $tenant = $this->tenants->whereUser_id($id)->join('users', 'users.id', '=', 'tenants.user_id')->first();

        return view('tenants.admin.show', ['tenant' => $tenant]);
    }

    public function adminTenantsStatus($status, $id)
    {
        if ($status == 'approve') {
            DB::table('role_user')->whereUser_id($id)->whereRole_id(10)->update(['role_id' => 7]);
        } elseif ($status === 'reject') {
            DB::table('role_user')->whereUser_id($id)->whereRole_id(10)->delete();
        }

        return Redirect::route('admin.tenants', ['role' => 'all']);
    }
}
