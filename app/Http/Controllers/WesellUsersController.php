<?php

namespace App\Http\Controllers;

use App\Models\User;
use Artdarek\OAuth\Facade\OAuth;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use In2Assets\Forms\EditForm;
use In2Assets\Users\User;
use In2Assets\Users\UserRepository;
use Laracasts\Flash\Flash;



class UsersController extends Controller {

    Private $editForm;
    protected $userRepository;

    function __construct(UserRepository $userRepository, EditForm $editForm, Property $property, AuctionRegister $auction_register, 
        PhysicalAuctionRegister $physical_auction_register, Auction $auction)
    {
        $this->userRepository = $userRepository;
        $this->editForm = $editForm;
        $this->auction = $auction;
        $this->property = $property;
        $this->physical_auction_register = $physical_auction_register;
        $this->auction_register = $auction_register;
        //$this->beforeFilter('currentUser', ['only' => ['edit', 'update']]);
        $this->beforeFilter('auth');

    }

    public function index()
    {
      $users = $this->userRepository->getPaginated();
      return view('users.index')->withUsers($users);
  }

  public function show($id)
  {
    $user = $this->userRepository->findById($id);
    return view('users.show')->with('user', $user);
}

public function adminList($role)

{
    return view('users.adminlist')->with('role', $role);
}

public function getDatatable($role)
{

    if ($role == 'admin') {
        $userList = User::whereHas('roles', function($q)
        {
            $q->where('name', 'Admin');
        })->get();
    }
    elseif ($role == 'tenant') {
        $userList = User::whereHas('roles', function($q)
        {
            $q->where('name', 'Tenant');
        })->get();
    }
    elseif ($role == 'all') {
        $userList = User::whereHas('roles', function($q)
        {
            $q->where('name', 'Admin')
            ->orWhere('name', 'Tenant')
            ->orWhere('name', 'Buyer')
            ->orWhere('name', 'Landlord')
            ->orWhere('name', 'Seller')
            ->orWhere('name', 'User')
            ->orWhere('name', 'Agent');
        })->get();
    }
    elseif ($role == 'buyer') {
        $userList = User::whereHas('roles', function($q)
        {
            $q->where('name', 'Buyer');
        })->get();
    }
    elseif ($role == 'landlord') {
        $userList = User::whereHas('roles', function($q)
        {
            $q->where('name', 'Landlord');
        })->get();
    }
    elseif ($role == 'seller') {
        $userList = User::whereHas('roles', function($q)
        {
            $q->where('name', 'Seller');
        })->get();
    }
    elseif ($role == 'agent') {
        $userList = User::whereHas('roles', function($q)
        {
            $q->where('name', 'Agent');
        })->get();
    }
        // elseif ($role == 'broker') {
        //     $userList = User::whereHas('roles', function($q){$q->where('name', 'Broker');})->get();
        // }  else {
        //     $userList = User::whereHas('roles', function($q){$q->where('name', 'Admin');})->get();
        // }

    return Datatable::collection($userList)
    ->showColumns('firstname', 'lastname', 'email', 'province', 'created_at')
    ->addColumn('action', function($userList)
    {
        if($userList->firstname == 'Admin')
        {
            return 'No Actions';
        }
        else
        {
            return '<a href="/admin/users/edit/' . $userList->id . '" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a><a href="/admin/messages/' . $userList->id . '/0/general/create/" title="Message" ><i class="i-circled i-light i-small icon-comments"></i></a>';
        }
    })
    ->searchColumns('firstname', 'lastname', 'email', 'province', 'created_at')
    ->orderColumns('firstname', 'lastname', 'email', 'province', 'created_at')
    ->make();
}

public function getUserProperties($user_id)
{

    $properties = $this->property->join('vetting', 'vetting.property_id', '=', 'property.id')
        ->select('property.reference', 'property.street_address', 'vetting.vetting_status', 'property.updated_at')
        ->whereUser_id($user_id)->
        ->whereProperty_type('Residential')
        ->get();


    return Datatable::collection($properties)
    ->showColumns('reference', 'street_address')
    ->addColumn('vetting_status', function($Property){
        switch ($Property->vetting_status)
        {
            case 1:
            return "Pending";
            break;
            case 2:
            return "Approved";
            break;
            case 3:
            return "Vetting";
            break;
            case 4:
            return "Sold < 60 Days";
            break;
            case 5:
            return "Rented < 14 Days";
            break;
            case 6:
            return "Sold > 60 Days";
            break;
            case 7:
            return "Rented > 14 Days";
            break;
            case 8:
            return "Rejected";
            break;
            case 9:
            return "Auction";
            break;
            default:
            return "No Status";
            break;
        }
    })
    ->showColumns('updated_at')
    ->addColumn('action', function($Property)
    {
            // return '<a href="/admin/users/edit/' . $userList->id . '" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a><a href="/admin/messages/' . $userList->id . '/0/general/create/" title="Message" ><i class="i-circled i-light i-small icon-comments"></i></a>';
        return '';
    })
    ->searchColumns('reference', 'street_address', 'vetting_status', 'updated_at')
    ->orderColumns('reference', 'street_address', 'vetting_status', 'updated_at')
    ->make();
}

public function edit()
{
    $user = Auth::user();
    $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

    foreach($citiesDB as $city ){$cities[] = $city->suburb_town_city. ', ' . $city->area . ', ' . $city->province;}
    $cities = implode('", "',$cities);

    return view('users.edit', ['user' => $user, 'listing' => 'All','cities' => $cities]);

}

public function adminEdit($id)
{
    $user = $this->userRepository->findById($id);
    $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

    foreach($citiesDB as $city ){$cities[] = $city->suburb_town_city. ', ' . $city->area . ', ' . $city->province;}
    $cities = implode('", "',$cities);

    $roles = DB::select( DB::raw('select role_id, roles.name from role_user left join roles on role_user.role_id=roles.id where user_id = ' . $id) );
    $allroles = DB::select( DB::raw('select id, name from roles') );

    $physical_register = $this->physical_auction_register->join('physical_auctions', 'physical_auctions.id', '=', 
        'physical_auction_register.auction_id')->whereUser_id($user->id)->orderBy('physical_auctions.id')->get();

    foreach($physical_register as $register)
    {
       $first_interest[] = $this->auction->leftJoin('property', 'auction.property_id', '=', 'property.id')->select('property.id', 'property.reference')
       ->where('auction.id', '=', $register->property_auction_id)->first();   
   }   

   $count = 0;
   foreach($physical_register as $register)
   {
    $register->property_id = $first_interest[$count]['id'];
    $register->reference = $first_interest[$count]['reference'];
    $count++;
}

$bid_to_buys = $this->auction_register->join('auction', 'auction.id', '=', 'auction_register.auction_id')
->join('property', 'property.id', '=', 'auction.property_id')->select('auction_register.*', 'property.reference', 
    'property.id', 'auction.start_date', 'auction.end_date')->where('auction_register.user_id', '=', $user->id)->orderBy('property.id')->get();


return view('users.adminedit', ['user' => $user, 'listing' => 'All', 'cities' => $cities, 'roles' => $roles, 
    'allroles' => $allroles, 'physical_register' => $physical_register, 'bid_to_buys' => $bid_to_buys]);

}

public function update($id)
{
    $input = Request::all();

    if(Request::get('allow_contact')){
        $input['allow_contact'] = 1;
    }
    else
    {
        $input['allow_contact'] = 0;
    }

    if(Request::get('Suburb') == '')
    {
        $input['province'] = null;
        $input['suburb_town_city'] = null;
        $input['area'] = null;
    }
    else
    {
        $areaArray = explode(',', Request::get('Suburb'));
        $input['province'] = $areaArray[0];
        $input['area'] = $areaArray[1];
        $input['suburb_town_city'] = $areaArray[2];
    }

    $this->editForm->validate($input);

    $user = $this->userRepository->findById($id);
    $user->update($input);
    Flash::message('Your user profile has been updated');
    return Redirect::back();
}

public function adminUpdate($id)
{
    $input = Request::all();
    if(Request::get('allow_contact')){
        $input['allow_contact'] = 1;
    }
    else
    {
        $input['allow_contact'] = 0;
    }

    $this->editForm->validate($input);

    $user = $this->userRepository->findById($id);

    $user->update($input);
    Flash::message('User Updated');
    return Redirect::route('users.show', $user->id);
}

public function addRole($userid, $role)

{
    $user = $this->userRepository->findById($userid);
    $user->roles()->attach($role);
    Flash::message('Role has been added!');
}

public function removeRole($userid, $role)

{
    $user = $this->userRepository->findById($userid);
    $user->roles()->detach($role);
    Flash::message('Role has been Removed!');
}

public function authFacebook()
{
    $user = Auth::user();

    try {
            // get data from input
        $code = Request::get( 'code' );
            // get fb service
        $fb = OAuth::consumer( 'Facebook' );
            // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            if(!isset($_SESSION['token'])){
                    // This was a callback request from facebook, get the token
                $token = $fb -> requestAccessToken($code);
                $_SESSION['token'] = $token->getAccessToken();
            }
                // Send a request with it
            $result = json_decode( $fb->request( '/me' ), true );

                //Find user with matching socialid
            $socialid = User::wheresocialid($result['id'])->first();


            if (is_null($socialid)) {
                $user->socialid = $result['id'];
                $user->save();
                Flash::message('You can now login with Facebook.');
                return redirect('/users/edit');
            } else {
                Flash::message('You have already authorised facebook login.');
                return redirect('/users/edit');
            }

        }
            // if not ask for permission first
        else {
                // get fb authorization
            $url = $fb->getAuthorizationUri();
                // return to facebook login url
            return redirect( (string)$url );
        }

    } catch (Exception $e) {
        Flash::message('There was an error communicating with Facebook');
        return redirect('/users/edit');
    }
}

public function setup()
{
    $user = Auth::user();
    $provinces = DB::table('areas')->groupBy('province')->get();
    return view('users.setup',['provinces' => $provinces, 'user' => $user]);
}



}
