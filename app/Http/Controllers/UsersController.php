<?php

namespace App\Http\Controllers;

use App\In2Assets\Forms\EditForm;
use App\In2Assets\Mailers\UserMailer;
use App\In2Assets\Users\User;
use App\In2Assets\Users\UserRepository;
use App\Models\Auction;
use App\Models\AuctionRegister;
use App\Models\PhysicalAuctionRegister;
use App\Models\Preferences;
use App\Models\Property;
use App\Models\Tag;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    private $editForm;

    protected $userRepository;

    public function __construct(UserRepository $userRepository, EditForm $editForm, Property $property, AuctionRegister $auction_register,
                         PhysicalAuctionRegister $physical_auction_register, Auction $auction, Preferences $preferences,
                         User $user, Tag $tag, UserMailer $mailer)
    {
        $this->mailer = $mailer;
        $this->userRepository = $userRepository;
        $this->editForm = $editForm;
        $this->auction = $auction;
        $this->property = $property;
        $this->physical_auction_register = $physical_auction_register;
        $this->auction_register = $auction_register;
        $this->preferences = $preferences;
        $this->users = $user;
        $this->tag = $tag;
//        $this->beforeFilter('currentUser', ['only' => ['edit', 'update']]);
        $this->middleware('auth');
    }

    public function index()
    {
        $users = $this->userRepository->getPaginated();

        return view('users.index')->withUsers($users);
    }

    public function show($id)
    {
        $user = $this->userRepository->findById($id);

        return view('users.show')->with('user', $user);
    }

    public function adminList($role)
    {
        return view('users.adminlist')->with('role', $role);
    }

    public function addUser()
    {
        return view('users.add');
    }

    public function getSavedSearches($user_id)
    {
        $preferences = $this->preferences->whereUser_id($user_id)->get();

        $slug = new Slugify();

        foreach ($preferences as $preference) {
            if ($preference['listing_type'] == 'to-let') {
                $preference['price1'] = '0-15000000+';
                $preference['price2'] = str_replace('R', '', str_replace(' ', '', $preference['price']));
            } else {
                $preference['price1'] = str_replace('R', '', str_replace(' ', '', $preference['price']));
                $preference['price2'] = '0-30000+';
            }

            $preference['province'] = $slug->slugify($preference['province']);
            $preference['area'] = $slug->slugify($preference['area']);
            $preference['suburb_town_city'] = $slug->slugify($preference['suburb_town_city']);

            switch ($preference->listing_type) {
                case 'welet':
                    $listing_type = ' properties to rent';

                    break;
                case 'weauction':
                    $listing_type = ' properties on auction';

                    break;
                case 'wesell':
                    $listing_type = ' properties on sale';

                    break;
                default:
                    $listing_type = '';

                    break;
            }
            if ($preference->suburb_town_city == 'all-suburbs') {
                if ($preference->area == 'all-cities') {
                    if ($preference->province == 'south-africa') {
                        $place = ' in South Africa';
                    } else {
                        $place = ' in '.ucfirst($preference->province);
                    }
                } else {
                    $place = ' in '.ucfirst($preference->area);
                }
            } else {
                $place = ' in '.ucfirst($preference->suburb_town_city);
            }
            $description = ucfirst($preference->property_type).$listing_type.$place;
            $preference->title = $description;

            switch ($preference->property_type) {
                    case 'residential':
                        if ($preference['dwelling_type'] == 'None') {
                            $sub_type = '';
                        } else {
                            $sub_type = '&st='.$preference['dwelling_type'];
                        }
                        if ($preference['bedrooms'] == 'None') {
                            $bedrooms = '';
                        } else {
                            $bedrooms = '&b1='.$preference['bedrooms'];
                        }
                        if ($preference['bathrooms'] == 'None') {
                            $bathrooms = '';
                        } else {
                            $bathrooms = '&b2='.$preference['bathrooms'];
                        }
                        $append = $sub_type.$bedrooms.$bathrooms;

                        break;
                    case 'commercial':
                        if ($preference['commercial_type'] == 'None') {
                            $sub_type = '';
                        } else {
                            $sub_type = '&st='.$preference['commercial_type'];
                        }
                        $append = $sub_type;

                        break;
                    case 'farm':
                        if ($preference['farm_type'] == 'None') {
                            $sub_type = '';
                        } else {
                            $sub_type = '&st='.$preference['farm_type'];
                        }
                        $append = $sub_type;

                        break;
                    default:
                        $append = '';

                        break;
                }
            switch ($preference->listing_type) {
                    case 'wesell':
                        $listing_type = 'for-sale';

                        break;
                    case 'welet':
                        if ($preference->property_type == 'residential') {
                            $listing_type = 'to-rent';
                        } else {
                            $listing_type = 'to-let';
                        }

                        break;
                    case 'weauction':
                        $listing_type = 'on-auction';

                        break;
                    default:
                        $listing_type = 'all-listings';

                        break;
                }

            if ($preference['price1'] == '0-15000000+') {
                $price1 = '';
            } else {
                $price1 = '&pr='.$preference['price1'];
            }

            if ($preference['price2'] == '0-30000+') {
                $price2 = '';
            } else {
                $price2 = '&rpr='.$preference['price2'];
            }

            if (str_replace(' ', '', $preference['floor_space']) == '0-2000+') {
                $floor_space = '';
            } else {
                $floor_space = '&fr='.str_replace(' ', '', $preference['floor_space']);
            }

            if (str_replace(' ', '', $preference['land_size']) == '0-20000+') {
                $land_size = '';
            } else {
                $land_size = '&lr='.str_replace(' ', '', $preference['land_size']);
            }

            $preference->actions = '<a href="/'.$preference['property_type'].'/property/'.$listing_type.'/'.
                $preference['province'].'/'.$preference['area'].'/'.$preference['suburb_town_city'].
                '?o=default&v=grid_view'.$price1.$price2.$floor_space.$land_size.$append.'
                " title="View Results" >
                <i class="i-circled i-light i-small fa fa-laptop"></i></a>
                <a href="/property-search/edit/'.$preference['id'].'" title="Edit Property Search" >
                <i class="i-circled i-light i-small fa fa-edit"></i></a>
                <a href="/property-search/delete/'.$preference['id'].'" title="Delete Property Search" >
                <i class="i-circled i-light i-small fa fa-times" style="background-color: #f04f23"></i></a>';
        }

        return Datatables::collection($preferences)
            ->rawColumns(['title', 'actions'])
            ->make(true);
    }

    public function getDatatable($role)
    {
        if ($role == 'admin') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'Admin');
            })->get();
        } elseif ($role == 'tenant') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'Tenant');
            })->get();
        } elseif ($role == 'all') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'Admin')
                    ->orWhere('name', 'Tenant')
                    ->orWhere('name', 'Buyer')
                    ->orWhere('name', 'Landlord')
                    ->orWhere('name', 'Seller')
                    ->orWhere('name', 'User')
                    ->orWhere('name', 'Agent')
                    ->orWhere('name', 'AuctionBidder');
            })->get();
        } elseif ($role == 'buyer') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'Buyer');
            })->get();
        } elseif ($role == 'landlord') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'Landlord');
            })->get();
        } elseif ($role == 'seller') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'Seller');
            })->get();
        } elseif ($role == 'agent') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->get();
        } elseif ($role == 'auctionbidder') {
            $userList = User::whereHas('roles', function ($q) {
                $q->where('name', 'AuctionBidder');
            })->get();
        }
        // elseif ($role == 'broker') {
        //     $userList = User::whereHas('roles', function($q){$q->where('name', 'Broker');})->get();
        // }  else {
        //     $userList = User::whereHas('roles', function($q){$q->where('name', 'Admin');})->get();
        // }

        foreach ($userList as $user) {
            if ($user->firstname == 'Admin') {
                $user->actions = 'No Actions';
            } else {
                $user->actions = '<a href="/admin/users/edit/'.$user->id.'" title="View User" >
            <i class="i-circled i-light i-small icon-laptop"></i>
            </a>
            <a href="/admin/messages/'.$user->id.'/0/general/create/" title="Message" >
            <i class="i-circled i-light i-small icon-comments"></i>
            </a>';
            }
        }

        return Datatables::collection($userList)
            ->rawColumns(['actions'])
            ->make(true);
//        return Datatable::collection($userList)
//            ->showColumns('firstname', 'lastname', 'email', 'province', 'created_at')
//            ->addColumn('action', function ($userList) {
//                if ($userList->firstname == 'Admin') {
//                    return 'No Actions';
//                } else {
//                    return '<a href="/admin/users/edit/' . $userList->id . '" title="View User" >
//            <i class="i-circled i-light i-small icon-laptop"></i>
//            </a>
//            <a href="/admin/messages/' . $userList->id . '/0/general/create/" title="Message" >
//            <i class="i-circled i-light i-small icon-comments"></i>
//            </a>';
//                }
//            })
//            ->searchColumns('firstname', 'lastname', 'email', 'province', 'created_at')
//            ->orderColumns('firstname', 'lastname', 'email', 'province', 'created_at')
//            ->make();
    }

    public function getUserProperties($user_id)
    {
        $roles = DB::select('select role_id, roles.name from role_user left join roles on role_user.role_id=roles.id where user_id = '.$user_id);
        $test = 0;
        foreach ($roles as $role) {
            if ($role->name == 'Agent') {
                $test++;
            }
        }
        if ($test >= 0) {
            $properties = $this->property->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->select('property.reference', 'property.street_address', 'vetting.vetting_status', 'property.updated_at')
                ->whereAgent_id($user_id)->get();
        } else {
            $properties = $this->property->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->select('property.reference', 'property.street_address', 'vetting.vetting_status', 'property.updated_at')
                ->whereUser_id($user_id)->get();
        }

        foreach ($properties as $property) {
            switch ($property->vetting_status) {
                case 1:
                    $property->vetting_status = 'Pending';

                    break;
                case 2:
                    $property->vetting_status = 'Approved';

                    break;
                case 3:
                    $property->vetting_status = 'Vetting';

                    break;
                case 4:
                    $property->vetting_status = 'Sold < 60 Days';

                    break;
                case 5:
                    $property->vetting_status = 'Rented < 14 Days';

                    break;
                case 6:
                    $property->vetting_status = 'Sold > 60 Days';

                    break;
                case 7:
                    $property->vetting_status = 'Rented > 14 Days';

                    break;
                case 8:
                    $property->vetting_status = 'Rejected';

                    break;
                case 9:
                    $property->vetting_status = 'Auction';

                    break;
                default:
                    $property->vetting_status = 'No Status';

                    break;
            }
        }

        return Datatables::collection($properties)
            ->rawColumns(['vetting_status'])
            ->make(true);
    }

    public function edit()
    {
        $user = Auth::user();
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        return view('users.edit', ['user' => $user, 'listing' => 'All', 'cities' => $cities]);
    }

    public function decryptPassword($encrypted_pass)
    {
        $key = 'djPlsjiDCN6soeif2JSGb';

        $pass_array = explode(':', $encrypted_pass);

        $salt = $pass_array[0];
        $iv = $pass_array[1];
        $pass_encrypt = $pass_array[2];

        $decrypted_pass = openssl_decrypt(
            "$pass_encrypt", 'aes-256-cbc', "$salt:$key", null, $iv
        );

        if ($decrypted_pass === false) {
            dd("Unable to decrypt message! (check password) \n");
        }

        return $decrypted_pass;
    }

    public function searchUser()
    {

//        $property_users = DB::table("property_user")->get();
//        foreach ($property_users AS $property_user) {
//            try {
//                $this->addTag($property_user->user_id, $property_user->role_id);
//            } catch (exception $e) {
//                de($property_user->role_id);
//            }
//        }
//        dd("Hello");
        $provinces = DB::table('areas')->groupBy('province')->get();

        $tagDB = DB::table('tags')->get();
        foreach ($tagDB as $tag) {
            $tags[] = $tag->name;
        }
        $tags = implode('", "', $tags);

        return view('users.search', ['provinces' => $provinces, 'firstname' => '', 'lastname' => '', 'cellnumber' => '',
            'email' => '', 'prov_area_sub' => '', 'listing_type' => '', 'property_type' => '', 'dwelling_type' => '', 'bedrooms' => '',
            'bathrooms' => '', 'commercial_type' => '', 'farm_type' => '', 'price' => '', 'land_size' => '',
            'floor_space' => '', 'current_tags' => [], 'tags' => $tags, 'province_array' => '',
            'area_array' => '', 'suburb_array' => '', 'count' => 0, ]);
    }

    public function searchUserResult()
    {
        $input = Request::all();
        unset($input['_token']);

        return view('users.userprefresult', ['input' => $input]);
    }

    public function getUserbyPrefList($input)
    {

        //gets all inputs
        $input = json_decode($input);

        //checks for user by name lastname cellnumber and email first
        if ($input->firstname != '') {
            $firstname = '%'.$input->firstname.'%';
        } else {
            $firstname = '%';
        }
        if ($input->lastname != '') {
            $lastname = '%'.$input->lastname.'%';
        } else {
            $lastname = '%';
        }
        if ($input->cellnumber != '') {
            $cellnumber = '%'.$input->cellnumber.'%';
        } else {
            $cellnumber = '%';
        }
        if ($input->email != '') {
            $email = '%'.$input->email.'%';
        } else {
            $email = '%';
        }

        //split tags
        $current_tags = explode(',', $input->current_tags);

        //creates a user list of all users that match above criteria
        if ($current_tags[0] != '') {
            $loopCount = 1;
            foreach ($current_tags as $tag) {
                $tag_dets = DB::table('tags')->whereName($tag)->first();
                if (is_object($tag_dets)) {
                    if ($loopCount == 1) {
                        $userList = $this->users->select('users.id')->
                        join('tag_user', 'tag_user.user_id', '=', 'users.id')->
                        where('firstname', 'LIKE', $firstname)->
                        where('lastname', 'LIKE', $lastname)->
                        where('cellnumber', 'LIKE', $cellnumber)->
                        where('email', 'LIKE', $email)->
                        where('tag_user.tag_id', '=', $tag_dets->id)->
                        get();
                        $loopCount++;
                    } else {
                        $userIds = [];
                        foreach ($userList as $list) {
                            $userIds[] = $list->id;
                        }
                        $userList = $this->users->select('users.id')->
                        join('tag_user', 'tag_user.user_id', '=', 'users.id')->
                        where('firstname', 'LIKE', $firstname)->
                        where('lastname', 'LIKE', $lastname)->
                        where('cellnumber', 'LIKE', $cellnumber)->
                        where('email', 'LIKE', $email)->
                        where('tag_user.tag_id', '=', $tag_dets->id)->
                        whereIn('users.id', $userIds)->
                        get();
                    }
                }
            }
        } else {
            $userList = $this->users->select('id')->
            where('firstname', 'LIKE', $firstname)->
            where('lastname', 'LIKE', $lastname)->
            where('cellnumber', 'LIKE', $cellnumber)->
            where('email', 'LIKE', $email)->
            get();
        }

        //gets all provinces
        $province_array = explode(',', $input->province_array);
        if (empty($province_array[0])) {
            $province_array = [];
        }
        //gets all areas
        $area_array = explode(',', $input->area_array);
        $suburb_array = explode(',', $input->suburb_array);
        $count = count($province_array);

        $count--;

        //if there is a array of provinces it searches thru each
        $multipleAreaSearchArray = [];
        while ($count >= 0) {
            if ($area_array[$count] != '' && $suburb_array[$count] != '') {
                $multipleAreaSearchArray[$count] = $province_array[$count].','.$area_array[$count].','.$suburb_array[$count];
            } elseif ($area_array[$count] == '') {
                $multipleAreaSearchArray[$count] = $province_array[$count];
            } elseif ($suburb_array[$count] == '') {
                $multipleAreaSearchArray[$count] = $province_array[$count].','.$area_array[$count];
            }
            $count--;
        }

        //if there are no fields other than user details it should create query
        if ($input->prov_area_sub == '' && $input->listing_type == '' && $input->property_type == '' &&
            $input->dwelling_type[0] == '' && $input->bedrooms == '' && $input->bathrooms == '' &&
            $input->commercial_type[0] == '' && $input->farm_type[0] == '' && $input->price_rent == '' &&
            $input->price == '' && $input->land_size == '' && $input->floor_space == '' &&
            empty($multipleAreaSearchArray)
        ) {
            $userIds = [];
            foreach ($userList as $list) {
                $userIds[] = $list->id;
            }
            if (empty($userIds)) {
                $query = 'SELECT * FROM `users` WHERE id = 0';
            } else {
                $query = 'SELECT * FROM `users` WHERE `id` IN ('.implode(',', array_map('intval', $userIds)).')';
            }
        } else {
            //checks areas in user preferences
            if ($input->prov_area_sub != '') {
                $multipleAreaSearchArray[] = $input->prov_area_sub;
            }
            $area_query2 = '';
            if (! empty($multipleAreaSearchArray)) {
                $first_check = 1;
                foreach ($multipleAreaSearchArray as $areaSearch) {
                    $areaArray = explode(',', $areaSearch);
                    if ($first_check == 1) {
                        if (trim($areaArray[0]) != '') {
                            if (count($areaArray) == 3) {
                                $province = 'WHERE (p.province LIKE "%'.trim($areaArray[0]).'%"';
                                $area = ' AND p.area LIKE "%'.trim($areaArray[1]).'%"';
                                $suburb_town_city = ' AND p.suburb_town_city LIKE "%'.trim($areaArray[2]).'%")';
                            } elseif (count($areaArray) == 2) {
                                $province = 'WHERE (p.province LIKE "%'.trim($areaArray[0]).'%"';
                                $area = ' AND (p.area LIKE "%'.trim($areaArray[1]).'%" OR p.suburb_town_city LIKE "%'.trim($areaArray[1]).'%"))';
                                $suburb_town_city = '';
                            } else {
                                $province = 'WHERE (p.province LIKE "%'.trim($areaArray[0]).'%")';
                                $area = '';
                                $suburb_town_city = '';
                            }
                            $area_query = ($province.$area.$suburb_town_city);
                            $first_check = 0;
                        }
                    } else {
                        if (count($areaArray) == 3) {
                            $province = ' OR (p.province LIKE "%'.trim($areaArray[0]).'%"';
                            $area = ' AND p.area LIKE "%'.trim($areaArray[1]).'%"';
                            $suburb_town_city = ' AND p.suburb_town_city LIKE "%'.trim($areaArray[2]).'%")';
                        } elseif (count($areaArray) == 2) {
                            $province = ' OR (p.province LIKE "%'.trim($areaArray[0]).'%"';
                            $area = ' AND (p.area LIKE "%'.trim($areaArray[1]).'%" OR p.suburb_town_city LIKE "%'.trim($areaArray[1]).'%"))';
                            $suburb_town_city = '';
                        } else {
                            $province = 'WHERE (p.province LIKE "%'.trim($areaArray[0]).'%")';
                            $area = '';
                            $suburb_town_city = '';
                        }
                        $area_query2 .= ($province.$area.$suburb_town_city);
                    }
                }
            } else {
                $province = 'WHERE p.province LIKE "%"';
                $area = ' AND p.area LIKE "%"';
                $suburb_town_city = ' AND p.suburb_town_city LIKE "%"';
                $area_query = ($province.$area.$suburb_town_city);
            }

            //checks listing type in preferences
            if ($input->listing_type == '') {
                $listing_type = ' AND p.listing_type LIKE "%"';
            } else {
                $listing_type = ' AND (p.listing_type LIKE "%'.$input->listing_type.'%" OR p.listing_type = "all")';
            }
            //checks property type in preferences
            if ($input->property_type == '') {
                $property_type = ' AND p.property_type LIKE "%"';
            } else {
                $property_type = ' AND (p.property_type LIKE "%'.$input->property_type.'%" OR p.property_type = "all")';
            }
            // checks other inputs by property type
            switch ($input->property_type) {
                case 'residential':
                    $dwelling_type = ' AND (';
                    $count_dwelling = 0;
                    foreach ($input->dwelling_type as $dwelling_type_choice) {
                        if ($count_dwelling == 0) {
                            $dwelling_type .= ' p.dwelling_type LIKE "%'.$dwelling_type_choice.'%"';
                        } else {
                            $dwelling_type .= ' OR p.dwelling_type LIKE "%'.$dwelling_type_choice.'%"';
                        }
                        $count_dwelling++;
                    }
                    $dwelling_type .= ' )';
                    if ($input->bedrooms == '') {
                        $bedrooms = ' AND p.bedrooms LIKE "%"';
                    } elseif ($input->bedrooms == '0') {
                        $bedrooms = ' AND p.bedrooms = 0';
                    } else {
                        $bedrooms = ' AND p.bedrooms <= '.$input->bedrooms;
                    }
                    if ($input->bathrooms == '') {
                        $bathrooms = ' AND p.bathrooms LIKE "%"';
                    } else {
                        $bathrooms = ' AND p.bathrooms <=  '.$input->bathrooms;
                    }
                    $property_attributes = $dwelling_type.$bedrooms.$bathrooms;

                    break;
                case 'commercial':
                    if ($input->commercial_type == []) {
                        $property_attributes = ' AND p.commercial_type LIKE "%"';
                    } else {
                        $property_attributes = ' AND (';
                        $count_commercial = 0;
                        foreach ($input->commercial_type as $commercial_type) {
                            if ($count_commercial == 0) {
                                $property_attributes .= ' p.commercial_type LIKE "%'.$commercial_type.'%"';
                            } else {
                                $property_attributes .= ' OR p.commercial_type LIKE "%'.$commercial_type.'%"';
                            }
                            $count_commercial++;
                        }
                        $property_attributes .= ' )';
                    }

                    break;
                case 'farm':
                    $property_attributes = ' AND (';
                    $count_farm = 0;
                    foreach ($input->farm_type as $farm_type) {
                        if ($count_farm == 0) {
                            $property_attributes .= ' p.farm_type LIKE "%'.$farm_type.'%"';
                        } else {
                            $property_attributes .= ' OR p.farm_type LIKE "%'.$farm_type.'%"';
                        }
                        $count_farm++;
                    }
                    $property_attributes .= ' )';

                    break;
                default:
                    $property_attributes = '';

                    break;
            }
            //checks price
            if ($input->listing_type == 'welet') {
                $price = $input->price_rent;
            } else {
                $price = $input->price;
            }

//            creates query
            $query = 'SELECT p.* FROM preferences p '.str_replace('WHERE ', 'WHERE (', $area_query).$area_query2
                .')'.$listing_type.$property_type.$property_attributes;

            $userListbyPref = [];
//            runs through each user from userlist created earlier, to check whether preferences match
            foreach ($userList as $id) {
                $userQuery = $query.' AND p.user_id = '.$id->id;

                $preferences = DB::select($userQuery);

                if (count($preferences) > 0) {
                    $check = 0;
                    foreach ($preferences as $preference) {
                        $price_array_pref = explode('-', preg_replace('~[R+]~', '', $preference->price));
                        if ($input->listing_type == 'welet') {
                            if ($price == '') {
                                $check = 1;
                            } else {
                                if (trim($price_array_pref[1]) < 30000) {
                                    if ((trim($price_array_pref[0]) <= trim($price)) &&
                                        (trim($price_array_pref[1]) >= trim($price))
                                    ) {
                                        $check = 1;
                                    }
                                } else {
                                    if (trim($price_array_pref[0]) <= trim($price)) {
                                        $check = 1;
                                    }
                                }
                            }
                        } else {
                            if ($price == '') {
                                $check = 1;
                            } else {
                                if (trim($price_array_pref[1]) < 15000000) {
                                    if ((trim($price_array_pref[0]) <= trim($price)) &&
                                        (trim($price_array_pref[1]) >= trim($price))
                                    ) {
                                        $check = 1;
                                    }
                                } else {
                                    if (trim($price_array_pref[0]) <= trim($price)) {
                                        $check = 1;
                                    }
                                }
                            }
                        }
                        if ($check == 1) {
                            $land_array_pref = explode('-', str_replace('+', '', $preference->land_size));
                            if ($input->land_size == '') {
                                $check = 1;
                            } else {
                                if (trim($land_array_pref[1]) < 20000) {
                                    if ((trim($land_array_pref[0]) <= trim($input->land_size)) &&
                                        (trim($land_array_pref[1]) >= trim($input->land_size))
                                    ) {
                                        $check = 1;
                                    } else {
                                        $check = 0;
                                    }
                                } else {
                                    if (trim($land_array_pref[0]) <= trim($input->land_size)) {
                                        $check = 1;
                                    } else {
                                        $check = 0;
                                    }
                                }
                            }
                        }
                        if (($input->property_type == 'residential' && $input->dwelling_type != 'Vacant Land') ||
                            ($input->property_type == 'commercial' && $input->commercial_type != 'Vacant Land')
                        ) {
                            if ($check == 1) {
                                $floor_array_pref = explode('-', str_replace('+', '', $preference->floor_space));
                                if ($input->floor_space == '') {
                                    $check = 1;
                                } else {
                                    if (trim($floor_array_pref[1]) < 2000) {
                                        if ((trim($floor_array_pref[0]) <= trim($input->floor_space)) &&
                                            (trim($floor_array_pref[1]) >= trim($input->floor_space))
                                        ) {
                                            $check = 1;
                                        } else {
                                            $check = 0;
                                        }
                                    } else {
                                        if (trim($floor_array_pref[0]) <= trim($input->floor_space)) {
                                            $check = 1;
                                        } else {
                                            $check = 0;
                                        }
                                    }
                                }
                            }
                        }
                        if ($check == 1) {
                            $userListbyPref[] = $id->id;
                            $check = 0;
                        }
                    }
                }
            }

            $userListbyPref = array_values(array_unique($userListbyPref));
            //creates final query
            if (empty($userListbyPref)) {
                $query = 'SELECT * FROM `users` WHERE id = 0';
            } else {
                $query = 'SELECT * FROM `users` WHERE `id` IN ('.implode(',', array_map('intval', $userListbyPref)).')';
            }
        }

//        runs query
        $users = DB::select($query);

//        returns table
        return Datatable::collection(new Collection($users))
            ->showColumns('firstname', 'lastname', 'email', 'province', 'created_at')
            ->addColumn('action', function ($userList) {
                if ($userList->firstname == 'Admin') {
                    return 'No Actions';
                } else {
                    return '<a href="/admin/users/edit/'.$userList->id.'" title="View User" >
            <i class="i-circled i-light i-small icon-laptop"></i>
            </a>
            <a href="/admin/messages/'.$userList->id.'/0/general/create/" title="Message" >
            <i class="i-circled i-light i-small icon-comments"></i>
            </a>';
                }
            })
            ->searchColumns('firstname', 'lastname', 'email', 'province', 'created_at')
            ->orderColumns('firstname', 'lastname', 'email', 'province', 'created_at')
            ->make();
    }

    public function searchReturnUser()
    {
        $input = Request::get('input');

        $provinces = DB::table('areas')->groupBy('province')->get();
        $tagDB = DB::table('tags')->get();
        foreach ($tagDB as $tag) {
            $tags[] = $tag->name;
        }
        $tags = implode('", "', $tags);

        $current_tags = explode(',', $input['current_tags']);

        if ($current_tags[0] == '') {
            $current_tags = [];
        }
        $dwelling_type = '';
        $farm_type = '';
        $commercial_type = '';

        if (isset($input['dwelling_type'])) {
            foreach ($input['dwelling_type'] as $type) {
                $dwelling_type .= $type.',';
            }
            $dwelling_type = substr($dwelling_type, 0, -1);
        }
        if (isset($input['farm_type'])) {
            foreach ($input['farm_type'] as $type) {
                $farm_type .= $type.',';
            }
            $farm_type = substr($farm_type, 0, -1);
        }

        if (isset($input['commercial_type'])) {
            foreach ($input['commercial_type'] as $type) {
                $commercial_type .= $type.',';
            }
            $commercial_type = substr($commercial_type, 0, -1);
        }

        $province_array = explode(',', $input['province_array']);
        $area_array = explode(',', $input['area_array']);
        $suburb_array = explode(',', $input['suburb_array']);

        $count = 0;
        $prov_area_sub = [];
        while ($count < count($province_array)) {
            $prov_string = $area_array[$count] != '' ? $province_array[$count].', ' : $province_array[$count];
            $area_string = $suburb_array[$count] != '' ? $area_array[$count].', ' : $area_array[$count];
            $suburb_string = $suburb_array[$count];
            $prov_area_sub[] = $prov_string.$area_string.$suburb_string;
            $count++;
        }

        $count = (count($prov_area_sub));

        implode($prov_area_sub);
        $pas = '';
        foreach ($prov_area_sub as $pas_str) {
            $pas .= $pas_str.'|';
        }

        return view('users.search', ['provinces' => $provinces, 'firstname' => $input['firstname'],
            'lastname' => $input['lastname'], 'cellnumber' => $input['cellnumber'], 'email' => $input['email'],
            'prov_area_sub' => $pas, 'province_array' => $input['province_array'],
            'area_array' => $input['area_array'], 'suburb_array' => $input['suburb_array'],
            'listing_type' => $input['listing_type'], 'property_type' => $input['property_type'],
            'dwelling_type' => $dwelling_type, 'bedrooms' => $input['bedrooms'], 'count' => $count,
            'bathrooms' => $input['bathrooms'], 'commercial_type' => $commercial_type,
            'farm_type' => $farm_type, 'price' => $input['price'], 'land_size' => $input['land_size'],
            'floor_space' => $input['floor_space'], 'current_tags' => $current_tags, 'tags' => $tags, ]);
    }

    public function adminEdit($id)
    {
        $user = $this->userRepository->findById($id);
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $roles = DB::select('select role_id, roles.name from role_user left join roles on role_user.role_id=roles.id where user_id = '.$id);
        $allroles = DB::select(DB::raw('select id, name from roles'));

        $tags = DB::select('select tag_id, tags.name from tag_user left join tags on tag_user.tag_id=tags.id where user_id = '.$id);
        $alltags = DB::select('select id, name from tags');

        $physical_register = $this->physical_auction_register->join('physical_auctions', 'physical_auctions.id', '=',
            'physical_auction_register.auction_id')->whereUser_id($user->id)->orderBy('physical_auctions.id')->get();

        foreach ($physical_register as $register) {
            $first_interest[] = $this->auction->leftJoin('property', 'auction.property_id', '=', 'property.id')->select('property.id', 'property.reference')
                ->where('auction.id', '=', $register->property_auction_id)->first();
        }

        $count = 0;
        foreach ($physical_register as $register) {
            $register->property_id = $first_interest[$count]['id'];
            $register->reference = $first_interest[$count]['reference'];
            $count++;
        }

        $bid_to_buys = $this->auction_register->join('auction', 'auction.id', '=', 'auction_register.auction_id')
            ->join('property', 'property.id', '=', 'auction.property_id')->select('auction_register.*', 'property.reference',
                'property.id', 'auction.start_date', 'auction.end_date')->where('auction_register.user_id', '=', $user->id)->orderBy('property.id')->get();

        $preferences = new Preferences();

        $preferences->price = 'R0-R15000000';
        $preferences->land_size = '0-20000+';
        $preferences->floor_space = '0-2000+';
        $preferences->province = 'south-africa';

        $added_by_admin = DB::table('user_pass')->whereUser_id($id)->first();

        $propertiesDB = $this->property->select('reference', 'street_number', 'street_address')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
            $address[] = $reference->street_number.' - '.$reference->street_address;
        }
        $references = implode('", "', $references);
        $address = implode('", "', $address);

        $autotags = DB::select('SELECT name FROM tags');
        $autotag_array = [];
        foreach ($autotags as $autotag) {
            $autotag_array[] = strtoupper(trim($autotag->name));
        }

        return view('users.adminedit', ['user' => $user, 'listing' => 'All', 'cities' => $cities, 'roles' => $roles,
            'allroles' => $allroles, 'physical_register' => $physical_register, 'bid_to_buys' => $bid_to_buys, 'tags' => $tags,
            'alltags' => $alltags, 'preferences' => $preferences, 'added_by_admin' => $added_by_admin, 'references' => $references,
            'address' => $address, 'autotag' => json_encode($autotag_array), ]);
    }

    public function sendWelcomeEmail()
    {
        $encrypted = DB::table('user_pass')->select('encrypted_pass')->whereUser_id(Request::get('user_id'))->first();

        $user = $this->users->whereId(Request::get('user_id'))->first();

        $user->assignRole(11);

        $password = $this->decryptPassword($encrypted->encrypted_pass);

        $this->mailer->sendWelcomeMessageToAddedUser($user);

        return 'Done';
    }

    public function update($id)
    {
        $input = Request::all();

        if (Request::get('allow_contact')) {
            $input['allow_contact'] = 1;
        } else {
            $input['allow_contact'] = 0;
        }

        if (Request::get('Suburb') == '') {
            $input['province'] = null;
            $input['suburb_town_city'] = null;
            $input['area'] = null;
        } else {
            $areaArray = explode(',', Request::get('Suburb'));
            if (count($areaArray) == 3) {
                $input['province'] = $areaArray[0];
                $input['area'] = $areaArray[1];
                $input['suburb_town_city'] = $areaArray[2];
            } elseif (count($areaArray) == 2) {
                $input['province'] = $areaArray[0];
                $input['area'] = $areaArray[1];
                $input['suburb_town_city'] = null;
            } else {
                $input['province'] = $areaArray[0];
                $input['area'] = null;
                $input['suburb_town_city'] = null;
            }
        }

        $validate = Validator::make($input, [
            'firstname' => 'required',
            'lastname' => 'required',
        ]);

        if ($validate->fails()) {
            foreach ($validate->errors()->all() as $error) {
                Flash::message('Your first name and last name are both required');
            }

            return redirect()->back()->withInput($input);
        }

        $user = $this->userRepository->findById($id);

        $user->update($input);
        Flash::message('Your user profile has been updated');

        return redirect()->back();
    }

    public function adminUpdate($id)
    {
        $input = Request::all();
        if (Request::get('allow_contact')) {
            $input['allow_contact'] = 1;
        } else {
            $input['allow_contact'] = 0;
        }

        $this->editForm->validate($input);

        $user = $this->userRepository->findById($id);

        $user->update($input);
        Flash::message('User Updated');

        return redirect()->route('users.show', $user->id);
    }

    public function getRoles($userid)
    {
//        $this->createNewTag("testtag3");
        $roles = DB::select('select role_id, roles.name from role_user left join roles on role_user.role_id = roles.id where user_id = '.$userid);

        return $roles;
    }

    public function addRole($userid, $role)
    {
        $roleName = DB::table('roles')->whereId($role)->first();
        $user = $this->userRepository->findById($userid);
        if ($user->hasRole($roleName->name) == false) {
            $user->roles()->attach($role);
            if ($role == 23) {
                $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
                if ($user->hasTag('BUYER') == false) {
                    $user->tags()->attach($tag->id);
                }
            }

            return 'Done';
        } else {
            return 'Done';
        }
    }

    public function removeRole($userid, $role)
    {
        $user = $this->userRepository->findById($userid);
        $user->roles()->detach($role);
        Flash::message('Role has been Removed!');
    }

    public function getTags($userid)
    {
//        $this->createNewTag("testtag3");
        $tags = DB::select('select tag_id, tags.name from tag_user left join tags on tag_user.tag_id=tags.id where user_id = '.$userid);

        return $tags;
    }

    public function createNewTag()
    {
        $value = null;
        $tag = DB::table('tags')->where('name', '=', Request::get('tag_name'))->first();
        $name = strtoupper(Request::get('tag_name'));
        if ($tag === null) {
            $value = DB::table('tags')->insertGetId(
                ['name' => $name, 'description' => $name, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );
        } else {
            $value = $tag->id;
        }

        return $value;
    }

    public function addTag($userid, $tag)
    {
        $tagName = DB::table('tags')->whereId($tag)->first();
        $user = $this->userRepository->findById($userid);
        if ($user->hasTag($tagName->name) == false) {
            $user->tags()->attach($tag);
            Flash::message('Tag has been added!');

            return 'Done';
        } else {
            Flash::message('User already has this tag!');

            return 'Done';
        }
    }

    public function removeTag($userid, $tag)
    {
        $user = $this->userRepository->findById($userid);
        $user->tags()->detach($tag);
        Flash::message('Tag has been Removed!');
    }

    public function authFacebook()
    {
        $user = Auth::user();

        try {
            // get data from input
            $code = Request::get('code');
            // get fb service
            $fb = OAuth::consumer('Facebook');
            // if code is provided get user data and sign in
            if (! empty($code)) {
                if (! isset($_SESSION['token'])) {
                    // This was a callback request from facebook, get the token
                    $token = $fb->requestAccessToken($code);
                    $_SESSION['token'] = $token->getAccessToken();
                }
                // Send a request with it
                $result = json_decode($fb->request('/me'), true);

                //Find user with matching socialid
                $socialid = User::wheresocialid($result['id'])->first();

                if (is_null($socialid)) {
                    $user->socialid = $result['id'];
                    $user->save();
                    Flash::message('You can now login with Facebook.');

                    return redirect()->to('/users/edit');
                } else {
                    Flash::message('You have already authorised facebook login.');

                    return redirect()->to('/users/edit');
                }
            } // if not ask for permission first
            else {
                // get fb authorization
                $url = $fb->getAuthorizationUri();
                // return to facebook login url
                return redirect()->to((string) $url);
            }
        } catch (Exception $e) {
            Flash::message('There was an error communicating with Facebook');

            return redirect()->to('/users/edit');
        }
    }

    public function setup()
    {
        $user = Auth::user();
        $provinces = DB::table('areas')->groupBy('province')->get();

        return view('users.setup', ['provinces' => $provinces, 'user' => $user]);
    }

    public function unsubscribe($email)
    {
        $user = User::whereEmail($email)->first();

        $user->allow_contact = 0;

        $user->save();

        return view('users.unsubscribe', ['name' => $user]);
    }

    public function destroy($id)
    {
        $user = $this->userRepository->findById($id);

        DB::table('lead_user')->where('user_id', '=', $id)->delete();

        $user->delete();

        return view('users.adminlist')->with('role', 'all');
    }

    public function saveNumber()
    {
        $tel_number = Request::get('tel_number');

        Auth::user()->cellnumber = $tel_number;

        Auth::user()->update();
    }

    public function getUserEnquiries($user_id)
    {
        $query = 'SELECT p.id AS property_id, l.id AS lead_id, l.reference AS lead_reference, lrt.type AS lead_type, p.reference AS property_reference,
                      p.street_number, p.street_address FROM lead_user ul INNER JOIN leads l ON l.id = ul.lead_id
                      INNER JOIN lead_ref_types lrt ON l.lead_ref_type = lrt.id
                      LEFT JOIN property p ON p.id = l.property_id WHERE ul.user_id = '.$user_id;

        $properties = DB::select($query);

        foreach ($properties as $property) {
            $note_query = 'SELECT pn.note FROM prop_notes pn WHERE pn.lead_id = '.$property->lead_id.'
            ORDER BY pn.id DESC LIMIT 1';
            $note = DB::select($note_query);

            if (count($note) > 0) {
                $property->note = $note[0]->note;
            } else {
                $property->note = 'None';
            }

            $property->lead_reference = "<a href='/leads/".$property->lead_id."/edit'>".$property->lead_reference.'</a>';

            if ($property->property_id != null) {
                $property->property_reference = "<a href='/admin/property/".$property->property_id."/edit'>".$property->property_reference.'</a>';
            } else {
                $property->property_reference = 'NA';
            }

            if ($property->property_id != null) {
                $property->address = $property->street_number.' '.$property->street_address;
            } else {
                $property->address = 'NA';
            }
        }

        return Datatables::of($properties)
            ->rawColumns(['lead_reference', 'note', 'property_reference', 'address'])
            ->make(true);
    }
}
