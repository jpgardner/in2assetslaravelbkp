<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use In2Assets\Core\CommandBus;
use In2Assets\Forms\PublishNoteForm;
use In2Assets\Notes\Note;
use In2Assets\Notes\NoteRepository;
use In2Assets\Notes\PublishNoteCommand;
use Laracasts\Flash\Flash;

class NoteController extends Controller
{
    use CommandBus;

    protected $noteRepository;

    protected $publishNoteForm;

    public function __construct(PublishNoteForm $publishNoteForm, NoteRepository $noteRepository)
    {
        $this->publishNoteForm = $publishNoteForm;
        $this->noteRepository = $noteRepository;
    }

    // List notes for user
    public function index()
    {
        $notes = $this->noteRepository->getAllForUser(Auth::user());
        if (Request::ajax()) {
            return $notes;
        } else {
            return view('notes.index', compact('notes'));
        }
    }

    // Store Note
    public function store()
    {
        $this->publishNoteForm->validate(Request::only('title', 'body'));
        $this->execute(
            new PublishNoteCommand(Request::get('title'), Request::get('body'), Auth::user()->id)
        );
        Flash::message('Your note has been saved.');

        return Redirect::refresh();
    }

    // Destroy Note
    public function destroy($id)
    {
        $note = Note::whereId($id)->first();
        if ($note->user_id == Auth::user()->id) {
            $note->delete();
            Flash::message('Note has been deleted!');
        } else {
            Flash::error('You do not have permission to delete this note!');
        }

        return Redirect::route('notes_path');
    }

    public function protect()
    {
        return Redirect::route('notes_path');
    }
}
