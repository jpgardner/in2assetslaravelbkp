<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\Viewing;
use Illuminate\Support\Facades\Auth;

class ViewingController extends Controller
{
    public function __construct(Viewing $viewing, Property $property)
    {
        $this->viewing = $viewing;
        $this->property = $property;
    }

    /**
     * Display a listing of the resource.
     * GET /viewing.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /viewing/create.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /viewing.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Request::all();

        if (! $this->viewing->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->viewing->errors);
        }

        $property = $this->property->whereId(Request::get('property_id'))->first();
        $this->viewing->user_id = $property->user_id;

        //dd($input);

        $this->viewing->save();

        return $this->viewing->id;
    }

    public function booking()
    {
        //
        $input = Request::all();

        if ($input['event_start'] == '') {
            return redirect()->back()->withInput()->withErrors('Please select a booking date and time');
        }

        $timeStart = strtotime($input['event_start']);

        $input['event_end'] = date('Y-m-d H:i', strtotime('+30 mins', $timeStart));

        $viewing = $this->viewing->where('event_start', '<=', $input['event_start'])->
        where('event_end', '>=', $input['event_end'])->
        whereProperty_id($input['property_id'])->
        whereEvent_type(1)->
        first();

        if ($viewing == null) {
            return redirect()->back()->withInput()->withErrors('This booking time does not fall within an allotted viewing time, please book another time or alternatively contact the owner to arrange for alternate booking times.');
        }

        $viewing = $this->viewing->where('event_start', '<=', $input['event_start'])->
        where('event_end', '>', $input['event_start'])->
        whereProperty_id($input['property_id'])->
        whereEvent_type(3)->
        first();

        if ($viewing != null) {
            return redirect()->back()->withInput()->withErrors('This date and time has already been booked, please book another time or alternatively contact the owner to arrange for alternate booking times.');
        }

        $input['event_type'] = 3;

        $this->viewing->fill($input);

        $this->viewing->booked_by = Auth::user()->id;
        $this->viewing->user_id = $input['user_id'];

        $this->viewing->save();

        return redirect()->route('property.calendar', ['property_id' => $input['property_id']]);
    }

    /**
     * Display the specified resource.
     * GET /viewing/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /viewing/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /viewing/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Request::all();
        $viewing = $this->viewing->where('id', '=', $id)->
        first();
        $viewing->update($input);

        return $viewing->id;
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /viewing/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $bookings = 0;
        $viewing = $this->viewing->whereId($id)->first();

        if ($viewing['event_type'] == 1) {
            $start = $viewing['event_start'];
            $end = $viewing['event_end'];
            $property_id = $viewing['property_id'];

            $viewbooked = $this->viewing->whereEvent_type(3)->whereProperty_id($property_id)->where('event_start', '>=', $start)->where('event_start', '<=', $end)->first();

            if ($viewbooked['id'] != null) {
                $bookings = 1;
            }
        }

        $property = $this->property->where('property.id', '=', $viewing->property_id)->
        join('vetting', 'vetting.property_id', '=', 'property.id')
            ->first();
        $property_seller = DB::table('users')->whereId($property->user_id)->first();

        //send emails to each cancellation for buyer, seller and agent

        //seller details
        $seller['name'] = $property_seller->firstname.' '.$property_seller->lastname;
        $seller['email'] = $property_seller->email;
        $seller['contact'] = $property_seller->phonenumber;

        //agent details
        $agent['name'] = $property->rep_firstname.' '.$property->rep_surname;
        $agent['email'] = $property->rep_email;
        if (trim($agent['name']) == '') {
            $agent['name'] = 'The WeSell team';
        }
        if (trim($agent['email']) == '') {
            $agent['email'] = 'viewing@wesell.co.za';
        }
        //address
        $address = $property->street_number.', '.$property->street_address.', '.$property->area;

        //buyers detais
        $bookingDets = DB::table('viewing_sessions_booked')->whereViewing_id($id)->get();

        if ($bookingDets) {
            foreach ($bookingDets as $booking) {
                $buyer['name'] = $booking->name;
                $buyer['email'] = $booking->email;
                $buyer['contact'] = $booking->phone;

                //booking date and time
                $datetimeArray = explode(' ', $booking->time);
                $bookingarr['date'] = $datetimeArray[0];
                $bookingarr['time'] = $datetimeArray[1];

                App::make(\App\Http\Controllers\ViewingSessionBookingController::class)->sendEmails(3, $buyer, $seller, $bookingarr, $agent, $address);
            }
        }

        $viewing->delete();
        //Deletes all sessions booked within this viewing
        DB::table('viewing_sessions_booked')->whereViewing_id($id)->delete();

        if ($bookings == 0) {
            return 'Done';
        } else {
            return 'Bookings';
        }
    }

    public function getUserViewing()
    {
        $userId = Request::get('userId');
        //		$viewings = $this->viewing->join('property', 'property.id', '=', 'viewing.property_id')->
        //		leftJoin('users', 'users.id', '=', 'viewing.user_id')->
        //		select('viewing.*', 'property.street_address', 'users.title', 'users.firstname', 'users.lastname', 'users.cellnumber')->
        //		where('viewing.user_id', '=', Auth::user()->id)->get();
//
        $viewings = $this->viewing->join('property', 'property.id', '=', 'viewing.property_id')->
        select('viewing.*', 'property.street_address', 'property.street_number')->
        where('viewing.user_id', '=', $userId)->get();
        $count = 0;

        //		dd($viewings);
        foreach ($viewings as $viewing) {
            $events[$count]['id'] = $viewing['id'];
            $events[$count]['property_id'] = $viewing['property_id'];
            $events[$count]['start_date'] = $viewing['event_start'];
            $events[$count]['end_date'] = $viewing['event_end'];
            $events[$count]['event_type'] = $viewing['event_type'];
            switch ($viewing['event_type']) {
                case 1:
                    $events[$count]['title'] = $viewing['street_address'];
                    $events[$count]['body'] = 'Private Viewing';
                    $events[$count]['contact'] = '';
                    $events[$count]['color'] = '#5b8fcb';

                    break;
                case 2:
                    $events[$count]['title'] = $viewing['street_address'];
                    $events[$count]['body'] = 'Open House';
                    $events[$count]['contact'] = '';
                    $events[$count]['color'] = '#29abbb';

                    break;
                case 3:
                    $events[$count]['title'] = 'Booking for: '.$viewing['street_address'];
                    $events[$count]['body'] = 'Booking made by: '.$viewing['title'].' '.$viewing['firstname'].' '.$viewing['lastname'];
                    $events[$count]['contact'] = 'Contact: '.$viewing['cellnumber'];
                    $events[$count]['color'] = '#f9b13b';

                    break;
            }
            $count++;
        }

        $event_array = [];

        for ($i = 0; $i < $count; $i++) {
            $event_array[] = [
                'id' => $events[$i]['id'],
                'property_id' => $events[$i]['property_id'],
                'title' => $events[$i]['title'],
                'body' => $events[$i]['body'],
                'contact' => $events[$i]['contact'],
                'start' => $events[$i]['start_date'],
                'end' => $events[$i]['end_date'],
                'color' => $events[$i]['color'],
                'event_type' => $events[$i]['event_type'],
            ];
        }

        echo json_encode($event_array);
    }

    public function getBuyerViewing()
    {
        $viewings = $this->viewing->join('property', 'property.id', '=', 'viewing.property_id')->
        leftJoin('users', 'users.id', '=', 'viewing.user_id')->
        select('viewing.*', 'property.street_address', 'users.title', 'users.firstname', 'users.lastname', 'users.cellnumber')->
        where('viewing.booked_by', '=', Auth::user()->id)->get();
        $count = 0;
        foreach ($viewings as $viewing) {
            $events[$count]['id'] = $viewing['id'];

            $events[$count]['start_date'] = $viewing['event_start'];
            $events[$count]['end_date'] = $viewing['event_end'];
            $events[$count]['event_type'] = $viewing['event_type'];
            $events[$count]['title'] = 'Booking for: '.$viewing['street_address'];
            $events[$count]['body'] = 'Owner: '.$viewing['title'].' '.$viewing['firstname'].' '.$viewing['lastname'];
            $events[$count]['contact'] = 'Contact: '.$viewing['cellnumber'];
            $events[$count]['color'] = '#36ba4e';

            $count++;
        }

        $event_array = [];

        for ($i = 0; $i < $count; $i++) {
            $event_array[] = [
                'id' => $events[$i]['id'],
                'title' => $events[$i]['title'],
                'body' => $events[$i]['body'],
                'contact' => $events[$i]['contact'],
                'start' => $events[$i]['start_date'],
                'end' => $events[$i]['end_date'],
                'color' => $events[$i]['color'],
                'event_type' => $events[$i]['event_type'],
            ];
        }

        echo json_encode($event_array);
    }

    public function getPropertyViewing()
    {
        $property_id = Request::get('property_id');

        $viewings = $this->viewing->join('property', 'property.id', '=', 'viewing.property_id')->
        leftJoin('users', 'users.id', '=', 'viewing.user_id')->
        select('viewing.*', 'property.street_address', 'users.title', 'users.firstname', 'users.lastname', 'users.cellnumber')->
        where('viewing.property_id', '=', $property_id)->get();
        $count = 0;
        foreach ($viewings as $viewing) {
            $events[$count]['id'] = $viewing['id'];

            $events[$count]['start_date'] = $viewing['event_start'];
            $events[$count]['end_date'] = $viewing['event_end'];
            $events[$count]['event_type'] = $viewing['event_type'];
            switch ($viewing['event_type']) {
                case 1:
                    $events[$count]['title'] = $viewing['street_address'];
                    $events[$count]['body'] = 'Private Viewing';
                    $events[$count]['contact'] = '';
                    $events[$count]['color'] = '#0d6ed1';

                    break;
                case 2:
                    $events[$count]['title'] = $viewing['street_address'];
                    $events[$count]['body'] = 'Open House';
                    $events[$count]['contact'] = '';
                    $events[$count]['color'] = '#a0c2df';

                    break;
                case 3:
                    if ($viewing['booked_by'] == Auth::user()->id) {
                        $events[$count]['title'] = 'Booking for: '.$viewing['street_address'];
                        $events[$count]['body'] = 'Owner: '.$viewing['title'].' '.$viewing['firstname'].' '.$viewing['lastname'];
                        $events[$count]['contact'] = 'Contact: '.$viewing['cellnumber'];
                        $events[$count]['color'] = '#36ba4e';
                    } else {
                        $events[$count]['title'] = 'Booked';
                        $events[$count]['body'] = '';
                        $events[$count]['contact'] = '';
                        $events[$count]['color'] = '#fb5858';
                    }

                    break;
            }
            $count++;
        }
        $event_array = [];

        for ($i = 0; $i < $count; $i++) {
            $event_array[] = [
                'id' => $events[$i]['id'],
                'title' => $events[$i]['title'],
                'body' => $events[$i]['body'],
                'contact' => $events[$i]['contact'],
                'start' => $events[$i]['start_date'],
                'end' => $events[$i]['end_date'],
                'color' => $events[$i]['color'],
                'event_type' => $events[$i]['event_type'],
            ];
        }

        echo json_encode($event_array);
    }

    public function calendarCount()
    {
        $dtToday = date('Y-m-d');
        $calendarCount = $this->viewing->where('event_start', '>', $dtToday)->whereUser_id(Auth::user()->id)->orWhere('booked_by', '=', Auth::user()->id)->count();

        return $calendarCount;
    }
}
