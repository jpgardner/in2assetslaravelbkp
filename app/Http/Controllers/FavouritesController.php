<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

class FavouritesController extends Controller
{
    // Add a property to favorites
    public function store()
    {
        $propertyID = Request::get('property');

        if (Auth::check()) {
            if (Request::get('add')) {
                $favourite = DB::insert('INSERT INTO favourites SET user_id = '.Auth::user()->id.', property_id = '.$propertyID);

                return Response::json(['update' => 'added', 'title' => 'Property Added to Favourites', 'body' => '<img src="/images/modal/fav.png" /><div><p>This property has been added to your list of favourites. You can access your favourites list at any time by going to your portal page and choosing “Favourites” from the menu.</p><p><a class="btn btn-modal" href="/portal/favorites">View Favourites.</a></p></div></a>'], 200);
            } elseif (Request::get('remove')) {
                $favourite = DB::delete('DELETE FROM favourites WHERE user_id = '.Auth::user()->id.' AND property_id = '.$propertyID);

                return Response::json(['update' => 'removed', 'title' => 'Property Removed from Favourites', 'body' => '<img src="/images/modal/fav.png" /><div><p>This property has been removed from your list of favourites. You can access your favourites list at any time by going to your portal page and choosing “Favorites” from the menu.</p><p><a class="btn btn-modal" href="/portal/favorites">View Favourites.</a></p></div></a>'], 200);
            } else {
                return Response::json(['title' => 'Favourites Error', 'body' => '<img src="/images/modal/fav.png" /><div><p>An Error has occured when adding this property to your favourites. Please try again.</p><p><a class="btn btn-modal" href="/portal/favorites">View Favourites.</a></p></div></a>'], 200);
            }
        } else {
            return Response::json(['title' => 'Please Login or Create an Account', 'body' => '<img src="/images/modal/fav.png" /><div><p>Please <a data-toggle="modal" data-target="#myModal" data-dismiss="modal">login</a> to your account in order to mark a property as a favourite and access your existing favourited properties.</p><p>Don\'t have an account? <a data-toggle="modal" data-target="#myModal" data-dismiss="modal">Sign up</a> today to get access to the sites features.</p></div></a>'], 200);
        }
    }

    // Find all favorites
    public function findFavourites()
    {
        if (Request::get('single')) {
            if (Auth::check()) {
                $id = Request::get('property');
                $user = Auth::user()->id;
                $ids = DB::table('favourites')->select('property_id')->where('user_id', '=', $user)->where('property_id', $id)->get();

                return $ids;
            } else {
                $ids = [];
            }

            return $ids;
        } else {
            if (Auth::check()) {
                $user = Auth::user()->id;
                $ids = DB::table('favourites')->select('property_id')->where('user_id', '=', $user)->get();
            } else {
                $ids = [];
            }

            return $ids;
        }
    }
}
