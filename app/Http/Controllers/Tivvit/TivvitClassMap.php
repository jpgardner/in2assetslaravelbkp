<?php

namespace App\Http\Controllers\Tivvit;

/**
 * File for the class which returns the class map definition.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * Class which returns the class map definition by the static method TivvitClassMap::classMap().
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS.
     * @return array
     */
    final public static function classMap()
    {
        return [
            'Agent' => 'TivvitStructAgent',
            'AgentInd' => 'TivvitStructAgentInd',
            'AgentRem' => 'TivvitStructAgentRem',
            'ArrayOfAgentInd' => 'TivvitStructArrayOfAgentInd',
            'ArrayOfFeatures' => 'TivvitStructArrayOfFeatures',
            'ArrayOfOnshow' => 'TivvitStructArrayOfOnshow',
            'ArrayOfPicture' => 'TivvitStructArrayOfPicture',
            'ArrayOfRooms' => 'TivvitStructArrayOfRooms',
            'ArrayOfSeller' => 'TivvitStructArrayOfSeller',
            'Auction' => 'TivvitStructAuction',
            'AuthenticateMe' => 'TivvitStructAuthenticateMe',
            'AuthenticateMeResponse' => 'TivvitStructAuthenticateMeResponse',
            'Features' => 'TivvitStructFeatures',
            'GetProperty' => 'TivvitStructGetProperty',
            'GetPropertyResponse' => 'TivvitStructGetPropertyResponse',
            'GetPropertyResult' => 'TivvitStructGetPropertyResult',
            'GetSuburbByCity' => 'TivvitStructGetSuburbByCity',
            'GetSuburbByCityResponse' => 'TivvitStructGetSuburbByCityResponse',
            'GetSuburbByCityResult' => 'TivvitStructGetSuburbByCityResult',
            'GetSuburbByProv' => 'TivvitStructGetSuburbByProv',
            'GetSuburbByProvResponse' => 'TivvitStructGetSuburbByProvResponse',
            'GetSuburbByProvResult' => 'TivvitStructGetSuburbByProvResult',
            'GetSuburbBySub' => 'TivvitStructGetSuburbBySub',
            'GetSuburbBySubResponse' => 'TivvitStructGetSuburbBySubResponse',
            'GetSuburbs' => 'TivvitStructGetSuburbs',
            'GetSuburbsResponse' => 'TivvitStructGetSuburbsResponse',
            'GetSuburbsResult' => 'TivvitStructGetSuburbsResult',
            'Ltype' => 'TivvitEnumLtype',
            'Onshow' => 'TivvitStructOnshow',
            'Picture' => 'TivvitStructPicture',
            'Property' => 'TivvitStructProperty',
            'PropertyRem' => 'TivvitStructPropertyRem',
            'Ptype' => 'TivvitEnumPtype',
            'RemoveAgent' => 'TivvitStructRemoveAgent',
            'RemoveAgentResponse' => 'TivvitStructRemoveAgentResponse',
            'RemoveProperty' => 'TivvitStructRemoveProperty',
            'RemovePropertyResponse' => 'TivvitStructRemovePropertyResponse',
            'Rentper' => 'TivvitEnumRentper',
            'RoomType' => 'TivvitEnumRoomType',
            'Rooms' => 'TivvitStructRooms',
            'SearchType' => 'TivvitEnumSearchType',
            'Seller' => 'TivvitStructSeller',
            'Suburb' => 'TivvitStructSuburb',
            'TivvitResponse' => 'TivvitStructTivvitResponse',
            'UpdateAgent' => 'TivvitStructUpdateAgent',
            'UpdateAgentResponse' => 'TivvitStructUpdateAgentResponse',
            'UpdateOnShow' => 'TivvitStructUpdateOnShow',
            'UpdateOnShowResponse' => 'TivvitStructUpdateOnShowResponse',
            'UpdateProperty' => 'TivvitStructUpdateProperty',
            'UpdatePropertyResponse' => 'TivvitStructUpdatePropertyResponse',
            'UserInfo' => 'TivvitStructUserInfo',
            'htm' => 'TivvitEnumHtm',
        ];
    }
}
