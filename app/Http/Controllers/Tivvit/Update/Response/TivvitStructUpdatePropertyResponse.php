<?php

namespace App\Http\Controllers\Tivvit\Update\Response;

/**
 * File for class TivvitStructUpdatePropertyResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructUpdatePropertyResponse originally named UpdatePropertyResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructUpdatePropertyResponse extends TivvitWsdlClass
{
    /**
     * The UpdatePropertyResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructTivvitResponse
     */
    public $UpdatePropertyResult;

    /**
     * Constructor method for UpdatePropertyResponse.
     * @see parent::__construct()
     * @param TivvitStructTivvitResponse $_updatePropertyResult
     * @return TivvitStructUpdatePropertyResponse
     */
    public function __construct($_updatePropertyResult = null)
    {
        parent::__construct(['UpdatePropertyResult' => $_updatePropertyResult], false);
    }

    /**
     * Get UpdatePropertyResult value.
     * @return TivvitStructTivvitResponse|null
     */
    public function getUpdatePropertyResult()
    {
        return $this->UpdatePropertyResult;
    }

    /**
     * Set UpdatePropertyResult value.
     * @param TivvitStructTivvitResponse $_updatePropertyResult the UpdatePropertyResult
     * @return TivvitStructTivvitResponse
     */
    public function setUpdatePropertyResult($_updatePropertyResult)
    {
        return $this->UpdatePropertyResult = $_updatePropertyResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructUpdatePropertyResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
