<?php

namespace App\Http\Controllers\Tivvit\Update\Response;

/**
 * File for class TivvitStructUpdateAgentResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructUpdateAgentResponse originally named UpdateAgentResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructUpdateAgentResponse extends TivvitWsdlClass
{
    /**
     * The UpdateAgentResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructTivvitResponse
     */
    public $UpdateAgentResult;

    /**
     * Constructor method for UpdateAgentResponse.
     * @see parent::__construct()
     * @param TivvitStructTivvitResponse $_updateAgentResult
     * @return TivvitStructUpdateAgentResponse
     */
    public function __construct($_updateAgentResult = null)
    {
        parent::__construct(['UpdateAgentResult' => $_updateAgentResult], false);
    }

    /**
     * Get UpdateAgentResult value.
     * @return TivvitStructTivvitResponse|null
     */
    public function getUpdateAgentResult()
    {
        return $this->UpdateAgentResult;
    }

    /**
     * Set UpdateAgentResult value.
     * @param TivvitStructTivvitResponse $_updateAgentResult the UpdateAgentResult
     * @return TivvitStructTivvitResponse
     */
    public function setUpdateAgentResult($_updateAgentResult)
    {
        return $this->UpdateAgentResult = $_updateAgentResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructUpdateAgentResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
