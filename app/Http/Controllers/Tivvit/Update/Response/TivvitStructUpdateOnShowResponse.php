<?php

namespace App\Http\Controllers\Tivvit\Update\Response;

/**
 * File for class TivvitStructUpdateOnShowResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructUpdateOnShowResponse originally named UpdateOnShowResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructUpdateOnShowResponse extends TivvitWsdlClass
{
    /**
     * The UpdateOnShowResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructTivvitResponse
     */
    public $UpdateOnShowResult;

    /**
     * Constructor method for UpdateOnShowResponse.
     * @see parent::__construct()
     * @param TivvitStructTivvitResponse $_updateOnShowResult
     * @return TivvitStructUpdateOnShowResponse
     */
    public function __construct($_updateOnShowResult = null)
    {
        parent::__construct(['UpdateOnShowResult' => $_updateOnShowResult], false);
    }

    /**
     * Get UpdateOnShowResult value.
     * @return TivvitStructTivvitResponse|null
     */
    public function getUpdateOnShowResult()
    {
        return $this->UpdateOnShowResult;
    }

    /**
     * Set UpdateOnShowResult value.
     * @param TivvitStructTivvitResponse $_updateOnShowResult the UpdateOnShowResult
     * @return TivvitStructTivvitResponse
     */
    public function setUpdateOnShowResult($_updateOnShowResult)
    {
        return $this->UpdateOnShowResult = $_updateOnShowResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructUpdateOnShowResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
