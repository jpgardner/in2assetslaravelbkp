<?php

namespace App\Http\Controllers\Tivvit\Update\Property;

/**
 * File for class TivvitStructUpdateProperty.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructUpdateProperty originally named UpdateProperty
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructUpdateProperty extends TivvitWsdlClass
{
    /**
     * The property
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructProperty
     */
    public $property;

    /**
     * Constructor method for UpdateProperty.
     * @see parent::__construct()
     * @param TivvitStructProperty $_property
     * @return TivvitStructUpdateProperty
     */
    public function __construct($_property = null)
    {
        parent::__construct(['property' => $_property], false);
    }

    /**
     * Get property value.
     * @return TivvitStructProperty|null
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set property value.
     * @param TivvitStructProperty $_property the property
     * @return TivvitStructProperty
     */
    public function setProperty($_property)
    {
        return $this->property = $_property;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructUpdateProperty
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
