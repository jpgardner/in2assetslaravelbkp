<?php

namespace App\Http\Controllers\Tivvit\Update;

use App\Http\Controllers\Tivvit\TivvitWsdlClass;

/**
 * File for class TivvitServiceUpdate.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitServiceUpdate originally named Update.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitServiceUpdate extends TivvitWsdlClass
{
    /**
     * Sets the UserInfo SoapHeader param.
     * @uses TivvitWsdlClass::setSoapHeader()
     * @param TivvitStructUserInfo $_tivvitStructUserInfo
     * @param string $_nameSpace http://sandbox.tivvit.com/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderUserInfo(TivvitStructUserInfo $_tivvitStructUserInfo, $_nameSpace = '', $_mustUnderstand = false, $_actor = null)
    {
        $_nameSpace = TivvitWsdlClass::$WSDL_URL_DOMAIN;

        return $this->setSoapHeader($_nameSpace, 'UserInfo', $_tivvitStructUserInfo, $_mustUnderstand, $_actor);
    }

    /**
     * Method to call the operation originally named UpdateAgent
     * Documentation : insert or Update of Agent Details
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructUpdateAgent $_tivvitStructUpdateAgent
     * @return TivvitStructUpdateAgentResponse
     */
    public function UpdateAgent(TivvitStructUpdateAgent $_tivvitStructUpdateAgent)
    {
        try {
            $returnval = $this->setResult(self::getSoapClient()->UpdateAgent($_tivvitStructUpdateAgent));

//            $output = "<script>console.log( 'Debug Objects: " . $client->__getLastRequest() . "' );</script>";
//           echo $output;
            return $returnval;
        } catch (SoapFault $soapFault) {
//            $client = self::getSoapClient();
//            dd($client->__getLastRequest());
            ////            $output = "<script>console.log( 'Debug Objects: " . $client->__getLastRequest() . "' );</script>";
            ////            echo $output;
//            dd($soapFault);
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Method to call the operation originally named UpdateProperty
     * Documentation : insert or Update of Property Listing
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructUpdateProperty $_tivvitStructUpdateProperty
     * @return TivvitStructUpdatePropertyResponse
     */
    public function UpdateProperty(TivvitStructUpdateProperty $_tivvitStructUpdateProperty)
    {
        try {
            //dd(self::getSoapClient()->UpdateProperty($_tivvitStructUpdateProperty));
            $returnval = $this->setResult(self::getSoapClient()->UpdateProperty($_tivvitStructUpdateProperty));
//            $client = self::getSoapClient();
//            dd($client->__getLastResponse());
//            $client = self::getSoapClient(); use for xml
//            dd($client->__getLastRequest())$client->__getLastResponse(); use for xml
            ////            $output = "<script>console.log( 'Debug Objects: " .  . "' );</script>";
//            dd($client->__getLastResponse());

            return $returnval;
        } catch (SoapFault $soapFault) {
            $client = self::getSoapClient();
            de($client->__getLastResponse());
            dd($client->__getLastRequest());
//            $output = "<script>console.log( 'Debug Objects: " . $client->__getLastRequest() . "' );</script>";
//            echo $output;
            //dd($soapFault);
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Method to call the operation originally named UpdateOnShow
     * Documentation : insert or Update of Onshow
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructUpdateOnShow $_tivvitStructUpdateOnShow
     * @return TivvitStructUpdateOnShowResponse
     */
    public function UpdateOnShow(TivvitStructUpdateOnShow $_tivvitStructUpdateOnShow)
    {
        try {
            return $this->setResult(self::getSoapClient()->UpdateOnShow($_tivvitStructUpdateOnShow));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Returns the result.
     * @see TivvitWsdlClass::getResult()
     * @return TivvitStructUpdateAgentResponse|TivvitStructUpdateOnShowResponse|TivvitStructUpdatePropertyResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
