<?php

namespace App\Http\Controllers\Tivvit\Update\Agent;

/**
 * File for class TivvitStructUpdateAgent.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructUpdateAgent originally named UpdateAgent
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructUpdateAgent extends TivvitWsdlClass
{
    /**
     * The agent
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructAgent
     */
    public $agent;

    /**
     * Constructor method for UpdateAgent.
     * @see parent::__construct()
     * @param TivvitStructAgent $_agent
     * @return TivvitStructUpdateAgent
     */
    public function __construct($_agent = null)
    {
        parent::__construct(['agent' => $_agent], false);
    }

    /**
     * Get agent value.
     * @return TivvitStructAgent|null
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set agent value.
     * @param TivvitStructAgent $_agent the agent
     * @return TivvitStructAgent
     */
    public function setAgent($_agent)
    {
        return $this->agent = $_agent;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructUpdateAgent
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
