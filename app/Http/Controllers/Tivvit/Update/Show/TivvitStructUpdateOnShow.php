<?php

namespace App\Http\Controllers\Tivvit\Update\Show;

use App\Http\Controllers\Tivvit\Array\Onshow\TivvitStructArrayOfOnshow;

/**
 * File for class TivvitStructUpdateOnShow.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructUpdateOnShow originally named UpdateOnShow
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructUpdateOnShow extends TivvitWsdlClass
{
    /**
     * The ponshow
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructArrayOfOnshow
     */
    public $ponshow;

    /**
     * The ppnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $ppnum;

    /**
     * Constructor method for UpdateOnShow.
     * @see parent::__construct()
     * @param TivvitStructArrayOfOnshow $_ponshow
     * @param string $_ppnum
     * @return TivvitStructUpdateOnShow
     */
    public function __construct($_ponshow = null, $_ppnum = null)
    {
        parent::__construct(['ponshow' => ($_ponshow instanceof TivvitStructArrayOfOnshow) ? $_ponshow : new TivvitStructArrayOfOnshow($_ponshow), 'ppnum' => $_ppnum], false);
    }

    /**
     * Get ponshow value.
     * @return TivvitStructArrayOfOnshow|null
     */
    public function getPonshow()
    {
        return $this->ponshow;
    }

    /**
     * Set ponshow value.
     * @param TivvitStructArrayOfOnshow $_ponshow the ponshow
     * @return TivvitStructArrayOfOnshow
     */
    public function setPonshow($_ponshow)
    {
        return $this->ponshow = $_ponshow;
    }

    /**
     * Get ppnum value.
     * @return string|null
     */
    public function getPpnum()
    {
        return $this->ppnum;
    }

    /**
     * Set ppnum value.
     * @param string $_ppnum the ppnum
     * @return string
     */
    public function setPpnum($_ppnum)
    {
        return $this->ppnum = $_ppnum;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructUpdateOnShow
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
