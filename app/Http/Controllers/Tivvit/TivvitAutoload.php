<?php
/**
 * File to load generated classes once at once time.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
/**
 * Includes for all generated classes files.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
require_once dirname(__FILE__).'/TivvitWsdlClass.php';
require_once dirname(__FILE__).'/Agent/Rem/TivvitStructAgentRem.php';
require_once dirname(__FILE__).'/Remove/Agent/TivvitStructRemoveAgent.php';
require_once dirname(__FILE__).'/Remove/Response/TivvitStructRemoveAgentResponse.php';
require_once dirname(__FILE__).'/Remove/Property/TivvitStructRemoveProperty.php';
require_once dirname(__FILE__).'/Property/Rem/TivvitStructPropertyRem.php';
require_once dirname(__FILE__).'/Update/Response/TivvitStructUpdateOnShowResponse.php';
require_once dirname(__FILE__).'/Update/Show/TivvitStructUpdateOnShow.php';
require_once dirname(__FILE__).'/Array/Onshow/TivvitStructArrayOfOnshow.php';
require_once dirname(__FILE__).'/Room/Type/TivvitEnumRoomType.php';
require_once dirname(__FILE__).'/Onshow/TivvitStructOnshow.php';
require_once dirname(__FILE__).'/Auction/TivvitStructAuction.php';
require_once dirname(__FILE__).'/Update/Response/TivvitStructUpdatePropertyResponse.php';
require_once dirname(__FILE__).'/Remove/Response/TivvitStructRemovePropertyResponse.php';
require_once dirname(__FILE__).'/Get/Suburbs/TivvitStructGetSuburbs.php';
require_once dirname(__FILE__).'/Get/Result/TivvitStructGetSuburbByCityResult.php';
require_once dirname(__FILE__).'/Get/Response/TivvitStructGetSuburbByCityResponse.php';
require_once dirname(__FILE__).'/Get/Sub/TivvitStructGetSuburbBySub.php';
require_once dirname(__FILE__).'/Get/Response/TivvitStructGetSuburbBySubResponse.php';
require_once dirname(__FILE__).'/Suburb/TivvitStructSuburb.php';
require_once dirname(__FILE__).'/Get/City/TivvitStructGetSuburbByCity.php';
require_once dirname(__FILE__).'/Get/Result/TivvitStructGetSuburbByProvResult.php';
require_once dirname(__FILE__).'/Get/Response/TivvitStructGetSuburbsResponse.php';
require_once dirname(__FILE__).'/Get/Result/TivvitStructGetSuburbsResult.php';
require_once dirname(__FILE__).'/Get/Prov/TivvitStructGetSuburbByProv.php';
require_once dirname(__FILE__).'/Get/Response/TivvitStructGetSuburbByProvResponse.php';
require_once dirname(__FILE__).'/Rooms/TivvitStructRooms.php';
require_once dirname(__FILE__).'/Array/Rooms/TivvitStructArrayOfRooms.php';
require_once dirname(__FILE__).'/Update/Agent/TivvitStructUpdateAgent.php';
require_once dirname(__FILE__).'/Get/Result/TivvitStructGetPropertyResult.php';
require_once dirname(__FILE__).'/Agent/TivvitStructAgent.php';
require_once dirname(__FILE__).'/Update/Response/TivvitStructUpdateAgentResponse.php';
require_once dirname(__FILE__).'/Update/Property/TivvitStructUpdateProperty.php';
require_once dirname(__FILE__).'/Get/Response/TivvitStructGetPropertyResponse.php';
require_once dirname(__FILE__).'/Search/Type/TivvitEnumSearchType.php';
require_once dirname(__FILE__).'/Authenticate/Response/TivvitStructAuthenticateMeResponse.php';
require_once dirname(__FILE__).'/Tivvit/Response/TivvitStructTivvitResponse.php';
require_once dirname(__FILE__).'/User/Info/TivvitStructUserInfo.php';
require_once dirname(__FILE__).'/Get/Property/TivvitStructGetProperty.php';
require_once dirname(__FILE__).'/Property/TivvitStructProperty.php';
require_once dirname(__FILE__).'/Rentper/TivvitEnumRentper.php';
require_once dirname(__FILE__).'/Array/Seller/TivvitStructArrayOfSeller.php';
require_once dirname(__FILE__).'/Agent/Ind/TivvitStructAgentInd.php';
require_once dirname(__FILE__).'/Seller/TivvitStructSeller.php';
require_once dirname(__FILE__).'/Array/Picture/TivvitStructArrayOfPicture.php';
require_once dirname(__FILE__).'/Picture/TivvitStructPicture.php';
require_once dirname(__FILE__).'/Array/Ind/TivvitStructArrayOfAgentInd.php';
require_once dirname(__FILE__).'/Htm/TivvitEnumHtm.php';
require_once dirname(__FILE__).'/Ltype/TivvitEnumLtype.php';
require_once dirname(__FILE__).'/Ptype/TivvitEnumPtype.php';
require_once dirname(__FILE__).'/Array/Features/TivvitStructArrayOfFeatures.php';
require_once dirname(__FILE__).'/Features/TivvitStructFeatures.php';
require_once dirname(__FILE__).'/Authenticate/Me/TivvitStructAuthenticateMe.php';
require_once dirname(__FILE__).'/Authenticate/TivvitServiceAuthenticate.php';
require_once dirname(__FILE__).'/Get/TivvitServiceGet.php';
require_once dirname(__FILE__).'/Update/TivvitServiceUpdate.php';
require_once dirname(__FILE__).'/Remove/TivvitServiceRemove.php';
require_once dirname(__FILE__).'/TivvitClassMap.php';
