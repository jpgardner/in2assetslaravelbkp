<?php
/**
 * Test with Tivvit for 'http://sandbox.tivvit.com/TivvitServices.asmx?WSDL'.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
ini_set('memory_limit', '512M');
ini_set('display_errors', true);
error_reporting(-1);
/**
 * Load autoload.
 */
require_once dirname(__FILE__).'/TivvitAutoload.php';
/**
 * Wsdl instanciation infos. By default, nothing has to be set.
 * If you wish to override the SoapClient's options, please refer to the sample below.
 *
 * This is an associative array as:
 * - the key must be a TivvitWsdlClass constant beginning with WSDL_
 * - the value must be the corresponding key value
 * Each option matches the {@link http://www.php.net/manual/en/soapclient.soapclient.php} options
 *
 * Here is below an example of how you can set the array:
 * $wsdl = array();
 * $wsdl[TivvitWsdlClass::WSDL_URL] = 'http://sandbox.tivvit.com/TivvitServices.asmx?WSDL';
 * $wsdl[TivvitWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
 * $wsdl[TivvitWsdlClass::WSDL_TRACE] = true;
 * $wsdl[TivvitWsdlClass::WSDL_LOGIN] = 'myLogin';
 * $wsdl[TivvitWsdlClass::WSDL_PASSWD] = '**********';
 * etc....
 * Then instantiate the Service class as:
 * - $wsdlObject = new TivvitWsdlClass($wsdl);
 */
/**
 * Examples.
 */

/***************************************
 * Example for TivvitServiceAuthenticate
 */
$tivvitServiceAuthenticate = new TivvitServiceAuthenticate();
// sample call for TivvitServiceAuthenticate::setSoapHeaderUserInfo() in order to initialize required SoapHeader
$tivvitServiceAuthenticate->setSoapHeaderUserInfo(new TivvitStructUserInfo(/*** update parameters list ***/));
// sample call for TivvitServiceAuthenticate::AuthenticateMe()
if ($tivvitServiceAuthenticate->AuthenticateMe(new TivvitStructAuthenticateMe(/*** update parameters list ***/))) {
    print_r($tivvitServiceAuthenticate->getResult());
} else {
    print_r($tivvitServiceAuthenticate->getLastError());
}

/******************************
 * Example for TivvitServiceGet
 */
$tivvitServiceGet = new TivvitServiceGet();
// sample call for TivvitServiceGet::setSoapHeaderUserInfo() in order to initialize required SoapHeader
$tivvitServiceGet->setSoapHeaderUserInfo(new TivvitStructUserInfo(/*** update parameters list ***/));
// sample call for TivvitServiceGet::GetProperty()
if ($tivvitServiceGet->GetProperty(new TivvitStructGetProperty(/*** update parameters list ***/))) {
    print_r($tivvitServiceGet->getResult());
} else {
    print_r($tivvitServiceGet->getLastError());
}
// sample call for TivvitServiceGet::GetSuburbs()
if ($tivvitServiceGet->GetSuburbs()) {
    print_r($tivvitServiceGet->getResult());
} else {
    print_r($tivvitServiceGet->getLastError());
}
// sample call for TivvitServiceGet::GetSuburbByProv()
if ($tivvitServiceGet->GetSuburbByProv(new TivvitStructGetSuburbByProv(/*** update parameters list ***/))) {
    print_r($tivvitServiceGet->getResult());
} else {
    print_r($tivvitServiceGet->getLastError());
}
// sample call for TivvitServiceGet::GetSuburbByCity()
if ($tivvitServiceGet->GetSuburbByCity(new TivvitStructGetSuburbByCity(/*** update parameters list ***/))) {
    print_r($tivvitServiceGet->getResult());
} else {
    print_r($tivvitServiceGet->getLastError());
}
// sample call for TivvitServiceGet::GetSuburbBySub()
if ($tivvitServiceGet->GetSuburbBySub(new TivvitStructGetSuburbBySub(/*** update parameters list ***/))) {
    print_r($tivvitServiceGet->getResult());
} else {
    print_r($tivvitServiceGet->getLastError());
}

/*********************************
 * Example for TivvitServiceUpdate
 */
$tivvitServiceUpdate = new TivvitServiceUpdate();
// sample call for TivvitServiceUpdate::setSoapHeaderUserInfo() in order to initialize required SoapHeader
$tivvitServiceUpdate->setSoapHeaderUserInfo(new TivvitStructUserInfo(/*** update parameters list ***/));
// sample call for TivvitServiceUpdate::UpdateAgent()
if ($tivvitServiceUpdate->UpdateAgent(new TivvitStructUpdateAgent(/*** update parameters list ***/))) {
    print_r($tivvitServiceUpdate->getResult());
} else {
    print_r($tivvitServiceUpdate->getLastError());
}
// sample call for TivvitServiceUpdate::UpdateProperty()
if ($tivvitServiceUpdate->UpdateProperty(new TivvitStructUpdateProperty(/*** update parameters list ***/))) {
    print_r($tivvitServiceUpdate->getResult());
} else {
    print_r($tivvitServiceUpdate->getLastError());
}
// sample call for TivvitServiceUpdate::UpdateOnShow()
if ($tivvitServiceUpdate->UpdateOnShow(new TivvitStructUpdateOnShow(/*** update parameters list ***/))) {
    print_r($tivvitServiceUpdate->getResult());
} else {
    print_r($tivvitServiceUpdate->getLastError());
}

/*********************************
 * Example for TivvitServiceRemove
 */
$tivvitServiceRemove = new TivvitServiceRemove();
// sample call for TivvitServiceRemove::setSoapHeaderUserInfo() in order to initialize required SoapHeader
$tivvitServiceRemove->setSoapHeaderUserInfo(new TivvitStructUserInfo(/*** update parameters list ***/));
// sample call for TivvitServiceRemove::RemoveAgent()
if ($tivvitServiceRemove->RemoveAgent(new TivvitStructRemoveAgent(/*** update parameters list ***/))) {
    print_r($tivvitServiceRemove->getResult());
} else {
    print_r($tivvitServiceRemove->getLastError());
}
// sample call for TivvitServiceRemove::RemoveProperty()
if ($tivvitServiceRemove->RemoveProperty(new TivvitStructRemoveProperty(/*** update parameters list ***/))) {
    print_r($tivvitServiceRemove->getResult());
} else {
    print_r($tivvitServiceRemove->getLastError());
}
