<?php

namespace App\Http\Controllers\Tivvit\Authenticate;

use App\Http\Controllers\Tivvit\TivvitWsdlClass;

/**
 * File for class TivvitServiceAuthenticate.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitServiceAuthenticate originally named Authenticate.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitServiceAuthenticate extends TivvitWsdlClass
{
    /**
     * Sets the UserInfo SoapHeader param.
     * @uses TivvitWsdlClass::setSoapHeader()
     * @param TivvitStructUserInfo $_tivvitStructUserInfo
     * @param string $_nameSpace http://sandbox.tivvit.com/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderUserInfo(TivvitStructUserInfo $_tivvitStructUserInfo, $_nameSpace = '', $_mustUnderstand = false, $_actor = null)
    {
        $_nameSpace = TivvitWsdlClass::$WSDL_URL_DOMAIN;

        return $this->setSoapHeader($_nameSpace, 'UserInfo', $_tivvitStructUserInfo, $_mustUnderstand, $_actor);
    }

    /**
     * Method to call the operation originally named AuthenticateMe
     * Documentation : Test user name and password to ensure correct authentication
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructAuthenticateMe $_tivvitStructAuthenticateMe
     * @return TivvitStructAuthenticateMeResponse
     */
    public function AuthenticateMe(TivvitStructAuthenticateMe $_tivvitStructAuthenticateMe)
    {
        try {
            return $this->setResult(self::getSoapClient()->AuthenticateMe($_tivvitStructAuthenticateMe));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Returns the result.
     * @see TivvitWsdlClass::getResult()
     * @return TivvitStructAuthenticateMeResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
