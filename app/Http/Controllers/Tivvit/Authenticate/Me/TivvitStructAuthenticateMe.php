<?php

namespace App\Http\Controllers\Tivvit\Authenticate\Me;

/**
 * File for class TivvitStructAuthenticateMe.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructAuthenticateMe originally named AuthenticateMe
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructAuthenticateMe extends TivvitWsdlClass
{
    /**
     * The userName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $userName;

    /**
     * The password
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $password;

    /**
     * Constructor method for AuthenticateMe.
     * @see parent::__construct()
     * @param string $_userName
     * @param string $_password
     * @return TivvitStructAuthenticateMe
     */
    public function __construct($_userName = null, $_password = null)
    {
        parent::__construct(['userName' => $_userName, 'password' => $_password], false);
    }

    /**
     * Get userName value.
     * @return string|null
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set userName value.
     * @param string $_userName the userName
     * @return string
     */
    public function setUserName($_userName)
    {
        return $this->userName = $_userName;
    }

    /**
     * Get password value.
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password value.
     * @param string $_password the password
     * @return string
     */
    public function setPassword($_password)
    {
        return $this->password = $_password;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructAuthenticateMe
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
