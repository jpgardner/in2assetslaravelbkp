<?php

namespace App\Http\Controllers\Tivvit\Authenticate\Response;

/**
 * File for class TivvitStructAuthenticateMeResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructAuthenticateMeResponse originally named AuthenticateMeResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructAuthenticateMeResponse extends TivvitWsdlClass
{
    /**
     * The AuthenticateMeResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructTivvitResponse
     */
    public $AuthenticateMeResult;

    /**
     * Constructor method for AuthenticateMeResponse.
     * @see parent::__construct()
     * @param TivvitStructTivvitResponse $_authenticateMeResult
     * @return TivvitStructAuthenticateMeResponse
     */
    public function __construct($_authenticateMeResult = null)
    {
        parent::__construct(['AuthenticateMeResult' => $_authenticateMeResult], false);
    }

    /**
     * Get AuthenticateMeResult value.
     * @return TivvitStructTivvitResponse|null
     */
    public function getAuthenticateMeResult()
    {
        return $this->AuthenticateMeResult;
    }

    /**
     * Set AuthenticateMeResult value.
     * @param TivvitStructTivvitResponse $_authenticateMeResult the AuthenticateMeResult
     * @return TivvitStructTivvitResponse
     */
    public function setAuthenticateMeResult($_authenticateMeResult)
    {
        return $this->AuthenticateMeResult = $_authenticateMeResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructAuthenticateMeResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
