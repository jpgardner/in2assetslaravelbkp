<?php

namespace App\Http\Controllers\Tivvit\Get\Prov;

/**
 * File for class TivvitStructGetSuburbByProv.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetSuburbByProv originally named GetSuburbByProv
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetSuburbByProv extends TivvitWsdlClass
{
    /**
     * The pProvince
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pProvince;

    /**
     * Constructor method for GetSuburbByProv.
     * @see parent::__construct()
     * @param string $_pProvince
     * @return TivvitStructGetSuburbByProv
     */
    public function __construct($_pProvince = null)
    {
        parent::__construct(['pProvince' => $_pProvince], false);
    }

    /**
     * Get pProvince value.
     * @return string|null
     */
    public function getPProvince()
    {
        return $this->pProvince;
    }

    /**
     * Set pProvince value.
     * @param string $_pProvince the pProvince
     * @return string
     */
    public function setPProvince($_pProvince)
    {
        return $this->pProvince = $_pProvince;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetSuburbByProv
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
