<?php

namespace App\Http\Controllers\Tivvit\Get\Sub;

/**
 * File for class TivvitStructGetSuburbBySub.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetSuburbBySub originally named GetSuburbBySub
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetSuburbBySub extends TivvitWsdlClass
{
    /**
     * The pProvince
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pProvince;

    /**
     * The pCity
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pCity;

    /**
     * The pSuburb
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pSuburb;

    /**
     * Constructor method for GetSuburbBySub.
     * @see parent::__construct()
     * @param string $_pProvince
     * @param string $_pCity
     * @param string $_pSuburb
     * @return TivvitStructGetSuburbBySub
     */
    public function __construct($_pProvince = null, $_pCity = null, $_pSuburb = null)
    {
        parent::__construct(['pProvince' => $_pProvince, 'pCity' => $_pCity, 'pSuburb' => $_pSuburb], false);
    }

    /**
     * Get pProvince value.
     * @return string|null
     */
    public function getPProvince()
    {
        return $this->pProvince;
    }

    /**
     * Set pProvince value.
     * @param string $_pProvince the pProvince
     * @return string
     */
    public function setPProvince($_pProvince)
    {
        return $this->pProvince = $_pProvince;
    }

    /**
     * Get pCity value.
     * @return string|null
     */
    public function getPCity()
    {
        return $this->pCity;
    }

    /**
     * Set pCity value.
     * @param string $_pCity the pCity
     * @return string
     */
    public function setPCity($_pCity)
    {
        return $this->pCity = $_pCity;
    }

    /**
     * Get pSuburb value.
     * @return string|null
     */
    public function getPSuburb()
    {
        return $this->pSuburb;
    }

    /**
     * Set pSuburb value.
     * @param string $_pSuburb the pSuburb
     * @return string
     */
    public function setPSuburb($_pSuburb)
    {
        return $this->pSuburb = $_pSuburb;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetSuburbBySub
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
