<?php

namespace App\Http\Controllers\Tivvit\Get\City;

/**
 * File for class TivvitStructGetSuburbByCity.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetSuburbByCity originally named GetSuburbByCity
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetSuburbByCity extends TivvitWsdlClass
{
    /**
     * The pProvince
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pProvince;

    /**
     * The pCity
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pCity;

    /**
     * Constructor method for GetSuburbByCity.
     * @see parent::__construct()
     * @param string $_pProvince
     * @param string $_pCity
     * @return TivvitStructGetSuburbByCity
     */
    public function __construct($_pProvince = null, $_pCity = null)
    {
        parent::__construct(['pProvince' => $_pProvince, 'pCity' => $_pCity], false);
    }

    /**
     * Get pProvince value.
     * @return string|null
     */
    public function getPProvince()
    {
        return $this->pProvince;
    }

    /**
     * Set pProvince value.
     * @param string $_pProvince the pProvince
     * @return string
     */
    public function setPProvince($_pProvince)
    {
        return $this->pProvince = $_pProvince;
    }

    /**
     * Get pCity value.
     * @return string|null
     */
    public function getPCity()
    {
        return $this->pCity;
    }

    /**
     * Set pCity value.
     * @param string $_pCity the pCity
     * @return string
     */
    public function setPCity($_pCity)
    {
        return $this->pCity = $_pCity;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetSuburbByCity
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
