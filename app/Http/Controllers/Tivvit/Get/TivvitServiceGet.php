<?php

namespace App\Http\Controllers\Tivvit\Get;

use App\Http\Controllers\Tivvit\TivvitWsdlClass;

/**
 * File for class TivvitServiceGet.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitServiceGet originally named Get.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitServiceGet extends TivvitWsdlClass
{
    /**
     * Sets the UserInfo SoapHeader param.
     * @uses TivvitWsdlClass::setSoapHeader()
     * @param TivvitStructUserInfo $_tivvitStructUserInfo
     * @param string $_nameSpace http://sandbox.tivvit.com/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderUserInfo(TivvitStructUserInfo $_tivvitStructUserInfo, $_nameSpace = '', $_mustUnderstand = false, $_actor = null)
    {
        $_nameSpace = TivvitWsdlClass::$WSDL_URL_DOMAIN;

        return $this->setSoapHeader($_nameSpace, 'UserInfo', $_tivvitStructUserInfo, $_mustUnderstand, $_actor);
    }

    /**
     * Method to call the operation originally named GetProperty
     * Documentation : Get a Property by entering ref no
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructGetProperty $_tivvitStructGetProperty
     * @return TivvitStructGetPropertyResponse
     */
    public function GetProperty(TivvitStructGetProperty $_tivvitStructGetProperty)
    {
        try {
            return $this->setResult(self::getSoapClient()->GetProperty($_tivvitStructGetProperty));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Method to call the operation originally named GetSuburbs
     * Documentation : Get Suburb List for South Africa
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructGetSuburbs $_tivvitStructGetSuburbs
     * @return TivvitStructGetSuburbsResponse
     */
    public function GetSuburbs()
    {
        try {
            return $this->setResult(self::getSoapClient()->GetSuburbs());
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Method to call the operation originally named GetSuburbByProv
     * Documentation : Get Suburb List for South Africa by Province
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructGetSuburbByProv $_tivvitStructGetSuburbByProv
     * @return TivvitStructGetSuburbByProvResponse
     */
    public function GetSuburbByProv(TivvitStructGetSuburbByProv $_tivvitStructGetSuburbByProv)
    {
        try {
            return $this->setResult(self::getSoapClient()->GetSuburbByProv($_tivvitStructGetSuburbByProv));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Method to call the operation originally named GetSuburbByCity
     * Documentation : Get Suburb List for South Africa by Province by City
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructGetSuburbByCity $_tivvitStructGetSuburbByCity
     * @return TivvitStructGetSuburbByCityResponse
     */
    public function GetSuburbByCity(TivvitStructGetSuburbByCity $_tivvitStructGetSuburbByCity)
    {
        try {
            return $this->setResult(self::getSoapClient()->GetSuburbByCity($_tivvitStructGetSuburbByCity));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Method to call the operation originally named GetSuburbBySub
     * Documentation : Get Suburb Detail for South Africa by Province by City by Suburb
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructGetSuburbBySub $_tivvitStructGetSuburbBySub
     * @return TivvitStructGetSuburbBySubResponse
     */
    public function GetSuburbBySub(TivvitStructGetSuburbBySub $_tivvitStructGetSuburbBySub)
    {
        try {
            return $this->setResult(self::getSoapClient()->GetSuburbBySub($_tivvitStructGetSuburbBySub));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Returns the result.
     * @see TivvitWsdlClass::getResult()
     * @return TivvitStructGetPropertyResponse|TivvitStructGetSuburbByCityResponse|TivvitStructGetSuburbByProvResponse|TivvitStructGetSuburbBySubResponse|TivvitStructGetSuburbsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
