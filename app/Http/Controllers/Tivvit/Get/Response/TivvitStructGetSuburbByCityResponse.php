<?php

namespace App\Http\Controllers\Tivvit\Get\Response;

/**
 * File for class TivvitStructGetSuburbByCityResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetSuburbByCityResponse originally named GetSuburbByCityResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetSuburbByCityResponse extends TivvitWsdlClass
{
    /**
     * The GetSuburbByCityResult.
     * @var TivvitStructGetSuburbByCityResult
     */
    public $GetSuburbByCityResult;

    /**
     * Constructor method for GetSuburbByCityResponse.
     * @see parent::__construct()
     * @param TivvitStructGetSuburbByCityResult $_getSuburbByCityResult
     * @return TivvitStructGetSuburbByCityResponse
     */
    public function __construct($_getSuburbByCityResult = null)
    {
        parent::__construct(['GetSuburbByCityResult' => $_getSuburbByCityResult], false);
    }

    /**
     * Get GetSuburbByCityResult value.
     * @return TivvitStructGetSuburbByCityResult|null
     */
    public function getGetSuburbByCityResult()
    {
        return $this->GetSuburbByCityResult;
    }

    /**
     * Set GetSuburbByCityResult value.
     * @param TivvitStructGetSuburbByCityResult $_getSuburbByCityResult the GetSuburbByCityResult
     * @return TivvitStructGetSuburbByCityResult
     */
    public function setGetSuburbByCityResult($_getSuburbByCityResult)
    {
        return $this->GetSuburbByCityResult = $_getSuburbByCityResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetSuburbByCityResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
