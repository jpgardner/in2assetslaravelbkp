<?php

namespace App\Http\Controllers\Tivvit\Get\Response;

/**
 * File for class TivvitStructGetSuburbByProvResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetSuburbByProvResponse originally named GetSuburbByProvResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetSuburbByProvResponse extends TivvitWsdlClass
{
    /**
     * The GetSuburbByProvResult.
     * @var TivvitStructGetSuburbByProvResult
     */
    public $GetSuburbByProvResult;

    /**
     * Constructor method for GetSuburbByProvResponse.
     * @see parent::__construct()
     * @param TivvitStructGetSuburbByProvResult $_getSuburbByProvResult
     * @return TivvitStructGetSuburbByProvResponse
     */
    public function __construct($_getSuburbByProvResult = null)
    {
        parent::__construct(['GetSuburbByProvResult' => $_getSuburbByProvResult], false);
    }

    /**
     * Get GetSuburbByProvResult value.
     * @return TivvitStructGetSuburbByProvResult|null
     */
    public function getGetSuburbByProvResult()
    {
        return $this->GetSuburbByProvResult;
    }

    /**
     * Set GetSuburbByProvResult value.
     * @param TivvitStructGetSuburbByProvResult $_getSuburbByProvResult the GetSuburbByProvResult
     * @return TivvitStructGetSuburbByProvResult
     */
    public function setGetSuburbByProvResult($_getSuburbByProvResult)
    {
        return $this->GetSuburbByProvResult = $_getSuburbByProvResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetSuburbByProvResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
