<?php

namespace App\Http\Controllers\Tivvit\Get\Response;

/**
 * File for class TivvitStructGetSuburbBySubResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetSuburbBySubResponse originally named GetSuburbBySubResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetSuburbBySubResponse extends TivvitWsdlClass
{
    /**
     * The GetSuburbBySubResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructSuburb
     */
    public $GetSuburbBySubResult;

    /**
     * Constructor method for GetSuburbBySubResponse.
     * @see parent::__construct()
     * @param TivvitStructSuburb $_getSuburbBySubResult
     * @return TivvitStructGetSuburbBySubResponse
     */
    public function __construct($_getSuburbBySubResult = null)
    {
        parent::__construct(['GetSuburbBySubResult' => $_getSuburbBySubResult], false);
    }

    /**
     * Get GetSuburbBySubResult value.
     * @return TivvitStructSuburb|null
     */
    public function getGetSuburbBySubResult()
    {
        return $this->GetSuburbBySubResult;
    }

    /**
     * Set GetSuburbBySubResult value.
     * @param TivvitStructSuburb $_getSuburbBySubResult the GetSuburbBySubResult
     * @return TivvitStructSuburb
     */
    public function setGetSuburbBySubResult($_getSuburbBySubResult)
    {
        return $this->GetSuburbBySubResult = $_getSuburbBySubResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetSuburbBySubResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
