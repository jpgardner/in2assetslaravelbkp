<?php

namespace App\Http\Controllers\Tivvit\Get\Response;

/**
 * File for class TivvitStructGetPropertyResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructGetPropertyResponse originally named GetPropertyResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructGetPropertyResponse extends TivvitWsdlClass
{
    /**
     * The GetPropertyResult.
     * @var TivvitStructGetPropertyResult
     */
    public $GetPropertyResult;

    /**
     * Constructor method for GetPropertyResponse.
     * @see parent::__construct()
     * @param TivvitStructGetPropertyResult $_getPropertyResult
     * @return TivvitStructGetPropertyResponse
     */
    public function __construct($_getPropertyResult = null)
    {
        parent::__construct(['GetPropertyResult' => $_getPropertyResult], false);
    }

    /**
     * Get GetPropertyResult value.
     * @return TivvitStructGetPropertyResult|null
     */
    public function getGetPropertyResult()
    {
        return $this->GetPropertyResult;
    }

    /**
     * Set GetPropertyResult value.
     * @param TivvitStructGetPropertyResult $_getPropertyResult the GetPropertyResult
     * @return TivvitStructGetPropertyResult
     */
    public function setGetPropertyResult($_getPropertyResult)
    {
        return $this->GetPropertyResult = $_getPropertyResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructGetPropertyResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
