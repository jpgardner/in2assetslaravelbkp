<?php

namespace App\Http\Controllers\Tivvit\Ltype;

/**
 * File for class TivvitEnumLtype.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitEnumLtype originally named Ltype
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitEnumLtype extends TivvitWsdlClass
{
    /**
     * Constant for value 'OpenMandate'.
     * @return string 'OpenMandate'
     */
    const VALUE_OPENMANDATE = 'OpenMandate';

    /**
     * Constant for value 'MLSMandate'.
     * @return string 'MLSMandate'
     */
    const VALUE_MLSMANDATE = 'MLSMandate';

    /**
     * Constant for value 'SharedMandate'.
     * @return string 'SharedMandate'
     */
    const VALUE_SHAREDMANDATE = 'SharedMandate';

    /**
     * Constant for value 'SoleMandate'.
     * @return string 'SoleMandate'
     */
    const VALUE_SOLEMANDATE = 'SoleMandate';

    /**
     * Constant for value 'OfficeListing'.
     * @return string 'OfficeListing'
     */
    const VALUE_OFFICELISTING = 'OfficeListing';

    /**
     * Constant for value 'GroupListing'.
     * @return string 'GroupListing'
     */
    const VALUE_GROUPLISTING = 'GroupListing';

    /**
     * Constant for value 'OppositionMandate'.
     * @return string 'OppositionMandate'
     */
    const VALUE_OPPOSITIONMANDATE = 'OppositionMandate';

    /**
     * Constant for value 'CountryWide'.
     * @return string 'CountryWide'
     */
    const VALUE_COUNTRYWIDE = 'CountryWide';

    /**
     * Return true if value is allowed.
     * @uses TivvitEnumLtype::VALUE_OPENMANDATE
     * @uses TivvitEnumLtype::VALUE_MLSMANDATE
     * @uses TivvitEnumLtype::VALUE_SHAREDMANDATE
     * @uses TivvitEnumLtype::VALUE_SOLEMANDATE
     * @uses TivvitEnumLtype::VALUE_OFFICELISTING
     * @uses TivvitEnumLtype::VALUE_GROUPLISTING
     * @uses TivvitEnumLtype::VALUE_OPPOSITIONMANDATE
     * @uses TivvitEnumLtype::VALUE_COUNTRYWIDE
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value, [self::VALUE_OPENMANDATE, self::VALUE_MLSMANDATE, self::VALUE_SHAREDMANDATE, self::VALUE_SOLEMANDATE, self::VALUE_OFFICELISTING, self::VALUE_GROUPLISTING, self::VALUE_OPPOSITIONMANDATE, self::VALUE_COUNTRYWIDE]);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
