<?php

namespace App\Http\Controllers\Tivvit\Remove\Property;

/**
 * File for class TivvitStructRemoveProperty.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructRemoveProperty originally named RemoveProperty
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructRemoveProperty extends TivvitWsdlClass
{
    /**
     * The proprem
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructPropertyRem
     */
    public $proprem;

    /**
     * Constructor method for RemoveProperty.
     * @see parent::__construct()
     * @param TivvitStructPropertyRem $_proprem
     * @return TivvitStructRemoveProperty
     */
    public function __construct($_proprem = null)
    {
        parent::__construct(['proprem' => $_proprem], false);
    }

    /**
     * Get proprem value.
     * @return TivvitStructPropertyRem|null
     */
    public function getProprem()
    {
        return $this->proprem;
    }

    /**
     * Set proprem value.
     * @param TivvitStructPropertyRem $_proprem the proprem
     * @return TivvitStructPropertyRem
     */
    public function setProprem($_proprem)
    {
        return $this->proprem = $_proprem;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructRemoveProperty
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
