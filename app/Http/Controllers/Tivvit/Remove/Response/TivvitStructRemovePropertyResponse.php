<?php

namespace App\Http\Controllers\Tivvit\Remove\Response;

/**
 * File for class TivvitStructRemovePropertyResponse.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructRemovePropertyResponse originally named RemovePropertyResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructRemovePropertyResponse extends TivvitWsdlClass
{
    /**
     * The RemovePropertyResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var TivvitStructTivvitResponse
     */
    public $RemovePropertyResult;

    /**
     * Constructor method for RemovePropertyResponse.
     * @see parent::__construct()
     * @param TivvitStructTivvitResponse $_removePropertyResult
     * @return TivvitStructRemovePropertyResponse
     */
    public function __construct($_removePropertyResult = null)
    {
        parent::__construct(['RemovePropertyResult' => $_removePropertyResult], false);
    }

    /**
     * Get RemovePropertyResult value.
     * @return TivvitStructTivvitResponse|null
     */
    public function getRemovePropertyResult()
    {
        return $this->RemovePropertyResult;
    }

    /**
     * Set RemovePropertyResult value.
     * @param TivvitStructTivvitResponse $_removePropertyResult the RemovePropertyResult
     * @return TivvitStructTivvitResponse
     */
    public function setRemovePropertyResult($_removePropertyResult)
    {
        return $this->RemovePropertyResult = $_removePropertyResult;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructRemovePropertyResponse
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
