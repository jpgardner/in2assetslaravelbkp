<?php

namespace App\Http\Controllers\Tivvit\Remove;

use App\Http\Controllers\Tivvit\TivvitWsdlClass;

/**
 * File for class TivvitServiceRemove.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitServiceRemove originally named Remove.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitServiceRemove extends TivvitWsdlClass
{
    /**
     * Sets the UserInfo SoapHeader param.
     * @uses TivvitWsdlClass::setSoapHeader()
     * @param TivvitStructUserInfo $_tivvitStructUserInfo
     * @param string $_nameSpace http://sandbox.tivvit.com/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderUserInfo(TivvitStructUserInfo $_tivvitStructUserInfo, $_nameSpace = '', $_mustUnderstand = false, $_actor = null)
    {
        $_nameSpace = TivvitWsdlClass::$WSDL_URL_DOMAIN;

        return $this->setSoapHeader($_nameSpace, 'UserInfo', $_tivvitStructUserInfo, $_mustUnderstand, $_actor);
    }

    /**
     * Method to call the operation originally named RemoveAgent
     * Documentation : Removal of Agent
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructRemoveAgent $_tivvitStructRemoveAgent
     * @return TivvitStructRemoveAgentResponse
     */
    public function RemoveAgent(TivvitStructRemoveAgent $_tivvitStructRemoveAgent)
    {
        try {
            return $this->setResult(self::getSoapClient()->RemoveAgent($_tivvitStructRemoveAgent));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Method to call the operation originally named RemoveProperty
     * Documentation : Removal of Agent
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : UserInfo
     * - SOAPHeaderNamespaces : http://sandbox.tivvit.com/
     * - SOAPHeaderTypes : {@link TivvitStructUserInfo}
     * - SOAPHeaders : required.
     * @uses TivvitWsdlClass::getSoapClient()
     * @uses TivvitWsdlClass::setResult()
     * @uses TivvitWsdlClass::saveLastError()
     * @param TivvitStructRemoveProperty $_tivvitStructRemoveProperty
     * @return TivvitStructRemovePropertyResponse
     */
    public function RemoveProperty(TivvitStructRemoveProperty $_tivvitStructRemoveProperty)
    {
        try {
            return $this->setResult(self::getSoapClient()->RemoveProperty($_tivvitStructRemoveProperty));
        } catch (SoapFault $soapFault) {
            return ! $this->saveLastError(__METHOD__, $soapFault);
        }
    }

    /**
     * Returns the result.
     * @see TivvitWsdlClass::getResult()
     * @return TivvitStructRemoveAgentResponse|TivvitStructRemovePropertyResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
