<?php

namespace App\Http\Controllers\Tivvit\Agent\Ind;

/**
 * File for class TivvitStructAgentInd.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructAgentInd originally named AgentInd
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructAgentInd extends TivvitWsdlClass
{
    /**
     * The name
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $name;

    /**
     * The surname
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $surname;

    /**
     * The phone
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $phone;

    /**
     * The email
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $email;

    /**
     * The cell
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $cell;

    /**
     * The agent
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $agent;

    /**
     * The pic
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pic;

    /**
     * Constructor method for AgentInd.
     * @see parent::__construct()
     * @param string $_name
     * @param string $_surname
     * @param string $_phone
     * @param string $_email
     * @param string $_cell
     * @param string $_agent
     * @param string $_pic
     * @return TivvitStructAgentInd
     */
    public function __construct($_name = null, $_surname = null, $_phone = null, $_email = null, $_cell = null, $_agent = null, $_pic = null)
    {
        parent::__construct(['name' => $_name, 'surname' => $_surname, 'phone' => $_phone, 'email' => $_email, 'cell' => $_cell, 'agent' => $_agent, 'pic' => $_pic], false);
    }

    /**
     * Get name value.
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name value.
     * @param string $_name the name
     * @return string
     */
    public function setName($_name)
    {
        return $this->name = $_name;
    }

    /**
     * Get surname value.
     * @return string|null
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set surname value.
     * @param string $_surname the surname
     * @return string
     */
    public function setSurname($_surname)
    {
        return $this->surname = $_surname;
    }

    /**
     * Get phone value.
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone value.
     * @param string $_phone the phone
     * @return string
     */
    public function setPhone($_phone)
    {
        return $this->phone = $_phone;
    }

    /**
     * Get email value.
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email value.
     * @param string $_email the email
     * @return string
     */
    public function setEmail($_email)
    {
        return $this->email = $_email;
    }

    /**
     * Get cell value.
     * @return string|null
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * Set cell value.
     * @param string $_cell the cell
     * @return string
     */
    public function setCell($_cell)
    {
        return $this->cell = $_cell;
    }

    /**
     * Get agent value.
     * @return string|null
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set agent value.
     * @param string $_agent the agent
     * @return string
     */
    public function setAgent($_agent)
    {
        return $this->agent = $_agent;
    }

    /**
     * Get pic value.
     * @return string|null
     */
    public function getPic()
    {
        return $this->pic;
    }

    /**
     * Set pic value.
     * @param string $_pic the pic
     * @return string
     */
    public function setPic($_pic)
    {
        return $this->pic = $_pic;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructAgentInd
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
