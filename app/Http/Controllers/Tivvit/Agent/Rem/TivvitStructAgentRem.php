<?php

namespace App\Http\Controllers\Tivvit\Agent\Rem;

/**
 * File for class TivvitStructAgentRem.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructAgentRem originally named AgentRem
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructAgentRem extends TivvitWsdlClass
{
    /**
     * The bid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bid;

    /**
     * The agent
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $agent;

    /**
     * The extref
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $extref;

    /**
     * Constructor method for AgentRem.
     * @see parent::__construct()
     * @param string $_bid
     * @param string $_agent
     * @param string $_extref
     * @return TivvitStructAgentRem
     */
    public function __construct($_bid = null, $_agent = null, $_extref = null)
    {
        parent::__construct(['bid' => $_bid, 'agent' => $_agent, 'extref' => $_extref], false);
    }

    /**
     * Get bid value.
     * @return string|null
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set bid value.
     * @param string $_bid the bid
     * @return string
     */
    public function setBid($_bid)
    {
        return $this->bid = $_bid;
    }

    /**
     * Get agent value.
     * @return string|null
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set agent value.
     * @param string $_agent the agent
     * @return string
     */
    public function setAgent($_agent)
    {
        return $this->agent = $_agent;
    }

    /**
     * Get extref value.
     * @return string|null
     */
    public function getExtref()
    {
        return $this->extref;
    }

    /**
     * Set extref value.
     * @param string $_extref the extref
     * @return string
     */
    public function setExtref($_extref)
    {
        return $this->extref = $_extref;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructAgentRem
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
