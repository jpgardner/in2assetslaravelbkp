<?php

namespace App\Http\Controllers\Tivvit\Agent;

/**
 * File for class TivvitStructAgent.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructAgent originally named Agent
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructAgent extends TivvitWsdlClass
{
    /**
     * The age
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var int
     */
    public $age;

    /**
     * The comsplitl
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $comsplitl;

    /**
     * The comsplits
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $comsplits;

    /**
     * The atax
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $atax;

    /**
     * The appdate
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var dateTime
     */
    public $appdate;

    /**
     * The bid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bid;

    /**
     * The agent
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $agent;

    /**
     * The extref
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $extref;

    /**
     * The notes
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $notes;

    /**
     * The name
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $name;

    /**
     * The surname
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $surname;

    /**
     * The phone
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $phone;

    /**
     * The cell
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $cell;

    /**
     * The area
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $area;

    /**
     * The fax
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $fax;

    /**
     * The email
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $email;

    /**
     * The address
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $address;

    /**
     * The picture
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $picture;

    /**
     * The idno
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $idno;

    /**
     * The fanum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $fanum;

    /**
     * The sex
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $sex;

    /**
     * The designation
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $designation;

    /**
     * The facebookpage
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $facebookpage;

    /**
     * The linkedin
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $linkedin;

    /**
     * The twitter
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $twitter;

    /**
     * Constructor method for Agent.
     * @see parent::__construct()
     * @param int $_age
     * @param decimal $_comsplitl
     * @param decimal $_comsplits
     * @param decimal $_atax
     * @param dateTime $_appdate
     * @param string $_bid
     * @param string $_agent
     * @param string $_extref
     * @param string $_notes
     * @param string $_name
     * @param string $_surname
     * @param string $_phone
     * @param string $_cell
     * @param string $_area
     * @param string $_fax
     * @param string $_email
     * @param string $_address
     * @param string $_picture
     * @param string $_idno
     * @param string $_fanum
     * @param string $_sex
     * @param string $_designation
     * @param string $_facebookpage
     * @param string $_linkedin
     * @param string $_twitter
     * @return TivvitStructAgent
     */
    public function __construct($_age, $_comsplitl, $_comsplits, $_atax, $_appdate, $_bid = null, $_agent = null, $_extref = null, $_notes = null, $_name = null, $_surname = null, $_phone = null, $_cell = null, $_area = null, $_fax = null, $_email = null, $_address = null, $_picture = null, $_idno = null, $_fanum = null, $_sex = null, $_designation = null, $_facebookpage = null, $_linkedin = null, $_twitter = null)
    {
        parent::__construct(['age' => $_age, 'comsplitl' => $_comsplitl, 'comsplits' => $_comsplits, 'atax' => $_atax, 'appdate' => $_appdate, 'bid' => $_bid, 'agent' => $_agent, 'extref' => $_extref, 'notes' => $_notes, 'name' => $_name, 'surname' => $_surname, 'phone' => $_phone, 'cell' => $_cell, 'area' => $_area, 'fax' => $_fax, 'email' => $_email, 'address' => $_address, 'picture' => $_picture, 'idno' => $_idno, 'fanum' => $_fanum, 'sex' => $_sex, 'designation' => $_designation, 'facebookpage' => $_facebookpage, 'linkedin' => $_linkedin, 'twitter' => $_twitter], false);
    }

    /**
     * Get age value.
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set age value.
     * @param int $_age the age
     * @return int
     */
    public function setAge($_age)
    {
        return $this->age = $_age;
    }

    /**
     * Get comsplitl value.
     * @return decimal
     */
    public function getComsplitl()
    {
        return $this->comsplitl;
    }

    /**
     * Set comsplitl value.
     * @param decimal $_comsplitl the comsplitl
     * @return decimal
     */
    public function setComsplitl($_comsplitl)
    {
        return $this->comsplitl = $_comsplitl;
    }

    /**
     * Get comsplits value.
     * @return decimal
     */
    public function getComsplits()
    {
        return $this->comsplits;
    }

    /**
     * Set comsplits value.
     * @param decimal $_comsplits the comsplits
     * @return decimal
     */
    public function setComsplits($_comsplits)
    {
        return $this->comsplits = $_comsplits;
    }

    /**
     * Get atax value.
     * @return decimal
     */
    public function getAtax()
    {
        return $this->atax;
    }

    /**
     * Set atax value.
     * @param decimal $_atax the atax
     * @return decimal
     */
    public function setAtax($_atax)
    {
        return $this->atax = $_atax;
    }

    /**
     * Get appdate value.
     * @return dateTime
     */
    public function getAppdate()
    {
        return $this->appdate;
    }

    /**
     * Set appdate value.
     * @param dateTime $_appdate the appdate
     * @return dateTime
     */
    public function setAppdate($_appdate)
    {
        return $this->appdate = $_appdate;
    }

    /**
     * Get bid value.
     * @return string|null
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set bid value.
     * @param string $_bid the bid
     * @return string
     */
    public function setBid($_bid)
    {
        return $this->bid = $_bid;
    }

    /**
     * Get agent value.
     * @return string|null
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set agent value.
     * @param string $_agent the agent
     * @return string
     */
    public function setAgent($_agent)
    {
        return $this->agent = $_agent;
    }

    /**
     * Get extref value.
     * @return string|null
     */
    public function getExtref()
    {
        return $this->extref;
    }

    /**
     * Set extref value.
     * @param string $_extref the extref
     * @return string
     */
    public function setExtref($_extref)
    {
        return $this->extref = $_extref;
    }

    /**
     * Get notes value.
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set notes value.
     * @param string $_notes the notes
     * @return string
     */
    public function setNotes($_notes)
    {
        return $this->notes = $_notes;
    }

    /**
     * Get name value.
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name value.
     * @param string $_name the name
     * @return string
     */
    public function setName($_name)
    {
        return $this->name = $_name;
    }

    /**
     * Get surname value.
     * @return string|null
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set surname value.
     * @param string $_surname the surname
     * @return string
     */
    public function setSurname($_surname)
    {
        return $this->surname = $_surname;
    }

    /**
     * Get phone value.
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone value.
     * @param string $_phone the phone
     * @return string
     */
    public function setPhone($_phone)
    {
        return $this->phone = $_phone;
    }

    /**
     * Get cell value.
     * @return string|null
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * Set cell value.
     * @param string $_cell the cell
     * @return string
     */
    public function setCell($_cell)
    {
        return $this->cell = $_cell;
    }

    /**
     * Get area value.
     * @return string|null
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set area value.
     * @param string $_area the area
     * @return string
     */
    public function setArea($_area)
    {
        return $this->area = $_area;
    }

    /**
     * Get fax value.
     * @return string|null
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set fax value.
     * @param string $_fax the fax
     * @return string
     */
    public function setFax($_fax)
    {
        return $this->fax = $_fax;
    }

    /**
     * Get email value.
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email value.
     * @param string $_email the email
     * @return string
     */
    public function setEmail($_email)
    {
        return $this->email = $_email;
    }

    /**
     * Get address value.
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address value.
     * @param string $_address the address
     * @return string
     */
    public function setAddress($_address)
    {
        return $this->address = $_address;
    }

    /**
     * Get picture value.
     * @return string|null
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set picture value.
     * @param string $_picture the picture
     * @return string
     */
    public function setPicture($_picture)
    {
        return $this->picture = $_picture;
    }

    /**
     * Get idno value.
     * @return string|null
     */
    public function getIdno()
    {
        return $this->idno;
    }

    /**
     * Set idno value.
     * @param string $_idno the idno
     * @return string
     */
    public function setIdno($_idno)
    {
        return $this->idno = $_idno;
    }

    /**
     * Get fanum value.
     * @return string|null
     */
    public function getFanum()
    {
        return $this->fanum;
    }

    /**
     * Set fanum value.
     * @param string $_fanum the fanum
     * @return string
     */
    public function setFanum($_fanum)
    {
        return $this->fanum = $_fanum;
    }

    /**
     * Get sex value.
     * @return string|null
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set sex value.
     * @param string $_sex the sex
     * @return string
     */
    public function setSex($_sex)
    {
        return $this->sex = $_sex;
    }

    /**
     * Get designation value.
     * @return string|null
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set designation value.
     * @param string $_designation the designation
     * @return string
     */
    public function setDesignation($_designation)
    {
        return $this->designation = $_designation;
    }

    /**
     * Get facebookpage value.
     * @return string|null
     */
    public function getFacebookpage()
    {
        return $this->facebookpage;
    }

    /**
     * Set facebookpage value.
     * @param string $_facebookpage the facebookpage
     * @return string
     */
    public function setFacebookpage($_facebookpage)
    {
        return $this->facebookpage = $_facebookpage;
    }

    /**
     * Get linkedin value.
     * @return string|null
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Set linkedin value.
     * @param string $_linkedin the linkedin
     * @return string
     */
    public function setLinkedin($_linkedin)
    {
        return $this->linkedin = $_linkedin;
    }

    /**
     * Get twitter value.
     * @return string|null
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set twitter value.
     * @param string $_twitter the twitter
     * @return string
     */
    public function setTwitter($_twitter)
    {
        return $this->twitter = $_twitter;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructAgent
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
