<?php

namespace App\Http\Controllers\Tivvit\Picture;

/**
 * File for class TivvitStructPicture.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructPicture originally named Picture
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructPicture extends TivvitWsdlClass
{
    /**
     * The num
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $num;

    /**
     * The url
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $url;

    /**
     * The desc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $desc;

    /**
     * Constructor method for Picture.
     * @see parent::__construct()
     * @param string $_num
     * @param string $_url
     * @param string $_desc
     * @return TivvitStructPicture
     */
    public function __construct($_num = null, $_url = null, $_desc = null)
    {
        parent::__construct(['num' => $_num, 'url' => $_url, 'desc' => $_desc], false);
    }

    /**
     * Get num value.
     * @return string|null
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set num value.
     * @param string $_num the num
     * @return string
     */
    public function setNum($_num)
    {
        return $this->num = $_num;
    }

    /**
     * Get url value.
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set url value.
     * @param string $_url the url
     * @return string
     */
    public function setUrl($_url)
    {
        return $this->url = $_url;
    }

    /**
     * Get desc value.
     * @return string|null
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set desc value.
     * @param string $_desc the desc
     * @return string
     */
    public function setDesc($_desc)
    {
        return $this->desc = $_desc;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructPicture
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
