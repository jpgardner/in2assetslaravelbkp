<?php

namespace App\Http\Controllers\Tivvit\Rentper;

/**
 * File for class TivvitEnumRentper.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitEnumRentper originally named Rentper
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitEnumRentper extends TivvitWsdlClass
{
    /**
     * Constant for value 'NA'.
     * @return string 'NA'
     */
    const VALUE_NA = 'NA';

    /**
     * Constant for value 'PM'.
     * @return string 'PM'
     */
    const VALUE_PM = 'PM';

    /**
     * Constant for value 'PA'.
     * @return string 'PA'
     */
    const VALUE_PA = 'PA';

    /**
     * Constant for value 'PW'.
     * @return string 'PW'
     */
    const VALUE_PW = 'PW';

    /**
     * Constant for value 'PD'.
     * @return string 'PD'
     */
    const VALUE_PD = 'PD';

    /**
     * Constant for value 'SQ'.
     * @return string 'SQ'
     */
    const VALUE_SQ = 'SQ';

    /**
     * Return true if value is allowed.
     * @uses TivvitEnumRentper::VALUE_NA
     * @uses TivvitEnumRentper::VALUE_PM
     * @uses TivvitEnumRentper::VALUE_PA
     * @uses TivvitEnumRentper::VALUE_PW
     * @uses TivvitEnumRentper::VALUE_PD
     * @uses TivvitEnumRentper::VALUE_SQ
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value, [self::VALUE_NA, self::VALUE_PM, self::VALUE_PA, self::VALUE_PW, self::VALUE_PD, self::VALUE_SQ]);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
