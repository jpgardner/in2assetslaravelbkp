<?php

namespace App\Http\Controllers\Tivvit\Search\Type;

/**
 * File for class TivvitEnumSearchType.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitEnumSearchType originally named SearchType
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitEnumSearchType extends TivvitWsdlClass
{
    /**
     * Constant for value 'PropertyNum'.
     * @return string 'PropertyNum'
     */
    const VALUE_PROPERTYNUM = 'PropertyNum';

    /**
     * Constant for value 'WebRef'.
     * @return string 'WebRef'
     */
    const VALUE_WEBREF = 'WebRef';

    /**
     * Constant for value 'P24'.
     * @return string 'P24'
     */
    const VALUE_P24 = 'P24';

    /**
     * Constant for value 'PP'.
     * @return string 'PP'
     */
    const VALUE_PP = 'PP';

    /**
     * Return true if value is allowed.
     * @uses TivvitEnumSearchType::VALUE_PROPERTYNUM
     * @uses TivvitEnumSearchType::VALUE_WEBREF
     * @uses TivvitEnumSearchType::VALUE_P24
     * @uses TivvitEnumSearchType::VALUE_PP
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value, [self::VALUE_PROPERTYNUM, self::VALUE_WEBREF, self::VALUE_P24, self::VALUE_PP]);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
