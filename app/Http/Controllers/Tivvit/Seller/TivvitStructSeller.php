<?php

namespace App\Http\Controllers\Tivvit\Seller;

/**
 * File for class TivvitStructSeller.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructSeller originally named Seller
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructSeller extends TivvitWsdlClass
{
    /**
     * The title
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $title;

    /**
     * The name
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $name;

    /**
     * The surname
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $surname;

    /**
     * The telephone
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $telephone;

    /**
     * The tel2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $tel2;

    /**
     * The cell
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $cell;

    /**
     * The cell2
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $cell2;

    /**
     * The bid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $bid;

    /**
     * The cnum
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $cnum;

    /**
     * Constructor method for Seller.
     * @see parent::__construct()
     * @param string $_title
     * @param string $_name
     * @param string $_surname
     * @param string $_telephone
     * @param string $_tel2
     * @param string $_cell
     * @param string $_cell2
     * @param string $_bid
     * @param string $_cnum
     * @return TivvitStructSeller
     */
    public function __construct($_title = null, $_name = null, $_surname = null, $_telephone = null, $_tel2 = null, $_cell = null, $_cell2 = null, $_bid = null, $_cnum = null)
    {
        parent::__construct(['title' => $_title, 'name' => $_name, 'surname' => $_surname, 'telephone' => $_telephone, 'tel2' => $_tel2, 'cell' => $_cell, 'cell2' => $_cell2, 'bid' => $_bid, 'cnum' => $_cnum], false);
    }

    /**
     * Get title value.
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title value.
     * @param string $_title the title
     * @return string
     */
    public function setTitle($_title)
    {
        return $this->title = $_title;
    }

    /**
     * Get name value.
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name value.
     * @param string $_name the name
     * @return string
     */
    public function setName($_name)
    {
        return $this->name = $_name;
    }

    /**
     * Get surname value.
     * @return string|null
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set surname value.
     * @param string $_surname the surname
     * @return string
     */
    public function setSurname($_surname)
    {
        return $this->surname = $_surname;
    }

    /**
     * Get telephone value.
     * @return string|null
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set telephone value.
     * @param string $_telephone the telephone
     * @return string
     */
    public function setTelephone($_telephone)
    {
        return $this->telephone = $_telephone;
    }

    /**
     * Get tel2 value.
     * @return string|null
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set tel2 value.
     * @param string $_tel2 the tel2
     * @return string
     */
    public function setTel2($_tel2)
    {
        return $this->tel2 = $_tel2;
    }

    /**
     * Get cell value.
     * @return string|null
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * Set cell value.
     * @param string $_cell the cell
     * @return string
     */
    public function setCell($_cell)
    {
        return $this->cell = $_cell;
    }

    /**
     * Get cell2 value.
     * @return string|null
     */
    public function getCell2()
    {
        return $this->cell2;
    }

    /**
     * Set cell2 value.
     * @param string $_cell2 the cell2
     * @return string
     */
    public function setCell2($_cell2)
    {
        return $this->cell2 = $_cell2;
    }

    /**
     * Get bid value.
     * @return string|null
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set bid value.
     * @param string $_bid the bid
     * @return string
     */
    public function setBid($_bid)
    {
        return $this->bid = $_bid;
    }

    /**
     * Get cnum value.
     * @return string|null
     */
    public function getCnum()
    {
        return $this->cnum;
    }

    /**
     * Set cnum value.
     * @param string $_cnum the cnum
     * @return string
     */
    public function setCnum($_cnum)
    {
        return $this->cnum = $_cnum;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructSeller
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
