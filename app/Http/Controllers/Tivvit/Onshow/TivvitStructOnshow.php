<?php

namespace App\Http\Controllers\Tivvit\Onshow;

/**
 * File for class TivvitStructOnshow.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructOnshow originally named Onshow
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructOnshow extends TivvitWsdlClass
{
    /**
     * The showd
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var dateTime
     */
    public $showd;

    /**
     * The todate
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * - nillable : true.
     * @var dateTime
     */
    public $todate;

    /**
     * The notes
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $notes;

    /**
     * The StartTime
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $StartTime;

    /**
     * The EndTime
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $EndTime;

    /**
     * Constructor method for Onshow.
     * @see parent::__construct()
     * @param dateTime $_showd
     * @param dateTime $_todate
     * @param string $_notes
     * @param string $_startTime
     * @param string $_endTime
     * @return TivvitStructOnshow
     */
    public function __construct($_showd, $_todate, $_notes = null, $_startTime = null, $_endTime = null)
    {
        parent::__construct(['showd' => $_showd, 'todate' => $_todate, 'notes' => $_notes, 'StartTime' => $_startTime, 'EndTime' => $_endTime], false);
    }

    /**
     * Get showd value.
     * @return dateTime
     */
    public function getShowd()
    {
        return $this->showd;
    }

    /**
     * Set showd value.
     * @param dateTime $_showd the showd
     * @return dateTime
     */
    public function setShowd($_showd)
    {
        return $this->showd = $_showd;
    }

    /**
     * Get todate value.
     * @return dateTime
     */
    public function getTodate()
    {
        return $this->todate;
    }

    /**
     * Set todate value.
     * @param dateTime $_todate the todate
     * @return dateTime
     */
    public function setTodate($_todate)
    {
        return $this->todate = $_todate;
    }

    /**
     * Get notes value.
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set notes value.
     * @param string $_notes the notes
     * @return string
     */
    public function setNotes($_notes)
    {
        return $this->notes = $_notes;
    }

    /**
     * Get StartTime value.
     * @return string|null
     */
    public function getStartTime()
    {
        return $this->StartTime;
    }

    /**
     * Set StartTime value.
     * @param string $_startTime the StartTime
     * @return string
     */
    public function setStartTime($_startTime)
    {
        return $this->StartTime = $_startTime;
    }

    /**
     * Get EndTime value.
     * @return string|null
     */
    public function getEndTime()
    {
        return $this->EndTime;
    }

    /**
     * Set EndTime value.
     * @param string $_endTime the EndTime
     * @return string
     */
    public function setEndTime($_endTime)
    {
        return $this->EndTime = $_endTime;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructOnshow
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
