<?php

namespace App\Http\Controllers\Tivvit\Array\Onshow;

/**
 * File for class TivvitStructArrayOfOnshow.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructArrayOfOnshow originally named ArrayOfOnshow
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructArrayOfOnshow extends TivvitWsdlClass
{
    /**
     * The Onshow
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true.
     * @var TivvitStructOnshow
     */
    public $Onshow;

    /**
     * Constructor method for ArrayOfOnshow.
     * @see parent::__construct()
     * @param TivvitStructOnshow $_onshow
     * @return TivvitStructArrayOfOnshow
     */
    public function __construct($_onshow = null)
    {
        parent::__construct(['Onshow' => $_onshow], false);
    }

    /**
     * Get Onshow value.
     * @return TivvitStructOnshow|null
     */
    public function getOnshow()
    {
        return $this->Onshow;
    }

    /**
     * Set Onshow value.
     * @param TivvitStructOnshow $_onshow the Onshow
     * @return TivvitStructOnshow
     */
    public function setOnshow($_onshow)
    {
        return $this->Onshow = $_onshow;
    }

    /**
     * Returns the current element.
     * @see TivvitWsdlClass::current()
     * @return TivvitStructOnshow
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Returns the indexed element.
     * @see TivvitWsdlClass::item()
     * @param int $_index
     * @return TivvitStructOnshow
     */
    public function item($_index)
    {
        return parent::item($_index);
    }

    /**
     * Returns the first element.
     * @see TivvitWsdlClass::first()
     * @return TivvitStructOnshow
     */
    public function first()
    {
        return parent::first();
    }

    /**
     * Returns the last element.
     * @see TivvitWsdlClass::last()
     * @return TivvitStructOnshow
     */
    public function last()
    {
        return parent::last();
    }

    /**
     * Returns the element at the offset.
     * @see TivvitWsdlClass::last()
     * @param int $_offset
     * @return TivvitStructOnshow
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }

    /**
     * Returns the attribute name.
     * @see TivvitWsdlClass::getAttributeName()
     * @return string Onshow
     */
    public function getAttributeName()
    {
        return 'Onshow';
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructArrayOfOnshow
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
