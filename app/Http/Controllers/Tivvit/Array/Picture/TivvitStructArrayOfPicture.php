<?php

namespace App\Http\Controllers\Tivvit\Array\Picture;

/**
 * File for class TivvitStructArrayOfPicture.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructArrayOfPicture originally named ArrayOfPicture
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructArrayOfPicture extends TivvitWsdlClass
{
    /**
     * The Picture
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true.
     * @var TivvitStructPicture
     */
    public $Picture;

    /**
     * Constructor method for ArrayOfPicture.
     * @see parent::__construct()
     * @param TivvitStructPicture $_picture
     * @return TivvitStructArrayOfPicture
     */
    public function __construct($_picture = null)
    {
        parent::__construct(['Picture' => $_picture], false);
    }

    /**
     * Get Picture value.
     * @return TivvitStructPicture|null
     */
    public function getPicture()
    {
        return $this->Picture;
    }

    /**
     * Set Picture value.
     * @param TivvitStructPicture $_picture the Picture
     * @return TivvitStructPicture
     */
    public function setPicture($_picture)
    {
        return $this->Picture = $_picture;
    }

    /**
     * Returns the current element.
     * @see TivvitWsdlClass::current()
     * @return TivvitStructPicture
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Returns the indexed element.
     * @see TivvitWsdlClass::item()
     * @param int $_index
     * @return TivvitStructPicture
     */
    public function item($_index)
    {
        return parent::item($_index);
    }

    /**
     * Returns the first element.
     * @see TivvitWsdlClass::first()
     * @return TivvitStructPicture
     */
    public function first()
    {
        return parent::first();
    }

    /**
     * Returns the last element.
     * @see TivvitWsdlClass::last()
     * @return TivvitStructPicture
     */
    public function last()
    {
        return parent::last();
    }

    /**
     * Returns the element at the offset.
     * @see TivvitWsdlClass::last()
     * @param int $_offset
     * @return TivvitStructPicture
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }

    /**
     * Returns the attribute name.
     * @see TivvitWsdlClass::getAttributeName()
     * @return string Picture
     */
    public function getAttributeName()
    {
        return 'Picture';
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructArrayOfPicture
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
