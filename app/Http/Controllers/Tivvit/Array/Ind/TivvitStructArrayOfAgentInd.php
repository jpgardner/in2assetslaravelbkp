<?php

namespace App\Http\Controllers\Tivvit\Array\Ind;

/**
 * File for class TivvitStructArrayOfAgentInd.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructArrayOfAgentInd originally named ArrayOfAgentInd
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructArrayOfAgentInd extends TivvitWsdlClass
{
    /**
     * The AgentInd
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true.
     * @var TivvitStructAgentInd
     */
    public $AgentInd;

    /**
     * Constructor method for ArrayOfAgentInd.
     * @see parent::__construct()
     * @param TivvitStructAgentInd $_agentInd
     * @return TivvitStructArrayOfAgentInd
     */
    public function __construct($_agentInd = null)
    {
        parent::__construct(['AgentInd' => $_agentInd], false);
    }

    /**
     * Get AgentInd value.
     * @return TivvitStructAgentInd|null
     */
    public function getAgentInd()
    {
        return $this->AgentInd;
    }

    /**
     * Set AgentInd value.
     * @param TivvitStructAgentInd $_agentInd the AgentInd
     * @return TivvitStructAgentInd
     */
    public function setAgentInd($_agentInd)
    {
        return $this->AgentInd = $_agentInd;
    }

    /**
     * Returns the current element.
     * @see TivvitWsdlClass::current()
     * @return TivvitStructAgentInd
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Returns the indexed element.
     * @see TivvitWsdlClass::item()
     * @param int $_index
     * @return TivvitStructAgentInd
     */
    public function item($_index)
    {
        return parent::item($_index);
    }

    /**
     * Returns the first element.
     * @see TivvitWsdlClass::first()
     * @return TivvitStructAgentInd
     */
    public function first()
    {
        return parent::first();
    }

    /**
     * Returns the last element.
     * @see TivvitWsdlClass::last()
     * @return TivvitStructAgentInd
     */
    public function last()
    {
        return parent::last();
    }

    /**
     * Returns the element at the offset.
     * @see TivvitWsdlClass::last()
     * @param int $_offset
     * @return TivvitStructAgentInd
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }

    /**
     * Returns the attribute name.
     * @see TivvitWsdlClass::getAttributeName()
     * @return string AgentInd
     */
    public function getAttributeName()
    {
        return 'AgentInd';
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructArrayOfAgentInd
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
