<?php

namespace App\Http\Controllers\Tivvit\Array\Seller;

/**
 * File for class TivvitStructArrayOfSeller.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructArrayOfSeller originally named ArrayOfSeller
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructArrayOfSeller extends TivvitWsdlClass
{
    /**
     * The Seller
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true.
     * @var TivvitStructSeller
     */
    public $Seller;

    /**
     * Constructor method for ArrayOfSeller.
     * @see parent::__construct()
     * @param TivvitStructSeller $_seller
     * @return TivvitStructArrayOfSeller
     */
    public function __construct($_seller = null)
    {
        parent::__construct(['Seller' => $_seller], false);
    }

    /**
     * Get Seller value.
     * @return TivvitStructSeller|null
     */
    public function getSeller()
    {
        return $this->Seller;
    }

    /**
     * Set Seller value.
     * @param TivvitStructSeller $_seller the Seller
     * @return TivvitStructSeller
     */
    public function setSeller($_seller)
    {
        return $this->Seller = $_seller;
    }

    /**
     * Returns the current element.
     * @see TivvitWsdlClass::current()
     * @return TivvitStructSeller
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Returns the indexed element.
     * @see TivvitWsdlClass::item()
     * @param int $_index
     * @return TivvitStructSeller
     */
    public function item($_index)
    {
        return parent::item($_index);
    }

    /**
     * Returns the first element.
     * @see TivvitWsdlClass::first()
     * @return TivvitStructSeller
     */
    public function first()
    {
        return parent::first();
    }

    /**
     * Returns the last element.
     * @see TivvitWsdlClass::last()
     * @return TivvitStructSeller
     */
    public function last()
    {
        return parent::last();
    }

    /**
     * Returns the element at the offset.
     * @see TivvitWsdlClass::last()
     * @param int $_offset
     * @return TivvitStructSeller
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }

    /**
     * Returns the attribute name.
     * @see TivvitWsdlClass::getAttributeName()
     * @return string Seller
     */
    public function getAttributeName()
    {
        return 'Seller';
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructArrayOfSeller
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
