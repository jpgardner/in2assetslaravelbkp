<?php

namespace App\Http\Controllers\Tivvit\Array\Rooms;

/**
 * File for class TivvitStructArrayOfRooms.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructArrayOfRooms originally named ArrayOfRooms
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructArrayOfRooms extends TivvitWsdlClass
{
    /**
     * The Rooms
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true.
     * @var TivvitStructRooms
     */
    public $Rooms;

    /**
     * Constructor method for ArrayOfRooms.
     * @see parent::__construct()
     * @param TivvitStructRooms $_rooms
     * @return TivvitStructArrayOfRooms
     */
    public function __construct($_rooms = null)
    {
        parent::__construct(['Rooms' => $_rooms], false);
    }

    /**
     * Get Rooms value.
     * @return TivvitStructRooms|null
     */
    public function getRooms()
    {
        return $this->Rooms;
    }

    /**
     * Set Rooms value.
     * @param TivvitStructRooms $_rooms the Rooms
     * @return TivvitStructRooms
     */
    public function setRooms($_rooms)
    {
        return $this->Rooms = $_rooms;
    }

    /**
     * Returns the current element.
     * @see TivvitWsdlClass::current()
     * @return TivvitStructRooms
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Returns the indexed element.
     * @see TivvitWsdlClass::item()
     * @param int $_index
     * @return TivvitStructRooms
     */
    public function item($_index)
    {
        return parent::item($_index);
    }

    /**
     * Returns the first element.
     * @see TivvitWsdlClass::first()
     * @return TivvitStructRooms
     */
    public function first()
    {
        return parent::first();
    }

    /**
     * Returns the last element.
     * @see TivvitWsdlClass::last()
     * @return TivvitStructRooms
     */
    public function last()
    {
        return parent::last();
    }

    /**
     * Returns the element at the offset.
     * @see TivvitWsdlClass::last()
     * @param int $_offset
     * @return TivvitStructRooms
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }

    /**
     * Returns the attribute name.
     * @see TivvitWsdlClass::getAttributeName()
     * @return string Rooms
     */
    public function getAttributeName()
    {
        return 'Rooms';
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructArrayOfRooms
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
