<?php

namespace App\Http\Controllers\Tivvit\Array\Features;

/**
 * File for class TivvitStructArrayOfFeatures.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructArrayOfFeatures originally named ArrayOfFeatures
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructArrayOfFeatures extends TivvitWsdlClass
{
    /**
     * The Features
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true.
     * @var TivvitStructFeatures
     */
    public $Features;

    /**
     * Constructor method for ArrayOfFeatures.
     * @see parent::__construct()
     * @param TivvitStructFeatures $_features
     * @return TivvitStructArrayOfFeatures
     */
    public function __construct($_features = null)
    {
        parent::__construct(['Features' => $_features], false);
    }

    /**
     * Get Features value.
     * @return TivvitStructFeatures|null
     */
    public function getFeatures()
    {
        return $this->Features;
    }

    /**
     * Set Features value.
     * @param TivvitStructFeatures $_features the Features
     * @return TivvitStructFeatures
     */
    public function setFeatures($_features)
    {
        return $this->Features = $_features;
    }

    /**
     * Returns the current element.
     * @see TivvitWsdlClass::current()
     * @return TivvitStructFeatures
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * Returns the indexed element.
     * @see TivvitWsdlClass::item()
     * @param int $_index
     * @return TivvitStructFeatures
     */
    public function item($_index)
    {
        return parent::item($_index);
    }

    /**
     * Returns the first element.
     * @see TivvitWsdlClass::first()
     * @return TivvitStructFeatures
     */
    public function first()
    {
        return parent::first();
    }

    /**
     * Returns the last element.
     * @see TivvitWsdlClass::last()
     * @return TivvitStructFeatures
     */
    public function last()
    {
        return parent::last();
    }

    /**
     * Returns the element at the offset.
     * @see TivvitWsdlClass::last()
     * @param int $_offset
     * @return TivvitStructFeatures
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }

    /**
     * Returns the attribute name.
     * @see TivvitWsdlClass::getAttributeName()
     * @return string Features
     */
    public function getAttributeName()
    {
        return 'Features';
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructArrayOfFeatures
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
