<?php

namespace App\Http\Controllers\Tivvit\Rooms;

use App\Http\Controllers\Tivvit\Room\Type\TivvitEnumRoomType;

/**
 * File for class TivvitStructRooms.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructRooms originally named Rooms
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructRooms extends TivvitWsdlClass
{
    /**
     * The type
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var TivvitEnumRoomType
     */
    public $type;

    /**
     * The name
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $name;

    /**
     * The desc
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $desc;

    /**
     * The pk
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $pk;

    /**
     * Constructor method for Rooms.
     * @see parent::__construct()
     * @param TivvitEnumRoomType $_type
     * @param string $_name
     * @param string $_desc
     * @param string $_pk
     * @return TivvitStructRooms
     */
    public function __construct($_type, $_name = null, $_desc = null, $_pk = null)
    {
        parent::__construct(['type' => $_type, 'name' => $_name, 'desc' => $_desc, 'pk' => $_pk], false);
    }

    /**
     * Get type value.
     * @return TivvitEnumRoomType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type value.
     * @uses TivvitEnumRoomType::valueIsValid()
     * @param TivvitEnumRoomType $_type the type
     * @return TivvitEnumRoomType
     */
    public function setType($_type)
    {
        if (! TivvitEnumRoomType::valueIsValid($_type)) {
            return false;
        }

        return $this->type = $_type;
    }

    /**
     * Get name value.
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name value.
     * @param string $_name the name
     * @return string
     */
    public function setName($_name)
    {
        return $this->name = $_name;
    }

    /**
     * Get desc value.
     * @return string|null
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set desc value.
     * @param string $_desc the desc
     * @return string
     */
    public function setDesc($_desc)
    {
        return $this->desc = $_desc;
    }

    /**
     * Get pk value.
     * @return string|null
     */
    public function getPk()
    {
        return $this->pk;
    }

    /**
     * Set pk value.
     * @param string $_pk the pk
     * @return string
     */
    public function setPk($_pk)
    {
        return $this->pk = $_pk;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructRooms
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
