<?php

namespace App\Http\Controllers\Tivvit\Ptype;

/**
 * File for class TivvitEnumPtype.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitEnumPtype originally named Ptype
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitEnumPtype extends TivvitWsdlClass
{
    /**
     * Constant for value 'ForSale'.
     * @return string 'ForSale'
     */
    const VALUE_FORSALE = 'ForSale';

    /**
     * Constant for value 'ToRent'.
     * @return string 'ToRent'
     */
    const VALUE_TORENT = 'ToRent';

    /**
     * Constant for value 'Both'.
     * @return string 'Both'
     */
    const VALUE_BOTH = 'Both';

    /**
     * Constant for value 'NotListed'.
     * @return string 'NotListed'
     */
    const VALUE_NOTLISTED = 'NotListed';

    /**
     * Return true if value is allowed.
     * @uses TivvitEnumPtype::VALUE_FORSALE
     * @uses TivvitEnumPtype::VALUE_TORENT
     * @uses TivvitEnumPtype::VALUE_BOTH
     * @uses TivvitEnumPtype::VALUE_NOTLISTED
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value, [self::VALUE_FORSALE, self::VALUE_TORENT, self::VALUE_BOTH, self::VALUE_NOTLISTED]);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
