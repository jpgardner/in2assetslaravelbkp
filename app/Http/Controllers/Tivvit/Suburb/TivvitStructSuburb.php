<?php

namespace App\Http\Controllers\Tivvit\Suburb;

/**
 * File for class TivvitStructSuburb.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */

/**
 * This class stands for TivvitStructSuburb originally named Suburb
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://sandbox.tivvit.com/TivvitServices.asmx?WSDL}.
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2015-10-19
 */
class TivvitStructSuburb extends TivvitWsdlClass
{
    /**
     * The uniid
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $uniid;

    /**
     * The longitude
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $longitude;

    /**
     * The latitude
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1.
     * @var decimal
     */
    public $latitude;

    /**
     * The country
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $country;

    /**
     * The province
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $province;

    /**
     * The city
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $city;

    /**
     * The suburb
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $suburb;

    /**
     * The orig_province
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0.
     * @var string
     */
    public $orig_province;

    /**
     * Constructor method for Suburb.
     * @see parent::__construct()
     * @param decimal $_uniid
     * @param decimal $_longitude
     * @param decimal $_latitude
     * @param string $_country
     * @param string $_province
     * @param string $_city
     * @param string $_suburb
     * @param string $_orig_province
     * @return TivvitStructSuburb
     */
    public function __construct($_uniid, $_longitude, $_latitude, $_country = null, $_province = null, $_city = null, $_suburb = null, $_orig_province = null)
    {
        parent::__construct(['uniid' => $_uniid, 'longitude' => $_longitude, 'latitude' => $_latitude, 'country' => $_country, 'province' => $_province, 'city' => $_city, 'suburb' => $_suburb, 'orig_province' => $_orig_province], false);
    }

    /**
     * Get uniid value.
     * @return decimal
     */
    public function getUniid()
    {
        return $this->uniid;
    }

    /**
     * Set uniid value.
     * @param decimal $_uniid the uniid
     * @return decimal
     */
    public function setUniid($_uniid)
    {
        return $this->uniid = $_uniid;
    }

    /**
     * Get longitude value.
     * @return decimal
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set longitude value.
     * @param decimal $_longitude the longitude
     * @return decimal
     */
    public function setLongitude($_longitude)
    {
        return $this->longitude = $_longitude;
    }

    /**
     * Get latitude value.
     * @return decimal
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set latitude value.
     * @param decimal $_latitude the latitude
     * @return decimal
     */
    public function setLatitude($_latitude)
    {
        return $this->latitude = $_latitude;
    }

    /**
     * Get country value.
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country value.
     * @param string $_country the country
     * @return string
     */
    public function setCountry($_country)
    {
        return $this->country = $_country;
    }

    /**
     * Get province value.
     * @return string|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set province value.
     * @param string $_province the province
     * @return string
     */
    public function setProvince($_province)
    {
        return $this->province = $_province;
    }

    /**
     * Get city value.
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city value.
     * @param string $_city the city
     * @return string
     */
    public function setCity($_city)
    {
        return $this->city = $_city;
    }

    /**
     * Get suburb value.
     * @return string|null
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set suburb value.
     * @param string $_suburb the suburb
     * @return string
     */
    public function setSuburb($_suburb)
    {
        return $this->suburb = $_suburb;
    }

    /**
     * Get orig_province value.
     * @return string|null
     */
    public function getOrig_province()
    {
        return $this->orig_province;
    }

    /**
     * Set orig_province value.
     * @param string $_orig_province the orig_province
     * @return string
     */
    public function setOrig_province($_orig_province)
    {
        return $this->orig_province = $_orig_province;
    }

    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values.
     * @see  TivvitWsdlClass::__set_state()
     * @uses TivvitWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return TivvitStructSuburb
     */
    public static function __set_state(array $_array, $_className = __CLASS__)
    {
        return parent::__set_state($_array, $_className);
    }

    /**
     * Method returning the class name.
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
