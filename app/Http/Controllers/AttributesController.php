<?php

namespace App\Http\Controllers;

use App\Models\Attributes;

/**
 * Created by PhpStorm.
 * User: Erich
 * Date: 2016/02/12
 * Time: 9:23 AM.
 */
class AttributesController extends Controller
{
    public function __construct(Attributes $attributes)
    {
        $this->attributes = $attributes;
    }

    public function getAttributesByType($type)
    {
        return $this->attributes->whereType($type)->get();
    }

    public function getAttributesByName($name)
    {
        return $this->attributes->whereName($name)->get();
    }
}
