<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use In2Assets\Mailers\UserMailer;
use In2Assets\Notify\Notify;
use In2Assets\Users\User;
use Laracasts\Flash\Flash;

class ClassifiedsController extends Controller
{
    public function __construct(Classified $Classified)
    {
        $this->classified = $Classified;
    }

    /**
     * Display a listing of the resource.
     * GET /classifieds.
     *
     * @return Response
     */
    public function index()
    {
        //
        $categories = DB::table('classifieds_category')->groupBy('category')->get();
        $sub_categories = DB::table('classifieds_category')->groupBy('sub_category')->get();
        $provinces = DB::table('areas')->groupBy('province')->get();
        $areas = DB::table('areas')->groupBy('area')->get();
        $cities = DB::table('areas')->groupBy('suburb_town_city')->get();

        $classifieds = $this->classified->whereStatus('live')->orWhere('status', '=', 'sold')->orderBy('updated_at')->simplePaginate(20);

        return view('classifieds.index', ['classifieds' => $classifieds, 'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    public function search()
    {
        $input = Request::all();

        if ($input['province'] != 'south-africa') {
            $province = '%'.$input['province'];
        } else {
            $province = '%';
        }
        if ($input['suburb_town_city'] != 'all-cities') {
            $suburb_town_city = '%'.$input['suburb_town_city'];
        } else {
            $suburb_town_city = '%';
        }
        if ($input['area'] != 'all-suburbs') {
            $area = '%'.$input['area'];
        } else {
            $area = '%';
        }
        if ($input['category'] != '') {
            $category = '%'.$input['category'];
        } else {
            $category = '%';
        }
        if ($input['sub_category'] != '') {
            $sub_category = '%'.$input['sub_category'];
        } else {
            $sub_category = '%';
        }

        $classifieds = $this->classified->whereStatus('live')
            ->where('province', 'like', $province)
            ->where('suburb_town_city', 'like', $suburb_town_city)
            ->where('area', 'like', $area)
            ->where('category', 'like', $category)
            ->where('sub_category', 'like', $sub_category)
            ->orderBy('updated_at')->get();

        //         $queries = DB::getQueryLog();
        // $query = end($queries);
        // de($query);

        $categories = DB::table('classifieds_category')->groupBy('category')->get();
        $sub_categories = DB::table('classifieds_category')->groupBy('sub_category')->get();
        $provinces = DB::table('areas')->groupBy('province')->get();
        $areas = DB::table('areas')->groupBy('area')->get();
        $cities = DB::table('areas')->groupBy('suburb_town_city')->get();

        return view('classifieds.index', ['classifieds' => $classifieds, 'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /classifieds/create.
     *
     * @return Response
     */
    public function create()
    {
        //
        $categories = DB::table('classifieds_category')->groupBy('category')->get();
        $sub_categories = DB::table('classifieds_category')->groupBy('sub_category')->get();
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        return view('classifieds.create', ['cities' => $cities, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /classifieds.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Request::only('lead_image', 'title', 'category', 'sub_category', 'province', 'area', 'suburb_town_city', 'price', 'description');

        // dd(Request::get('province'));

        if (Request::get('province') == 'south-africa') {
            $input['province'] = '';
        }

        if (Request::get('area') == 'all-cities') {
            $input['area'] = '';
        }

        if (Request::get('suburb_town_city') == 'all-suburbs') {
            $input['suburb_town_city'] = '';
        }

        if (! $this->classified->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->classified->errors);
        }

        $this->classified->user_id = Auth::user()->id;
        $this->classified->status = 'pending';
        $this->classified->save();

        if (Auth::user()->hasRole('Admin')) {
            return Redirect::route('admin.classifieds', ['status' => 'all']);
        }

        //Notify In2Assets Admin of pending classified.
        $userName = 'Dear In2Assets Administrator,';
        $subject = 'In2Assets - New Classified Ad for Approval';
        $adminMail = 'marketing@in2assets.com';
        $emailData = '<p>A new classified has been added to the site and requires approval.</p>';
        $data = ['name' => $userName, 'subject' => $subject, 'messages' => $emailData];
        // Queue email
        Mail::queue('emails.registration.confirm', $data, function ($message) use ($adminMail, $userName, $subject) {
            $message->to($adminMail, $userName)
                ->subject($subject);
        });

        $classifieds = $this->classified->whereUser_id(Auth::user()->id)->withTrashed()->get();

        return view('classifieds.manage_classifieds', ['classifieds' => $classifieds]);
    }

    /**
     * Display the specified resource.
     * GET /classifieds/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $classified = $this->classified->whereId($id)->first();
        $nextClassified = $this->classified->whereStatus('live')->where('id', '>', $classified->id)->first();
        $previousClassified = $this->classified->whereStatus('live')->where('id', '<', $classified->id)->orderBy('id', 'desc')->first();

        $classifiedUser = DB::table('users')->whereId($classified->user_id)->first();
        // dd($classified);
        return view('classifieds.show', ['classified' => $classified, 'nextClassified' => $nextClassified, 'previousClassified' => $previousClassified, 'classifiedUser' => $classifiedUser]);
    }

    public function contact()
    {
        $id = Request::get('id');
        $class = Request::get('class');
        $name = Request::get('name');
        $number = Request::get('number');
        $email = Request::get('email');
        $message = Request::get('message');
        $subject = 'Classified Advert Contact Request';
        $classified = $this->classified->whereId($class)->first();

        $fullMessage = '<p>You have received a response to your classified advert titled '.$classified->title."</p>
                        <table border='0'><tr>
                        <td><strong>Name</strong></td><td>".$name.'</td></tr><tr>
                        <td><strong>Contact</strong></td><td>'.$number."</td></td></tr><tr>
                        <td><strong>Email</strong></td><td><a href='mailto:".$email."'>".$email.'</a></td></td></tr><tr>
                        <td><strong>Message</strong></td><td>'.$message.'</td></tr>
                        </table>
                        <p>The WeSell Property Race Team<br>
                        Your Formula One Property Champions</p>';

        Notify::sendMessage($id, '2', 'Classified', '', $subject, $fullMessage, 'classified');

        Flash::message('Your message has been sent.');

        return Redirect::back();
    }

    public function manageClassifieds()
    {
        $classifieds = $this->classified->whereUser_id(Auth::user()->id)->withTrashed()->get();

        return view('classifieds.manage_classifieds', ['classifieds' => $classifieds]);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /classifieds/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $categories = DB::table('classifieds_category')->groupBy('category')->get();
        $sub_categories = DB::table('classifieds_category')->groupBy('sub_category')->get();

        $citiesDB = DB::table('areas')->groupBy('suburb_town_city')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city;
        }
        $cities = implode('", "', $cities);

        $classified = $this->classified->whereId($id)->first();

        $classified['status'] = 'pending';
        $classified->save();

        return view('classifieds.edit', ['classified' => $classified, 'cities' => $cities, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /classifieds/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $input = Request::all();

        $this->classified = $this->classified->find($id);

        if (! $this->classified->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->classified->errors);
        }

        $this->classified->user_id = Auth::user()->id;
        $this->classified->status = 'pending';
        $this->classified->save();

        if (Auth::user()->hasRole('Admin')) {
            return Redirect::route('admin.classifieds', ['status' => 'all']);
        }

        $classifieds = $this->classified->whereUser_id(Auth::user()->id)->withTrashed()->get();

        return view('classifieds.manage_classifieds', ['classifieds' => $classifieds]);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /classifieds/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        // dd($id);

        $classified = $this->classified->whereId($id)->first();

        $classified['status'] = 'inactive';

        $classified->save();

        $this->classified->whereId($id)->first()->delete();

        if (Auth::user()->hasRole('Admin')) {
            return Redirect::route('admin.classifieds', ['status' => 'all']);
        }

        return Redirect::back();
    }

    public function markInactive($id)
    {
        //
        // dd($id);

        $classified = $this->classified->whereId($id)->first();

        $classified['status'] = 'inactive';

        $classified->save();

        if (Auth::user()->hasRole('Admin')) {
            return Redirect::route('admin.classifieds', ['status' => 'all']);
        }

        return Redirect::back();
    }

    //********************ADMIN SECTION**************************//

    public function AdminClassifieds()
    {
        return view('classifieds.admin.index', ['status' => 'all']);
    }

    //creates admin classifieds table

    public function getAdminClassifieds($status)
    {
        switch ($status):
            case 'all':
                $status = '%';

        break;
        case 'pending':
                $status = '%pending%';

        break;
        case 'live':
                $status = '%live%';

        break;
        case 'sold':
                $status = '%sold%';

        break;
        endswitch;

        $classifieds = DB::table('classifieds')
            ->where('classifieds.status', 'LIKE', $status)
            ->where('classifieds.deleted_at', '=', null)
            ->leftJoin('users', 'users.id', '=', 'classifieds.user_id')
            ->select('classifieds.title', 'classifieds.suburb_town_city', 'classifieds.category', 'classifieds.sub_category', 'users.firstname', 'users.lastname', 'classifieds.status', 'user_id', 'classifieds.id')
            ->get();

        // dd($classifieds);

        // $queries = DB::getQueryLog();
//             $query = end($queries);
//             dd($query);

        return Datatable::collection(new Collection($classifieds))
            ->showColumns('title', 'suburb_town_city', 'category', 'sub_category', 'firstname', 'lastname', 'status')
            ->addColumn('action', function ($Classified) {
                if ($Classified->status === 'pending') {
                    return '<a href="/admin/classifieds/preview/'.$Classified->id.'" title="Classified Preview" ><i class="i-circled i-light i-alt i-small icon-laptop" ></i></a>
				<a href="/admin/classifieds/approve/'.$Classified->id.'" title="Approve Advert" ><i class="i-circled i-light i-alt i-small icon-thumbs-up" style="background-color:#2ecc71" ></i></a>
				<a href="#" onClick="delete_ad('.$Classified->id.');" title="Reject Advert" ><i class="i-circled i-light i-alt i-small icon-remove" style="background-color:#dc143c"></i></a>';
                } elseif ($Classified->status === 'live') {
                    return '<a href="/admin/classifieds/preview/'.$Classified->id.'" title="Classified Preview" ><i class="i-circled i-light i-alt i-small icon-laptop" ></i></a>
				<a href="/admin/classifieds/sold/'.$Classified->id.'" title="Mark as Sold" ><i class="i-circled i-light i-alt i-small icon-thumbs-up"></i></a>
				<a href="/admin/classifieds/inactive/'.$Classified->id.'" title="De-activate Advert" ><i class="i-circled i-light i-alt i-small icon-external-link" style="background-color:#FF7410"></i></a>
				<a href="/admin/classifieds/delete/'.$Classified->id.'" title="Delete Advert" ><i class="i-circled i-light i-alt i-small icon-remove" style="background-color:#dc143c"></i></a>';
                } elseif ($Classified->status === 'inactive') {
                    return '<a href="/admin/classifieds/preview/'.$Classified->id.'" title="Classified Preview" ><i class="i-circled i-light i-alt i-small icon-laptop" ></i></a>
				<a href="/admin/classifieds/activate/'.$Classified->id.'" title="Activate Advert" ><i class="i-circled i-light i-alt i-small icon-plus" style="background-color:#65C200"></i></a>
				<a href="/admin/classifieds/delete/'.$Classified->id.'" title="Delete Advert" ><i class="i-circled i-light i-alt i-small icon-remove" style="background-color:#dc143c"></i></a>';
                } elseif ($Classified->status === 'sold') {
                    return '<a href="/admin/classifieds/preview/'.$Classified->id.'" title="Classified Preview" ><i class="i-circled i-light i-alt i-small icon-laptop" ></i></a>
				<a href="/admin/classifieds/delete/'.$Classified->id.'" title="Delete Advert" ><i class="i-circled i-light i-alt i-small icon-remove" style="background-color:#dc143c"></i></a>';
                }
            })
            ->searchColumns('title', 'suburb_town_city', 'category', 'sub_category', 'firstname', 'lastname', 'status')
            ->orderColumns('title', 'suburb_town_city', 'category', 'sub_category', 'firstname', 'lastname', 'status')
            ->make();
    }

    public function AdminPreview($id)
    {
        $classified = $this->classified->whereId($id)->withTrashed()->first();

        return view('classifieds.admin.show', ['classified' => $classified]);
    }

    public function AdminApprove($id)
    {
        $classified = $this->classified->whereId($id)->first();
        $classified['status'] = 'live';
        $classified->save();

        $user = User::whereId($classified['user_id'])->first();
        //Notify User that Classified Approved.
        $userName = 'Dear '.$user->firstname.' '.$user->lastname;
        $subject = 'In2Assets - Classified Advert Approved';
        $adminMail = $user->email;
        $emailData = "<p>Your classified advert titled '".$classified->title."' has been approved by an In2Assets administrator. You can view your classified online by <a href='https://www.in2assets.co.za/".$classified->id."'>clicking here</a>.</p>";
        $data = ['name' => $userName, 'subject' => $subject, 'messages' => $emailData];
        // Queue email
        Mail::queue('emails.registration.confirm', $data, function ($message) use ($adminMail, $userName, $subject) {
            $message->to($adminMail, $userName)
                ->subject($subject);
        });

        return Redirect::route('admin.classifieds');
    }

    public function AdminCreate()
    {
        $categories = DB::table('classifieds_category')->groupBy('category')->get();
        $sub_categories = DB::table('classifieds_category')->groupBy('sub_category')->get();

        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        return view('classifieds.admin.create', ['cities' => $cities, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    public function AdminInactive()
    {
        return view('classifieds.admin.inactive');
    }

    public function getAdminInactive()
    {
        $classifieds = DB::table('classifieds')
            ->where('classifieds.status', '=', 'inactive')
            ->join('users', 'users.id', '=', 'classifieds.user_id')
            ->select('classifieds.title', 'classifieds.suburb_town_city', 'classifieds.category', 'classifieds.sub_category', 'firstname', 'lastname', 'classifieds.status', 'user_id', 'classifieds.id')
            ->get();

        // $queries = DB::getQueryLog();
//             $query = end($queries);
//             dd($query);

        return Datatable::collection(new Collection($classifieds))
            ->showColumns('title', 'suburb_town_city', 'category', 'sub_category', 'firstname', 'lastname', 'status')
            ->addColumn('action', function ($Classified) {
                return '<a href="/admin/classifieds/preview/'.$Classified->id.'" title="Classified Preview" ><i class="i-circled i-light i-alt i-small icon-laptop" ></i></a>
			<a href="/admin/classifieds/activate/'.$Classified->id.'" title="Activate Advert" ><i class="i-circled i-light i-alt i-small icon-check" style="background-color:#2ecc71"></i></a>';
            })
            ->searchColumns('title', 'suburb_town_city', 'category', 'sub_category', 'firstname', 'lastname')
            ->orderColumns('title', 'suburb_town_city', 'category', 'sub_category', 'firstname', 'lastname')
            ->make();
    }

    public function AdminActivate($id)
    {
        $this->classified->whereId($id)->restore();

        $classified = $this->classified->whereId($id)->first();

        $classified['status'] = 'pending';

        $classified->save();

        return view('classifieds.admin.inactive');
    }

    public function AdminSold($id)
    {
        $classified = $this->classified->whereId($id)->first();

        $classified['status'] = 'sold';

        $classified->save();

        return view('classifieds.admin.index', ['status' => 'all']);
    }
}
