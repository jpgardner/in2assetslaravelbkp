<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Laracasts\Flash\Flash;

class MakeContactController extends Controller
{
    public function __construct(Property $property, User $user)
    {
        $this->property = $property;
        $this->user = $user;
    }

    //Show Table of users who have shown interest in property
    public function show($id)
    {
        $user = Auth::user();

        return view::make('makecontact.show', ['id' => $id])->withUser($user);
    }

    public function delete($id)
    {
        DB::table('make_contact')->whereId($id)->update(['deleted_at' => date('Y-m-d H:i:s')]);

        return Redirect::back();
    }

    // adds user to table
    public function makecontact()
    {
        $id = Request::get('property');
        $user = Auth::user()->id;
        $date = date('Y-m-d H:i:s');
        $offer = Request::get('offer');

        $user_details = $this->user->whereId($user)->first();
        $property = $this->property->whereId($id)->first();

        $name = $user_details->title.' '.$user_details->firstname.' '.$user_details->lastname;

        switch ($user_details->communicationpref) {
            case 'email':
                $contactline = '<tr><td>User Email </td><td>'.$user_details->email.'</td></tr>';

                break;
            case 'landline':
                $contactline = '<tr><td>User Landline </td><td>'.$user_details->phonenumber.'</td></tr>';

                break;
            case 'cellphone':
                $contactline = '<tr><td>User Cellphone </td><td>'.$user_details->cellphone.'</td></tr>';

                break;
            default:
                $contactline = '<tr><td>User Cellphone </td><td>'.$user_details->cellphone.'</td></tr>';

                break;
        }

        $userName = 'Dear In2Assets Administrator,';
        $subject = 'In2Assets Contact Form - Make Contact/Offer';
        $adminMail = 'info@in2assets.com';
        $message = "One of our Users has made an offer of $offer on property reference number $property->reference and
        would like you to contact them.";

        $emailData = '<p>Interest has been shown on property '.$property->reference.".</p>
		<table border='0'>
			<tr><td>Name </td><td>".$name.'</td></tr>'
            .$contactline.
            '<tr><td>Subject </td><td>Make Contact/Offer</td></tr>
			<tr><td>Service </td><td>Property</td></tr>
			<tr><td>Message </td><td>'.$message.'</td></tr>
		</table>
		';
        $data = ['name' => $userName, 'subject' => $subject, 'messages' => $emailData];

        // Queue email
        Mail::queue('emails.registration.confirm', $data, function ($message) use ($adminMail, $userName, $subject) {
            $message->to($adminMail, $userName)
                ->cc('rmoodley@in2assets.com', 'Rowena Moodley')
                ->cc('neeraj@in2assets.com', 'Neeraj Ramautar')
                ->subject($subject);
        });

        if (! $result = DB::table('make_contact')->where('user_id', '=', $user)->where('property_id', $id)->get()) {
            DB::table('make_contact')->insert(['user_id' => $user, 'property_id' => $id, 'offer' => $offer, 'created_at' => $date,
                'updated_at' => $date, ]);
        } else {
            DB::table('make_contact')->where('user_id', '=', $user)->where('property_id', $id)->update(['offer' => $offer]);
        }
        Flash::message('Thank you for your offer, we will contact you shortly.');

        return 'complete';
    }

    //Datatable creation for show
    public function getMakeContactTable($id)
    {
        $contacts = DB::table('make_contact')->select()->whereProperty_id($id)
            ->join('users', 'users.id', '=', 'make_contact.user_id')->get();

        return Datatable::collection(new Collection($contacts))
            ->showColumns('title', 'firstname', 'lastname')
            ->addColumn('offer', function ($Contact) {
                if ($Contact->offer > 0) {
                    return 'R'.$Contact->offer;
                } else {
                    return 'No Offer Made';
                }
            })
            ->addColumn('action', function ($Contact) {
                $listing_type = DB::table('property')->select('listing_type')->whereId($Contact->property_id)->first();

                if ($listing_type->listing_type === 'welet') {
                    $result = DB::table('lease_agreements')->where('tenant_id', $Contact->user_id)->where('property_id', $Contact->property_id)->get();

                    if (! $result) {
                        $hasLease = 0;
                    } else {
                        $hasLease = 1;
                    }

                    if ($hasLease == 0) {
                        return '<a href="/property/'.$Contact->user_id.'/message" title="Message User" ><i class="i-circled i-light i-small icon-comments"></i></a>
					<a href="/lease_agreements/'.$Contact->property_id.'/'.$Contact->user_id.'/newlease" title="Send Lease" ><i class="i-circled i-light i-small icon-folder"></i></a>';
                    } else {
                        return '<a href="/property/'.$Contact->user_id.'/message" title="Message User" ><i class="i-circled i-light i-small icon-comments"></i></a>
					<a href="/lease_agreements/'.$Contact->property_id.'/'.$Contact->user_id.'/edit" title="Edit Lease" ><i class="i-circled i-light i-small icon-edit"></i></a>
					<a href="/lease_agreements/'.$Contact->property_id.'/'.$Contact->user_id.'/approve/in2assets" title="Have lease approved" ><i class="i-circled i-light i-small icon-thumbs-up"></i></a>';
                    }
                } elseif ($listing_type->listing_type === 'wesell') {
                    $result = DB::table('deed_of_sale')->where('buyer_id', $Contact->user_id)->where('property_id', $Contact->property_id)->get();

                    if (! $result) {
                        $hasDeed = 0;
                    } else {
                        $hasDeed = 1;
                    }

                    if ($hasDeed == 0) {
                        return '<a href="/property/'.$Contact->user_id.'/message" title="Message User" ><i class="i-circled i-light i-small icon-comments"></i></a>
					<a href="/deed_of_sale/'.$Contact->property_id.'/'.$Contact->user_id.'/newDeed" title="Send Deed of Sale" ><i class="i-circled i-light i-small icon-folder"></i></a>';
                    } else {
                        return '<a href="/property/'.$Contact->user_id.'/message" title="Message User" ><i class="i-circled i-light i-small icon-comments"></i></a>
					<a href="/deed_of_sale/'.$Contact->property_id.'/'.$Contact->user_id.'/edit" title="Edit Deed of Sale" ><i class="i-circled i-light i-small icon-edit"></i></a>
					<a href="/deed_of_sale/'.$Contact->property_id.'/'.$Contact->user_id.'/approve/in2assets" title="Have  Deed of Sale approved" ><i class="i-circled i-light i-small icon-thumbs-up"></i></a>';
                    }
                }
            })
            ->searchColumns('title', 'firstname', 'lastname')
            ->orderColumns('title', 'firstname', 'lastname')
            ->make();
    }
}
