<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Broker;
use App\Models\BrokerLogos;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class BrokerController extends Controller
{
    public function __construct(Broker $broker, BrokerLogos $broker_logos)
    {
        $this->broker = $broker;
        $this->broker_logos = $broker_logos;
    }

    public function index()
    {
        $noForm = false;

        if (! DB::table('broker')->select('id')->whereUser_id(Auth::user()->id)->first()) {
            $noForm = true;
        }

        return view('broker.index', ['noform' => $noForm]);
    }

    public function store()
    {
        //
        $input = Request::all();
        $input['user_id'] = Auth::user()->id;

        if (! $this->broker->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->broker->errors);
        }

        $this->broker->save();

        return redirect()->route('broker.index');
    }

    public function otherBrokers($status)
    {
        return view('admin.broker_logos.index', ['status' => $status]);
    }

    public function getBrokerLogos($status)
    {
        switch ($status):
            case 'live':
                $brokers = $this->broker_logos->get();

        break;
        case 'deleted':
                $brokers = $this->broker_logos->onlyTrashed()->get();

        break;
        endswitch;

        return Datatable::collection($brokers)
            ->showColumns('broker_name')
            ->addColumn('logo', function ($Broker) {
                return '<image src=/'.$Broker->logo_url.' style="height:30px; width:95px;" alt="'.$Broker->broker_name.' Logo" />';
            })
            ->addColumn('action', function ($Broker) {
                return '<a href="/admin/broker_logos/'.$Broker->id.'/edit" title="Edit Logo" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>';
            })
            ->searchColumns('broker_name')
            ->orderColumns('broker_name')
            ->make();
    }

    public function createBrokerLogo()
    {
        return view('admin.broker_logos.create');
    }

    public function storeBrokerLogo()
    {
        $input = Request::all();

        $broker = $this->broker_logos->fill($input)->save();

        return redirect()->route('admin.other_brokers', ['status' => 'live']);
    }

    public function editBrokerLogo($id)
    {
        $broker = $this->broker_logos->whereId($id)->first();

        return view('admin.broker_logos.edit', ['broker' => $broker]);
    }

    public function deleteBrokerLogo($id)
    {
        $this->broker_logos->whereId($id)->delete();

        return redirect()->route('admin.other_brokers', ['status' => 'live']);
    }

    public function updateBrokerLogo($id)
    {
        $input = Request::all();
        $this->broker = $this->broker_logos->whereId($id)->first();

        $this->broker->fill($input)->update();

        return redirect()->route('admin.other_brokers', ['status' => 'live']);
    }
}
