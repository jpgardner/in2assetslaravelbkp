<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class MarketingController extends Controller
{
    /**
     * Display a listing of the resource.
     * GET /marketing.
     *
     * @return Response
     */
    public function index()
    {
        //
        $title_types = DB::table('title_deed_types')->pluck('type', 'id');

        return view::make('marketing.index', ['title_types' => $title_types]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /marketing/create.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /marketing.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /marketing/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /marketing/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /marketing/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /marketing/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function CommercialPackageSell()
    {
        return view('marketing.commercial_package_sell');
    }
}
