<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use In2Assets\Users\User;

class MessagesController extends Controller
{
    public function index()
    {
        $currentUserId = Auth::user()->id;
        $threads = Thread::forUser($currentUserId);

        foreach ($threads as $thread) {
            $participants = $thread->participants()->withTrashed()->pluck('user_id');
            $userId = Auth::user()->id;

            if (($key = array_search($userId, $participants)) !== false) {
                unset($participants[$key]);
            }
            $sentTo = User::whereId($participants)->first();
            $thread->otheruser = $sentTo->firstname.' '.$sentTo->lastname;

            if ($thread->reference == 'property') {
                $property = '<a href="/property/'.$thread->reference_id.'">View Property</a>';
                $thread->reference = $property;
            }
        }

        // All threads that user is participating in, with new messages
        return view('messenger.index', compact('threads', 'currentUserId'));
    }

    public function indexadmin()
    {
        $currentUserId = 1;
        $threads = Thread::forUser($currentUserId);

        foreach ($threads as $thread) {
            $participants = $thread->participants()->withTrashed()->pluck('user_id');
            $userId = 1;

            if (($key = array_search($userId, $participants)) !== false) {
                unset($participants[$key]);
            }
            $sentTo = User::whereId($participants)->first();
            $thread->otheruser = $sentTo->firstname.' '.$sentTo->lastname;

            if ($thread->reference == 'property') {
                $property = '<a href="/property/'.$thread->reference_id.'">View Property</a>';
                $thread->reference = $property;
            }
        }

        return view('messenger.indexadmin', compact('threads', 'currentUserId'));
    }

    //Show Thread
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: '.$id.' was not found.');

            return redirect('messages');
        }
        $userId = Auth::user()->id;
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $thread->markAsRead($userId);

        foreach ($thread->messages as $message) {
            $sender = User::whereId($message->user_id)->first();
            $message->sender = $sender->firstname.' '.$sender->lastname;
        }

        return view('messenger.show', compact('thread', 'users'));
    }

    //Admin Show Thread
    public function adminshow($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: '.$id.' was not found.');

            return redirect('adminmessages');
        }
        $userId = 1;
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $thread->markAsRead($userId);

        foreach ($thread->messages as $message) {
            $sender = User::whereId($message->user_id)->first();
            $message->sender = $sender->firstname.' '.$sender->lastname;
        }

        return view('messenger.adminshow', compact('thread', 'users'));
    }

    //Create New Thread
    public function create()
    {
        $users = User::where('id', '!=', Auth::id())->get();

        return view('messenger.create', compact('users'));
    }

    //Create for Admin with property (allows user selection)
    public function adminCreate($usr, $ref, $refid)
    {
        $users = User::where('id', '!=', 1)->get();
        $usr = User::whereId($usr)->first();

        return view('messenger.admincreate', compact('users', 'usr', 'ref', 'refid'));
    }

    //Create for admin General
    public function adminCreateGeneral()
    {
        $users = User::where('id', '!=', 1)->get();

        return view('messenger.admingeneralcreate', compact('users'));
    }

    //Store Messages
    public function store()
    {
        $input = Request::all();

        if ($input['message']) {
            $recipients = [$input['recipients']];

            $thread = Thread::create(
                [
                    'subject' => $input['subject'],
                    'reference' => $input['reference'],
                    'reference_id' => $input['reference_id'],
                ]
            );
            // Message
            Message::create(
                [
                    'thread_id' => $thread->id,
                    'user_id' => Auth::user()->id,
                    'body' => $input['message'],
                ]
            );
            // Sender
            Participant::create(
                [
                    'thread_id' => $thread->id,
                    'user_id' => Auth::user()->id,
                    'last_read' => new Carbon,
                ]
            );
            // Recipients
            if (Request::has('recipients')) {
                $thread->addParticipants($recipients);
            }

            if (Request::ajax()) {
                return Response::json('success', 200);
            } else {
                return redirect('portal/messages');
            }
        } else {
            return Response::json('error', 500);
        }
    }

    public function adminStore()
    {
        $input = Request::all();

        if ($input['message']) {
            $thread = Thread::create(
                [
                    'subject' => $input['subject'],
                    'reference' => $input['reference'],
                    'reference_id' => $input['reference_id'],
                ]
            );
            // Message
            Message::create(
                [
                    'thread_id' => $thread->id,
                    'user_id' => 1,
                    'body' => $input['message'],
                ]
            );
            // Sender
            Participant::create(
                [
                    'thread_id' => $thread->id,
                    'user_id' => 1,
                    'last_read' => new Carbon,
                ]
            );
            // Recipients
            if (Request::has('recipients')) {
                $thread->addParticipants($input['recipients']);
            }

            if (Request::ajax()) {
                return redirect('admin/messages');
            } else {
                return redirect('admin/messages');
            }
        } else {
            return Response::json('error', 500);
        }
    }

    //Add Message for current threads
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: '.$id.' was not found.');

            return redirect('portal/messages');
        }
        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'body' => Request::get('message'),
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id' => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Request::has('recipients')) {
            $thread->addParticipants(Request::get('recipients'));
        }

        return redirect('portal/messages/'.$id);
    }

    //Add Message for current threads
    public function adminupdate($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: '.$id.' was not found.');

            return redirect('portal/messages');
        }
        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id' => 1,
                'body' => Request::get('message'),
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id' => 1,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Request::has('recipients')) {
            $thread->addParticipants(Request::get('recipients'));
        }

        return redirect('admin/messages/'.$id);
    }

    public function listMessages()
    {
        $currentUserId = Auth::user()->id;
        $threads = Thread::forUser($currentUserId);

        foreach ($threads as $thread) {
            $participants = $thread->participants()->withTrashed()->pluck('user_id');
            $userId = Auth::user()->id;

            if (($key = array_search($userId, $participants)) !== false) {
                unset($participants[$key]);
            }
            $sentTo = User::whereId($participants)->first();
            $thread->otheruser = $sentTo->firstname.' '.$sentTo->lastname;

            if ($thread->reference == 'property') {
                $property = '<a href="/property/'.$thread->reference_id.'">View Property</a>';
                $thread->reference = $property;
            }
        }

        return $threads;
    }

    public function adminlistMessages()
    {
        $currentUserId = 1;
        $threads = Thread::forUser($currentUserId);

        foreach ($threads as $thread) {
            $participants = $thread->participants()->withTrashed()->pluck('user_id');
            $userId = 1;

            if (($key = array_search($userId, $participants)) !== false) {
                unset($participants[$key]);
            }
            $sentTo = User::whereId($participants)->first();
            $thread->otheruser = $sentTo->firstname.' '.$sentTo->lastname;

            if ($thread->reference == 'property') {
                $property = '<a href="/property/'.$thread->reference_id.'">View Property</a>';
                $thread->reference = $property;
            }
        }

        return $threads;
    }
}
