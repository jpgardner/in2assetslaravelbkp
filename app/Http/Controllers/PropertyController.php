<?php

namespace App\Http\Controllers;

use App\helpers\AppHelper;
use App\In2Assets\Users\User;
use App\Models\AssetRealEstate;
use App\Models\Attributes;
use App\Models\Auction;
use App\Models\Auctionbids;
use App\Models\AuctioneerNotes;
use App\Models\AuctionRegister;
use App\Models\Checklist;
use App\Models\ClosedFile;
use App\Models\Commercial;
use App\Models\Commission;
use App\Models\Farm;
use App\Models\Featured;
use App\Models\Highlight;
use App\Models\Lead;
use App\Models\Ownattourney;
use App\Models\PhysicalAuction;
use App\Models\PhysicalAuctionRegister;
use App\Models\Property;
use App\Models\PropertyAdvertBoards;
use App\Models\PropertyAdvertPhotos;
use App\Models\PropertyAdvertPrints;
use App\Models\PropertyAdvertSocialMedia;
use App\Models\PropertyFile;
use App\Models\PropNotes;
use App\Models\Residential;
use App\Models\Seller;
use App\Models\ServiceProvider;
use App\Models\TransferAttorney;
use App\Models\Vetting;
use App\Models\ZoneType;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;

//use Dompdf\Dompdf;
//use Dompdf\Options;

class PropertyController extends Controller
{
    protected $property;

    public function __construct(Property $property, User $user, Vetting $vetting, Auction $auction, Auctionbids $auction_bids,
                                AuctionRegister $auction_register, PhysicalAuctionRegister $physical_auction_register,
                                Featured $featured, Ownattourney $attourney, ServiceProvider $ServiceProvider,
                                PhysicalAuction $physical_auction, Attributes $attributes, PropNotes $prop_notes,
                                Checklist $checklist, Seller $seller, AssetRealEstate $asset_real_estate,
                                TransferAttorney $transfer_attorney, PropertyFile $property_file, ClosedFile $closed_file,
                                PropertyAdvertBoards $property_advert_boards, PropertyAdvertPrints $property_advert_prints,
                                PropertyAdvertSocialMedia $property_advert_social_media, Highlight $highlight,
                                AuctioneerNotes $auctioneer_notes, PropertyAdvertPhotos $advert_photos, Lead $lead,
                                Commission $commission)
    {
        $this->property = $property;
        $this->user = $user;
        $this->vetting = $vetting;
        $this->auction = $auction;
        $this->auction_bids = $auction_bids;
        $this->auction_register = $auction_register;
        $this->physical_auction_register = $physical_auction_register;
        $this->physical_auction = $physical_auction;
        $this->featured = $featured;
        $this->attourney = $attourney;
        $this->service_provider = $ServiceProvider;
        $this->attributes = $attributes;
        $this->prop_notes = $prop_notes;
        $this->checklist = $checklist;
        $this->seller = $seller;
        $this->asset_real_estate = $asset_real_estate;
        $this->transfer_attorney = $transfer_attorney;
        $this->property_file = $property_file;
        $this->closed_file = $closed_file;
        $this->property_advert_boards = $property_advert_boards;
        $this->property_advert_prints = $property_advert_prints;
        $this->property_advert_social_media = $property_advert_social_media;
        $this->advert_photos = $advert_photos;
        $this->auctioneer_notes = $auctioneer_notes;
        $this->highlight = $highlight;
        $this->lead = $lead;
        $this->commission = $commission;
    }

    public function setc($count)
    {
        $cookie = Cookie::forever('paginatecount', $count);

        return response()->withCookie($cookie);
    }

    public function getc()
    {
        $value = Cookie::get('paginatecount');

        return $value;
    }

    public function ajax()
    {
        if (Request::ajax()) {
            $paginate = 12;

            $listing_type = Request::get('action_values');

            $min = Request::get('saleMin');
            $max = Request::get('saleMax');

            $query_min = "AND ((price >= '".$min."'";
            if ($max == '15000000') {
                $query_max = 'OR price <= 1)';
            } else {
                $query_max = "AND price <= '".$max."' OR price <= 1)";
            }

            $query_min_final = "AND (final_price >= '".$min."'";
            if ($max == '15000000') {
                $query_max_final = 'OR final_price <= 1 OR final_price IS NULL))';
            } else {
                $query_max_final = "AND final_price <= '".$max."' OR final_price <= 1 OR final_price IS NULL))";
            }

            $query_max .= $query_min_final.$query_max_final;

            if ($listing_type !== 'all') {
                if ($listing_type == 'wesell') {
                    $listing = '%';
                    $reference = "'%SALE%' OR property.reference LIKE '%AUCT%'";
                } elseif ($listing_type == 'welet') {
                    $listing = '%';
                    $reference = "'%RENT%'";
                    $min = Request::get('saleMin_rent');
                    $max = Request::get('saleMax_rent');
                    $query_min = "AND (price >= '".$min."'";
                    if ($max == '30000') {
                        $query_max = 'OR price <= 1)';
                    } else {
                        $query_max = "AND price <= '".$max."' OR price <= 1)";
                    }
                } else {
                    $listing = '%'.$listing_type;
                    $reference = "'%'";
                }
            } else {
                $listing = '%';
                $reference = "'%'";
            }

            $property_type = Request::get('category_values');

            $bed_query = 'AND 1=1';
            $bath_query = 'AND 1=1';
            if ($property_type != 'property') {
                $property = 'AND property.property_type LIKE "%'.$property_type.'"';
                if ($property_type == 'commercial') {
                    $property = 'AND (property.property_type LIKE "%'.$property_type.'" OR property.property_type
                    LIKE "%farm")';
                }
                switch ($property_type) {

                    case 'residential':
                        if (Request::get('dwelling_type') != 'None') {
                            $sub_type = '%'.str_replace('_', '-', str_replace('-', ' ', Request::get('dwelling_type')));
                        } else {
                            $sub_type = '%';
                        }

                        if (Request::get('bedrooms') != 'None') {
                            $bedrooms = Request::get('bedrooms');
                            if ($bedrooms == '0') {
                                $bed_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN
                                attributes_property ap ON ap.attributes_id = a.id  WHERE a.name = 'bedrooms' AND value
                                = 0");
                                $bedIds = $this->ObjectToString($bed_ids);
                                $bed_query = 'AND property.id IN ('.$bedIds.')';
                            } else {
                                $bed_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN
                                attributes_property ap ON ap.attributes_id = a.id  WHERE a.name = 'bedrooms' AND value
                                >= '".$bedrooms."'");
                                $bedIds = $this->ObjectToString($bed_ids);
                                $bed_query = 'AND property.id IN ('.$bedIds.')';
                            }
                        }
                        if (Request::get('bathrooms') != 'None') {
                            $bathrooms = Request::get('bathrooms');
                            $bath_ids = DB::select("SELECT ap.property_id FROM attributes a INNER JOIN
                            attributes_property ap ON ap.attributes_id = a.id  WHERE a.name = 'bathrooms' AND value >=
                            '".$bathrooms."'");
                            $bathIds = $this->ObjectToString($bath_ids);
                            $bath_query = 'AND property.id IN ('.$bathIds.')';
                        }

                        break;
                    case 'commercial':
                        if (Request::get('commercial_type') != 'None') {
                            $sub_type = '%'.str_replace('_', '-', str_replace('-', ' ', Request::get('commercial_type')));
                            if ($sub_type == '%industrial space') {
                                $sub_type = '%industrial%';
                            }
                        } else {
                            $sub_type = '%';
                        }

                        break;
                    case 'farm':
                        if (Request::get('farm_type') != 'None') {
                            $sub_type = '%'.str_replace('_', '-', str_replace('-', ' ', Request::get('farm_type')));
                        } else {
                            $sub_type = '%';
                        }

                        break;
                }
            } else {
                $property = 'AND property.property_type LIKE "%"';
                $sub_type = '%';
            }
            //fix this
            $floor_min = Request::get('floorMin');
            $floor_max = Request::get('floorMax');

            if (($floor_max == '2000' && $floor_min == '0') || ($floor_max == '0' && $floor_min == '0') || ($floor_max == '' && $floor_min == '')) {
                $floor_query = 'AND 1=1';
            } else {
                $floor_query_max = 'AND value < '.$floor_max;
                if ($floor_max == '2000') {
                    $id_query = "SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                  ON ap.attributes_id = a.id  WHERE a.name = 'floor_space' AND value > $floor_min
                                  GROUP BY ap.property_id";
                } else {
                    $id_query = "SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                 ON ap.attributes_id = a.id  WHERE a.name = 'floor_space' AND value > $floor_min
                                 $floor_query_max GROUP BY ap.property_id";
                }

                $floor_ids = DB::select($id_query);
                $floorIds = $this->ObjectToString($floor_ids);
                $floor_query = 'AND property.id IN ('.$floorIds.')';
            }

            //fix this
            $land_min = Request::get('landMin');
            $land_max = Request::get('landMax');
            if (($land_max == '20000' && $land_min == '0') || ($floor_max == '0' && $floor_min == '0')) {
                $land_query = 'AND 1=1';
            } else {
                $land_query_max = 'AND value < '.$land_max;
                if ($land_max == '20000') {
                    $land_id_query = "SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                    ON ap.attributes_id = a.id  WHERE a.name = 'land_size' AND value > $land_min
                                    GROUP BY ap.property_id";
                } else {
                    $land_id_query = "SELECT ap.property_id FROM attributes a INNER JOIN attributes_property ap
                                    ON ap.attributes_id = a.id  WHERE a.name = 'land_size' AND value > $land_min
                                    $land_query_max GROUP BY ap.property_id";
                }
                $land_ids = DB::select($land_id_query);
                $landIds = $this->ObjectToString($land_ids);
                $land_query = 'AND property.id IN ('.$landIds.')';
            }

            $provinceInput = Request::get('province');
            if ($provinceInput != 'south-africa') {
                $province = '%'.$provinceInput;
            } else {
                $province = '%';
            }

            $city_Input = Request::get('city');
            if ($city_Input != 'all-cities') {
                $city = '%'.$city_Input;
            } else {
                $city = '%';
            }

            $suburb_Input = Request::get('suburb');

            if ($suburb_Input != '') {
                if ($suburb_Input[0] == '0') {
                    parse_str($suburb_Input, $suburbs);
                    $suburb = '(';
                    foreach ($suburbs as $suburbInputMultiple) {
                        if ($suburbInputMultiple === end($suburbs)) {
                            $suburb .= 'property.suburb_town_city LIKE "%'.str_replace('-', ' ', $suburbInputMultiple)
                                .'"';
                        } else {
                            $suburb .= 'property.suburb_town_city LIKE "%'.str_replace('-', ' ', $suburbInputMultiple)
                                .'" OR ';
                        }
                    }
                    $suburb .= ')';
                } else {
                    $suburb = '(property.suburb_town_city LIKE "%'.str_replace('-', ' ', $suburb_Input).'" OR
                    property.area LIKE "%'.str_replace('-', ' ', $suburb_Input).'")';
                }
            } else {
                $suburb = '1=1';
            }

            $order = Request::get('order');
            if ($order == '0' && $listing_type == 'weauction') {
                $order = '4';
            }

            if (Request::get('agent') > 0) {
                $order = '5';
                $agent = Request::get('agent');
            }

            switch ($order) {

                //ordered by most recent
                case '0':
                case '3':
                    $properties = $this->property->
                    select('*', 'property.id as actual_id')->
                    join('vetting', 'vetting.property_id', '=', 'property.id')->
                    leftJoin('residential', 'residential.property_id', '=', 'property.id')->
                    leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
                    leftJoin('farm', 'farm.property_id', '=', 'property.id')->
                    leftJoin('auction', 'auction.property_id', '=', 'property.id')->
                    whereRaw('(property.reference LIKE '.$reference.")
                AND CASE '".$listing."'
                        WHEN '%welet'
                        THEN (property.listing_type = 'welet'
                            OR property.listing_type = 'in2rental')
                        WHEN '%wesell'
                        THEN  (property.listing_type = 'wesell'
                            OR property.listing_type = 'in2assets'
                            OR property.listing_type = 'weauction')
                        WHEN '%weauction'
                        THEN  property.listing_type = 'weauction'
                        WHEN '%'
                        THEN  property.listing_type LIKE '%'
                        ELSE '1=1'
                        END
                AND CASE  '".$suburb."'
                        WHEN '1=1'
                        THEN (property.suburb_town_city LIKE '".$city."'
                OR property.area LIKE '".$city."')
                        ELSE ".$suburb.'
                       END
                       '.$property."
                AND property.province  LIKE '".$province."'
                AND ( vetting.vetting_status = 2 OR vetting.vetting_status = 4 OR vetting.vetting_status = 5
                OR vetting.vetting_status = 10 OR vetting.vetting_status = 12 OR vetting.vetting_status = 13
                OR vetting.vetting_status = 15 OR vetting.vetting_status = 16 )
                AND CASE '".$property_type."'
                        WHEN 'residential'
                        THEN residential.dwelling_type LIKE '".$sub_type."' $bed_query $bath_query
                        WHEN 'commercial'
                        THEN (commercial.commercial_type LIKE '".$sub_type."' OR farm.farm_type LIKE '".$sub_type
                        ."')
                        WHEN 'farm'
                        THEN farm.farm_type LIKE '".$sub_type."'
                        ELSE '1=1'
                        END
                        $floor_query
                        $land_query
                        $query_min
                        $query_max
                   ORDER BY property.id DESC, property.suburb_town_city")->
                    paginate($paginate);

                    break;

                //ordered by price highest to lowest
                case '1':
                    $properties = $this->property->
                    select('*', 'property.id as actual_id')->
                    join('vetting', 'vetting.property_id', '=', 'property.id')->
                    leftJoin('residential', 'residential.property_id', '=', 'property.id')->
                    leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
                    leftJoin('farm', 'farm.property_id', '=', 'property.id')->
                    leftJoin('auction', 'auction.property_id', '=', 'property.id')->
                    whereRaw('(property.reference LIKE '.$reference.")
                AND CASE '".$listing."'
                        WHEN '%welet'
                        THEN (property.listing_type = 'welet'
                            OR property.listing_type = 'in2rental')
                        WHEN '%wesell'
                        THEN  (property.listing_type = 'wesell'
                            OR property.listing_type = 'in2assets'
                            OR property.listing_type = 'weauction')
                        WHEN '%weauction'
                        THEN  property.listing_type = 'weauction'
                        WHEN '%'
                        THEN  property.listing_type LIKE '%'
                        ELSE '1=1'
                        END
                        AND CASE  '".$suburb."'
                        WHEN '1=1'
                        THEN (property.suburb_town_city LIKE '".$city."'
                OR property.area LIKE '".$city."')
                        ELSE ".$suburb.'
                       END
                       '.$property."
                AND property.province  LIKE '".$province."'
                AND ( vetting.vetting_status = 2 OR vetting.vetting_status = 4 OR vetting.vetting_status = 5
                OR vetting.vetting_status = 10 OR vetting.vetting_status = 12 OR vetting.vetting_status = 13
                OR vetting.vetting_status = 15 OR vetting.vetting_status = 16 )
                AND CASE '".$property_type."'
                        WHEN 'residential'
                        THEN residential.dwelling_type LIKE '".$sub_type."' $bed_query $bath_query
                        WHEN 'commercial'
                        THEN (commercial.commercial_type LIKE '".$sub_type."' OR farm.farm_type LIKE '".$sub_type
                        ."')
                        WHEN 'farm'
                        THEN farm.farm_type LIKE '".$sub_type."'
                        ELSE '1=1'
                        END
                        $floor_query
                        $land_query
                        $query_min
                        $query_max")->orderBy('property.price', 'desc')->
                    orderBy(DB::raw('LOCATE("'.str_replace('%', '', $suburb_Input).'",
                    property.suburb_town_city)'))->
                    paginate($paginate);

                    break;

                //ordered by price lowest to highest
                case '2':
                    $properties = $this->property->
                    select('*', 'property.id as actual_id')->
                    join('vetting', 'vetting.property_id', '=', 'property.id')->
                    leftJoin('residential', 'residential.property_id', '=', 'property.id')->
                    leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
                    leftJoin('farm', 'farm.property_id', '=', 'property.id')->
                    leftJoin('auction', 'auction.property_id', '=', 'property.id')->
                    whereRaw('(property.reference LIKE '.$reference.")
                AND CASE '".$listing."'
                        WHEN '%welet'
                        THEN (property.listing_type = 'welet'
                            OR property.listing_type = 'in2rental')
                        WHEN '%wesell'
                        THEN  (property.listing_type = 'wesell'
                            OR property.listing_type = 'in2assets'
                            OR property.listing_type = 'weauction')
                        WHEN '%weauction'
                        THEN  property.listing_type = 'weauction'
                        WHEN '%'
                        THEN  property.listing_type LIKE '%'
                        ELSE '1=1'
                        END
                        AND CASE  '".$suburb."'
                        WHEN '1=1'
                        THEN (property.suburb_town_city LIKE '".$city."'
                OR property.area LIKE '".$city."')
                        ELSE ".$suburb.'
                       END
                       '.$property."
                AND property.province  LIKE '".$province."'
                AND ( vetting.vetting_status = 2 OR vetting.vetting_status = 4 OR vetting.vetting_status = 5
                OR vetting.vetting_status = 10 OR vetting.vetting_status = 12 OR vetting.vetting_status = 13
                OR vetting.vetting_status = 15 OR vetting.vetting_status = 16 )
                AND CASE '".$property_type."'
                        WHEN 'residential'
                        THEN residential.dwelling_type LIKE '".$sub_type."' $bed_query $bath_query
                        WHEN 'commercial'
                        THEN (commercial.commercial_type LIKE '".$sub_type."' OR farm.farm_type LIKE '".$sub_type
                        ."')
                        WHEN 'farm'
                        THEN farm.farm_type LIKE '".$sub_type."'
                        ELSE '1=1'
                        END
                        $floor_query
                        $land_query
                        $query_min
                        $query_max
                   ORDER BY property.price ASC, property.suburb_town_city")->
                    paginate($paginate);

                    break;

                //ordered by most recent but with upcoming auctions taking preference
                case '4':
                    $properties = $this->property->
                    select('*', 'property.id as actual_id',
                        DB::raw('IF(DATEDIFF(auction.start_date, NOW())<0 AND DATEDIFF(auction.end_date, NOW())<0,
                        (DATEDIFF(auction.start_date, NOW())*-100),(IF(DATEDIFF(auction.end_date, NOW())>0,
                        DATEDIFF(auction.end_date, NOW()),DATEDIFF(auction.start_date, NOW())))) AS dTdiff,
                        auction.end_date'))->
                    join('vetting', 'vetting.property_id', '=', 'property.id')->
                    leftJoin('residential', 'residential.property_id', '=', 'property.id')->
                    leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
                    leftJoin('farm', 'farm.property_id', '=', 'property.id')->
                    leftJoin('auction', 'auction.property_id', '=', 'property.id')->
                    whereRaw('(property.reference LIKE '.$reference.")
                AND CASE '".$listing."'
                        WHEN '%welet'
                        THEN (property.listing_type = 'welet'
                            OR property.listing_type = 'in2rental')
                        WHEN '%wesell'
                        THEN  (property.listing_type = 'wesell'
                            OR property.listing_type = 'in2assets'
                            OR property.listing_type = 'weauction')
                        WHEN '%weauction'
                        THEN  property.listing_type = 'weauction'
                        WHEN '%'
                        THEN  property.listing_type LIKE '%'
                        ELSE '1=1'
                        END
                        AND CASE  '".$suburb."'
                        WHEN '1=1'
                        THEN (property.suburb_town_city LIKE '".$city."'
                OR property.area LIKE '".$city."')
                        ELSE ".$suburb.'
                       END
                       '.$property."
                AND property.province  LIKE '".$province."'
                AND ( vetting.vetting_status = 2 OR vetting.vetting_status = 4 OR vetting.vetting_status = 5
                OR vetting.vetting_status = 10 OR vetting.vetting_status = 12 OR vetting.vetting_status = 13
                OR vetting.vetting_status = 15 OR vetting.vetting_status = 16 )
                AND CASE '".$property_type."'
                        WHEN 'residential'
                        THEN residential.dwelling_type LIKE '".$sub_type."' $bed_query $bath_query
                        WHEN 'commercial'
                        THEN (commercial.commercial_type LIKE '".$sub_type."' OR farm.farm_type LIKE '".$sub_type
                        ."')
                        WHEN 'farm'
                        THEN farm.farm_type LIKE '".$sub_type."'
                        ELSE '1=1'
                        END
                        $floor_query
                        $land_query
                        $query_min
                        $query_max")->
                    orderBy('dTdiff', 'asc')->
                    orderBy('auction.auction_type')->
                    orderBy('vetting_status')->
                    orderBy(DB::raw('LOCATE("'.str_replace('%', '', $suburb_Input).'",
                    property.suburb_town_city)'))->
                    paginate($paginate);

                    break;

//                ordered by most recent ** Agent properties **

                case '5':
                    $properties = $this->property->
                    select('*', 'property.id as actual_id')->
                    join('vetting', 'vetting.property_id', '=', 'property.id')->
                    leftJoin('residential', 'residential.property_id', '=', 'property.id')->
                    leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
                    leftJoin('farm', 'farm.property_id', '=', 'property.id')->
                    leftJoin('auction', 'auction.property_id', '=', 'property.id')->
                    whereRaw('(property.reference LIKE '.$reference.")
                AND CASE '".$listing."'
                        WHEN '%welet'
                        THEN (property.listing_type = 'welet'
                            OR property.listing_type = 'in2rental')
                        WHEN '%wesell'
                        THEN  (property.listing_type = 'wesell'
                            OR property.listing_type = 'in2assets'
                            OR property.listing_type = 'weauction')
                        WHEN '%weauction'
                        THEN  property.listing_type = 'weauction'
                        WHEN '%'
                        THEN  property.listing_type LIKE '%'
                        ELSE '1=1'
                        END
                        AND CASE  '".$suburb."'
                        WHEN '1=1'
                        THEN (property.suburb_town_city LIKE '".$city."'
                OR property.area LIKE '".$city."')
                        ELSE ".$suburb.'
                       END
                       '.$property."
                AND property.province  LIKE '".$province."'
                AND ( vetting.vetting_status = 2 OR vetting.vetting_status = 4 OR vetting.vetting_status = 5
                OR vetting.vetting_status = 10 OR vetting.vetting_status = 12 OR vetting.vetting_status = 13
                OR vetting.vetting_status = 15 OR vetting.vetting_status = 16 )
                AND CASE '".$property_type."'
                        WHEN 'residential'
                        THEN residential.dwelling_type LIKE '".$sub_type."' $bed_query $bath_query
                        WHEN 'commercial'
                        THEN (commercial.commercial_type LIKE '".$sub_type."' OR farm.farm_type LIKE '".$sub_type
                        ."')
                        WHEN 'farm'
                        THEN farm.farm_type LIKE '".$sub_type."'
                        ELSE '1=1'
                        END
                        $floor_query
                        $land_query
                        $query_min
                        $query_max")->where('vetting.rep_id_number', '=', $agent)->
                    orderBy('property.id', 'DESC')->
                    orderBy(DB::raw('LOCATE("'.str_replace('%', '', $suburb_Input).'",
                    property.suburb_town_city)'))->
                    paginate($paginate);

                    break;
            }

//            dd(AppHelper::getLastQuery());

            foreach ($properties as $property) {
                $property->id = $property->actual_id;

                if ($bedroomsDets = $property->getFeatureValue('bedrooms', $property->id)) {
                    $property->bedrooms = $bedroomsDets[0]->value;
                    $bedroomsDets[0]->value = '';
                } else {
                    $property->bedrooms = '';
                }
                if ($bathroomsDets = $property->getFeatureValue('bathrooms', $property->id)) {
                    $property->bathrooms = $bathroomsDets[0]->value;
                    $bathroomsDets[0]->value = '';
                } else {
                    $property->bathrooms = '';
                }
            }

            return $properties;
        }
    }

    public function indexMultiple()
    {
        $input = Request::get('input');
        $input = json_decode(json_encode(json_decode($input)), true);

        $count = $input['count'] - 1;
        $count2 = $input['count'] - 1;
        $reference_count = 0;
        $area_array = [];
        $ref = '';
        while ($count > -1) {
            if (! $id = $this->searchReference($input['multiple'.$count])) {
                $area_array[] = explode(', ', $input['multiple'.$count]);
                $count2--;
            } else {
                if ($count == $count2) {
                    $ref .= 'property.reference = "'.$input['multiple'.$count].'"';
                } else {
                    $ref .= ' OR property.reference = "'.$input['multiple'.$count].'"';
                }
                $reference_count++;
            }
            $count--;
        }

        if ($ref == '') {
            $ref_sql = '1 != 1';
        } else {
            $ref_sql = '('.$ref.')';
        }

        $area_count = count($area_array);

        $area_chk = $area_count + $reference_count;

        $raw_sql = '(';
        while ($area_count > 0) {
            if ($area_chk == $input['count']) {
                $raw_sql .= " (property.province = '".$area_array[$area_count - 1][0]."'";
                $area_chk--;
            } else {
                $raw_sql .= " OR (property.province = '".$area_array[$area_count - 1][0]."'";
            }
            if (isset($area_array[$area_count - 1][2])) {
                $raw_sql .= " AND property.area = '".$area_array[$area_count - 1][1]."' AND property.suburb_town_city = '".$area_array[$area_count - 1][2]."'";
            } elseif (isset($area_array[$area_count - 1][1])) {
                $raw_sql .= " AND (property.area = '".$area_array[$area_count - 1][1]."' OR property.suburb_town_city = '".$area_array[$area_count - 1][1]."')";
            }

            $raw_sql .= ')';

            $area_count--;
        }

        $raw_sql .= ')';

        if ($raw_sql == '()') {
            $raw_sql = $ref_sql;
        }

        if ($input['listing_type'] == 'in2assets') {
            $listing_sql = "(property.listing_type = 'in2assets' OR property.listing_type = 'weauction')";
        } else {
            $listing_sql = "property.listing_type = '".$input['listing_type']."'";
        }

        $properties = $this->property->
        select('property.*', 'residential.*', 'commercial.*', 'farm.*', 'vetting.*')->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('farm', 'property.id', '=', 'farm.property_id')->
        leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        where(function ($q) {
            $q->where('vetting_status', '=', 2)
                ->orWhere('vetting_status', '=', 4)
                ->orWhere('vetting_status', '=', 5)
                ->orWhere('vetting_status', '=', 10)
                ->orWhere('vetting_status', '=', 12)
                ->orWhere('vetting_status', '=', 13)
                ->orWhere('vetting_status', '=', 15)
                ->orWhere('vetting_status', '=', 16);
        })->
        whereRaw($raw_sql)->
        whereRaw($listing_sql)->
        where('property.property_type', '=', $input['property_type'])->
        orWhereRaw($ref_sql)->
        orderBy('property.created_at', 'DESC')->
        paginate(12);

        foreach ($properties as $property) {
            $property['smallImage'] = substr_replace($property['lead_image'], '_265_163', -4, 0);
        }

        return view('property.index_multiple', ['properties' => $properties->appends(['input' => json_encode($input)]),
            'listing' => 'Search', ]);
    }

    public function getFeaturedProperties($property_type)
    {
        if ($property_type == 'property') {
            $featured = $this->property
                ->whereHas('featured', function ($q) {
                    $dtToday = date('Y-m-d');
                    $q->where('featured_property.start_date', '<=', $dtToday)->where('featured_property.end_date', '>=',
                        $dtToday);
                })
                ->orWhere('property.featured', '=', 1)
                ->where('deleted_at', '=', null)
                ->with('residential')->with('commercial')->with('farm')->with('vetting')->with('auction')
                ->orderBy('property.id', 'desc')
                ->limit(4)
                ->get();
        } else {
            $featured = $this->property
                ->whereHas('featured', function ($q) {
                    $dtToday = date('Y-m-d');
                    $q->where('featured_property.start_date', '<=', $dtToday)->where('featured_property.end_date', '>=',
                        $dtToday);
                })
                ->orWhere('property.featured', '=', 1)
                ->whereProperty_type($property_type)
                ->where('deleted_at', '=', null)
                ->where('listing_type', '!=', 'wesell')
                ->with('residential')->with('commercial')->with('farm')->with('vetting')->with('auction')
                ->get();
        }

        return $featured;
    }

    public function showMap($type)
    {
        $set_action_type = 'index';
        $set_all_type = 'property';

        $provinces = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areas = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $cities = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        $listing_type = Request::get('action_values');
        if ($listing_type !== 'property') {
            $listing = '%'.$listing_type;
        } else {
            $listing = '%';
        }
        $property_type = Request::get('category_values');

        if ($property_type !== 'property') {
            $property = '%'.$property_type;
        } else {
            $property = '%';
        }
        $provinceInput = Request::get('city');
        if ($provinceInput != 'south-africa') {
            $province = '%'.$provinceInput;
        } else {
            $province = '%';
        }
        $suburb_town_city_Input = Request::get('area');
        if ($suburb_town_city_Input !== 'all-suburbs') {
            $suburb_town_city = '%'.$suburb_town_city_Input;
        } else {
            $suburb_town_city = '%';
        }

        switch ($type) {
            case 'all':
                $properties = Property::whereHas('vetting', function ($q) {
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();
                $leads = $this->lead->join('users', 'users.id', '=', 'leads.bdm_allocated')->select('leads.*',
                    'users.firstname')->where('x_coord', '!=', 0)->get();

                break;

            case 'leads':
                $properties = new stdClass();
                $leads = $this->lead->join('users', 'users.id', '=', 'leads.bdm_allocated')->select('leads.*',
                    'users.firstname')->where('x_coord', '!=', 0)->get();

                break;

            case 'archived':
                $properties = Property::whereHas('vetting', function ($q) {
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                onlyTrashed()->
                get();
                $leads = $this->lead->join('users', 'users.id', '=', 'leads.bdm_allocated')->select('leads.*',
                    'users.firstname')->where('x_coord', '!=', 0)->onlyTrashed()->get();

                break;

            case 'commercial':
                $properties = Property::whereHas('vetting', function ($q) {
                })->with('commercial')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', '=', 'commercial')->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();
                $leads = $this->lead->join('users', 'users.id', '=', 'leads.bdm_allocated')->select('leads.*',
                    'users.firstname')->where('x_coord', '!=', 0)->where('property_type', '=', 'commercial')->get();

                break;

            case 'residential':
                $properties = Property::whereHas('vetting', function ($q) {
                })->with('residential')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', '=', 'residential')->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();
                $leads = $this->lead->join('users', 'users.id', '=', 'leads.bdm_allocated')->select('leads.*',
                    'users.firstname')->where('x_coord', '!=', 0)->where('property_type', '=', 'residential')->get();

                break;
            case 'farm':
                $properties = Property::whereHas('vetting', function ($q) {
                })->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', '=', 'farm')->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();
                $leads = $this->lead->join('users', 'users.id', '=', 'leads.bdm_allocated')->select('leads.*',
                    'users.firstname')->where('x_coord', '!=', 0)->where('property_type', '=', 'farm')->get();

                break;
            case 'rental':
                $properties = Property::whereHas('vetting', function ($q) {
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', '=', 'in2rental')->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            case 'approved':
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', 2);
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            case 'soldless60':
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', 4);
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            case 'soldover60':
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', 6);
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            case 'rentedless14':
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', 5);
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            case 'rentedover14':
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', 7);
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            case 'auction':
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', 9);
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            case 'auctionsold':
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', 10);
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();

                break;
            default:
                $properties = Property::whereHas('vetting', function ($q) {
                    $q->where('vetting_status', '=', '%');
                })->with('residential')->with('commercial')->with('farm')->
                where('property.listing_type', 'LIKE', $listing)->
                where('property.property_type', 'LIKE', $property)->
                where('property.province', 'LIKE', $province)->
                where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
                orderBy('property.price', 'desc')->
                get();
                $leads = $this->lead->join('users', 'users.id', '=', 'leads.bdm_allocated')->select('leads.*',
                    'users.firstname')->where('x_coord', '!=', 0)->get();

                break;
        }

        return view('property.proptest', ['properties' => $properties, 'set_action_type' => $set_action_type,
            'set_all_type' => $set_all_type, 'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas,
            'type' => $type, 'leads' => $leads, ]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /property/create.
     *
     * @return Response
     */
    public function create($listing_type)
    {
//        $this->beforefilter('guest');

        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $user = Auth::user();

        if ($listing_type == 'admin') {
            $agents = $this->user->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->get();

            $zone_types = DB::table('zone_types')->whereActive(1)->pluck('type', 'id');

            if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('LettingAgent')) {
                $brokers = DB::table('broker_logos')->select('id', 'broker_name')->whereDeleted_at(null)->get();
                $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);

                return view('property.admin.create', ['cities' => $cities, 'agents' => $agents,
                    'brokers' => $brokers, 'features' => $features, 'zone_types' => $zone_types, ]);
            } else {
                return redirect()->route('home');
            }
        }

        return view('property.create', ['cities' => $cities, 'listing_type' => $listing_type, 'user' => $user]);
    }

    public function adminCreate()
    {
        $citiesDB = DB::table('areas')->groupBy('suburb_town_city')->get();
        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city;
        }
        $cities = implode('", "', $cities);

        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();

        $zone_types = DB::table('zone_types')->whereActive(1)->pluck('type', 'id');

        return view('property.admin.create', ['cities' => $cities, 'agents' => $agents,
            'zone_types' => $zone_types, ]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /property.
     *
     * @return Response
     */
    public function store()
    {
        // check property section of form and validate

        $propertyInput = Request::only(['street_address', 'country', 'listing_type', 'property_type', 'x_coord',
            'y_coord', 'floor_space', 'land_size', 'land_measurement', 'sale_type', 'company', 'vat_num', 'reg_num',
            'province', 'area', 'suburb_town_city', ]);

        if ($propertyInput['land_measurement'] == 'HA') {
            $propertyInput['land_size'] = $propertyInput['land_size'] * 10000;
        }

        if ($propertyInput['floor_space'] == null) {
            $propertyInput['floor_space'] = 0;
        }

        //RESIDENTIAL
        //check whether amenity and security checkboxes have been checked and set input value to 1 if checked
        if (Request::get('dining_room')) {
            $dining_room = 1;
        } else {
            $dining_room = 0;
        }
        $propertyInput['dining_room'] = $dining_room;
        if (Request::get('entrance_hall')) {
            $entrance_hall = 1;
        } else {
            $entrance_hall = 0;
        }
        $propertyInput['entrance_hall'] = $entrance_hall;
        if (Request::get('tv_room')) {
            $tv_room = 1;
        } else {
            $tv_room = 0;
        }
        $propertyInput['tv_room'] = $tv_room;
        if (Request::get('study')) {
            $study = 1;
        } else {
            $study = 0;
        }
        $propertyInput['study'] = $study;
        if (Request::get('pets')) {
            $pets = 1;
        } else {
            $pets = 0;
        }
        $propertyInput['pets'] = $pets;
        if (Request::get('lounge')) {
            $lounge = 1;
        } else {
            $lounge = 0;
        }
        $propertyInput['lounge'] = $lounge;
        if (Request::get('kitchen')) {
            $kitchen = 1;
        } else {
            $kitchen = 0;
        }
        $propertyInput['kitchen'] = $kitchen;
        if (Request::get('recreation_room')) {
            $recreation_room = 1;
        } else {
            $recreation_room = 0;
        }
        $propertyInput['recreation_room'] = $recreation_room;
        if (Request::get('pantry')) {
            $pantry = 1;
        } else {
            $pantry = 0;
        }
        $propertyInput['pantry'] = $pantry;
        if (Request::get('laundry')) {
            $laundry = 1;
        } else {
            $laundry = 0;
        }
        $propertyInput['laundry'] = $laundry;
        if (Request::get('guest_toilet')) {
            $guest_toilet = 1;
        } else {
            $guest_toilet = 0;
        }
        $propertyInput['guest_toilet'] = $guest_toilet;
        if (Request::get('patio')) {
            $patio = 1;
        } else {
            $patio = 0;
        }
        $propertyInput['patio'] = $patio;
        if (Request::get('view')) {
            $view = 1;
        } else {
            $view = 0;
        }
        $propertyInput['view'] = $view;
        if (Request::get('tennis_court')) {
            $tennis_court = 1;
        } else {
            $tennis_court = 0;
        }
        $propertyInput['tennis_court'] = $tennis_court;
        if (Request::get('pool')) {
            $pool = 1;
        } else {
            $pool = 0;
        }
        $propertyInput['pool'] = $pool;
        if (Request::get('sprinklers')) {
            $sprinklers = 1;
        } else {
            $sprinklers = 0;
        }
        $propertyInput['sprinklers'] = $sprinklers;
        if (Request::get('aircon')) {
            $aircon = 1;
        } else {
            $aircon = 0;
        }
        $propertyInput['aircon'] = $aircon;
        if (Request::get('aerial')) {
            $aerial = 1;
        } else {
            $aerial = 0;
        }
        $propertyInput['aerial'] = $aerial;
        if (Request::get('dstv')) {
            $dstv = 1;
        } else {
            $dstv = 0;
        }
        $propertyInput['dstv'] = $dstv;
        if (Request::get('bic')) {
            $bic = 1;
        } else {
            $bic = 0;
        }
        $propertyInput['bic'] = $bic;
        if (Request::get('shower')) {
            $shower = 1;
        } else {
            $shower = 0;
        }
        $propertyInput['shower'] = $shower;
        if (Request::get('staff_quarters')) {
            $staff_quarters = 1;
        } else {
            $staff_quarters = 0;
        }
        $propertyInput['staff_quarters'] = $staff_quarters;
        if (Request::get('lapa_braai')) {
            $lapa_braai = 1;
        } else {
            $lapa_braai = 0;
        }
        $propertyInput['lapa_braai'] = $lapa_braai;
        if (Request::get('intercom')) {
            $intercom = 1;
        } else {
            $intercom = 0;
        }
        $propertyInput['intercom'] = $intercom;
        if (Request::get('alarm')) {
            $alarm = 1;
        } else {
            $alarm = 0;
        }
        $propertyInput['alarm'] = $alarm;
        if (Request::get('elec_gate')) {
            $elec_gate = 1;
        } else {
            $elec_gate = 0;
        }
        $propertyInput['elec_gate'] = $elec_gate;
        if (Request::get('sec_fence')) {
            $sec_fence = 1;
        } else {
            $sec_fence = 0;
        }
        $propertyInput['sec_fence'] = $sec_fence;
        if (Request::get('sec_lights')) {
            $sec_lights = 1;
        } else {
            $sec_lights = 0;
        }
        $propertyInput['sec_lights'] = $sec_lights;
        if (Request::get('stove')) {
            $stove = 1;
        } else {
            $stove = 0;
        }
        $propertyInput['stove'] = $stove;
        if (Request::get('dam')) {
            $dam = 1;
        } else {
            $dam = 0;
        }
        $propertyInput['dam'] = $dam;
        if (Request::get('borehole')) {
            $borehole = 1;
        } else {
            $borehole = 0;
        }
        $propertyInput['borehole'] = $borehole;
        if (Request::get('electric_fence')) {
            $electric_fence = 1;
        } else {
            $electric_fence = 0;
        }
        $propertyInput['electric_fence'] = $electric_fence;

        //COMMERCIAL
        //check whether amenity and security checkboxes have been checked and set input value to 1 if checked
        if (Request::get('yard_space')) {
            $yard_space = 1;
        } else {
            $yard_space = 0;
        }
        $propertyInput['yard_space'] = $yard_space;
        if (Request::get('tea_kitchen')) {
            $tea_kitchen = 1;
        } else {
            $tea_kitchen = 0;
        }
        $propertyInput['tea_kitchen'] = $tea_kitchen;
        if (Request::get('staff_parking')) {
            $staff_parking = 1;
        } else {
            $staff_parking = 0;
        }
        $propertyInput['staff_parking'] = $staff_parking;
        if (Request::get('visitors_parking')) {
            $visitors_parking = 1;
        } else {
            $visitors_parking = 0;
        }
        $propertyInput['visitors_parking'] = $visitors_parking;
        if (Request::get('shaded_parking')) {
            $shaded_parking = 1;
        } else {
            $shaded_parking = 0;
        }
        $propertyInput['shaded_parking'] = $shaded_parking;
        if (Request::get('industrial_power')) {
            $industrial_power = 1;
        } else {
            $industrial_power = 0;
        }
        $propertyInput['industrial_power'] = $industrial_power;
        if (Request::get('admin_offices')) {
            $admin_offices = 1;
        } else {
            $admin_offices = 0;
        }
        $propertyInput['admin_offices'] = $admin_offices;
        if (Request::get('roller_shutter_gate')) {
            $roller_shutter_gate = 1;
        } else {
            $roller_shutter_gate = 0;
        }
        $propertyInput['roller_shutter_gate'] = $roller_shutter_gate;
        if (Request::get('gantry')) {
            $gantry = 1;
        } else {
            $gantry = 0;
        }
        $propertyInput['gantry'] = $gantry;
        if (Request::get('extra_heights')) {
            $extra_heights = 1;
        } else {
            $extra_heights = 0;
        }
        $propertyInput['extra_heights'] = $extra_heights;
        if (Request::get('concrete_floor')) {
            $concrete_floor = 1;
        } else {
            $concrete_floor = 0;
        }
        $propertyInput['concrete_floor'] = $concrete_floor;
        if (Request::get('tiled_floor')) {
            $tiled_floor = 1;
        } else {
            $tiled_floor = 0;
        }
        $propertyInput['tiled_floor'] = $tiled_floor;
        if (Request::get('carpet_floor')) {
            $carpet_floor = 1;
        } else {
            $carpet_floor = 0;
        }
        $propertyInput['carpet_floor'] = $carpet_floor;
        if (Request::get('security_system')) {
            $security_system = 1;
        } else {
            $security_system = 0;
        }
        $propertyInput['security_system'] = $security_system;
        if (Request::get('fully_fenced')) {
            $fully_fenced = 1;
        } else {
            $fully_fenced = 0;
        }
        $propertyInput['fully_fenced'] = $fully_fenced;
        if (Request::get('gated_estate')) {
            $gated_estate = 1;
        } else {
            $gated_estate = 0;
        }
        $propertyInput['gated_estate'] = $gated_estate;
        if (Request::get('fully_walled')) {
            $fully_walled = 1;
        } else {
            $fully_walled = 0;
        }
        $propertyInput['fully_walled'] = $fully_walled;
        if (Request::get('shop_front')) {
            $shop_front = 1;
        } else {
            $shop_front = 0;
        }
        $propertyInput['shop_front'] = $shop_front;
        if (Request::get('sprinkler_system')) {
            $sprinkler_system = 1;
        } else {
            $sprinkler_system = 0;
        }
        $propertyInput['sprinkler_system'] = $sprinkler_system;
        if (Request::get('fire_hydrant')) {
            $fire_hydrant = 1;
        } else {
            $fire_hydrant = 0;
        }
        $propertyInput['fire_hydrant'] = $fire_hydrant;
        if (Request::get('garbage_area')) {
            $garbage_area = 1;
        } else {
            $garbage_area = 0;
        }
        $propertyInput['garbage_area'] = $garbage_area;
        if (Request::get('heavy_load_floors')) {
            $heavy_load_floors = 1;
        } else {
            $heavy_load_floors = 0;
        }
        $propertyInput['heavy_load_floors'] = $heavy_load_floors;
        if (Request::get('swing_gate')) {
            $swing_gate = 1;
        } else {
            $swing_gate = 0;
        }
        $propertyInput['swing_gate'] = $swing_gate;
        if (Request::get('delivery_entrance')) {
            $delivery_entrance = 1;
        } else {
            $delivery_entrance = 0;
        }
        $propertyInput['delivery_entrance'] = $delivery_entrance;
        if (Request::get('loading_bay')) {
            $loading_bay = 1;
        } else {
            $loading_bay = 0;
        }
        $propertyInput['loading_bay'] = $loading_bay;
        if (Request::get('train_track')) {
            $train_track = 1;
        } else {
            $train_track = 0;
        }
        $propertyInput['train_track'] = $train_track;
        if (Request::get('garden_area')) {
            $garden_area = 1;
        } else {
            $garden_area = 0;
        }
        $propertyInput['garden_area'] = $garden_area;
        if (Request::get('mezzanine_level')) {
            $mezzanine_level = 1;
        } else {
            $mezzanine_level = 0;
        }
        $propertyInput['mezzanine_level'] = $mezzanine_level;
        if (Request::get('entertainment_area')) {
            $entertainment_area = 1;
        } else {
            $entertainment_area = 0;
        }
        $propertyInput['entertainment_area'] = $entertainment_area;
        if (Request::get('pre_paid_meters')) {
            $pre_paid_meters = 1;
        } else {
            $pre_paid_meters = 0;
        }
        $propertyInput['pre_paid_meters'] = $pre_paid_meters;

        // end of security and ameneties

        $user_id = Auth::user()->id;
        $propertyInput['user_id'] = $user_id;

        //create unique reference number
        $reference = $this->newReference($propertyInput['listing_type']);

        $propertyInput['reference'] = $reference;

        if (! $this->property->fill($propertyInput)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->property->errors);
        }

        $this->property['available_from'] = date('Y-m-d');

        $this->property['postcode'] = Request::get('postcode');

        $this->property->save();

        if ($propertyInput['property_type'] === 'Residential') {
            $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);
            $resInput['property_id'] = $this->property->id;
            $residential = new Residential();
            $residential->fill($resInput)->save();
        }

        if ($propertyInput['property_type'] === 'Commercial') {
            $comInput = Request::only(['commercial_type', 'parking']);
            $comInput['property_id'] = $this->property->id;
            $commercial = new Commercial();
            $commercial->fill($comInput)->save();
        }

        if ($propertyInput['property_type'] === 'Farm') {
            $farmInput = Request::only(['farm_type']);
            $farmInput['property_id'] = $this->property->id;
            $farm = new Farm();
            $farm->fill($farmInput)->save();
        }

        $vettingInput = Request::only(['property_id', 'owner_title', 'owner_firstname', 'owner_surname', 'ownership_type',
            'rep_title', 'rep_firstname', 'rep_surname', 'rep_id_number', 'rep_cell_num', 'rep_email', 'marriage_stat',
            'mar_in_com', 'spouse_title', 'spouse_firstname', 'spouse_surname', 'spouse_id_number',
            'extra_owner_title1', 'extra_owner_firstname1', 'extra_owner_surname1', 'extra_owner_title2',
            'extra_owner_firstname2', 'extra_owner_surname2', 'extra_owner_title3', 'extra_owner_firstname3',
            'extra_owner_surname3', ]);

        if ($vettingInput['owner_firstname'] == '') {
            $vettingInput['owner_firstname'] = $propertyInput['company'];
        }

        $vettingInput['property_id'] = $this->property->id;

        $vettingInput['vetting_status'] = 1;

        // validate and save to vetting table if not valid return and delete $this->property
        $vetting = new Vetting();

        if (! $vetting->fill($vettingInput)->isValid()) {
            $this->property->forceDelete();

            return redirect()->back()->withInput()->withErrors($vetting->errors);
        }

        $vetting->save();

        return redirect()->route('property.createcont', $this->property->id);
    }

    public function newReference($listing_type)
    {
        $datetime = date('Y-m-d H:i:s');
        $user_id = Auth::user()->id;
        switch ($listing_type):
            case 'welet':
            case 'in2rental':
                $rent_ref = DB::select('SELECT `rent_ref` FROM `reference` WHERE `id` = ?', [1]);
        $rent_ref = $rent_ref[0]->rent_ref;
        $reference = 'RENT-'.str_pad($rent_ref, 6, '0', STR_PAD_LEFT);
        $rent_ref++;
        DB::update('UPDATE `reference` SET `rent_ref` = ?, `updated_at` = NOW()', [$rent_ref]);
        if (Auth::user()->hasTag('LANDLORD')) {
        } else {
            $tag = DB::table('tags')->where('name', '=', 'OWNER')->first();
            DB::table('role_user')->insert(['role_id' => $tag->id, 'user_id' => $user_id, 'created_at' => $datetime,
                        'updated_at' => $datetime, ]);
        }

        break;
        case 'wesell':
            case 'in2assets':
                $sale_ref = DB::select('SELECT `sale_ref` FROM `reference` WHERE `id` = ?', [1]);
        $sale_ref = $sale_ref[0]->sale_ref;
        $reference = 'SALE-'.str_pad($sale_ref, 6, '0', STR_PAD_LEFT);
        $sale_ref++;
        DB::update('UPDATE `reference` SET `sale_ref` = ?, `updated_at` = NOW()', [$sale_ref]);
        if (Auth::user()->hasTag('SELLER')) {
        } else {
            $tag = DB::table('tags')->where('name', '=', 'SELLER')->first();
            DB::table('role_user')->insert(['role_id' => $tag->id, 'user_id' => $user_id, 'created_at' => $datetime,
                        'updated_at' => $datetime, ]);
        }

        break;
        case 'weauction':
                $auction_ref = DB::select('SELECT `auction_ref` FROM `reference` WHERE `id` = ?', [1]);
        $auction_ref = $auction_ref[0]->auction_ref;
        $reference = 'AUCT-'.str_pad($auction_ref, 6, '0', STR_PAD_LEFT);
        $auction_ref++;
        DB::update('UPDATE `reference` SET `auction_ref` = ?, `updated_at` = NOW()', [$auction_ref]);

        break;

        endswitch;

        return $reference;
    }

    public function getBroker($id)
    {
        $property = $this->property->select('broker')->whereId($id)->withTrashed()->first();

        if ($property->broker != null) {
            $broker = DB::table('broker_logos')->select('broker_name', 'logo_url')->whereId($property->broker)->first();
        } else {
            $broker = null;
        }

        return $broker;
    }

    public function hasBooleanAttributes($id)
    {
        $property = $this->property->whereId($id)->withTrashed()->first();
        $test = 0;
        foreach ($property->features as $attribute) {
            if ($value = $property->getFeatureValuebyType($attribute->name, $id, 1)) {
                $test++;
            }
        }
        if ($test > 1) {
            return true;
        }

        return false;
    }

    /**
     * Display the specified resource.
     * GET /property/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $slug = '')
    {
        if (is_numeric($id)) {
            $images = $this->property->
            join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')
                ->where('property.id', $id)->orderBy('uploads.position')->get();
            //dd($images[0]['property_type']);
            if (! $images->isEmpty()) {
                switch ($images[0]['property_type']):
                    case 'Residential':
                        $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                            ->join('vetting', 'property.id', '=', 'vetting.property_id')
                            ->where('property.id', $id)->first();

                break;
                case 'Commercial':
                        $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                            ->join('vetting', 'property.id', '=', 'vetting.property_id')
                            ->where('property.id', $id)->first();
//                        dd($property);
                break;
                case 'Farm':
                        $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                            ->join('vetting', 'property.id', '=', 'vetting.property_id')
                            ->where('property.id', $id)->first();

                break;
                default:
                        return redirect()->back();

                break;
                endswitch;
            } else {
                return redirect()->route('property.removed', ['id' => $id], 301);
            }
            //dd($property);
            if ($property->land_measurement == 'HA') {
                $property->land_size = $property->land_size / 10000;
            }

            $contact = DB::table('users')->whereId($property->user_id)->first();

            if ($property['attourney'] == 0) {
                $attorney = DB::table('own_attourney')->whereProperty_id($id)->first();
            } else {
                $attorney = $this->service_provider->whereId($property['attourney'])->first();
            }

            if (Auth::check()) {
                $madeContact = DB::table('make_contact')->whereUser_id(Auth::user()->id)->whereProperty_id($id)
                    ->first();
            } else {
                $madeContact = null;
            }
            $hasFacebookImage = file_exists(AppHelper::imgFacebook($property->lead_image));

            switch ($property->listing_type) {
                case 'wesell':
                    $listing_type = 'for-sale';

                    break;
                case 'welet':
                    if ($property->property_type == 'residential') {
                        $listing_type = 'to-rent';
                    } else {
                        $listing_type = 'to-let';
                    }

                    break;
                case 'weauction':
                    $listing_type = 'on-auction';

                    break;
                default:
                    $listing_type = 'all-listings';

                    break;
            }

            if ($property->listing_type == 'weauction') {
                $auctionDets = $this->auction->whereProperty_id($id)->first();

                $dtTNow = date('Y-m-d H:i:s');

                if ($auctionDets->status == 'pending' && $auctionDets->start_date <= $dtTNow) {
                    $property->price = $auctionDets->start_price;
                    $property->update();
                    $auctionDets->status = 'live';
                    $auctionDets->save();
                }

                if ($auctionDets->status == 'live' && $auctionDets->end_date <= $dtTNow &&
                    ($property->vetting_status == 2 || $property->vetting_status == 16)
                ) {
                    $auctionDets->status = 'not_sold';
                    $auctionDets->save();

                    if ($auctionDets->auction_type == 1) {
                        $property->vetting_status == 9;
                        $property->update();
                    }
                }

                $auctionCalendarShow = $this->property->with('auction')->with('residential')->with('commercial')
                    ->with('farm')->whereId($id)->first();

                $auctioneer = DB::table('auctioneers')->whereId($auctionDets->auctioneer)->first();

                $auctionDets->auctioneer = $auctioneer->auctioneer;

                $agent = $this->user->whereId($property->rep_id_number)->first();

                if ($agent == null) {
                    $agent = $this->user->whereId(1)->first();
                    $property->rep_firstname = 'In2assets';
                    $property->rep_surname = '';
                    $property->rep_cell_num = '0861 444 769';
                    $property->rep_email = 'marketing@in2assets.com';
                    $property->rep_id_number = '1';
                }

                if ($auctionDets != null) {
                    if (Auth::check()) {
                        $yourBids = $this->auction_bids->whereUser_id(Auth::user()->id)->orderBy('id', 'desc')->first();
                    } else {
                        $yourBids = null;
                    }

                    if ($auctionDets->auction_type == 2 || $auctionDets->auction_type == 3) {
                        if ($auctionDets->status == 'not_sold') {
                            $register = null;
                        } elseif (Auth::check()) {
                            $register = $this->auction_register->whereUser_id(Auth::user()->id)
                                ->whereAuction_id($auctionDets->id)->first();
                        } else {
                            $register = null;
                        }

                        $physicalAuction = null;
                    } elseif ($auctionDets->auction_type == 1) {
                        if ($auctionDets->status == 'not_sold') {
                            $register = null;
                        } elseif (Auth::check()) {
                            $register = $this->physical_auction_register->whereUser_id(Auth::user()->id)
                                ->whereAuction_id($auctionDets->physical_auction)->first();
                        } else {
                            $register = null;
                        }
                        $physicalAuction = $this->physical_auction->whereId($auctionDets->physical_auction)->first();
                    } elseif ($auctionDets->auction_type == 5) {
                        if ($auctionDets->status == 'not_sold') {
                            $register = null;
                        } elseif (Auth::check()) {
                            $register = $this->auction_register->whereUser_id(Auth::user()->id)
                                ->whereAuction_id($auctionDets->id)->first();
                        } else {
                            $register = null;
                        }
                        $physicalAuction = null;
                    } else {
                        $yourBids = null;
                        $register = null;
                        $physicalAuction = null;
                    }

                    $broker = $this->getBroker($property->property_id);

                    $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

                    foreach ($citiesDB as $city) {
                        $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
                    }
                    $cities = implode('", "', $cities);

                    if (! Auth::check()) {
                        $user_id = 0;
                    } else {
                        $user_id = Auth::user()->id;
                    }
                    DB::table('property_views')->insert(['property_id' => $property->property_id,
                        'user_id' => $user_id, ]);

                    $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
                    $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
                    $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);

                    $hasFeatures = $this->hasBooleanAttributes($property->property_id);
                    $slug = new Slugify();

                    $property->province_slug = $slug->slugify($property->province);
                    $property->city_slug = $slug->slugify($property->area);
                    $property->suburb_slug = $slug->slugify($property->suburb_town_city);

                    return view('property.show', ['property' => $property, 'images' => $images,
                        'contact' => $contact, 'auctionDets' => $auctionDets, 'yourBids' => $yourBids,
                        'register' => $register, 'madeContact' => $madeContact,
                        'auctionCalendarShow' => $auctionCalendarShow, 'attorney' => $attorney, 'agent' => $agent,
                        'physicalAuction' => $physicalAuction, 'broker' => $broker, 'popup' => false,
                        'cities' => $cities, 'features' => $features, 'numeric_features' => $numeric_features,
                        'text_features' => $text_features, 'showTitle' => '', 'hasFeatures' => $hasFeatures,
                        'hasFacebookImage' => $hasFacebookImage, 'listing_type' => $listing_type, ]);
                } else {
                    $broker = $this->getBroker($property->property_id);

                    if (! Auth::check()) {
                        $user_id = 0;
                    } else {
                        $user_id = Auth::user()->id;
                    }
                    DB::table('property_views')->insert(['property_id' => $property->property_id,
                        'user_id' => $user_id, ]);

                    $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
                    $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
                    $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);

                    $hasFeatures = $this->hasBooleanAttributes($property->property_id);
                    $slug = new Slugify();

                    $property->province_slug = $slug->slugify($property->province);
                    $property->city_slug = $slug->slugify($property->area);
                    $property->suburb_slug = $slug->slugify($property->suburb_town_city);

                    return view('property.show', ['property' => $property, 'images' => $images,
                        'contact' => $contact, 'auctionDets' => $auctionDets, 'madeContact' => $madeContact,
                        'auctionCalendarShow' => $auctionCalendarShow, 'attorney' => $attorney, 'agent' => $agent,
                        'broker' => $broker, 'popup' => false, 'features' => $features,
                        'numeric_features' => $numeric_features, 'text_features' => $text_features,
                        'showTitle' => '', 'hasFeatures' => $hasFeatures, 'hasFacebookImage' => $hasFacebookImage,
                        'listing_type' => $listing_type, ]);
                }
            } else {
                $broker = $this->getBroker($property->property_id);

                if (! Auth::check()) {
                    $user_id = 0;
                } else {
                    $user_id = Auth::user()->id;
                }
                DB::table('property_views')->insert(['property_id' => $property->property_id, 'user_id' => $user_id]);

                $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
                $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
                $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);

                $hasFeatures = $this->hasBooleanAttributes($property->property_id);
                $agent = $this->user->whereId($property->rep_id_number)->first();
                if ($agent == null) {
                    $agent = $this->user->whereId(1)->first();
                    $property->rep_firstname = 'In2assets';
                    $property->rep_surname = '';
                    $property->rep_cell_num = '0861 444 769';
                    $property->rep_email = 'marketing@in2assets.com';
                    $property->rep_id_number = '1';
                }
                $slug = new Slugify();

                $property->province_slug = $slug->slugify($property->province);
                $property->city_slug = $slug->slugify($property->area);
                $property->suburb_slug = $slug->slugify($property->suburb_town_city);

                return view('property.show', ['property' => $property, 'images' => $images,
                    'contact' => $contact, 'madeContact' => $madeContact, 'attorney' => $attorney, 'agent' => $agent,
                    'broker' => $broker, 'features' => $features, 'numeric_features' => $numeric_features,
                    'text_features' => $text_features, 'showTitle' => '', 'hasFeatures' => $hasFeatures,
                    'hasFacebookImage' => $hasFacebookImage, 'listing_type' => $listing_type, ]);
            }
        }

        return view('property.show', ['property' => '0', 'images' => '0', 'contact' => '0']);
    }

    public function propertyRemoved($id)
    {
        if (! $property = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->where('property.id', $id)->withTrashed()->first()
        ) {
            Flash::message('This property has been deleted from our database');

            return redirect()->route('home', 301);
        }

        switch ($property->property_type):
            case 'Residential':
                $property = $this->property->leftJoin('residential', 'property.id', '=', 'residential.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->withTrashed()->first();

        break;
        case 'Commercial':
                $property = $this->property->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->withTrashed()->first();

        break;
        case 'Farm':
                $property = $this->property->leftJoin('farm', 'property.id', '=', 'farm.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->withTrashed()->first();

        break;
        default:
                return view('property.removed');

        break;
        endswitch;

        if (strpos($property->reference, 'ZZ') !== false) {
            return view('property.removed');
        }

        $images = $this->property->
        join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')
            ->where('property.id', $id)->orderBy('uploads.position')->withTrashed()->get();

        $contact = DB::table('users')->whereId($property->user_id)->first();

        if (Auth::check()) {
            $madeContact = DB::table('make_contact')->whereUser_id(Auth::user()->id)->whereProperty_id($id)->first();
        } else {
            $madeContact = null;
        }

        if ($property->attourney == 0) {
            $attorney = DB::table('own_attourney')->whereProperty_id($id)->first();
        } else {
            $attorney = $this->service_provider->whereId($property->attourney)->first();
        }

        $agent = $this->user->whereId($property->rep_id_number)->first();
        if ($agent == null) {
            $agent = $this->user->whereId(1)->first();
            $property->rep_firstname = 'In2assets';
            $property->rep_surname = '';
            $property->rep_cell_num = '0861 444 769';
            $property->rep_email = 'marketing@in2assets.com';
            $property->rep_id_number = '1';
        }

        $broker = $this->getBroker($id);

        $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
        $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
        $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);

        $hasFeatures = $this->hasBooleanAttributes($property->property_id);

        $hasFacebookImage = file_exists(AppHelper::imgFacebook($property->lead_image));

        switch ($property->listing_type) {
            case 'wesell':
            case 'in2assets':
                $listing_type = 'for sale';

                break;
            case 'welet':
            case 'in2rental':
                if (Request::get('property_type') == 'Residential') {
                    $listing_type = 'to rent';
                } else {
                    $listing_type = 'to let';
                }

                break;
            case 'weauction':
                $listing_type = 'auction';

                break;
            default:
                $listing_type = 'all-listings';

                break;
        }

        if ($property->listing_type == 'weauction') {
            $auctionDets = $this->auction->whereProperty_id($id)->first();

            $dtTNow = date('Y-m-d H:i:s');

            if ($auctionDets->status == 'pending' && $auctionDets->start_date <= $dtTNow) {
                $property->price = $auctionDets->start_price;
                $property->update();
                $auctionDets->status = 'live';
                $auctionDets->save();
            }

            if ($auctionDets->status == 'live' && $auctionDets->end_date <= $dtTNow && ($property->vetting_status == 2
                    || $property->vetting_status == 16)
            ) {
                $auctionDets->status = 'not_sold';
                $auctionDets->save();

                if ($auctionDets->auction_type == 1) {
                    $property->vetting_status == 9;
                    $property->update();
                }
            }

            $auctionCalendarShow = $this->property->with('auction')->with('residential')->with('commercial')
                ->with('farm')->whereId($id)->first();

            $auctioneer = DB::table('auctioneers')->whereId($auctionDets->auctioneer)->first();

            $auctionDets->auctioneer = $auctioneer->auctioneer;

            $agent = $this->user->whereId($property->rep_id_number)->first();

            if ($agent == null) {
                $agent = $this->user->whereId(1)->first();
                $property->rep_firstname = 'In2assets';
                $property->rep_surname = '';
                $property->rep_cell_num = '0861 444 769';
                $property->rep_email = 'marketing@in2assets.com';
                $property->rep_id_number = '1';
            }

            if ($auctionDets != null) {
                if (Auth::check()) {
                    $yourBids = $this->auction_bids->whereUser_id(Auth::user()->id)->orderBy('id', 'desc')->first();
                } else {
                    $yourBids = null;
                }

                if ($auctionDets->auction_type == 2 || $auctionDets->auction_type == 3) {
                    if ($auctionDets->status == 'not_sold') {
                        $register = null;
                    } elseif (Auth::check()) {
                        $register = $this->auction_register->whereUser_id(Auth::user()->id)
                            ->whereAuction_id($auctionDets->id)->first();
                    } else {
                        $register = null;
                    }

                    $physicalAuction = null;
                } elseif ($auctionDets->auction_type == 1) {
                    if ($auctionDets->status == 'not_sold') {
                        $register = null;
                    } elseif (Auth::check()) {
                        $register = $this->physical_auction_register->whereUser_id(Auth::user()->id)
                            ->whereAuction_id($auctionDets->physical_auction)->first();
                    } else {
                        $register = null;
                    }
                    $physicalAuction = $this->physical_auction->whereId($auctionDets->physical_auction)->first();
                } elseif ($auctionDets->auction_type == 5) {
                    if ($auctionDets->status == 'not_sold') {
                        $register = null;
                    } elseif (Auth::check()) {
                        $register = $this->auction_register->whereUser_id(Auth::user()->id)
                            ->whereAuction_id($auctionDets->id)->first();
                    } else {
                        $register = null;
                    }
                    $physicalAuction = null;
                } else {
                    $yourBids = null;
                    $register = null;
                    $physicalAuction = null;
                }

                $broker = $this->getBroker($property->property_id);

                $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

                foreach ($citiesDB as $city) {
                    $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
                }
                $cities = implode('", "', $cities);

                if (! Auth::check()) {
                    $user_id = 0;
                } else {
                    $user_id = Auth::user()->id;
                }
                DB::table('property_views')->insert(['property_id' => $property->property_id, 'user_id' => $user_id]);

                $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
                $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
                $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);

                $hasFeatures = $this->hasBooleanAttributes($property->property_id);
                $slug = new Slugify();

                $property->province_slug = $slug->slugify($property->province);
                $property->city_slug = $slug->slugify($property->area);
                $property->suburb_slug = $slug->slugify($property->suburb_town_city);

                return view('property.show', ['property' => $property, 'images' => $images, 'contact' => $contact,
                    'auctionDets' => $auctionDets, 'yourBids' => $yourBids, 'register' => $register,
                    'madeContact' => $madeContact, 'auctionCalendarShow' => $auctionCalendarShow, 'attorney' => $attorney,
                    'agent' => $agent, 'physicalAuction' => $physicalAuction, 'broker' => $broker, 'popup' => false,
                    'cities' => $cities, 'features' => $features, 'numeric_features' => $numeric_features,
                    'text_features' => $text_features, 'showTitle' => '', 'hasFeatures' => $hasFeatures,
                    'hasFacebookImage' => $hasFacebookImage, 'listing_type' => $listing_type, ]);
            } else {
                $broker = $this->getBroker($property->property_id);

                if (! Auth::check()) {
                    $user_id = 0;
                } else {
                    $user_id = Auth::user()->id;
                }
                DB::table('property_views')->insert(['property_id' => $property->property_id, 'user_id' => $user_id]);

                $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
                $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
                $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);

                $hasFeatures = $this->hasBooleanAttributes($property->property_id);
                $slug = new Slugify();

                $property->province_slug = $slug->slugify($property->province);
                $property->city_slug = $slug->slugify($property->area);
                $property->suburb_slug = $slug->slugify($property->suburb_town_city);

                return view('property.show', ['property' => $property, 'images' => $images, 'contact' => $contact,
                    'auctionDets' => $auctionDets, 'madeContact' => $madeContact,
                    'auctionCalendarShow' => $auctionCalendarShow, 'attorney' => $attorney, 'agent' => $agent,
                    'broker' => $broker, 'popup' => false, 'features' => $features,
                    'numeric_features' => $numeric_features, 'text_features' => $text_features,
                    'showTitle' => '', 'hasFeatures' => $hasFeatures, 'hasFacebookImage' => $hasFacebookImage,
                    'listing_type' => $listing_type, ]);
            }
        }

        return view('property.show', ['property' => $property, 'images' => $images,
            'contact' => $contact, 'madeContact' => $madeContact, 'attorney' => $attorney, 'agent' => $agent,
            'broker' => $broker, 'features' => $features, 'numeric_features' => $numeric_features,
            'text_features' => $text_features, 'showTitle' => '', 'hasFeatures' => $hasFeatures,
            'hasFacebookImage' => $hasFacebookImage, 'listing_type' => $listing_type, ], compact('message'), 410);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /property/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $property = $this->property->whereId($id)->first();
        if (is_null($property)) {
            return redirect()->route('property.index');
        }

        switch ($property['property_type']):
            case 'Residential':
                $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                    ->where('property.id', $id)->first();

        break;
        case 'Commercial':
                $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                    ->where('property.id', $id)->first();

        break;
        case 'Farm':
                $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                    ->where('property.id', $id)->first();

        break;
        default:
                return redirect()->route('property.index');

        break;
        endswitch;

        $user = $this->user->whereId($property->user_id)->first();
        $files = $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')
            ->orderBy('uploads.position')->get();

        return view('property.edit', ['property' => $property, 'files' => $files]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /property/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function Prop_userAdd($pId, $uId, $tId)
    {
        if (! $id = DB::select("SELECT id FROM property_user WHERE property_id = $pId AND user_id = $uId AND role_id = $tId")) {
            DB::table('property_user')->insert(['property_id' => $pId, 'user_id' => $uId, 'role_id' => $tId]);
            if (! $this->user->find($uId)->hasTag($tId)) {
                $this->user->find($uId)->assignTag($tId);
            }
        }
    }

    public function update($id)
    {
        $propertyInput = Request::only(['complex_number', 'complex_name', 'street_number', 'street_address', 'country',
            'listing_type', 'property_type', 'price', 'x_coord', 'y_coord', 'sale_type', 'vat_num', 'reg_num',
            'company', 'province', 'area', 'suburb_town_city', 'broker', 'office_size', 'office_price', 'factory_price',
            'shop_price', 'yard_price', 'vacant_land_price', 'factory_size', 'shop_size', 'yard_size', 'showroom_size',
            'vacant_land_size', 'covered_parking_bays', 'covered_parking_price', 'parking_bays', 'parking_price',
            'zoning', 'amps', 'agent_percent', 'listing_title', 'lead_image', 'short_descrip', 'long_descrip',
            'property_highlight', 'youtube', 'virtual_tour', 'lead_image', ]);

        if (Request::get('show_street_address') != null) {
            $propertyInput['show_street_address'] = 1;
        } else {
            $propertyInput['show_street_address'] = 0;
        }

        if (Request::get('owner_email') != '') {
            if (Request::get('owner_name') == '') {
                if (! $owner_id = DB::table('users')->select('id')->where('email', '=', Request::get('owner_email'))
                    ->first()
                ) {
                    $owner_id = DB::table('users')->insertGetId(['firstname' => Request::get('company'),
                        'id_number' => Request::get('owner_id'), 'email' => Request::get('owner_email'),
                        'title' => Request::get('owner_title'), 'reg_num' => Request::get('reg_num'),
                        'vat_num' => Request::get('vat_num'), 'cellnumber' => Request::get('owner_cellnumber'), ]);
                    $this->Prop_userAdd($id, $owner_id, 4);
                } else {
                    $this->user->find($owner_id->id)->update(['firstname' => Request::get('company'),
                        'id_number' => Request::get('owner_id'), 'email' => Request::get('owner_email'),
                        'title' => Request::get('owner_title'), 'reg_num' => Request::get('reg_num'),
                        'vat_num' => Request::get('vat_num'), 'cellnumber' => Request::get('owner_cellnumber'), ]);
                    $this->Prop_userAdd($id, $owner_id->id, 4);
                }
            } else {
                if (! $owner_id = DB::table('users')->select('id')->where('email', '=', Request::get('owner_email'))
                    ->first()
                ) {
                    $owner_id = DB::table('users')->insertGetId(['firstname' => Request::get('owner_name'),
                        'id_number' => Request::get('owner_id'), 'email' => Request::get('owner_email'),
                        'title' => Request::get('owner_title'), 'reg_num' => Request::get('reg_num'),
                        'vat_num' => Request::get('vat_num'), 'cellnumber' => Request::get('owner_cellnumber'),
                        'companyname' => Request::get('company'), ]);
                    $this->Prop_userAdd($id, $owner_id, 4);
                } else {
                    $this->user->find($owner_id->id)->update(['firstname' => Request::get('owner_name'),
                        'id_number' => Request::get('owner_id'), 'email' => Request::get('owner_email'),
                        'title' => Request::get('owner_title'), 'reg_num' => Request::get('reg_num'),
                        'vat_num' => Request::get('vat_num'), 'cellnumber' => Request::get('owner_cellnumber'),
                        'companyname' => Request::get('company'), ]);
                    $this->Prop_userAdd($id, $owner_id->id, 4);
                }
            }
        }

        if ($propertyInput['listing_type'] == 'in2rental') {
            $propertyInput['available_from'] = date('Y-m-d', strtotime(Request::get('available_from')));
            $propertyInput['rental_terms'] = Request::get('rental_terms');
        }

        $slug = new Slugify();

        switch ($propertyInput['property_type']) {
            case 'Commercial':
                $property_type = Request::get('commercial_type');

                break;
            case 'Residential':
                $property_type = Request::get('residential_type');

                break;
            case 'Farm':
                $property_type = Request::get('farm_type');

                break;
        }

        switch ($propertyInput['listing_type']) {
            case 'wesell':
            case 'in2assets':
                $listing_type = 'for sale';

                break;
            case 'welet':
            case 'in2rental':
                if (Request::get('property_type') == 'Residential') {
                    $listing_type = 'to rent';
                } else {
                    $listing_type = 'to let';
                }

                break;
            case 'weauction':
                $listing_type = 'auction';

                break;
        }

        if ($propertyInput['show_street_address'] == 1) {
            $street_address = ' '.$propertyInput['street_number'].' '.$propertyInput['street_address'];
        } else {
            $street_address = '';
        }

        if ($propertyInput['area'] == $propertyInput['suburb_town_city']) {
            $propertyInput['slug'] = $slug->slugify($propertyInput['property_type'].' '.$property_type.' '.
                $listing_type.' '.$propertyInput['province'].' '.$propertyInput['area'].$street_address);
        } else {
            $propertyInput['slug'] = $slug->slugify($propertyInput['property_type'].' '.$property_type.' '.
                $listing_type.' '.$propertyInput['province'].' '.$propertyInput['area'].' '.
                $propertyInput['suburb_town_city'].$street_address);
        }

        $this->property = $this->property->find($id);

        $changeImageTitle = false;
        if ($propertyInput['listing_title'] != $this->property->listing_title) {
            $changeImageTitle = true;
        }

        if (Request::get('land_measurement') == 'HA') {
            $land_size = Request::get('land_size') * 10000;
        } else {
            $land_size = Request::get('land_size');
        }

        $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
        foreach ($numeric_features as $numeric_feature) {
            if (Request::get($numeric_feature->name) > 0 && ! $this->property->hasAttribute($numeric_feature->name)) {
                if ($numeric_feature->name == 'land_size') {
                    $this->property->assignAttribute($numeric_feature->id, $land_size);
                } elseif ($numeric_feature->name == 'rates' || $numeric_feature->name == 'levies') {
                    $this->property->assignAttribute($numeric_feature->id, str_replace(' ', '', Request::get($numeric_feature->name)));
                } else {
                    $this->property->assignAttribute($numeric_feature->id, Request::get($numeric_feature->name));
                }
            } elseif (Request::get($numeric_feature->name) > 0 && $this->property->hasAttribute($numeric_feature->name)) {
                if ($numeric_feature->name == 'land_size') {
                    $this->property->ammendAttribute($numeric_feature->id, $this->property->id, $land_size);
                } elseif ($numeric_feature->name == 'rates' || $numeric_feature->name == 'levies') {
                    $this->property->ammendAttribute($numeric_feature->id, $this->property->id, str_replace(' ', '', Request::get($numeric_feature->name)));
                } else {
                    $this->property->ammendAttribute($numeric_feature->id, $this->property->id, Request::get($numeric_feature->name));
                }
            } elseif (! Request::get($numeric_feature->name) > 0 && $this->property->hasAttribute($numeric_feature->name)) {
                $this->property->removeAttribute($numeric_feature->name);
            }
        }

        $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);
        foreach ($text_features as $text_feature) {
            if (Request::get($text_feature->name) != '' && ! $this->property->hasAttribute($text_feature->name)) {
                $this->property->assignAttribute($text_feature->id, Request::get($text_feature->name));
            } elseif (Request::get($text_feature->name) != '' && $this->property->hasAttribute($text_feature->name)) {
                $this->property->ammendAttribute($text_feature->id, $id, Request::get($text_feature->name));
            } elseif (Request::get($text_feature->name) == '' && $this->property->hasAttribute($text_feature->name)) {
                $this->property->removeAttribute($text_feature->name);
            }
        }

        $boolean_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
        foreach ($boolean_features as $boolean_feature) {
            if (Request::get('feature'.$boolean_feature->name) && ! $this->property->hasAttribute($boolean_feature->name)) {
                $this->property->assignAttribute($boolean_feature->name, 1);
            } elseif (! Request::get('feature'.$boolean_feature->name) && $this->property->hasAttribute($boolean_feature->name)) {
                $this->property->removeAttribute($boolean_feature->id);
            }
        }
        $this->property->fill($propertyInput)->update();

        // if property type is residential then validate and save or redirect back to form with residential errors
        if ($this->property->property_type == 'Residential') {
            $resInput = Request::only(['dwelling_type']);
            $resInput['property_id'] = $this->property->id;

            // validate and save to residential table if not valid return and delete $this->property
            $residential = new Residential();
            $residential = $residential->whereProperty_id($id)->first();
            $residential->fill($resInput)->update();
        }

        // if property type is commercial then validate and save or redirect back to form with commercial errors
        if ($this->property->property_type == 'Commercial') {
            $comInput = Request::only(['parking', 'commercial_type']);
            $comInput['property_id'] = $this->property->id;

            // validate and save to residential table if not valid return and delete $this->property
            $commercial = new Commercial();
            $commercial = $commercial->whereProperty_id($id)->first();
            $commercial->fill($comInput)->update();
        }

        if ($this->property->property_type == 'Farm') {
            $farmInput = Request::only(['farm_type']);
            $farmInput['property_id'] = $this->property->id;

            // validate and save to residential table if not valid return and delete $this->property
            $farm = new Farm();
            $farm = $farm->whereProperty_id($id)->first();
            $farm->fill($farmInput)->update();
        }

        $vettingInput = Request::only(['rep_email', 'rep_cell_num', 'rep_firstname', 'rep_surname', 'ownership_type']);
        $vettingInput['property_id'] = $this->property->id;
        $vettingInput['rep_id_number'] = Request::get('agents');

        // validate and save to vetting table if not valid return and delete $this->property
        $vetting = $this->vetting->whereProperty_id($id)->first();

        $vetting->fill($vettingInput);

        if ($vetting->vetting_status == 1) {
            $vetting->vetting_status = 3;
        } elseif ($vetting->vetting_status == '') {
            $vetting->vetting_status = 1;
        }
        $vetting->save();

        //save Agent Notes

        if (Request::get('agent_notes') != '') {
            $this->prop_notes->note = Request::get('agent_notes');
            $this->prop_notes->property_id = $id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->save();
        }

        if ($this->property->property_type == 'Residential') {
            $type = Request::get('dwelling_type').' in '.$this->property->suburb_town_city;
        } elseif ($this->property->property_type == 'Commercial') {
            $type = Request::get('commercial_type').' in '.$this->property->suburb_town_city;
        } elseif ($this->property->property_type == 'Farm') {
            $type = Request::get('farm_type').' in '.$this->property->suburb_town_city;
        } else {
            $type = 'property';
        }

        if ($changeImageTitle) {
            $listing_title_slug = $slug->slugify($propertyInput['listing_title']);
            $directory = 'uploads/users/'.$this->property->user_id.'/'.$id.'/';
            if ($listing_title_slug == '') {
                $image_append = strtolower(str_replace(' ', '-', str_replace('<br>', '', $type)));
            } else {
                $image_append = $listing_title_slug;
            }
//            rename images

            $files = DB::select("SELECT u.file FROM uploads u WHERE u.class = 'property' AND u.type = 'image' AND u.link = ".$id);

            foreach ($files as $file) {
                $ext = pathinfo($file->file, PATHINFO_EXTENSION);
                $filename = 'image_'.$image_append.'_'.Str::random(12);
                $new_name = $directory.$filename.'.'.$ext;

                try {
                    rename($file->file, $new_name);
                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }
                $file2 = AppHelper::imgMini($file->file);
                $new_name2 = $directory.$filename.'_143_83.'.$ext;

                try {
                    rename($file2, $new_name2);
                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }
                $file3 = AppHelper::imgLarge($file->file);
                $new_name3 = $directory.$filename.'_835_467.'.$ext;

                try {
                    rename($file3, $new_name3);
                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }
//                rename database images
                $this->renameDBFileName($file->file, $new_name, 'image');
            }

//            rename lead image files

            $file_lead = DB::select('SELECT p.lead_image FROM property p WHERE p.id = '.$id);

            if (! empty($file_lead)) {
                $ext_lead = pathinfo($file_lead[0]->lead_image, PATHINFO_EXTENSION);

                $filename_lead = 'lead_image_'.$image_append.'_'.Str::random(12);
                $new_name_lead = $directory.$filename_lead.'.'.$ext_lead;

                try {
                    rename($file_lead[0]->lead_image, $new_name_lead);

                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }

                $file_lead2 = AppHelper::imgThumb($file_lead[0]->lead_image);
                $new_name_lead2 = $directory.$filename_lead.'_265_163.'.$ext_lead;

                try {
                    rename($file_lead2, $new_name_lead2);

                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }

                $file_lead3 = AppHelper::imgMap($file_lead[0]->lead_image);
                $new_name_lead3 = $directory.$filename_lead.'_400_161.'.$ext_lead;

                try {
                    rename($file_lead3, $new_name_lead3);

                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }

                $file_lead4 = AppHelper::imgMini($file_lead[0]->lead_image);
                $new_name_lead4 = $directory.$filename_lead.'_143_83.'.$ext_lead;

                try {
                    rename($file_lead4, $new_name_lead4);

                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }

                $file_lead5 = AppHelper::imgLarge($file_lead[0]->lead_image);
                $new_name_lead5 = $directory.$filename_lead.'_835_467.'.$ext_lead;

                try {
                    rename($file_lead5, $new_name_lead5);

                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }

                $file_lead6 = AppHelper::imgLandscape($file_lead[0]->lead_image);
                $new_name_lead6 = $directory.$filename_lead.'_1920_600.'.$ext_lead;

                try {
                    rename($file_lead6, $new_name_lead6);

                    //If the exception is thrown, this text will not be shown
                } //catch exception
                catch (Exception $e) {
                }

                $this->renameDBFileName($file_lead[0]->lead_image, $new_name_lead, 'lead_image');
            }
        }

        Flash::message('Property has been updated.');

        return redirect()->route('admin.property.edit', ['id' => $id]);
    }

    public function renameDBFileName($old_name, $new_name, $type)
    {
        $query = 'UPDATE uploads u SET u.file = "'.$new_name.'" WHERE u.file = "'.$old_name.'"';

        DB::update($query);

        if ($type == 'lead_image') {
            $query2 = 'UPDATE property p SET p.lead_image = "'.$new_name.'" WHERE p.lead_image = "'.$old_name.'"';

            DB::update($query2);
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /property/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $property = $this->property->find($id);

        if ($property->residential) {
            $property->residential->delete();
        }
        if ($property->commercial) {
            $property->commercial->delete();
        }
        if ($property->farm) {
            $property->farm->delete();
        }

        if ($property->listing_type = 'weauction') {
            $auction = $this->auction->whereProperty_id($id)->first();
            $auction->status = 'complete';
            $auction->update();
        }

        $property->vetting->delete();
        $property->delete();
        if (Auth::user()->hasRole('Admin')) {
            return redirect()->route('property.admin', ['status' => 'all']);
        }

        Session::start();

        return redirect()->route('property.manage_properties');
    }

    /** lists property by listing type and property type*/
    public function listType($listing_type)
    {

// $listingtype in each case becomes the title of the page
        switch ($listing_type) {
            case 'RentRes':
                $listing_type = 'Residential Rental';
                $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
                where('vetting.vetting_status', '=', 2)->whereListing_type('Rent')->whereProperty_type('Residential')->get();

                break;
            case 'RentCom':
                $listing_type = 'Commercial Rental';
                $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
                where('vetting.vetting_status', '=', 2)->whereListing_type('Rent')->whereProperty_type('Commercial')->get();

                break;
            case 'RentFarm':
                $listing_type = 'Farm Rental';
                $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
                where('vetting.vetting_status', '=', 2)->whereListing_type('Rent')->whereProperty_type('Farm')->get();

                break;
            case 'SaleRes':
                $listing_type = 'Residential Sale';
                $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
                where('vetting.vetting_status', '=', 2)->whereListing_type('Sale')->whereProperty_type('Residential')->
                orWhere('listing_type', 'Auction')->whereProperty_type('Residential')->get();

                break;
            case 'SaleCom':
                $listing_type = 'Commercial Sale';
                $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
                where('vetting.vetting_status', '=', 2)->whereListing_type('Sale')->whereProperty_type('Commercial')->
                orWhere('listing_type', 'Auction')->whereProperty_type('Commercial')->get();

                break;
            case 'SaleFarm':
                $listing_type = 'Farm Sale';
                $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
                where('vetting.vetting_status', '=', 2)->whereListing_type('Sale')->whereProperty_type('Farm')->
                orWhere('listing_type', 'Auction')->whereProperty_type('Farm')->get();

                break;
            default:
                $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
                where('vetting.vetting_status', '=', 2)->whereListing_type($listing_type)->get();

                break;
        }

        return view('property.index', ['properties' => $properties, 'listing' => $listing_type]);
    }

    public function get_adv_areas()
    {
        $provincesDB = DB::table('areas')->select('province')->groupBy('province')->get();
        $areasDB = DB::table('areas')->select('province', 'area')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->select('province', 'area', 'suburb_town_city')->groupBy('province', 'area', 'suburb_town_city')->get();
        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $areas['cities'] = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $areas['provinces'] = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas['areas'] = implode('", "', $areas);

        return $areas;
    }

    /**
     * Display a listing of the resource.
     * GET /property.
     *
     * @return Response
     */
    public function index($property_type, $listing_type, $province, $city, $suburb)
    {
        if ($property_type == 'agricultral') {
            $property_type = 'farm';
        }

        $params = explode('&', substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?') + 1));

        $parameters['o'] = 'default';
        $parameters['v'] = 'grid_view';
        $parameters['pr'] = '0-15000000+';
        $parameters['rpr'] = '0-30000+';
        $parameters['fr'] = '0-2000+';
        $parameters['lr'] = '0-20000+';
        $parameters['st'] = 'None';
        if ($property_type == 'industrial') {
            $parameters['st'] = 'industrial-space';
            $property_type = 'commercial';
        }
        $parameters['b1'] = 'None';
        $parameters['b2'] = 'None';

        foreach ($params as $param) {
            $x = explode('=', $param);
            if (isset($x[1]) && $x[1] != '') {
                $parameters[$x[0]] = $x[1];
            }
        }

        $province_slug = $province;
        $city_slug = $city;
        $suburb_slug = $suburb;

        if ($property_type == 'residential') {
            $featured = $this->getFeaturedProperties('Residential');
        } else {
            $featuredCom = $this->getFeaturedProperties('Commercial');
            $featuredFarm = $this->getFeaturedProperties('Farm');
            $featured = $featuredCom->merge($featuredFarm);
        }
        if ($province == 'south-africa') {
            $province = '';
            $city = '';
            $suburb = '';
            $suburb2 = '';
        } elseif ($province != 'south-africa' && $city != 'all-cities' && $suburb != 'all-suburbs') {
            if (substr($suburb, 0, 1) == '0') {
                $province = $this->getProvince($province);
                $city = $this->getCity($city);
                $suburb2 = $province.', '.$city.', Multiple suburbs';
            } else {
                $province = $this->getProvince($province);
                $city = $this->getCity($city);
                $suburb = $this->getSuburb($suburb);
                $suburb2 = $province.', '.$city.', '.$suburb;
            }
        } elseif ($city == 'all-cities' && $suburb == 'all-suburbs') {
            $province = $this->getProvince($province);
            $city = '';
            $suburb = '';
            $suburb2 = $province;
        } elseif ($city != 'all-cities' && $suburb == 'all-suburbs') {
            $province = $this->getProvince($province);
            $city = $this->getCity($city);
            $suburb = '';
            $suburb2 = $province.', '.$city;
        } elseif ($suburb != 'all-suburbs') {
            if (substr($suburb, 0, 1) == '0') {
                $province = $this->getProvince($province);
                $suburb2 = $province.', Multiple suburbs';
            } else {
                $province = $this->getProvince($province);
                $city = '';
                $suburb = $this->getSuburb($suburb);
                $suburb2 = $province.', '.$suburb;
            }
        }

        switch ($listing_type) {
            case 'for-sale':
                $set_action_type = 'wesell';

                break;
            case 'to-rent':
            case 'to-let':
                $set_action_type = 'welet';

                break;
            case 'on-auction':
                $set_action_type = 'weauction';

                break;
            default:
                $set_action_type = 'all-listings';

                break;
        }

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $set_all_type = $property_type;

        $adv_search_set['floor_space_range'] = $parameters['fr'];
        $adv_search_set['land_size_range'] = $parameters['lr'];
        $adv_search_set['price_range'] = $parameters['pr'];
        $adv_search_set['price_range_rent'] = $parameters['rpr'];
        $adv_search_set['sub_type'] = $parameters['st'];
        $adv_search_set['bathrooms'] = $parameters['b1'];
        $adv_search_set['bedrooms'] = $parameters['b2'];

        if (Request::get('agent') > 0) {
            $agent = Request::get('agent');
        } else {
            $agent = '';
        }

        if ($set_all_type == 'property') {
            $title_1 = 'Property ';
        } else {
            $title_1 = ucfirst($set_all_type).' property ';
        }

        switch ($set_action_type) {
            case 'wesell':
                $title_2 = 'for sale ';

                break;
            case 'welet':
                if ($set_all_type == 'residential') {
                    $title_2 = 'to rent ';
                } else {
                    $title_2 = 'to let ';
                }

                break;
            case 'weauction':
                $title_2 = 'for sale by auction ';

                break;
            default:
                $title_2 = '';

                break;
        }

        if ($suburb == '') {
            if ($city == '') {
                if ($province == '') {
                    $title_3 = 'in South Africa';
                } else {
                    $title_3 = 'in '.$province;
                }
            } else {
                $title_3 = 'in '.$city;
            }
        } else {
            if (substr($suburb, 0, 1) == '0') {
                $title_3 = 'in Multiple suburbs';
            } else {
                $title_3 = 'in '.ucfirst(str_replace(' ', '-', $suburb));
            }
        }

        $propertiesCity = null;
        $propertiesSuburb = null;
        $propertiesProvince = null;

        if (($city != '' && $suburb == '') || ($city != '' && $suburb != '')) {
            $propertiesSuburb = $this->property
                ->selectRaw('trim(suburb_town_city) as suburb_town_city_trim, count(suburb_town_city) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->where('area', 'LIKE', '%'.$city)
                ->whereRaw('trim(property.suburb_town_city) != trim(property.area)')
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('count', 'desc')
                ->groupBy('suburb_town_city_trim')
                ->limit(30)->get();

            $propertiesCity = $this->property
                ->selectRaw('trim(area) as area_trim,  count(area) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->where('province', 'LIKE', '%'.$province)
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('count', 'desc')
                ->groupBy('area_trim')
                ->limit(5)->get();
        } elseif ($suburb == '' && $province != '') {
            $propertiesCity = $this->property
                ->selectRaw('trim(area) as area_trim,  count(area) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->where('province', 'LIKE', '%'.$province)
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('count', 'desc')
                ->groupBy('area_trim')
                ->limit(30)->get();

            //dd(AppHelper::getLastQuery());
            $propertiesProvince = $this->property
                ->selectRaw('trim(province) as province_trim, count(province) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('count', 'desc')
                ->groupBy('property.id')
                ->groupBy('province_trim')
                ->limit(5)
                ->get();
        } else {
            $propertiesProvince = $this->property
                ->selectRaw('trim(province) as province_trim, count(province) as count')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
                ->orderBy('count', 'desc')
                ->groupBy('property.id')
                ->groupBy('province_trim')
                ->get();
        }

        $slug = new Slugify();

        $area_search = 'visible';

        if (Session::get('area_search') != 'visible') {
            $area_search = 'not-visible';
        }

        $kvp[] = 'o='.$parameters['o'];
        $kvp[] = 'v='.$parameters['v'];
        if ($parameters['pr'] != '0-15000000+') {
            $kvp[] = 'pr='.$parameters['pr'];
        }
        if ($parameters['rpr'] != '0-30000+') {
            $kvp[] = 'rpr='.$parameters['rpr'];
        }
        if ($parameters['lr'] != '0-20000+') {
            $kvp[] = 'lr='.$parameters['lr'];
        }
        if ($parameters['fr'] != '0-2000+') {
            $kvp[] = 'fr='.$parameters['fr'];
        }
        if ($property_type != 'property' && $parameters['st'] != 'None') {
            $kvp[] = 'st='.$parameters['st'];
        }
        if ($property_type == 'residential') {
            if ($parameters['b1'] != 'None') {
                $kvp[] = 'b1='.$parameters['b1'];
            }
            if ($parameters['b2'] != 'None') {
                $kvp[] = 'b2='.$parameters['b2'];
            }
        }

        $kvp = json_encode($kvp);

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'listing' => 'Search', 'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'],
            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set,
            'province' => $province, 'city' => $city, 'suburb' => $suburb, 'province_slug' => $province_slug,
            'city_slug' => $city_slug, 'suburb_slug' => $suburb_slug, 'order' => $parameters['o'], 'view' => $parameters['v'],
            'agent' => $agent, 'listing_type' => $listing_type, 'suburb2' => $suburb2, 'title_1' => $title_1, 'title_2' => $title_2,
            'title_3' => $title_3, 'propertiesProvince' => $propertiesProvince, 'kvp' => $kvp, 'propertiesSuburb' => $propertiesSuburb,
            'propertiesCity' => $propertiesCity, 'slug' => $slug, 'area_search' => $area_search, 'sub_type' => $parameters['st'],
            'bedrooms' => $parameters['b1'], 'bathrooms' => $parameters['b2'], ]);
    }

    public function setAreaSearchSession()
    {
        $area_search = Request::get('area_search');

        if ($area_search != 'visible') {
            Session::put('area_search', 'visible');
            $area_search = 'visible';
        } else {
            Session::put('area_search', 'not-visible');
            $area_search = 'not-visible';
        }

        return $area_search;
    }

    public function search()
    {
        $input = Request::all();

        if ($input['floor_space_range'] == '0 - 2000+') {
            $floor_space_range = '?';
        } else {
            $floor_space_range = '?fr='.str_replace(' ', '', $input['floor_space_range']);
        }
        if ($input['land_size_range'] == '0 - 20000+') {
            $land_size_range = '';
        } else {
            $land_size_range = '&lr='.str_replace(' ', '', $input['land_size_range']);
        }
        if ($input['price_range'] == 'R0 - R15000000+') {
            $price_range = '';
        } else {
            $price_range = '&pr='.str_replace('R', '', str_replace(' ', '', $input['price_range']));
        }

        if ($input['price_range_rent'] == 'R0 - R30000+' || $input['price_range_rent'] == 'R0 - R30000') {
            $price_range_rent = '';
        } else {
            $price_range_rent = '&rpr='.str_replace('R', '', str_replace(' ', '', $input['price_range_rent']));
        }

        $province = $input['province'];
        $city = $input['city'];
        $suburb = $input['suburb'];
        $listing_type = $input['listing_type'];
        $property_type = $input['property_type'];

        switch ($listing_type) {
            case 'wesell':
                $listing_type = 'for-sale';

                break;
            case 'welet':
                if ($property_type == 'residential') {
                    $listing_type = 'to-rent';
                } else {
                    $listing_type = 'to-let';
                }

                break;
            case 'weauction':
                $listing_type = 'on-auction';

                break;
            default:
                $listing_type = 'all-listings';

                break;
        }

        switch ($property_type) {
            case 'commercial':
                if ($input['commercial_type'] == 'None') {
                    $sub_type = '';
                } else {
                    $sub_type = '&st='.$input['commercial_type'];
                }
                $bathrooms = '';
                $bedrooms = '';

                break;
            case 'residential':
                if ($input['dwelling_type'] == 'None') {
                    $sub_type = '';
                } else {
                    $sub_type = '&st='.$input['dwelling_type'];
                }
                if ($input['bathrooms'] == 'None') {
                    $bathrooms = '';
                } else {
                    $bathrooms = '&b1='.$input['bathrooms'];
                }
                if ($input['bedrooms'] == 'None') {
                    $bedrooms = '';
                } else {
                    $bedrooms = '&b2='.$input['bedrooms'];
                }

                break;
            case 'farm':
                if ($input['farm_type'] == 'None') {
                    $sub_type = '';
                } else {
                    $sub_type = '&st='.$input['farm_type'];
                }
                $bathrooms = '';
                $bedrooms = '';

                break;
            default:
                $sub_type = '';
                $bathrooms = '';
                $bedrooms = '';

                break;
        }

        $slug = new Slugify();
        if ($province == '') {
            $province = 'south-africa';
        } else {
            $province = $slug->slugify($province);
        }
        if ($city == '') {
            $city = 'all-cities';
        } else {
            $city = $slug->slugify($city);
        }
        if ($suburb == '') {
            $suburb = 'all-suburbs';
        } else {
            $suburb = $slug->slugify($suburb);
        }
        if ($listing_type == '') {
            $listing_type = 'all-listings';
        }
        if ($property_type == '') {
            $property_type = 'property';
        }

        $order = '&o='.$input['order'];
        $view = '&v='.$input['view'];

        return redirect()->to('/'.$property_type.'/property/'.$listing_type.'/'.$province.'/'.$city.'/'.$suburb.
            $floor_space_range.$land_size_range.$price_range.$price_range_rent.$sub_type.$bedrooms
            .$bathrooms.$order.$view);
    }

    public function getCity($city)
    {
        $city = str_replace('_', '-', $city);
        $query = "SELECT area FROM areas WHERE area_slug = '$city' LIMIT 1";
//        dd($query);
        $cityDet = DB::Select($query);

        return $cityDet[0]->area;
    }

    public function getSuburb($suburb)
    {
        $suburb = str_replace('_', '-', $suburb);
        $query = "SELECT suburb_town_city FROM areas WHERE suburb_town_city_slug = '$suburb' LIMIT 1";
        //        dd($query);
        $suburbDet = DB::Select($query);

        return $suburbDet[0]->suburb_town_city;
    }

    public function getProvince($province)
    {
        $province = str_replace('_', '-', $province);
        $query = "SELECT province FROM areas WHERE province_slug = '$province' LIMIT 1";
//        dd($query);
        $provinceDet = DB::Select($query);
//        de($provinceDet[0]->province);
        return $provinceDet[0]->province;
    }

    public function vettingInfo()
    {
        return view('property.vettingInfo');
    }

    public function archive()
    {
        return view('property.admin.archive');
    }

    public function restore($id)
    {
        $this->property->whereId($id)->restore();

        $property = $this->property->find($id);

        $this->vetting->whereProperty_id($id)->restore();

        if ($property->residential) {
            $property->residential->restore();
        }
        if ($property->commercial) {
            $property->commercial->restore();
        }
        if ($property->farm) {
            $property->farm->restore();
        }

        return view('property.admin.archive');
    }

    public function getArchivedTable()
    {
        $properties = DB::table('property')
            ->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereNotNull('property.deleted_at')
            ->join('users', 'users.id', '=', 'property.user_id')
            ->select('reference', 'property.complex_number', 'property.complex_name', 'property.street_number', 'property.street_address', 'property.province', 'property.suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname', 'property_id', 'user_id', 'listing_type')
            ->get();

        return Datatable::collection(new Collection($properties))
            ->showColumns('reference', 'listing_type')
            ->addColumn('street_address', function ($Property) {
                return $Property->complex_number.' '.$Property->complex_name.' '.$Property->street_number.' '.$Property->street_address;
            })
            ->showColumns('province', 'suburb_town_city', 'property_type', 'firstname', 'lastname')
            ->addColumn('action', function ($Property) {
                return '<a href="/admin/property/history/'.$Property->property_id.'" title="View Property History" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
            <a href="/admin/property/restore/'.$Property->property_id.'" title="Restore Property" ><i class="i-circled i-light i-alt i-small icon-ok" style="background-color: #2ecc71"></i></a>';
            })
            ->searchColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname')
            ->orderColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname')
            ->make();
    }

    public function getArea()
    {
        $province = Request::get('option');

        $areas = DB::table('areas')->where('province', $province)->groupBy('area')->pluck('area');

        return response($areas);
    }

    public function getSuburbTown()
    {
        $area = Request::get('option');
        $suburb_town = DB::table('areas')->where('area', $area)->groupBy('suburb_town_city')->pluck('suburb_town_city');

        return response($suburb_town);
    }

    public function createcont($id)
    {
        $property = $this->getPropertyDetsbyId($id);
        $availDate = strtotime($property['available_from']);
        $available_from = date('m/d/Y', $availDate);
        $property->available_from = $available_from;
        $user = Auth::user();

        $files = $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')->orderBy('uploads.position')->get();

        $catcode = DB::table('service_provider_categories')->whereCategory('Conveyancing Attorney')->select('id')->first();

        $provinces = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('province')->get();
        $areas = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('area')->get();
        $cities = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('suburb_town_city')->get();
        $attorneys = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('company')->get();

        return view('property.createcont', ['property' => $property, 'files' => $files, 'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas, 'attorneys' => $attorneys]);
    }

    public function createEdit()
    {

//saves details without validating
        if (Request::get('save')) {
            $id = Request::get('id');
            $propertyInput = Request::only(['lead_image', 'short_descrip', 'long_descrip', 'image_gallery', 'price', 'youtube', 'virtual_tour', 'attourney']);
            $propertyInput['attourney'] = $propertyInput['attourney'][0];

            $this->property = $this->property->find($id);

            if ($propertyInput['attourney'] == 0) {
                $attourneyInput = Request::only(['company_name', 'contact_name', 'contact_number', 'email']);
                $this->attourney = $this->attourney->whereProperty_id($id)->first();
                if ($this->attourney == null) {
                    $attourneyId = DB::table('own_attourney')->insertGetId(['property_id' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                    $this->attourney = new Ownattourney();
                    $this->attourney = $this->attourney->whereId($attourneyId)->first();
                }

                $this->attourney->save();
            } else {
                $propertyInput['attourney'] = Request::get('company');
            }

            if ($propertyInput['image_gallery'] !== '' || $propertyInput['image_gallery'] !== null || $propertyInput['image_gallery'] !== 0) {
                $propertyInput['image_gallery'] = 1;
            }
            $availDate = strtotime(Request::get('available_from'));
            $available_from = date('Y-m-d', $availDate);
            $this->property->available_from = $available_from;
            $this->property->fill($propertyInput)->update();
            if ($this->property->property_type === 'Residential') {
                $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);

                $resInput['property_id'] = $this->property->id;
                // save to residential table
                $residential = new Residential();
                $residential = $residential->whereProperty_id($id)->first();
                $residential->fill($resInput)->update();
            }

            if ($this->property->property_type === 'Commercial') {
                $comInput = Request::only(['commercial_type', 'parking']);
                $comInput['property_id'] = $this->property->id;
                // save to commercial table
                $commercial = new Commercial();
                $commercial = $commercial->whereProperty_id($id)->first();
                $commercial->fill($comInput)->update();
            }

            if ($this->property->property_type === 'Farm') {
                $farmInput = Request::only(['farm_type']);
                $farmInput['property_id'] = $this->property->id;
                // save to farm table
                $farm = new Farm();
                $farm = $farm->whereProperty_id($id)->first();
                $farm->fill($farmInput)->update();
            }
            $vetting = new Vetting();
            $vetting = $vetting->whereProperty_id($id)->first();
            $id_photos = json_encode(Request::get('id_photos'));
            if ($id_photos === '[""]') {
                $vetting->id_photos = null;
            } else {
                $vetting->id_photos = $id_photos;
            }
            if ($vetting->ownership_type === 'Closed Corporation' || $vetting->ownership_type === 'Company (Pty) Limited' || $vetting->ownership_type === 'Trust') {
                $vetting->resolution = Request::get('resolution');
            }

            $vetting->vetting_status = 1;

            $vetting->update();

            if ($this->property->listing_type === 'welet') {
                return redirect()->route('portalLandlords_path');
            } elseif ($this->property->listing_type === 'wesell') {
                return redirect()->route('portalSellers_path');
            } else {
                return redirect()->route('portalSellers_path');
            }

            // submit must validate outstanding data
        } elseif (Request::get('submit')) {
            $id = Request::get('id');
            $propertyInput = Request::only(['lead_image', 'short_descrip', 'long_descrip', 'image_gallery', 'price', 'youtube', 'virtual_tour', 'attourney']);
            $propertyInput['attourney'] = $propertyInput['attourney'][0];

            $this->property = $this->property->find($id);

            if ($propertyInput['image_gallery'] !== '' || $propertyInput['image_gallery'] !== null || $propertyInput['image_gallery'] !== 0) {
                $propertyInput['image_gallery'] = 1;
            }

            $availDate = strtotime(Request::get('available_from'));
            $available_from = date('Y-m-d', $availDate);
            $this->property->available_from = $available_from;

            if (! $this->property->fill($propertyInput)->isValid2()) {
                return redirect()->back()->withInput()->withErrors($this->property->errors);
            }
            if ($propertyInput['attourney'] == 0) {
                $attourneyInput = Request::only(['company_name', 'contact_name', 'contact_number', 'email']);
                $this->attourney = $this->attourney->whereProperty_id($id)->first();
                if ($this->attourney == null) {
                    $attourneyId = DB::table('own_attourney')->insertGetId(['property_id' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                    $this->attourney = new Ownattourney();
                    $this->attourney = $this->attourney->whereId($attourneyId)->first();
                }

                if (! $this->attourney->fill($attourneyInput)->isValid()) {
                    return redirect()->back()->withInput()->withErrors($this->attourney->errors);
                }

                $this->attourney->save();
            } else {
                $this->property->attourney = Request::get('company');
            }

            $this->property->update();

            // if property type is residential then validate and save or redirect back to form with residential errors
            if ($this->property->property_type === 'Residential') {
                if (Request::get('dwelling_type') !== 'Vacant Land') {
                    $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);

                    $resInput['property_id'] = $this->property->id;
                    // validate and save to residential table if not valid return
                    $residential = new Residential();
                    $residential = $residential->whereProperty_id($id)->first();
                    if (! $residential->fill($resInput)->isValid()) {
                        return redirect()->back()->withInput()->withErrors($residential->errors);
                    }
                    $residential->update();
                } else {
                    $resInput = Request::only(['dwelling_type']);

                    $resInput['property_id'] = $this->property->id;
                    // validate and save to residential table if not valid return
                    $residential = new Residential();
                    $residential = $residential->whereProperty_id($id)->first();
                    if (! $residential->fill($resInput)->isValid2()) {
                        return redirect()->back()->withInput()->withErrors($residential->errors);
                    }
                    $residential->update();
                }
            }

            // if property type is commercial then validate and save or redirect back to form with commercial errors
            if ($this->property->property_type === 'Commercial') {
                $comInput = Request::only(['commercial_type', 'parking']);
                $comInput['property_id'] = $this->property->id;
                // validate and save to commercial table if not valid return
                $commercial = new Commercial();
                $commercial = $commercial->whereProperty_id($id)->first();
                if (! $commercial->fill($comInput)->isValid()) {
                    return redirect()->back()->withInput()->withErrors($commercial->errors);
                }
                $commercial->update();
            }
            // if property type is farm then validate and save or redirect back to form with farm errors
            if ($this->property->property_type === 'Farm') {
                $farmInput = Request::only(['farm_type']);
                $farmInput['property_id'] = $this->property->id;
                // validate and save to farm table if not valid return
                $farm = new Farm();
                $farm = $farm->whereProperty_id($id)->first();
                if (! $farm->fill($farmInput)->isValid()) {
                    return redirect()->back()->withInput()->withErrors($farm->errors);
                }
                $farm->update();
            }

            // validate and save to vetting table if not valid return
            $vetting = new Vetting();
            $vetting = $vetting->whereProperty_id($id)->first();
            $id_photos = json_encode(Request::get('id_photos'));
            if ($id_photos === '[""]') {
                $vetting->id_photos = null;
            } else {
                $vetting->id_photos = $id_photos;
            }
            if ($vetting->ownership_type === 'Closed Corporation' || $vetting->ownership_type === 'Company (Pty) Limited' || $vetting->ownership_type === 'Trust') {
                $vetting->resolution = Request::get('resolution');
            }
            if (! $vetting->isValid2()) {
                return redirect()->back()->withInput()->withErrors();
            }
            $vetting->vetting_status = 3;

            $vetting->update();

            if ($this->property->listing_type === 'welet') {
                return redirect()->route('portalLandlords_path');
            } elseif ($this->property->listing_type === 'wesell') {
                return redirect()->route('portalSellers_path');
            } else {
                return redirect()->route('portalSellers_path');
            }
        }
    }

    /**
     * Get all property dtails by id.
     */
    public function getPropertyDetsbyId($id)
    {
        $property = $this->property->whereId($id)->first();

        if (is_null($property)) {
            return redirect()->route('property.index');
        }
        switch ($property['property_type']):
            case 'Residential':
                $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();
        $property['id'] = $id;

        break;
        case 'Commercial':
                $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();
        $property['id'] = $id;

        break;
        case 'Farm':
                $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();
        $property['id'] = $id;

        break;
        default:
                return redirect()->route('property.index');

        break;
        endswitch;

        return $property;
    }

    public function manageProperties()
    {
        $userId = Auth::user()->id;
        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
        leftJoin('farm', 'property.id', '=', 'farm.property_id')->
        where('property.user_id', '=', $userId)->orderBy('vetting_status')->orderBy('listing_type')->get();

        $percent = [];

        foreach ($properties as $property) {
            $percent[] = $this->form_complete_percent($property['property_id']);
        }

        return view('property.manage_properties', ['properties' => $properties, 'listing' => 'My', 'percent' => $percent]);
    }

    public function portal($type)
    {
        $userId = Auth::user()->id;
        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('property.user_id', '=', $userId)->orderBy('vetting_status')->whereListing_type($type)->orderBy('listing_type')->get();

        $percent = [];

        foreach ($properties as $property) {
            $percent[] = $this->form_complete_percent($property['property_id']);
        }
        if ($type === 'welet') {
            return redirect()->route('portalLandlords_path');
        } else {
            return redirect()->route('portalSellers_path');
        }
    }

    public function getCoordinates($address)
    {
        $address = urlencode($address);
        $url = 'https://maps.google.com/maps/api/geocode/json?sensor=false&address='.$address;
        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];

        return ['xcoord' => $lat, 'ycoord' => $lng];
    }

    public function form_complete_percent($id)
    {
        $property = $this->getPropertyDetsbyId($id);

        switch ($property['property_type']) {
            case 'Residential':
                $total = 11;
                $startCount = 11;
                if ($property['dwelling_type'] === 'Vacant Land') {
                    $startCount += 3;
                }
                if ($property['price'] === '' || $property['price'] === null || $property['price'] == 0) {
                    $startCount--;
                }
                if ($property['dwelling_type'] === '' || $property['dwelling_type'] === null) {
                    $startCount--;
                }
                if ($property['bedrooms'] === '' || $property['bedrooms'] === null) {
                    $startCount--;
                }
                if ($property['bathrooms'] === '' || $property['bathrooms'] === null) {
                    $startCount--;
                }
                if ($property['parking'] === '' || $property['parking'] === null) {
                    $startCount--;
                }

                break;
            case 'Commercial':
                $total = 8;
                $startCount = 8;
                if ($property['price'] === '' || $property['price'] === null || $property['price'] == 0) {
                    $startCount--;
                }
                if ($property['commercial_type'] === '' || $property['commercial_type'] === null) {
                    $startCount--;
                }

                break;
            case 'Farm':
                $total = 8;
                $startCount = 8;
                if ($property['price'] === '' || $property['price'] === null || $property['price'] == 0) {
                    $startCount--;
                }
                if ($property['farm_type'] === '' || $property['farm_type'] === null) {
                    $startCount--;
                }

                break;
            default:
                // code...
                break;
        }

        if ($property['lead_image'] === '' || $property['lead_image'] === null || $property['lead_image'] === 'images/lead_placeholder.png') {
            $startCount--;
        }
        if ($property['image_gallery'] === '' || $property['image_gallery'] === null || $property['image_gallery'] == 0) {
            $startCount--;
        }
        if ($property['ownership_type'] === 'Private Owner' || $property['ownership_type'] === 'Other') {
            $total--;
            $startCount--;
        } else {
            if ($property['resolution'] === '' || $property['resolution'] === null) {
                $startCount--;
            }
        }
        if ($property['id_photos'] === '' || $property['id_photos'] === null) {
            $startCount--;
        }
        if ($property['short_descrip'] === '' || $property['short_descrip'] === null) {
            $startCount--;
        }
        if ($property['long_descrip'] === '' || $property['long_descrip'] === null) {
            $startCount--;
        }
        // dd($total . ' ' . $startCount);
        return number_format($startCount / $total * 100, 0);
    }

    public function admin($type, $vetting)
    {
        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting_status', '=', $vetting)->whereListing_type($type)->get();

        return view('vetting.index', ['properties' => $properties, 'title' => $type.' to vet']);
    }

    public function addOwnerIdtoProperty_user()
    {
        $prop_and_owner = DB::select("SELECT p.id, p.owner_id FROM property p WHERE p.owner_id IS NOT NULL
                                      AND p.owner_id < 2000
                                      AND p.owner_id <> ''
                                      AND concat('',p.owner_id * 1) = p.owner_id");

        foreach ($prop_and_owner as $prop_object) {
            try {
                $this->Prop_userAdd($prop_object->id, $prop_object->owner_id, 4);
            } catch (Exception $e) {
                de($e->getMessage());
                de($prop_object->id.' '.$prop_object->owner_id);
            }
        }

        de('done');
    }

    public function adminProperty($status)
    {
        return view('property.admin.properties', ['status' => $status]);
    }

    public function getAdminPropertyTable($status)
    {
        switch ($status):
            case 'all':
                $status = '%';

        break;
        case 1:
                $status = '%1%';

        break;
        case 2:
                $status = '%2%';

        break;
        case 3:
                $status = '%3%';

        break;
        case 4:
                $status = '%4%';

        break;
        case 5:
                $status = '%5%';

        break;
        case 6:
                $status = '%6%';

        break;
        case 7:
                $status = '%7%';

        break;
        case 8:
                $status = '%8%';

        break;
        endswitch;

        if (Auth::user()->hasRole('Admin')) {
            $properties = DB::table('property')
                ->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->where('vetting_status', 'LIKE', $status)
                ->where('property.deleted_at', '=', null)
                ->join('users', 'users.id', '=', 'property.user_id')
                ->select('reference', 'property.complex_number', 'property.complex_name', 'property.street_number',
                    'property.street_address', 'property.province', 'property.suburb_town_city', 'property_type',
                    'vetting_status', 'firstname', 'lastname', 'property_id', 'user_id', 'listing_type', 'featured')
                ->get();

            foreach ($properties as $property) {
                $property->address = $property->complex_number.' '.$property->complex_name.' '.$property->street_number.' '.$property->street_address;
                $property->name = '<a href="/admin/users/edit/'.$property->user_id.'" target="_blank" >'.$property->firstname.' '.$property->lastname.'</a>';
                switch ($property->vetting_status) {
                    case 1:
                        $property->vetting_status = 'Pending';

                        break;
                    case 2:
                        $property->vetting_status = 'Approved';

                        break;
                    case 3:
                        $property->vetting_status = 'Vetting';

                        break;
                    case 4:
                        $property->vetting_status = 'Sold < 60 Days';

                        break;
                    case 5:
                        $property->vetting_status = 'Rented < 14 Days';

                        break;
                    case 6:
                        $property->vetting_status = 'Sold > 60 Days';

                        break;
                    case 7:
                        $property->vetting_status = 'Rented > 14 Days';

                        break;
                    case 8:
                        $property->vetting_status = 'Rejected';

                        break;
                    case 9:
                        $property->vetting_status = 'Ready For Auction';

                        break;
                    case 10:
                        $property->vetting_status = 'Sold on Auction';

                        break;
                    case 11:
                        $property->vetting_status = 'Not Sold';

                        break;
                    case 12:
                        $property->vetting_status = 'Sold Private Treaty Pre Auction';

                        break;
                    case 13:
                        $property->vetting_status = 'Sold Private Treaty Post Auction';

                        break;
                    case 14:
                        $property->vetting_status = 'Withdrawn';

                        break;
                    case 15:
                        $property->vetting_status = 'Postponed';

                        break;
                    case 16:
                        $property->vetting_status = 'S.T.C.';

                        break;
                }

                if ($property->vetting_status == 1) {
                    $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/admin/messages/'.$property->user_id.'/'.$property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                            <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                } elseif ($property->vetting_status == 2) {
                    if ($property->listing_type == 'welet') {
                        $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/property/'.$property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                <a href="/admin/messages/'.$property->user_id.'/'.$property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>

                                <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    } elseif ($property->listing_type == 'wesell') {
                        if ($property->featured != 1) {
                            $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                    <a href="/brochure/'.$property->property_id.'" title="Print Brochure" target="_blank" ><i class="i-circled i-light i-alt i-small icon-book"></i></a>
                                    <a href="/wesell/calendar/property/'.$property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        } else {
                            $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_not_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                    <a href="/brochure/'.$property->property_id.'" title="Print Brochure" target="_blank" ><i class="i-circled i-light i-alt i-small icon-book"></i></a>
                                    <a href="/wesell/calendar/property/'.$property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        }
                    } else {
                        if ($property->featured != 1) {
                            $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                    <a href="/wesell/calendar/property/'.$property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        } else {
                            $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_not_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                    <a href="/wesell/calendar/property/'.$property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        }
                    }
                } elseif ($property->vetting_status == 3) {
                    $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/property/'.$property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                            <a href="/admin/messages/'.$property->user_id.'/'.$property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                            <a href="/admin/vetting/'.$property->property_id.'" title="Vet Property" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>
                            <a href="/wesell/calendar/property/'.$property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                            <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                } elseif ($property->vetting_status == 9) {
                    if ($property->featured != 1) {
                        $property->action = '<a href="#" title="Feature Property" onclick="mark_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    } else {
                        $property->action = '<a href="#" title="Feature Property" onclick="mark_not_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    }
                } else {
                    if ($property->featured != 1) {
                        $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/admin/messages/'.$property->user_id.'/'.$property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                                <a href="#" title="Feature Property" onclick="mark_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    } else {
                        $property->action = '<a href="/admin/property/'.$property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/admin/messages/'.$property->user_id.'/'.$property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                                <a href="#" title="Feature Property" onclick="mark_not_featured('.$property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                <a href="/admin/clone/property/\' . $property->property_id . \'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>
                                <a href="/admin/clone/property/'.$property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    }
                }
            }

            return Datatables::of($properties)
                ->rawColumns(['address', 'name', 'vetting_status', 'action'])
                ->make(true);

//            return Datatable::collection(new Collection($properties))

//                ->addColumn('vetting_status', function ($Property) {

//                })
//                ->addColumn('action', function ($Property) {
//
//                })
//                ->searchColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'name')
//                ->orderColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'name')
//                ->make();
        } elseif (Auth::user()->hasRole('LettingAgent')) {
            $roleId = DB::table('roles')->whereName('LettingAgent')->first();
            $role_id = $roleId->id;
            $floorSpaceAttribute = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByName('floor_space');

            $floor_space_id = $floorSpaceAttribute[0]->id;
            $tag = DB::table('tags')->where('name', '=', 'OWNER')->first();
            $properties = DB::table('property')->
            join('vetting AS v', 'v.property_id', '=', 'property.id')->
            join('users AS bdm', 'v.rep_id_number', '=', 'bdm.id')->
            leftJoin('property_user AS pu', function ($join) use ($tag) {
                $join->on('pu.property_id', '=', 'property.id')
                    ->where('role_id', '=', $tag->id);
            })->
            leftJoin('users AS o', 'pu.user_id', '=', 'o.id')->
            leftJoin('commercial AS c', 'c.property_id', '=', 'property.id')->
            leftJoin('residential AS r', 'r.property_id', '=', 'property.id')->
            leftJoin('farm AS f', 'f.property_id', '=', 'property.id')->
            leftJoin('role_user', 'bdm.id', '=', 'role_user.user_id')->
            leftJoin('attributes_property', function ($join) use ($floor_space_id) {
                $join->on('property.id', '=', 'attributes_property.property_id')
                    ->where('attributes_property.attributes_id', '=', $floor_space_id);
            })->
            select('bdm.firstname AS bdm_firstname', 'property.suburb_town_city', 'property.street_number', 'property.street_address',
                'property.reference', 'property.created_at', 'property.listing_type', 'attributes_property.value AS floor_space', 'property.price',
                'c.commercial_type', 'company', 'o.firstname AS owner_firstname', 'o.lastname AS owner_surname', 'o.cellnumber',
                'o.email', 'v.property_id', 'v.vetting_status', 'property.featured', 'property.user_id',
                DB::raw('(CASE property.rental_terms
                 WHEN "1" THEN "per month"
                 WHEN "2" THEN "per m2"
                 END) AS rental_terms'),
                DB::raw('(CASE property.property_type
                 WHEN "residential" THEN r.dwelling_type
                 WHEN "commercial" THEN c.commercial_type
                 WHEN "farm" THEN f.farm_type
                 END) AS sub_type'))->
            where('role_user.role_id', '=', $role_id)->
            orderBy('bdm.firstname')->get();

            return Datatable::collection(new Collection($properties))
                ->showColumns('reference', 'created_at', 'bdm_firstname', 'suburb_town_city')
                ->addColumn('street_address', function ($Property) {
                    return $Property->street_number.' '.$Property->street_address;
                })
                ->showColumns('sub_type', 'listing_type', 'floor_space')
                ->addColumn('price', function ($Property) {
                    return $Property->price.' '.$Property->rental_terms;
                })
                ->addColumn('owner', function ($Property) {
                    return $Property->owner_firstname.' '.$Property->owner_surname;
                })
                ->addColumn('contact_number', function ($Property) {
                    if ($Property->email != '') {
                        $email = 'Email: <a href="mailto:'.$Property->email.'">'.$Property->email.'</a>';
                    } else {
                        $email = 'No email';
                    }
                    if ($Property->cellnumber != '') {
                        $cell = '<br>Tel:'.$Property->cellnumber;
                    } else {
                        $cell = '<br>No Number';
                    }

                    return $email.' '.$cell;
                })
                ->addColumn('action', function ($Property) {
                    if (Auth::user()->hasRole('LettingAgent')) {
                        return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    } elseif ($Property->vetting_status == 1) {
                        return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                            <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    } elseif ($Property->vetting_status == 2) {
                        if ($Property->listing_type == 'welet') {
                            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>

                                <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        } elseif ($Property->listing_type == 'wesell') {
                            if ($Property->featured != 1) {
                                return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                    <a href="/brochure/'.$Property->property_id.'" title="Print Brochure" target="_blank" ><i class="i-circled i-light i-alt i-small icon-book"></i></a>
                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                            } else {
                                return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                    <a href="/brochure/'.$Property->property_id.'" title="Print Brochure" target="_blank" ><i class="i-circled i-light i-alt i-small icon-book"></i></a>
                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                            }
                        } else {
                            if ($Property->featured != 1) {
                                return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                            } else {
                                return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                                    <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                            }
                        }
                    } elseif ($Property->vetting_status == 3) {
                        return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                            <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                            <a href="/admin/vetting/'.$Property->property_id.'" title="Vet Property" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>
                            <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>
                            <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                    } elseif ($Property->vetting_status == 9) {
                        if ($Property->featured != 1) {
                            return '<a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        } else {
                            return '<a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        }
                    } else {
                        if ($Property->featured != 1) {
                            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                                <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        } else {
                            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                                <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>
                                <a href="/admin/clone/property/'.$Property->property_id.'" title="Clone Property" ><i class="i-circled i-light i-alt i-small icon-copy"></i></a>';
                        }
                    }
                })
                ->searchColumns('reference', 'created_at', 'bdm_firstname', 'suburb_town_city', 'street_address', 'sub_type', 'listing_type', 'floor_space', 'price', 'owner', 'contact_number')
                ->orderColumns('reference', 'created_at', 'bdm_firstname', 'suburb_town_city', 'street_address', 'sub_type', 'listing_type', 'floor_space', 'price', 'owner', 'contact_number')
                ->make();
        }
    }

    public function printPropertyNotes($property_id)
    {
        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.property_id = ".$property_id.'
                  ORDER BY p.created_at DESC';
        $property = $this->property->whereId($property_id)->first();
        $previous_notes = DB::select($query);
        $html = '<html><body>
                    <div style="padding: 50px; font-size: 12px;">
                    <h4 style="text-decoration: underline;">Property notes for '.($property->listing_title == null ? 'No Title' : $property->listing_title).'</h4>';
        foreach ($previous_notes as $note) {
            $html .= '<div><p><q>'.$note->note.' : </q>
                                                <i>'.$note->name.' '.$note->date.'</i></p></div>';
        }
        $html .= '</div></body></html>';

        return PDF::load($html, 'A4', 'portrait')->show();
    }

    public function adminPropertyEdit($id)
    {
//        dd($id);
        $property = $this->property->whereId($id)->first();

        $physDets = null;

        if ($property->listing_type == 'weauction') {
            if (! $auction = $this->auction->whereProperty_id($id)->first()) {
                $auctionDets = null;
            } else {
                if ($auction->physical_auction == null) {
                    $auctionDets = null;
                } else {
                    $auctionDets = [];
                    $auctions = $this->auction->select('property_id')->wherePhysical_auction($auction->physical_auction)->get();
                    $physDets = $this->physical_auction->select(['start_date', 'auction_name'])->whereId($auction->physical_auction)->first();
                    $x = 0;
                    foreach ($auctions as $phys_auction) {
                        $auction_property = $this->property
                            ->select(['street_number', 'street_address', 'id'])
                            ->whereId($phys_auction->property_id)
                            ->first();

                        if ($auction_property['id'] != $id && $auction_property['id'] != null) {
                            $auctionDets[$x]['property_id'] = $auction_property['id'];
                            $auctionDets[$x]['address'] = $auction_property['street_number'].' '.$auction_property['street_address'];
                            $x++;
                        }
                    }
                }
            }
        } else {
            $auctionDets = null;
        }

        if (is_null($property)) {
            return redirect()->route('property.index');
        }

        switch ($property['property_type']):
            case 'Residential':
                $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        case 'Commercial':
                $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        case 'Farm':
                $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        default:
                return redirect()->back();

        break;
        endswitch;

        $tenant_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'TENANT' AND pu.property_id = ".$id);
        if (! empty($tenant_id)) {
            $tenant = User::find($tenant_id[0]->user_id);
        } else {
            $tenant = new User();
        }

        $keyholder_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'KEY HOLDER' AND pu.property_id = ".$id);
        if (! empty($keyholder_id)) {
            $keyholder = User::find($keyholder_id[0]->user_id);
        } else {
            $keyholder = new User();
        }

        $managing_agent_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'MANAGING AGENT' AND pu.property_id = ".$id);
        if (! empty($managing_agent_id)) {
            $managing_agent = User::find($managing_agent_id[0]->user_id);
        } else {
            $managing_agent = new User();
        }

        $owner_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'OWNER' AND pu.property_id = ".$id);

        if (! empty($owner_id)) {
            $owner = User::find($owner_id[0]->user_id);
        } else {
            $owner = new User();
        }

        $user = $this->user->whereId($property->user_id)->first();

        $files = [];

        if ($user != null) {
            $files = $user->files()
                ->where('link', '=', $id)
                ->where('type', '=', 'image')
                ->with('user')
                ->orderBy('uploads.position')
                ->get();

            foreach ($files as $file) {
                $file->file = str_replace('.jpg', '_143_83.jpg', $file->file);
            }
        }

        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();

        $agentsCom = $this->user
            ->select(DB::raw("CONCAT(firstname, ' ',lastname) AS fullname, id"))
            ->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->pluck('fullname', 'id');

        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $brokers = DB::table('broker_logos')->select('id', 'broker_name')->whereDeleted_at(null)->get();

        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name
                  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.property_id = $id
                  ORDER BY p.created_at DESC";

        $previous_agent_notes = DB::select($query);

//        dd($previous_agent_notes);
        $features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);

        $checklist = $this->checklist->whereProperty_id($id)->first();

        if ($checklist == null) {
            $checklist = new Checklist();
        }

        if ($checklist->title_deed_date == '0000-00-00') {
            $checklist->title_deed_date = '';
        }

        if ($checklist->sg_diagram_date == '0000-00-00') {
            $checklist->sg_diagram_date = '';
        }

        if ($checklist->update_letter_sent == '0000-00-00') {
            $checklist->update_letter_sent = '';
        }

        if ($checklist->inspection_date == '0000-00-00') {
            $checklist->inspection_date = '';
        }

        $asset_real_estate = $this->asset_real_estate->whereProperty_id($id)->first();

        if ($asset_real_estate == null) {
            $asset_real_estate = new AssetRealEstate();
        }

        $property_file = $this->property_file->whereProperty_id($id)->first();

        if ($property_file == null) {
            $property_file = new PropertyFile();
        }

        if ($property_file->auction_date == '1970-01-01') {
            $property_file->auction_date = '';
        }

        if ($property_file->viewing_date == '1970-01-01') {
            $property_file->viewing_date = '';
        }

        $zone_types = DB::table('zone_types')->whereActive(1)->pluck('type', 'id');

        $allow_advert_print = true;
        $property_advert_boards = $this->property_advert_boards->whereProperty_id($id)->first();
        if ($property_advert_boards == null) {
            $allow_advert_print = false;
            $property_advert_boards = new PropertyAdvertBoards();
        }

        $property_advert_prints = $this->property_advert_prints->whereProperty_id($id)->first();

        if ($property_advert_prints == null) {
            $property_advert_prints = new PropertyAdvertPrints();
        }

        $property_advert_social_media = $this->property_advert_social_media->whereProperty_id($id)->first();
        if ($property_advert_social_media == null) {
            $property_advert_social_media = new PropertyAdvertSocialMedia();
        }

        $newid = $id;
        if ($property->listing_type == 'weauction') {
            $newid = $id.'A';
        }

        $rules_of_auction = DB::table('auction_rule_change')->whereProperty_id($newid)->get();

        if (empty($rules_of_auction)) {
            $rules_of_auction = [];
        } else {
            $rules_of_auction = json_decode(json_encode($rules_of_auction), true);
        }

        $not_applicable = false;
        $attached = false;

        if (! $auctioneer_notes = $this->auctioneer_notes->whereProperty_id($id)->first()) {
            $auctioneer_notes = new AuctioneerNotes();
        }

        if ($auctioneer_notes->lease_details == 'Not Applicable') {
            $not_applicable = true;
        } else {
            $attached = true;
        }

        if (! $highlights = $this->highlight->whereProperty_id($id)->first()) {
            $highlights = new Highlight();
        }

        if (! $advert_photos = $this->advert_photos->whereProperty_id($id)->first()) {
            $advert_photos = new PropertyAdvertPhotos();
        }

        $usersDB = $this->user->select('firstname', 'lastname', 'email', 'cellnumber')->get();
        foreach ($usersDB as $user) {
            $emails[] = $user->email;
            $names[] = $user->firstname.' - '.$user->lastname;
            $contacts[] = $user->cellnumber;
        }
        $emails = implode('", "', $emails);
        $names = implode('", "', $names);
        $contacts = implode('", "', $contacts);

        $tel_nums = $contacts;

        $hasPurchaser = true;
        $tag = DB::table('tags')->where('name', '=', 'PURCHASER')->first();
        $hasPurchaser_query = DB::select('SELECT * FROM property_user WHERE property_id = '.$id.' AND role_id = '.$tag->id);
        if (empty($hasPurchaser_query)) {
            $hasPurchaser = false;
        }

        $hasTenant = true;
        $tag2 = DB::table('tags')->where('name', '=', 'TENANT')->first();
        $hasTenant_query = DB::select('SELECT * FROM property_user WHERE property_id = '.$id.' AND role_id = '.$tag2->id);
        if (empty($hasTenant_query)) {
            $hasTenant = false;
        }

        $autotags = DB::select('SELECT name FROM tags');
        $autotag_array = [];
        foreach ($autotags as $autotag) {
            $autotag_array[] = strtoupper(trim($autotag->name));
        }

        if (! $zone = DB::table('zone_types')->whereId($property->zoning)->first()) {
            $zone = new ZoneType();
            $zone->type = 'NA';
        }

        $autoslas = DB::select('SELECT broker_name FROM broker_logos');
        $autosla_array = [];
        foreach ($autoslas as $autosla) {
            $autosla_array[] = trim($autosla->broker_name);
        }

        $gross_type = DB::select("SELECT * FROM commission_types WHERE type = 'Gross'");
        $sla_type = DB::select("SELECT * FROM commission_types WHERE type = 'SLA'");
        $seller_type = DB::select("SELECT * FROM commission_types WHERE type = 'Seller'");
        $absorb_cost_type = DB::select("SELECT * FROM commission_types WHERE type = 'Absorbed Costs'");

        if (! $gross_comm = $this->commission->whereLead_id($id)->whereType($gross_type[0]->id)->first()) {
            $gross_comm = new Commission();
        }

        if (! $sla_rebate = $this->commission->whereLead_id($id)->whereType($sla_type[0]->id)->first()) {
            $sla_rebate = new Commission();
        }

        if (! $absorb_cost = $this->commission->whereLead_id($id)->whereType($absorb_cost_type[0]->id)->first()) {
            $absorb_cost = new Commission();
        }

        if (! $seller_rebate = $this->commission->whereLead_id($id)->whereType($seller_type[0]->id)->first()) {
            $seller_rebate = new Commission();
        }

        $social_adverts = DB::select('SELECT * FROM social_advertising WHERE property_id = '.$id);

        if (empty($social_adverts)) {
            $social_adverts[0] = new \stdClass();
            $social_adverts[0]->facebook = 500;
            $social_adverts[0]->display = 1500;
            $social_adverts[0]->search = 1500;
            $social_adverts[0]->start_date = null;
        }

        return view('property.admin.edit', ['property' => $property, 'files' => $files, 'agents' => $agents,
            'cities' => $cities, 'brokers' => $brokers, 'tenant' => $tenant, 'keyholder' => $keyholder,
            'managing_agent' => $managing_agent, 'owner' => $owner, 'previous_agent_notes' => $previous_agent_notes,
            'features' => $features, 'zone_types' => $zone_types, 'checklist' => $checklist,
            'asset_real_estate' => $asset_real_estate, 'property_file' => $property_file,
            'property_advert_boards' => $property_advert_boards, 'property_advert_prints' => $property_advert_prints,
            'property_advert_social_media' => $property_advert_social_media, 'rules_of_auction' => $rules_of_auction,
            'highlights' => $highlights, 'advert_photos' => $advert_photos, 'auctioneer_notes' => $auctioneer_notes,
            'not_applicable' => $not_applicable, 'attached' => $attached, 'allow_advert_print' => $allow_advert_print,
            'names' => $names, 'emails' => $emails, 'contacts' => $contacts, 'tel_nums' => $tel_nums,
            'hasPurchaser' => $hasPurchaser, 'hasTenant' => $hasTenant, 'autotag' => json_encode($autotag_array),
            'agentsCom' => $agentsCom, 'gross_comm' => $gross_comm, 'sla_rebate' => $sla_rebate,
            'seller_rebate' => $seller_rebate, 'absorb_cost' => $absorb_cost, 'autosla' => json_encode($autosla_array),
            'auctionDets' => $auctionDets, 'physDets' => $physDets, 'social_adverts' => $social_adverts, 'zone' => $zone, ]);
    }

    public function getAllContacts($property_id)
    {
        $query = 'SELECT u.id, u.id AS user_id,  u.firstname, u.lastname, u.email, u.cellnumber AS tel, u.updated_at,
                  pu.property_id, pu.role_id, NULL AS company_type, NULL AS company_other, "Other" AS contact_other
                  FROM users u
                  INNER JOIN property_user pu ON u.id = pu.user_id
                  WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                  AND pu.property_id = '.$property_id.'
                  AND CONCAT(u.firstname, " ",u.lastname) <> " "';

        $contacts = DB::select($query);
        foreach ($contacts as $contact) {
            $query = 'select name from tags where id = '.$contact->role_id;

            $tag = DB::select($query);

            if (empty($tag[0]->name)) {
                $contact->tagname = 'NA';
            } else {
                $contact->tagname = $tag[0]->name;
            }

            $contact->updated_at = date('Y-m-d', strtotime($contact->updated_at));

            $contact->action = '<a href="/admin/users/edit/'.$contact->user_id.'" title="User Edit" >
                        <i class="i-circled i-light i-alt i-small fa fa-user"></i></a>
                        <a title="Remove from lead" onclick="deleteContact('.$contact->user_id.','.$contact->role_id.')" >
                        <i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
        }

        return Datatables::of($contacts)
            ->rawColumns(['tagname', 'updated_at', 'action'])
            ->make(true);
    }

    public function saveContact()
    {
        $input = Request::all();
        if (is_numeric(Request::get('tel'))) {
            $tel = Request::get('tel');
        } else {
            $tel = '';
        }

        $value = null;
        $tag = DB::table('tags')->where('name', '=', Request::get('contact_type'))->first();
        $name = strtoupper(Request::get('contact_type'));
        if ($tag === null) {
            $value = DB::table('tags')->insertGetId(
                ['name' => $name, 'description' => $name, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );
        } else {
            $value = $tag->id;
        }

        $property = $this->property->whereId($input['property_id'])->first();

        if (Request::get('email') != '' && Request::get('firstname') != '' && Request::get('lastname') != '') {
            $user_id = App::make('App\Http\Controllers\RegistrationControlrer')->storeLeadUser(Request::get('email'), Request::get('firstname'),
                Request::get('lastname'), $tel, 11);
            if (! DB::select('SELECT * FROM property_user WHERE user_id = '.$user_id.' AND property_id = '.
                $input['property_id'].' AND role_id = '.$value)
            ) {
                DB::insert('INSERT INTO property_user SET user_id = '.$user_id.', property_id = '.
                    $input['property_id'].', role_id = '.$value);
            }
            if ($property->id != null) {
                if (! DB::table('property_user')->whereProperty_id($property->id)->whereUser_id($user_id)->first()) {
                    DB::insert('INSERT INTO property_user SET property_id = '.$property->id.', user_id = '.
                        $user_id.', role_id = '.$value);
                }
            }
        }

        App::make(\App\Http\Controllers\UsersController::class)->rddTag($user_id, $value);

        $contacts = DB::select('SELECT u.firstname, u.lastname, u.email, u.cellnumber AS tel, u.created_at, pu.role_id
                                FROM users u
                                INNER JOIN property_user pu ON u.id = pu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND pu.property_id = '.$input['property_id'].'
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "');

        $prop_notes = new PropNotes();
        $prop_notes->property_id = $input['property_id'];
        $prop_notes->users_id = Auth::user()->id;
        $message = $input['firstname'].' '.$input['lastname'].' was added to this property. Tagged: '.Request::get('contact_type');
        $prop_notes->note = $message;

        $prop_notes->save();

        return $contacts;
    }

    public function deletePropertyUser($property_id, $user_id, $tag_id)
    {
        DB::delete('DELETE FROM property_user WHERE property_id = '.$property_id.' AND user_id = '.$user_id.'
        AND role_id = '.$tag_id);
        $tag = DB::table('tags')->whereId($tag_id)->first();
        $user = DB::table('users')->select(['firstname', 'lastname'])->whereId($user_id)->first();
        $prop_notes = new PropNotes();
        $prop_notes->property_id = $property_id;
        $prop_notes->users_id = Auth::user()->id;
        $message = $user->firstname.' '.$user->lastname.' who had the '.$tag->description.
            ' tag was removed from this property';
        $prop_notes->note = $message;

        $prop_notes->save();

        $contacts = DB::select('SELECT u.firstname, u.lastname, u.email, u.cellnumber AS tel, u.created_at, pu.role_id
                                FROM users u
                                INNER JOIN property_user pu ON u.id = pu.user_id
                                WHERE CONCAT(u.firstname, " ",u.lastname) IS NOT NULL
                                AND pu.property_id = '.$property_id.'
                                AND CONCAT(u.firstname, " ",u.lastname) <> " "');

        return $contacts;
    }

    public function adminPrint_advert($id)
    {
        $property = $this->property->whereId($id)->first();

        $property_advert_boards = $this->property_advert_boards->whereProperty_id($id)->first();

        $property_advert_social_media = $this->property_advert_social_media->whereProperty_id($id)->first();

        $property_advert_prints = $this->property_advert_prints->whereProperty_id($id)->first();

        $advert_photos = $this->advert_photos->whereProperty_id($id)->first();

        $owner_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'OWNER' AND pu.property_id = ".$id);
        if (! empty($owner_id)) {
            $owner = User::find($owner_id[0]->user_id);
        } else {
            $owner = new User();
        }

        $seller = $this->seller->whereProperty_id($id)->first();

        if ($seller == null) {
            $seller = new Seller();
        }

        $property_file = $this->property_file->whereProperty_id($id)->first();

        if ($property_file == null) {
            $property_file = new PropertyFile();
        }

        if ($seller->owner_is_seller_check == 1) {
            if ($owner == '') {
                $content['client'] = $property->company;
            } else {
                $content['client'] = $owner->firstname.' '.$owner->lastname;
                if (trim($content['client'])) {
                    $content['client'] = $property->company;
                }
            }
            $content['contact'] = $owner->cellnumber;
            $content['email'] = $owner->email;
        } else {
            $content['client'] = $seller->seller_name;
            $content['contact'] = $seller->seller_tel;
            $content['email'] = $seller->seller_email;
        }

        $social_media_total = $property_advert_social_media->facebook_adverts + $property_advert_social_media->google_adwords +
            $property_advert_social_media->linkedin_post + $property_advert_social_media->facebook_adverts +
            $property_advert_social_media->google_adverts + $property_advert_social_media->blog_post +
            $property_advert_social_media->twitter_adverts + $property_advert_social_media->portal_syndication +
            $property_advert_social_media->tele_marketing;

        $print_total = $property_advert_prints->print_1_price + $property_advert_prints->print_2_price + $property_advert_prints->print_3_price
            + $property_advert_prints->print_4_price + $property_advert_prints->print_5_price + $property_advert_prints->print_6_price;

        $board_total = $property_advert_boards->Property_Board_1000_1250_Price + $property_advert_boards->Property_Board_2000_800_Price +
            $property_advert_boards->Lamppost_Board_Price + $property_advert_boards->Door_Board_Price +
            $property_advert_boards->Large_Board_Price + $property_advert_boards->Banner_Price;

        $photo_total = $advert_photos->ground_aerial_video + $advert_photos->ground_photo_tour + $advert_photos->ground_photo
            + $advert_photos->other_price;

        $total_ABC = $social_media_total + $print_total + $board_total;

        $total_with_photo = $total_ABC + $photo_total;

        return view('property.print.advertising_budget', ['property_advert_boards' => $property_advert_boards,
            'content' => $content, 'property' => $property, 'property_file' => $property_file,
            'property_advert_social_media' => $property_advert_social_media, 'social_media_total' => $social_media_total,
            'property_advert_prints' => $property_advert_prints, 'print_total' => $print_total, 'board_total' => $board_total,
            'advert_photos' => $advert_photos, 'photo_total' => $photo_total, 'total_ABC' => $total_ABC,
            'total_with_photo' => $total_with_photo, ]);
    }

    public function adminPropertyStore()
    {

        // check property section of form and validate
        $propertyInput = Request::only(['complex_number', 'complex_name', 'street_number', 'street_address', 'country',
            'listing_type', 'property_type', 'price', 'x_coord', 'y_coord', 'sale_type', 'vat_num', 'reg_num', 'company',
            'province', 'area', 'suburb_town_city', 'broker', 'office_size', 'factory_size', 'shop_size', 'yard_size',
            'vacant_land_size', 'office_price', 'factory_price', 'shop_price', 'yard_price', 'vacant_land_price',
            'covered_parking_bays', 'covered_parking_price', 'parking_bays', 'parking_price', 'zoning', 'amps',
            'agent_percent', 'broker_percent', ]);

        if (Request::get('show_street_address') != null) {
            $propertyInput['show_street_address'] = 1;
        } else {
            $propertyInput['show_street_address'] = 0;
        }

        $propertyInput['user_id'] = Auth::user()->id;

        //create unique reference number
        switch ($propertyInput['listing_type']):
            case 'wesell':
            case 'in2assets':
                $sale_ref = DB::select('SELECT `sale_ref` FROM `reference` WHERE `id` = ?', [1]);
        $sale_ref = $sale_ref[0]->sale_ref;
        $reference = 'SALE-'.str_pad($sale_ref, 6, '0', STR_PAD_LEFT);
        $sale_ref++;
        DB::update('UPDATE `reference` SET `sale_ref` = ?, `updated_at` = NOW()', [$sale_ref]);

        break;
        case 'weauction':
                $auction_ref = DB::select('SELECT `auction_ref` FROM `reference` WHERE `id` = ?', [1]);
        $auction_ref = $auction_ref[0]->auction_ref;
        $reference = 'AUCT-'.str_pad($auction_ref, 6, '0', STR_PAD_LEFT);
        $auction_ref++;
        DB::update('UPDATE `reference` SET `auction_ref` = ?, `updated_at` = NOW()', [$auction_ref]);

        break;
        case 'in2rental':
                $rent_ref = DB::select('SELECT `rent_ref` FROM `reference` WHERE `id` = ?', [1]);
        $rent_ref = $rent_ref[0]->rent_ref;
        $reference = 'RENT-'.str_pad($rent_ref, 6, '0', STR_PAD_LEFT);
        $rent_ref++;
        DB::update('UPDATE `reference` SET `rent_ref` = ?, `updated_at` = NOW()', [$rent_ref]);
        $propertyInput['listing_type'] = 'in2rental';
        $propertyInput['available_from'] = date('Y-m-d', strtotime(Request::get('available_from')));
        $propertyInput['rental_terms'] = Request::get('rental_terms');

        break;
        default:
                return redirect()->back()->withInput()->withErrors('Please select a Listing Type');

        break;
        endswitch;

        $propertyInput['reference'] = $reference;

        $slug = new Slugify();

        switch ($propertyInput['property_type']) {
            case 'Commercial':
                $property_type = Request::get('commercial_type');

                break;
            case 'Residential':
                $property_type = Request::get('residential_type');

                break;
            case 'Farm':
                $property_type = Request::get('farm_type');

                break;
        }
        switch ($propertyInput['listing_type']) {
            case 'wesell':
            case 'in2assets':
                $listing_type = 'for sale';

                break;
            case 'welet':
            case 'in2rental':
                if ($propertyInput['property_type'] == 'Residential') {
                    $listing_type = 'to rent';
                } else {
                    $listing_type = 'to let';
                }

                break;
            case 'weauction':
                $listing_type = 'auction';

                break;
        }

        if ($propertyInput['show_street_address'] == 1) {
            $street_address = ' '.$propertyInput['street_number'].' '.$propertyInput['street_address'];
        } else {
            $street_address = '';
        }

        if ($propertyInput['area'] == $propertyInput['suburb_town_city']) {
            $propertyInput['slug'] = $slug->slugify($propertyInput['property_type'].' '.$property_type.' '.$listing_type
                .' '.$propertyInput['province'].' '.$propertyInput['area'].$street_address);
        } else {
            $propertyInput['slug'] = $slug->slugify($propertyInput['property_type'].' '.$property_type.' '.$listing_type
                .' '.$propertyInput['province'].' '.$propertyInput['area'].' '.$propertyInput['suburb_town_city']
                .$street_address);
        }

        if (! $this->property->fill($propertyInput)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->property->errors);
        }

        $this->property['postcode'] = Request::get('postcode');

        $this->property->save();

        if (Request::get('ownership_type') == 'Private Sale') {
            if (Request::get('owner_email') != '') {
                if (! $owner_id = DB::table('users')->select('id')->where('email', '=', Request::get('owner_email'))->first()) {
                    $owner_id = DB::table('users')->insertGetId(['firstname' => Request::get('company'), 'id_number' => Request::get('owner_id'),
                        'email' => Request::get('owner_email'), 'title' => Request::get('owner_title'), 'reg_num' => Request::get('reg_num'),
                        'vat_num' => Request::get('vat_num'), 'cellnumber' => Request::get('owner_cellnumber'), ]);
                    $this->Prop_userAdd($this->property->id, $owner_id, 4);
                } else {
                    $this->Prop_userAdd($this->property->id, $owner_id->id, 4);
                }
            }
        } else {
            if (Request::get('owner_email') != '') {
                if (! $owner_id = DB::table('users')->select('id')->where('email', '=', Request::get('owner_email'))->first()) {
                    $owner_id = DB::table('users')->insertGetId(['firstname' => Request::get('owner_name'), 'companyname' => Request::get('company'),
                        'id_number' => Request::get('owner_id'), 'email' => Request::get('owner_email'), 'title' => Request::get('owner_title'),
                        'reg_num' => Request::get('reg_num'), 'vat_num' => Request::get('vat_num'), ]);
                    $this->Prop_userAdd($this->property->id, $owner_id, 4);
                } else {
                    $this->Prop_userAdd($this->property->id, $owner_id->id, 4);
                }
            }
        }
        if (Request::get('tenants_email') != '') {
            if (! $tenants_id = DB::table('users')->select('id')->where('email', '=', Request::get('tenants_email'))->first()) {
                $tenants_id = DB::table('users')->insertGetId(['firstname' => Request::get('tenants_firstname'),
                    'email' => Request::get('tenants_email'), 'cellnumber' => Request::get('tenants_cell_num'),
                    'companyname' => Request::get('tenants_company'), ]);
                $this->Prop_userAdd($this->property->id, $tenants_id, 7);
            } else {
                $this->Prop_userAdd($this->property->id, $tenants_id->id, 7);
            }
        }

        if (Request::get('keyholder_email') != '') {
            if (! $keyholder_id = DB::table('users')->select('id')->where('email', '=', Request::get('keyholder_email'))->first()) {
                $keyholder_id = DB::table('users')->insertGetId(['firstname' => Request::get('keyholder_firstname'),
                    'email' => Request::get('keyholder_email'), 'cellnumber' => Request::get('keyholder_cell_num'), ]);
                $this->Prop_userAdd($this->property->id, $keyholder_id, 14);
            } else {
                $this->Prop_userAdd($this->property->id, $keyholder_id->id, 14);
            }
        }

        if (Request::get('managing_agent_email') != '') {
            if (! $managing_agent_id = DB::table('users')->select('id')->where('email', '=', Request::get('managing_agent_email'))->first()) {
                $managing_agent_id = DB::table('users')->insertGetId(['firstname' => Request::get('managing_agent_firstname'),
                    'email' => Request::get('managing_agent_email'), 'cellnumber' => Request::get('managing_agent_cell_num'),
                    'companyname' => Request::get('managing_agent_company'), ]);
                $this->Prop_userAdd($this->property->id, $managing_agent_id, 13);
            } else {
                $this->Prop_userAdd($this->property->id, $managing_agent_id->id, 13);
            }
        }

        if (Request::get('land_measurement') == 'HA') {
            $land_size = Request::get('land_size') * 10000;
        } else {
            $land_size = Request::get('land_size');
        }

        $boolean_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(1);
        foreach ($boolean_features as $boolean_feature) {
            if (Request::get('feature'.$boolean_feature->id) && ! $this->property->hasAttribute($boolean_feature->name)) {
                $this->property->assignAttribute($boolean_feature->id, 1);
            } elseif (! Request::get('feature'.$boolean_feature->id) && $this->property->hasAttribute($boolean_feature->name)) {
                $this->property->removeAttribute($boolean_feature->id);
            }
        }

        $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
        foreach ($numeric_features as $numeric_feature) {
            if (Request::get($numeric_feature->name) > 0 && ! $this->property->hasAttribute($numeric_feature->name)) {
                if ($numeric_feature->name == 'land_size') {
                    $this->property->assignAttribute($numeric_feature->id, $land_size);
                }
                $this->property->assignAttribute($numeric_feature->id, Request::get($numeric_feature->name));
            } elseif (Request::get($numeric_feature->name) > 0 && $this->property->hasAttribute($numeric_feature->name)) {
                if ($numeric_feature->name == 'land_size') {
                    $this->property->ammendAttribute($numeric_feature->id, $this->property->id, $land_size);
                }
                $this->property->ammendAttribute($numeric_feature->id, $this->property->id, Request::get($numeric_feature->name));
            } elseif (! Request::get($numeric_feature->name) > 0 && $this->property->hasAttribute($numeric_feature->name)) {
                $this->property->removeAttribute($numeric_feature->name);
            }
        }

        $text_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(3);
        foreach ($text_features as $text_feature) {
            if (Request::get($text_feature->name) != '' && ! $this->property->hasAttribute($text_feature->name)) {
                $this->property->assignAttribute($text_feature->id, Request::get($text_feature->name));
            } elseif (Request::get($text_feature->name) != '' && $this->property->hasAttribute($text_feature->name)) {
                $this->property->ammendAttribute($text_feature->id, $this->property->id, Request::get($text_feature->name));
            } elseif (Request::get($text_feature->name) == '' && $this->property->hasAttribute($text_feature->name)) {
                $this->property->removeAttribute($text_feature->name);
            }
        }

        if ($propertyInput['property_type'] === 'Residential') {
            $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);
            $resInput['property_id'] = $this->property->id;
            $residential = new Residential();
            if ($resInput['dwelling_type'] == 'Vacant Land' || $resInput['dwelling_type'] == 'Development') {
                $resInput = Request::only(['dwelling_type']);
                $resInput['property_id'] = $this->property->id;
                if (! $residential->fill($resInput)->isValid2()) {
                    return redirect()->back()->withInput()->withErrors($residential->errors);
                }
            } else {
                if (! $residential->fill($resInput)->isValid()) {
                    return redirect()->back()->withInput()->withErrors($residential->errors);
                }
            }
            $residential->save();
        }

        if ($propertyInput['property_type'] === 'Commercial') {
            $comInput = Request::only(['commercial_type', 'parking']);
            $comInput['property_id'] = $this->property->id;
            $commercial = new Commercial();
            if (! $commercial->fill($comInput)->isValid()) {
                return redirect()->back()->withInput()->withErrors($commercial->errors);
            }
            $commercial->save();
        }

        if ($propertyInput['property_type'] === 'Farm') {
            $farmInput = Request::only(['farm_type']);
            $farmInput['property_id'] = $this->property->id;
            $farm = new Farm();
            if (! $farm->fill($farmInput)->isValid()) {
                return redirect()->back()->withInput()->withErrors($farm->errors);
            }
            $farm->save();
        }

        $vettingInput = Request::only(['ownership_type', 'rep_email', 'rep_cell_num', 'rep_firstname', 'rep_surname']);

        $vettingInput['property_id'] = $this->property->id;

        $vettingInput['vetting_status'] = 1;
        $vettingInput['owner_title'] = 'NA';
        $vettingInput['owner_firstname'] = $propertyInput['company'];
        $vettingInput['owner_surname'] = 'In2Assets';
        $vettingInput['marriage_stat'] = 'NA';
        $vettingInput['rep_id_number'] = Request::get('agents');

        // validate and save to vetting table if not valid return and delete $this->property
        $vetting = new Vetting();

        $vetting->vetting_status = 1;
        $vetting->fill($vettingInput)->save();

        //save Agent Notes
        if (Request::get('agent_notes') != '') {
            $this->prop_notes->note = Request::get('agent_notes');
            $this->prop_notes->property_id = $this->property->id;
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->save();
        }

        return redirect()->route('admin.property.edit', ['id' => $this->property->id]);
    }

    public function saveNote()
    {
        if (Request::get('notes') != '') {
            $this->prop_notes->note = Request::get('notes');
            $this->prop_notes->property_id = Request::get('property_id');
            $this->prop_notes->users_id = Auth::user()->id;
            $this->prop_notes->save();
        }

        $query = "SELECT p.note, p.created_at AS date, CONCAT(u.firstname, ' ',  u.lastname)
                  AS name
                  FROM prop_notes p
                  INNER JOIN users u ON u.id = p.users_id
                  WHERE p.property_id = ".Request::get('property_id').'
                  ORDER BY p.created_at DESC';

        $previous_notes = DB::select($query);

        return $previous_notes;
    }

    public function adminCloseProp($id)
    {
        $property = $this->getPropertyDetsbyId($id);

        return view('property.admin.closeProp', ['property' => $property]);
    }

    public function adminClose()
    {
        $input = Request::all();

        $id = $input['property_id'];
        $vetting_status = $input['vetting_status'];

        unset($input['property_id']);
        unset($input['vetting_status']);

        $vetting = $this->vetting->whereProperty_id($id)->first();

        $vetting->vetting_status = $vetting_status;

        $vetting->save();

        $property = $this->property->whereId($id)->first();

        $property->fill($input)->save();

        if ($vetting_status == '2') {
            App::make(\App\Http\Controllers\LeadsController::class)->rddNewActual($vetting->rep_id_number, 'Mandate', 1);
        } elseif ($vetting_status == '4') {
            App::make(\App\Http\Controllers\LeadsController::class)->rddNewActual($vetting->rep_id_number, 'Sales', $input['final_price']);
        }

        if ($vetting_status == '4' || $vetting_status == '5') {
            $property->sold_at = date('Y-m-d H:i:s');
            $property->save();
        }

        Flash::message('Status Updated');
        if ($vetting_status == '6' || $vetting_status == '7' || $vetting_status == '11' || $vetting_status == '14') {
            $property->featured = 0;
            $property->save();
            $property->delete();

            return redirect()->route('property.admin', ['status' => 'all']);
        }

        return redirect()->route('admin.property.edit', ['id' => $id]);
    }

    public function searchFavorites()
    {
        $user = Auth::user()->id; //Get Authed User
        $favorites = DB::table('favourites')->select('property_id')->whereUser_id($user)->get(); // Get IDs of all favorited properties for user that are live
        $resultArray = json_decode(json_encode($favorites), true); // Convert to array

        $properties = $this->property->
        select('property.*', 'residential.*', 'commercial.*', 'farm.*', 'vetting.*')->
        join('vetting', 'property.id', '=', 'vetting.property_id')->leftJoin('farm', 'property.id', '=', 'farm.property_id')->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        whereIn('property.id', $resultArray)->
        where(function ($q) {
            $q->where('vetting_status', '=', 2)
                ->orWhere('vetting_status', '=', 4)
                ->orWhere('vetting_status', '=', 5)
                ->orWhere('vetting_status', '=', 10)
                ->orWhere('vetting_status', '=', 12)
                ->orWhere('vetting_status', '=', 13)
                ->orWhere('vetting_status', '=', 15)
                ->orWhere('vetting_status', '=', 16);
        })->
        get();

        foreach ($properties as $property) {
            $property['smallImage'] = substr_replace($property['lead_image'], '_265_163', -4, 0);
        }

        return view('portal.favorites', ['properties' => $properties, 'listing' => 'Search']);
    }

    public function viewOffers($listing_type)
    {
        return view('property.admin.offers', ['listing_type' => $listing_type]);
    }

    public function getAdminOffersTable($listing_type)
    {
        switch ($listing_type):
            case 'all':
                $listing_type = '%';

        break;
        case 'Auction':
                $listing_type = '%weauction%';

        break;
        case 'In2assets':
                $listing_type = '%in2assets%';

        break;
        case 'WeSell':
                $listing_type = '%wesell%';

        break;
        endswitch;

        $properties = DB::table('property')
            ->join('make_contact', 'make_contact.property_id', '=', 'property.id')
            ->where('listing_type', 'LIKE', $listing_type)
            ->where('property.deleted_at', '=', null)
            ->where('make_contact.deleted_at', '=', null)
            ->join('users', 'users.id', '=', 'property.user_id')
            ->select('reference', 'property.province', 'property.suburb_town_city', 'property_type', 'firstname', 'lastname',
                'make_contact.offer', 'property_id', 'listing_type', 'make_contact.id', 'property.user_id', 'make_contact.id')
            ->get();

        foreach ($properties as $property) {
            $offer = DB::table('property')
                ->join('make_contact', 'make_contact.property_id', '=', 'property.id')
                ->where('make_contact.id', '=', $property->id)
                ->join('users', 'users.id', '=', 'make_contact.user_id')
                ->select('firstname', 'lastname', 'make_contact.user_id')
                ->first();

            $property->offer_firstname = $offer->firstname;
            $property->offer_lastname = $offer->lastname;
            $property->offer_user_id = $offer->user_id;
        }

        return Datatable::collection(new Collection($properties))
            ->showColumns('reference', 'province', 'suburb_town_city', 'property_type')
            ->addColumn('owner', function ($Property) {
                return $Property->firstname.' '.$Property->lastname;
            })
            ->addColumn('madeOffer', function ($Property) {
                return $Property->offer_firstname.' '.$Property->offer_lastname;
            })
            ->showColumns('offer')
            ->addColumn('listing_type', function ($Property) {
                switch ($Property->listing_type):
                    case 'weauction':
                        return 'Auction';

                break;
                case 'in2assets':
                        return 'In2Assets';

                break;
                case 'wesell':
                        return 'WeSell';

                break;
                endswitch;
            })
            ->addColumn('action', function ($Property) {
                if ($Property->listing_type == 'wesell') {
                    return '<a href="/admin/users/edit/'.$Property->user_id.'" title="View Seller" ><i class="i-circled i-light i-small icon-laptop"></i></a>
                <a href="/admin/users/edit/'.$Property->offer_user_id.'" title="View Buyer" ><i class="i-circled i-light i-small icon-diamond"></i></a>
                <a href="/admin/messages/'.$Property->offer_user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                <a href="/portal/makecontact/delete/'.$Property->id.'" title="Delete Offer" ><i class="i-circled i-light i-small icon-remove" style="background-color: #ff0000"></i></a>';
                } else {
                    return '<a href="/admin/users/edit/'.$Property->offer_user_id.'" title="View Buyer" ><i class="i-circled i-light i-small icon-diamond"></i></a>
                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                <a href="/portal/makecontact/delete/'.$Property->id.'" title="Delete Offer" ><i class="i-circled i-light i-small icon-remove" style="background-color: #ff0000"></i></a>';
                }
            })
            ->searchColumns('reference', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname', 'listing_type')
            ->orderColumns('reference', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname', 'listing_type')
            ->make();
    }

    public function featureProperty()
    {
        $input = Request::all();

        $dtStart = new DateTime('today');
        $dtStart->modify('+1 day');
        $dtStart = $dtStart->format('Y-m-d');

        $dtEnd = new DateTime($dtStart);
        $dtEnd->modify('+'.$input['days'].'day');
        $dtEnd = $dtEnd->format('Y-m-d');

        $property_id = $input['property_id'];
        $amount = $input['amount'];

        $this->featured->property_id = $property_id;
        $this->featured->amount_paid = $amount;
        $this->featured->start_date = $dtStart;
        $this->featured->end_date = $dtEnd;

        $this->featured->save();

        return $this->featured->id;
    }

    public function calendar($property_id)
    {
        $property = $this->property->whereId($property_id)->first();
        if ($property->listing_type == 'weauction') {
            $auctionDets = $this->auction->whereProperty_id($property_id)->first();
            $yourBids = $this->auction_bids->whereUser_id(Auth::user()->id)->orderBy('id', 'desc')->first();
            if ($auctionDets->auction_type == 2 || $auctionDets->auction_type == 3) {
                $register = $this->auction_register->whereUser_id(Auth::user()->id)->whereAuction_id($auctionDets->id)->first();
            } else {
                $yourBids = null;
                $register = null;
            }
            $auctionCalendarShow = $this->property->with('auction')->with('residential')->with('commercial')->with('farm')->whereId($property_id)->first();
        } else {
            $auctionDets = null;
            $yourBids = null;
            $auctionCalendarShow = null;
        }

        return view('property.calendar', ['property' => $property, 'auctionDets' => $auctionDets, 'yourBids' => $yourBids, 'auctionCalendarShow' => $auctionCalendarShow]);
    }

    public function history($id)
    {
        $images = $this->property->
        join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')
            ->where('property.id', $id)->withTrashed()->orderBy('uploads.position')->get();

        switch ($images[0]['property_type']):
            case 'Residential':
                $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->withTrashed()->first();

        break;
        case 'Commercial':
                $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->withTrashed()->first();

        break;
        case 'Farm':
                $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->withTrashed()->first();

        break;
        default:
                return redirect()->route('property.index');

        break;
        endswitch;

        $contact = DB::table('users')->whereId($property->user_id)->first();

        return view('property.admin.history', ['property' => $property, 'images' => $images,
            'contact' => $contact, ]);
    }

    public function trimAreas()
    {
        $provinces = DB::table('areas')->groupBy('province')->pluck('province');
        foreach ($provinces as $province) {
            DB::table('areas')->where('province', '=', $province)->update(['province' => trim($province)]);
        }
        $areas = DB::table('areas')->groupBy('area')->pluck('area');
        foreach ($areas as $area) {
            DB::table('areas')->where('area', '=', $area)->update(['area' => trim($area)]);
        }
        $suburb_town_citys = DB::table('areas')->groupBy('suburb_town_city')->pluck('suburb_town_city');
        foreach ($suburb_town_citys as $suburb_town_city) {
            DB::table('areas')->where('suburb_town_city', '=', $suburb_town_city)->update(['suburb_town_city' => trim($suburb_town_city)]);
        }
    }

    public function getAgentDetails()
    {
        $id = Request::get('agent_id');

        $agent_dets = $this->user->whereId($id)->first();

        return ['agent_dets' => $agent_dets];
    }

    public function getAgentProperties($id)
    {
        return redirect()->route('property.index', ['property_type' => 'property', 'listing_type' => 'all',
            'province' => 'south-africa', 'city' => 'all-cities', 'suburb' => 'all-suburbs', 'order' => 'default', 'view' => 'grid_view',
            'price_range' => '', 'rent_price_range' => '', 'floor_range' => '', ' land_range' => '', 'sub_type' => '',
            'bedrooms' => '', 'bathrooms' => '',
            'agent' => $id, ]);
    }

    public function searchHome()
    {
        $input = Request::all();

        if (Request::get('count') > 1) {
            unset($input['_token']);
            unset($input['province']);
            unset($input['area']);
            unset($input['suburb_town_city']);

            return redirect()->route('property.indexMulti', ['input' => json_encode($input)]);
        } else {
            if ($input['count'] == 1) {
                $searchField = $input['multiple0'];
            } else {
                $searchField = $input['AreaSearch'];
            }

            if (! $id = $this->searchReference($searchField)) {
                $searchArray = explode(',', $searchField);

                $province = 'south-africa';
                $city = 'all-cities';
                $suburb = 'all-suburbs';
                $slug = new Slugify();
                switch (count($searchArray)) {
                    case 1:
                        $province = $slug->slugify($searchArray[0]);
                        if ($province == '') {
                            $province = 'south-africa';
                        }

                        break;
                    case 2:
                        $province = $slug->slugify($searchArray[0]);
                        $city = $slug->slugify($searchArray[1]);

                        break;
                    case 3:
                        $province = $slug->slugify($searchArray[0]);
                        $city = $slug->slugify($searchArray[1]);
                        $suburb = $slug->slugify($searchArray[2]);

                        break;
                }

                switch ($input['listing_type']) {
                    case 'wesell':
                    case 'in2assets':
                        $listing_type = 'for-sale';

                        break;
                    case 'welet':
                    case 'in2rental':
                        $listing_type = 'to-let';

                        break;
                    case 'weauction':
                        $listing_type = 'on-auction';

                        break;
                    default:
                        $listing_type = 'all-listings';

                        break;
                }

                return redirect()->route('property.index', [$input['property_type'], $listing_type, $province, $city, $suburb, 'default', 'grid_view']);
            } elseif ($this->searchReference($searchField) == 'refcheck') {
                Flash::error('The reference number you are looking for does not exist, please contact us if you have found an error.');

                return redirect()->back();
            } else {
                $property = $this->getPropertyDetsbyId($id);

                return redirect()->to('/property/'.$id.'/'.$property->slug);
            }
        }
    }

    public function searchReference($searchField)
    {
        $refCheck = strtoupper(substr($searchField, 0, 5));

        if ($refCheck == 'SALE-' || $refCheck == 'RENT-' || $refCheck == 'AUCT-') {
            if ($property = $this->property->
            join('vetting', 'property.id', '=', 'vetting.property_id')->
            with('residential')->
            with('commercial')->
            with('farm')->
            with('auction')->
            where('property.reference', '=', $searchField)->
            where(function ($q) {
                $q->where('vetting_status', '=', 2)
                    ->orWhere('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 5)
                    ->orWhere('vetting_status', '=', 10)
                    ->orWhere('vetting_status', '=', 12)
                    ->orWhere('vetting_status', '=', 13)
                    ->orWhere('vetting_status', '=', 15)
                    ->orWhere('vetting_status', '=', 16);
            })->first()
            ) {
                return $property->property_id;
            } else {
                return 'refcheck';
            }
        } else {
            return false;
        }
    }

    public function searchResult($place, $listing_type, $adv_search_set, $property_types, $agent = '%')
    {
        if ($property_types[0] == 'Residential' && $property_types[1] == null && $property_types[2] == null) {
            $featured = $this->getFeaturedProperties('Residential');
        } else {
            $featuredCom = $this->getFeaturedProperties('Commercial');
            $featuredFarm = $this->getFeaturedProperties('Farm');

            $featured = $featuredCom->merge($featuredFarm);
        }

        switch ($listing_type) {
            case 'wesell':
                $adv_search_set['listing_type'] = 'wesell';

                break;
            case 'welet':
                $adv_search_set['listing_type'] = 'welet';

                break;
            case 'weauction':
                $adv_search_set['listing_type'] = 'weauction';

                break;
            default:
                $adv_search_set['listing_type'] = 'all';

                break;
        }

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        leftJoin('farm', 'property.id', '=', 'farm.property_id')->
        leftJoin('auction', 'property.id', '=', 'auction.property_id')->
        with('commercial')->
        with('residential')->
        with('farm')->
        with('auction')->
        with('vetting')->
        where('vetting.rep_id_number', 'LIKE', $agent)->
        where(function ($lq) use ($listing_type) {
            switch ($listing_type) {
                case 'wesell':
                    $lq->where('listing_type', '=', 'wesell')
                        ->orWhere('listing_type', '=', 'in2assets');

                    break;
                case 'welet':
                    $lq->where('listing_type', '=', 'welet')
                        ->orWhere('listing_type', '=', 'in2rental');

                    break;
                case 'weauction':
                    $lq->where('listing_type', '=', 'weauction');

                    break;
            }
        })->
        where(function ($q) {
            $q->where('vetting_status', '=', 2)
                ->orWhere('vetting_status', '=', 4)
                ->orWhere('vetting_status', '=', 5)
                ->orWhere('vetting_status', '=', 10)
                ->orWhere('vetting_status', '=', 12)
                ->orWhere('vetting_status', '=', 13)
                ->orWhere('vetting_status', '=', 15)
                ->orWhere('vetting_status', '=', 16);
        })->
        where(function ($query) use ($property_types) {
            $query->where('property_type', '=', $property_types[0])
                ->orWhere('property_type', '=', $property_types[1])
                ->orWhere('property_type', '=', $property_types[2]);
        })->
        where(function ($query) use ($place) {
            $placeArray = explode(',', $place);

            switch (count($placeArray)) {
                case 1:
                    $province = '%'.trim($placeArray[0]);
                    $query->whereRaw('province LIKE "'.$province.'"');

                    break;
                case 2:
                    $province = '%'.trim($placeArray[0]);
                    $area = '%'.trim($placeArray[1]);
                    $query->whereRaw('province LIKE "'.$province.'" AND ( area LIKE "'.$area.'" AND suburb_town_city LIKE "%" ) OR (suburb_town_city LIKE "'.$area.'")');

                    break;
                case 3:
                    $province = '%'.trim($placeArray[0]);
                    $area = '%'.trim($placeArray[1]);
                    $suburb_town_city = '%'.trim($placeArray[2]);
                    $query->whereRaw('province LIKE "'.$province.'" AND ( area LIKE "'.$area.'" AND suburb_town_city LIKE "'.$suburb_town_city.'" )  OR (area LIKE "'.$suburb_town_city.'") ');

                    break;
                default:
                    $query->where('1');

                    break;
            }
        })->
        get();

//        dd(AppHelper::getLastQuery());

        foreach ($properties as $property) {
            $property['id'] = $this->getIdbyReference($property['reference']);

            $property['smallImage'] = AppHelper::imgThumb($property['lead_image']);

            if ($bedroomsDets = $property->getFeatureValue('bedrooms', $property['id'])) {
                $property['bedrooms'] = $bedroomsDets[0]->value;
                $bedroomsDets[0]->value = '';
            } else {
                $property['bedrooms'] = '';
            }
            if ($bathroomsDets = $property->getFeatureValue('bathrooms', $property['id'])) {
                $property['bathrooms'] = $bathroomsDets[0]->value;
                $bathroomsDets[0]->value = '';
            } else {
                $property['bathrooms'] = '';
            }
        }

        return view('property.searchResult', ['properties' => $properties, 'listing' => 'Search', 'featured' => $featured,
            'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'], 'cities_adv' => $areas['cities'],
            'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set, ]);
    }

    public function getIdbyReference($reference)
    {
        $this->property = $this->property->select('id')->whereReference($reference)->first();

        return $this->property->id;
    }

    public function AddWeSellProperty()
    {
        $input = Request::all();

        $input['user_id'] = Auth::user()->id;
        $input['country'] = 'South Africa';
        $input['listing_type'] = 'wesell';
        $input['reference'] = date('Y-m-d H:i:s');

        $this->property->fill($input);

        $this->property->save();

        return redirect()->route('wesell-congratulations');
    }

    public function brochureHTML($id)
    {
        $property = $this->getPropertyDetsbyId($id);

        $images = $this->property->
        join('uploads', 'uploads.link', '=', 'property.id')->
        whereClass('property')->
        whereType('image')->
        where('property.id', $id)->
        take(3)->
        orderBy('uploads.position')->
        get();

        $agent = $this->user->whereId($property->rep_id_number)->first();

        if ($bedroomsDets = $property->getFeatureValue('bedrooms', $property->id)) {
            $property->bedrooms = $bedroomsDets[0]->value;
            $bedroomsDets[0]->value = '';
        } else {
            $property->bedrooms = '';
        }
        if ($bathroomsDets = $property->getFeatureValue('bathrooms', $property->id)) {
            $property->bathrooms = $bathroomsDets[0]->value;
            $bathroomsDets[0]->value = '';
        } else {
            $property->bathrooms = '';
        }
        if ($garagesDets = $property->getFeatureValue('garages', $property->id)) {
            $property->garages = $garagesDets[0]->value;
            $garagesDets[0]->value = '';
        } else {
            $property->garages = '';
        }

        return view('property.admin.brochure', ['property' => $property, 'images' => $images, 'agent' => $agent]);
    }

    public function PrintBrochure($id)
    {
        $url = str_replace('/print', '', "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

        $html = file_get_contents($url);

        // First. Convert all relative image paths to file system paths.
        // Test that the generated path is where your images files are located
        $html = str_replace('/images', public_path().'/images', $html);

        // Generate PDF file
        $options = new Options();
        $options->set('defaultFont', 'Open Sans');
        $dompdf = new Dompdf($options);
        $dompdf->setPaper('A4', 'portrait');

        $dompdf->loadHtml($html);
        $dompdf->render();
        $dompdf->stream('brochure_print.pdf');
    }

    public function advancedPropertySearch()
    {
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);
        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();
        $brokers = DB::table('broker_logos')->select('id', 'broker_name')->whereDeleted_at(null)->get();

        $zone_types = DB::table('zone_types')->whereActive(1)->pluck('type', 'id');

        return view('property.admin.advanced_search', ['brokers' => $brokers, 'agents' => $agents, 'cities' => $cities,
            'zone_types' => $zone_types, 'count' => 1, ]);
    }

    public function adminSearch()
    {
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);
        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();
        $brokers = DB::table('broker_logos')->select('id', 'broker_name')->whereDeleted_at(null)->get();

        $input = Request::all();

        //region setting vars
        $listing_type = '1=1';
        $rental_terms = '1=1';
        $available_from = '1=1';
        $price_min = '1=1';
        $price_max = '1=1';
        $company = '1=1';
        $agency = '1=1';
        $agent = '1=1';
        $land_size_min = '1=1';
        $land_size_max = '1=1';

        $count_area = 1;
        $count_check = 1;
        while ($count_area == $count_check) {
            if (isset($input['suburb_town_city'.$count_area])) {
                $count_area++;
            }
            $count_check++;
        }
        $area = '';
        $area_count = $count_area - 1;
        $suburb = [];
        if ($area_count > 0) {
            if ($area_count > 1) {
                while ($area_count > 0) {
                    if ($area_count == $count_area - 1) {
                        $area .= '(';
                    }
                    if ($area_count == 1) {
                        $area .= 'p.suburb_town_city = "'.$input['suburb_town_city'.$area_count].'"';
                        $suburb[] = $input['suburb_town_city'.$area_count].', '.$input['area'.$area_count].', '.$input['province'.$area_count];
                    } else {
                        $area .= 'p.suburb_town_city = "'.$input['suburb_town_city'.$area_count].'" OR ';
                        $suburb[] = $input['suburb_town_city'.$area_count].', '.$input['area'.$area_count].', '.$input['province'.$area_count];
                    }
                    $area_count--;
                }
                $area .= ')';
            } else {
                $area = '(p.suburb_town_city = "'.$input['suburb_town_city'.$area_count].'")';
                $suburb[] = $input['suburb_town_city'.$area_count].', '.$input['area'.$area_count].', '.$input['province'.$area_count];
            }
        } else {
            $area = '1=1';
        }

        if ($area == '1=1') {
            if ($input['suburb_town_city'] != '') {
                $area = 'p.suburb_town_city = "'.$input['suburb_town_city'].'"';
            }
        } else {
            if ($input['suburb_town_city'] != '') {
                $area = str_replace(')', ' OR p.suburb_town_city = "'.$input['suburb_town_city'].'")', $area);
            }
        }

        $farm_type = '1=1';
        $floor_space_min = '1=1';
        $floor_space_max = '1=1';
        $residential_type = '1=1';
        $parking = '1=1';
        $commercial_type = '1=1';
        $office_size_min = '1=1';
        $office_size_max = '1=1';
        $factory_size_min = '1=1';
        $factory_size_max = '1=1';
        $shop_size_min = '1=1';
        $shop_size_max = '1=1';
        $yard_size_min = '1=1';
        $yard_size_max = '1=1';
        $vacant_land_size_min = '1=1';
        $vacant_land_size_max = '1=1';
        $office_price_min = '1=1';
        $office_price_max = '1=1';
        $factory_price_min = '1=1';
        $factory_price_max = '1=1';
        $shop_price_min = '1=1';
        $shop_price_max = '1=1';
        $yard_price_min = '1=1';
        $yard_price_max = '1=1';
        $vacant_land_price_min = '1=1';
        $vacant_land_price_max = '1=1';
        $zoning = '1=1';
        $parking_bays_min = '1=1';
        $parking_bays_max = '1=1';
        $parking_price_min = '1=1';
        $parking_price_max = '1=1';
        $covered_parking_bays_min = '1=1';
        $covered_parking_bays_max = '1=1';
        $covered_parking_price_min = '1=1';
        $covered_parking_price_max = '1=1';
        $amps_min = '1=1';
        $amps_max = '1=1';
        //endregion

        $tag_o = DB::table('tags')->where('name', '=', 'OWNER')->first();
        $tag_t = DB::table('tags')->where('name', '=', 'TENANT')->first();
        $tag_k = DB::table('tags')->where('name', '=', 'KEYHOLDER')->first();
        $tag_ma = DB::table('tags')->where('name', '=', 'MANAGING AGENT')->first();

        switch ($input['listing_type']) {
            case 'in2assets':
                $listing_type = "(p.listing_type = 'in2assets' OR p.listing_type = 'wesell')";
                if ($input['price_min'] != '') {
                    $price_min = "p.price >= '".$input['price_min']."'";
                }
                if ($input['price_max'] != '' || isset($input['price_max'])) {
                    $price_max = "p.price <= '".$input['price_max']."'";
                }

                break;
            case 'weauction':
                $listing_type = "(p.listing_type = 'weauction')";
                if ($input['price_min'] != '') {
                    $price_min = "p.price >= '".$input['price_min']."'";
                }
                if ($input['price_max'] != '') {
                    $price_max = "p.price <= '".$input['price_max']."'";
                }

                break;
            case 'in2rental':
                $listing_type = "(p.listing_type = 'in2rental' OR p.listing_type = 'welet')";
                if ($input['price_min'] != '') {
                    $price_min = "p.price >= '".$input['price_min']."'";
                }
                if ($input['price_max'] != '') {
                    $price_max = "p.price <= '".$input['price_max']."'";
                }
                if ($input['available_from'] != '') {
                    $available_from = "p.available_from < '".$input['available_from']."'";
                }
                if ($input['rental_terms'] != '') {
                    $rental_terms = "p.rental_terms = '".$input['rental_terms']."'";
                }

                break;
            case 'all':
                $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN broker b ON p.broker = b.id
                            WHERE p.deleted_at IS null';

                $properties = DB::select($query);

                break;
            case 'residential':
                $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN broker b ON p.broker = b.id
                            WHERE p.property_type = "residential"
                            AND  p.deleted_at IS null';
                $properties = DB::select($query);

                break;
            case 'commercial':
                $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN broker b ON p.broker = b.id
                            WHERE p.property_type = "commercial"
                            AND  p.deleted_at IS null';
                $properties = DB::select($query);

                break;
            case 'farm':
                $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN broker b ON p.broker = b.id
                            WHERE p.property_type = "farm"
                            AND  p.deleted_at IS null';
                $properties = DB::select($query);

                break;
            case 'rental':
                $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN broker b ON p.broker = b.id
                            WHERE p.listing_type = "in2rental"
                            AND  p.deleted_at IS null';
                $properties = DB::select($query);

                break;
            case 'archived':
                $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN broker b ON p.broker = b.id
                            WHERE p.deleted_at IS NOT null';
                $properties = DB::select($query);

                break;
            default:
                break;
        }

        if ($input['company'] != '') {
            $company = "o.firstname = '".$input['company']."'";
        }
        if (array_key_exists('ext_broker', $input) && $input['ext_broker'] != '') {
            $agency = "b.agency = '".$input['ext_broker']."'";
        }

        if ($input['agents'] != 0 && $input['agents'] != '') {
            $agent = "v.rep_id_number = '".$input['agents']."'";
        }

        $propertyIdObject = DB::select('SELECT p.id FROM property p WHERE p.deleted_at IS NULL');
        $propertyIdString = '';
        $count = 0;
        foreach ($propertyIdObject as $propertyId) {
            if ($count == 0) {
                $propertyIdString .= $propertyId->id;
            } else {
                $propertyIdString .= ', '.$propertyId->id;
            }
            $count++;
        }

        if (array_key_exists('property_type', $input) && $input['property_type'] != '') {
            if ($input['land_measurement'] == 'HA') {
                $land_size_min_in_m = $input['land_size_min'] * 10000;
                $land_size_max_in_m = $input['land_size_max'] * 10000;
            } else {
                $land_size_min_in_m = $input['land_size_min'];
                $land_size_max_in_m = $input['land_size_max'];
            }

            if ($input['land_size_min'] != '') {
                $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'land_size' AND ap.value >=
                                                ".$land_size_min_in_m;
                $propertyIdObject = DB::select($query);
            }
            $propertyIdString = $this->ObjectToString($propertyIdObject);
            if ($input['land_size_max'] != '') {
                $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'land_size' AND ap.value <=
                                                ".$land_size_max_in_m;
                $propertyIdObject = DB::select($query);
            }
        }

        if (array_key_exists('property_type', $input)) {
            switch ($input['property_type']) {
                case 'Farm':
                    $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            INNER JOIN farm f ON p.id = f.property_id
                            LEFT JOIN broker b ON p.broker = b.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id ';
                    if ($input['farm_type'] != '') {
                        $farm_type = "f.farm_type = '".$input['farm_type']."'";
                    }
                    $propertyIdString = $this->ObjectToString($propertyIdObject);
                    if (empty($propertyIdString)) {
                        $whereIn = 'WHERE p.id IN (" ") AND ';
                    } else {
                        $whereIn = "WHERE p.id IN ($propertyIdString) AND ";
                    }
                    $fullQuery = $query.$whereIn.$listing_type.' AND '.$agent.' AND '.$rental_terms.' AND '.$available_from
                        .' AND '.$price_min.' AND '.$price_max.' AND '.$agency.' AND '.$company
                        .' AND '.$farm_type.' AND '.$land_size_min.' AND '.$land_size_max
                        .' AND '.$area.' AND p.deleted_at IS NULL';

                    $properties = DB::select($fullQuery);

                    break;
                case 'Residential':
                    $queryMain = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            INNER JOIN residential r ON p.id = r.property_id
                            LEFT JOIN broker b ON p.broker = b.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id ';

                    if ($input['floor_space_min'] != '') {
                        $propertyIdString = $this->ObjectToString($propertyIdObject);
                        $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'floor_space' AND ap.value >=
                                                ".$input['floor_space_min'];
                        $propertyIdObject = DB::select($query);
                    }

                    if ($input['floor_space_max'] != '') {
                        $propertyIdString = $this->ObjectToString($propertyIdObject);
                        $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'floor_space' AND ap.value <=
                                                ".$input['floor_space_max'];

                        $propertyIdObject = DB::select($query);
                    }

                    if ($input['dwelling_type'] != '') {
                        $residential_type = "r.dwelling_type = '".$input['dwelling_type']."'";
                    }

                    switch ($input['bedrooms']) {
                        case 'Studio':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bedrooms' AND ap.value =
                                                0";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '1 Bedroom or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bedrooms' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '2 Bedrooms or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bedrooms' AND ap.value >=
                                                2";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '3 Bedrooms or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bedrooms' AND ap.value >=
                                                3";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '4 Bedrooms or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bedrooms' AND ap.value >=
                                                4";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '5 Bedrooms or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bedrooms' AND ap.value >=
                                                5";
                            $propertyIdObject = DB::select($query);

                            break;
                        default:
                            break;
                    }

                    switch ($input['bathrooms']) {
                        case '1 Bathroom or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bathrooms' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '2 Bathrooms or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bathrooms' AND ap.value >=
                                                2";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '3 Bathrooms or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bathrooms' AND ap.value >=
                                                3";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '4 Bathrooms or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'bathrooms' AND ap.value >=
                                                4";
                            $propertyIdObject = DB::select($query);

                            break;
                        default:
                            break;
                    }

                    switch ($input['parking']) {
                        case '1 Garage or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'garages' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '2 Garages or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'garages' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '3 Garages or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'garages' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        default:
                            break;
                    }
                    $propertyIdString = $this->ObjectToString($propertyIdObject);
                    if (empty($propertyIdString)) {
                        $whereIn = 'WHERE p.id IN (" ") AND ';
                    } else {
                        $whereIn = "WHERE p.id IN ($propertyIdString) AND ";
                    }

                    $fullQuery = $queryMain.$whereIn.$listing_type.' AND '.$agent.' AND '.$rental_terms.' AND '.$available_from
                        .' AND '.$price_min.' AND '.$price_max.' AND '.$company.' AND '.$agency
                        .' AND '.$residential_type.' AND '.$area.' AND p.deleted_at IS NULL';

                    $properties = DB::select($fullQuery);

                    break;
                case 'Commercial':
                    $queryMain = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            INNER JOIN commercial c ON p.id = c.property_id
                            LEFT JOIN broker b ON p.broker = b.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id ';
                    if ($input['floor_space_min'] != '') {
                        $propertyIdString = $this->ObjectToString($propertyIdObject);
                        $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'floor_space' AND ap.value >=
                                                ".$input['floor_space_min'];
                        $propertyIdObject = DB::select($query);
                    }

                    if ($input['floor_space_max'] != '') {
                        $propertyIdString = $this->ObjectToString($propertyIdObject);
                        $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'floor_space' AND ap.value <=
                                                ".$input['floor_space_max'];

                        $propertyIdObject = DB::select($query);
                    }
                    if ($input['commercial_type'] != '') {
                        $commercial_type = "c.commercial_type = '".$input['commercial_type']."'";
                    }
                    switch ($input['parking']) {
                        case '1 Garage or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'garages' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '2 Garages or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'garages' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        case '3 Garages or more':
                            $propertyIdString = $this->ObjectToString($propertyIdObject);
                            $query = "SELECT ap.property_id
                                                FROM attributes_property ap
                                                INNER JOIN attributes a ON a.id = ap.attributes_id
                                                WHERE ap.property_id IN ($propertyIdString)
                                                AND a.name = 'garages' AND ap.value >=
                                                1";
                            $propertyIdObject = DB::select($query);

                            break;
                        default:
                            break;
                    }
                    if ($input['office_size_min'] != '') {
                        $office_size_min = "p.office_size >= '".$input['office_size_min']."'";
                    }
                    if ($input['office_size_max'] != '') {
                        $office_size_max = "p.office_size <= '".$input['office_size_max']."'";
                    }
                    if ($input['factory_size_min'] != '') {
                        $factory_size_min = "p.factory_size >= '".$input['factory_size_min']."'";
                    }
                    if ($input['factory_size_max'] != '') {
                        $factory_size_max = "p.factory_size <= '".$input['factory_size_max']."'";
                    }
                    if ($input['shop_size_min'] != '') {
                        $shop_size_min = "p.shop_size >= '".$input['shop_size_min']."'";
                    }
                    if ($input['shop_size_max'] != '') {
                        $shop_size_max = "p.shop_size <= '".$input['shop_size_max']."'";
                    }
                    if ($input['yard_size_min'] != '') {
                        $yard_size_min = "p.yard_size >= '".$input['yard_size_min']."'";
                    }
                    if ($input['yard_size_max'] != '') {
                        $yard_size_max = "p.yard_size <= '".$input['yard_size_max']."'";
                    }
                    if ($input['vacant_land_size_min'] != '') {
                        $vacant_land_size_min = "p.vacant_land_size >= '".$input['vacant_land_size_min']."'";
                    }
                    if ($input['vacant_land_size_max'] != '') {
                        $vacant_land_size_max = "p.vacant_land_size <= '".$input['vacant_land_size_max']."'";
                    }
                    if ($input['office_price_min'] != '') {
                        $office_price_min = "p.office_price >= '".$input['office_price_min']."'";
                    }
                    if ($input['office_price_max'] != '') {
                        $office_price_max = "p.office_price <= '".$input['office_price_max']."'";
                    }
                    if ($input['factory_price_min'] != '') {
                        $factory_price_min = "p.factory_price >= '".$input['factory_price_min']."'";
                    }
                    if ($input['factory_price_max'] != '') {
                        $factory_price_max = "p.factory_price <= '".$input['factory_price_max']."'";
                    }
                    if ($input['shop_price_min'] != '') {
                        $shop_price_min = "p.shop_price >= '".$input['shop_price_min']."'";
                    }
                    if ($input['shop_price_max'] != '') {
                        $shop_price_max = "p.shop_price <= '".$input['shop_price_max']."'";
                    }
                    if ($input['yard_price_min'] != '') {
                        $yard_price_min = "p.yard_price >= '".$input['yard_price_min']."'";
                    }
                    if ($input['yard_price_max'] != '') {
                        $yard_price_max = "p.yard_price <= '".$input['yard_price_max']."'";
                    }
                    if ($input['vacant_land_price_min'] != '') {
                        $vacant_land_price_min = "p.vacant_land_price >= '".$input['vacant_land_price_min']."'";
                    }
                    if ($input['vacant_land_price_max'] != '') {
                        $vacant_land_price_max = "p.vacant_land_price <= '".$input['vacant_land_price_max']."'";
                    }
                    if ($input['zoning'] != '') {
                        $zoning = "p.zoning = '".$input['zoning']."'";
                    }
                    if ($input['parking_bays_min'] != '') {
                        $parking_bays_min = "p.parking_bays >= '".$input['parking_bays_min']."'";
                    }
                    if ($input['parking_bays_max'] != '') {
                        $parking_bays_max = "p.parking_bays <= '".$input['parking_bays_max']."'";
                    }
                    if ($input['parking_price_min'] != '') {
                        $parking_price_min = "p.parking_price >= '".$input['parking_price_min']."'";
                    }
                    if ($input['parking_price_max'] != '') {
                        $parking_price_max = "p.parking_price <= '".$input['parking_price_max']."'";
                    }
                    if ($input['covered_parking_bays_min'] != '') {
                        $covered_parking_bays_min = "p.covered_parking_bays >= '".$input['covered_parking_bays_min']."'";
                    }
                    if ($input['covered_parking_bays_max'] != '') {
                        $covered_parking_bays_max = "p.covered_parking_bays <= '".$input['covered_parking_bays_max']."'";
                    }
                    if ($input['covered_parking_price_min'] != '') {
                        $covered_parking_price_min = "p.covered_parking_price >= '".$input['covered_parking_price_min']."'";
                    }
                    if ($input['covered_parking_price_max'] != '') {
                        $covered_parking_price_max = "p.covered_parking_price <= '".$input['covered_parking_price_max']."'";
                    }
                    if ($input['amps_min'] != '') {
                        $amps_min = "p.amps >= '".$input['amps_min']."'";
                    }
                    if ($input['amps_max'] != '') {
                        $amps_max = "p.amps <= '".$input['amps_max']."'";
                    }
                    $propertyIdString = $this->ObjectToString($propertyIdObject);
                    if (empty($propertyIdString)) {
                        $whereIn = 'WHERE p.id IN (" ") AND ';
                    } else {
                        $whereIn = "WHERE p.id IN ($propertyIdString) AND ";
                    }
                    $fullQuery = $queryMain.$whereIn.$listing_type.' AND '.$agent.' AND '.$rental_terms.' AND '.$available_from
                        .' AND '.$price_min.' AND '.$price_max.' AND '.$company.' AND '.$agency
                        .' AND '.$commercial_type.' AND '.$land_size_min.' AND '.$land_size_max
                        .' AND '.$floor_space_min.' AND '.$floor_space_max.' AND '.$parking
                        .' AND '.$office_size_min.' AND '.$office_size_max.' AND '.$factory_size_min
                        .' AND '.$factory_size_max.' AND '.$shop_size_min.' AND '.$shop_size_max.' AND '.$yard_size_min
                        .' AND '.$yard_size_max.' AND '.$vacant_land_size_min.' AND '.$vacant_land_size_max
                        .' AND '.$office_price_min.' AND '.$office_price_max.' AND '.$factory_price_min
                        .' AND '.$factory_price_max.' AND '.$shop_price_min.' AND '.$shop_price_max
                        .' AND '.$yard_price_min.' AND '.$yard_price_max.' AND '.$vacant_land_price_min
                        .' AND '.$vacant_land_price_max.' AND '.$zoning.' AND '.$parking_bays_min.' AND '.$parking_bays_max
                        .' AND '.$parking_price_min.' AND '.$parking_price_max.' AND '.$covered_parking_bays_min
                        .' AND '.$covered_parking_bays_max.' AND '.$covered_parking_price_min.' AND '.$covered_parking_price_max
                        .' AND '.$amps_min.' AND '.$amps_max.' AND '.$area.' AND p.deleted_at IS NULL';

                    $properties = DB::select($fullQuery);

                    break;
                default:
                    $query = 'SELECT p.*, a.firstname, a.lastname, v.vetting_status, v.property_id
                            FROM property p
                            INNER JOIN vetting v ON p.id = v.property_id
                            LEFT JOIN broker b ON p.broker = b.id
                            LEFT JOIN users a ON p.agent_id = a.id
                            LEFT JOIN property_user puo ON p.id = puo.property_id AND puo.role_id = '.$tag_o->id.'
                            LEFT JOIN property_user put ON p.id = put.property_id AND put.role_id = '.$tag_t->id.'
                            LEFT JOIN property_user puk ON p.id = puk.property_id AND puk.role_id = '.$tag_k->id.'
                            LEFT JOIN property_user puma ON p.id = puma.property_id AND puma.role_id = '.$tag_ma->id.'
                            LEFT JOIN users o ON puo.user_id = o.id
                            LEFT JOIN users t ON put.user_id = t.id
                            LEFT JOIN users k ON puk.user_id = k.id
                            LEFT JOIN users ma ON puma.user_id = ma.id ';

                    $propertyIdString = $this->ObjectToString($propertyIdObject);
                    if (empty($propertyIdString)) {
                        $whereIn = 'WHERE p.id IN (" ") AND ';
                    } else {
                        $whereIn = "WHERE p.id IN ($propertyIdString) AND ";
                    }
                    $fullQuery = $query.$whereIn.$listing_type.' AND '.$rental_terms.' AND '.$available_from
                        .' AND '.$price_min.' AND '.$price_max.' AND '.$company.' AND '.$agency.' AND '.$agent
                        .' AND '.$area.' AND p.deleted_at IS NULL';

                    $properties = DB::select($fullQuery);

                    break;
            }
        }

        $property_ids = [];
        foreach ($properties as $property) {
            $property_ids[] = $property->id;
        }
        $property_ids = json_encode($property_ids);

        $zone_types = DB::table('zone_types')->whereActive(1)->pluck('type', 'id');

        return view('property.admin.advanced_search', ['brokers' => $brokers, 'agents' => $agents, 'cities' => $cities,
            'property_ids' => $property_ids, 'listing_type' => $input['listing_type'], 'property_type' => $input['property_type'],
            'input' => $input, 'zone_types' => $zone_types, 'suburb' => $suburb, 'count' => count($suburb) + 1, ]);
    }

    public function ObjectToString($object)
    {
        $propertyIdString = '';
        $count = 0;
        foreach ($object as $propertyId) {
            if ($count == 0) {
                if (property_exists($propertyId, 'id')) {
                    $propertyIdString .= $propertyId->id;
                } else {
                    $propertyIdString .= $propertyId->property_id;
                }
            } else {
                if (property_exists($propertyId, 'id')) {
                    $propertyIdString .= ', '.$propertyId->id;
                } else {
                    $propertyIdString .= ', '.$propertyId->property_id;
                }
            }
            $count++;
        }

        return $propertyIdString;
    }

    public function getAdminPropertyUserTable()
    {
        $input = Request::all();

        $property_ids = $input['property_ids'];

        $propertyIdString = str_replace(']', ')', str_replace('[', '(', $property_ids));

        if ($propertyIdString == '()') {
            $propertyIdString = str_replace('()', "('')", $propertyIdString);
        }
        $query = 'SELECT user_id FROM property_user pu WHERE pu.property_id IN '.$propertyIdString.' GROUP BY user_id';

        $user_ids = DB::select($query);

        $user_id_array = [];

        foreach ($user_ids as $user_id) {
            $user_id_array[] = $user_id->user_id;
        }

        $users = $this->user
            ->whereIn('users.id', $user_id_array)
            ->get();

        return Datatable::collection($users)
            ->addColumn('name', function ($User) {
                return $User->firstname.' '.$User->lastname;
            })
            ->addColumn('email', function ($User) {
                return '<a href="mailto:'.$User->email.'">'.$User->email.'</a>';
            })
            ->showColumns('cellnumber')
            ->addColumn('action', function ($User) {
                return 'None';
            })
            ->searchColumns('name', 'email', 'cellnumber')
            ->orderColumns('name', 'email', 'cellnumber')
            ->make();
    }

    public function getAdminPropertySearchTable()
    {
        $input = Request::all();

        $property_ids = json_decode($input['property_ids']);

        $properties = $this->property
            ->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('property.id', $property_ids)
            ->get();

        return Datatable::collection($properties)
            ->showColumns('reference', 'listing_type')
            ->addColumn('street_address', function ($Property) {
                return $Property->complex_number.' '.$Property->complex_name.' '.$Property->street_number.' '.$Property->street_address;
            })
            ->showColumns('province', 'suburb_town_city', 'property_type')
            ->addColumn('vetting_status', function ($Property) {
                switch ($Property->vetting_status) {
                    case 1:
                        return 'Pending';

                        break;
                    case 2:
                        return 'Approved';

                        break;
                    case 3:
                        return 'Vetting';

                        break;
                    case 4:
                        return 'Sold < 60 Days';

                        break;
                    case 5:
                        return 'Rented < 14 Days';

                        break;
                    case 6:
                        return 'Sold > 60 Days';

                        break;
                    case 7:
                        return 'Rented > 14 Days';

                        break;
                    case 8:
                        return 'Rejected';

                        break;
                    case 9:
                        return 'Ready For Auction';

                        break;
                    case 10:
                        return 'Sold on Auction';

                        break;
                    case 11:
                        return 'Not Sold';

                        break;
                    case 12:
                        return 'Sold Private Treaty Pre Auction';

                        break;
                    case 13:
                        return 'Sold Private Treaty Post Auction';

                        break;
                    case 14:
                        return 'Withdrawn';

                        break;
                    case 15:
                        return 'Postponed';

                        break;
                    case 16:
                        return 'S.T.C.';

                        break;
                }
            })
            ->addColumn('action', function ($Property) {
                if ($Property->vetting_status == 1) {
                    return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>';
                } elseif ($Property->vetting_status == 2) {
                    if ($Property->listing_type == 'welet') {
                        return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>';
                    } elseif ($Property->listing_type == 'wesell') {
                        if ($Property->featured != 1) {
                            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                    <a href="/brochure/'.$Property->property_id.'" title="Print Brochure" target="_blank" ><i class="i-circled i-light i-alt i-small icon-book"></i></a>
                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>';
                        } else {
                            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                    <a href="/brochure/'.$Property->property_id.'" title="Print Brochure" target="_blank" ><i class="i-circled i-light i-alt i-small icon-book"></i></a>
                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>';
                        }
                    } else {
                        if ($Property->featured != 1) {
                            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>

                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>';
                        } else {
                            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                    <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                                    <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>

                                    <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>';
                        }
                    }
                } elseif ($Property->vetting_status == 3) {
                    return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                            <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                            <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                            <a href="/admin/vetting/'.$Property->property_id.'" title="Vet Property" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>
                            <a href="/wesell/calendar/property/'.$Property->property_id.'" title="Organise Viewings" ><i class="i-circled i-light i-alt i-small icon-calendar"></i></a>';
                } elseif ($Property->vetting_status == 9) {
                    if ($Property->featured != 1) {
                        return '<a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>';
                    } else {
                        return '<a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>';
                    }
                } else {
                    if ($Property->featured != 1) {
                        return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                                <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>';
                    } else {
                        return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                                <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>';
                    }
                }
            })
            ->searchColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status')
            ->orderColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status')
            ->make();
    }

    public function cloneProperty($id)
    {
        $clonedProperty = $this->property->with('commercial')->with('residential')->with('farm')->whereId($id)->first();

        $propertyAttribute = [];
        $propertyAttribute['user_id'] = Auth::user()->id;
        $propertyAttribute['image_gallery'] = 0;
        $propertyAttribute['street_number'] = $clonedProperty->street_number;
        $propertyAttribute['street_address'] = $clonedProperty->street_address;
        $propertyAttribute['suburb_town_city'] = $clonedProperty->suburb_town_city;
        $propertyAttribute['area'] = $clonedProperty->area;
        $propertyAttribute['province'] = $clonedProperty->province;
        $propertyAttribute['country'] = $clonedProperty->country;
        $propertyAttribute['listing_type'] = $clonedProperty->listing_type;
        $propertyAttribute['available_from'] = $clonedProperty->available_from;
        $propertyAttribute['zoning'] = $clonedProperty->zoning;
        $propertyAttribute['floor_space'] = $clonedProperty->floor_space;
        $propertyAttribute['vacant_land_size'] = $clonedProperty->vacant_land_size;
        $propertyAttribute['office_size'] = $clonedProperty->office_size;
        $propertyAttribute['factory_size'] = $clonedProperty->factory_size;
        $propertyAttribute['shop_size'] = $clonedProperty->shop_size;
        $propertyAttribute['yard_size'] = $clonedProperty->yard_size;
        $propertyAttribute['agent_id'] = $clonedProperty->agent_id;
        $propertyAttribute['show_street_address'] = $clonedProperty->show_street_address;
        $propertyAttribute['agent_percent'] = $clonedProperty->agent_percent;
        $propertyAttribute['office_price'] = $clonedProperty->office_price;
        $propertyAttribute['factory_price'] = $clonedProperty->factory_price;
        $propertyAttribute['vacant_land_price'] = $clonedProperty->vacant_land_price;
        $propertyAttribute['shop_price'] = $clonedProperty->shop_price;
        $propertyAttribute['yard_price'] = $clonedProperty->yard_price;
        $propertyAttribute['property_type'] = $clonedProperty->property_type;
        $reference = $this->newReference($clonedProperty->listing_type);
        $propertyAttribute['reference'] = $reference;

        $this->property->fill($propertyAttribute)->save();
        $propertyId = $this->property->id;
        $vettingInfo = $this->vetting->whereProperty_id($id)->first();

        $vettingAttribute = [];
        $vettingAttribute['property_id'] = $propertyId;
        $vettingAttribute['owner_title'] = $vettingInfo->owner_title;
        $vettingAttribute['owner_firstname'] = $vettingInfo->owner_firstname;
        $vettingAttribute['owner_surname'] = $vettingInfo->owner_surname;
        $vettingAttribute['ownership_type'] = $vettingInfo->ownership_type;
        $vettingAttribute['rep_title'] = $vettingInfo->rep_title;
        $vettingAttribute['rep_firstname'] = $vettingInfo->rep_firstname;
        $vettingAttribute['rep_surname'] = $vettingInfo->rep_surname;
        $vettingAttribute['rep_id_number'] = $vettingInfo->rep_id_number;
        $vettingAttribute['rep_cell_num'] = $vettingInfo->rep_cell_num;
        $vettingAttribute['rep_email'] = $vettingInfo->rep_email;
        $vettingAttribute['marriage_stat'] = $vettingInfo->marriage_stat;
        $vettingAttribute['mar_in_com'] = $vettingInfo->mar_in_com;
        $vettingAttribute['spouse_title'] = $vettingInfo->spouse_title;
        $vettingAttribute['spouse_firstname'] = $vettingInfo->spouse_firstname;
        $vettingAttribute['spouse_surname'] = $vettingInfo->spouse_surname;
        $vettingAttribute['spouse_id_number'] = $vettingInfo->spouse_id_number;
        $vettingAttribute['extra_owner_title1'] = $vettingInfo->extra_owner_title1;
        $vettingAttribute['extra_owner_firstname1'] = $vettingInfo->extra_owner_firstname1;
        $vettingAttribute['extra_owner_surname1'] = $vettingInfo->extra_owner_surname1;
        $vettingAttribute['extra_owner_title2'] = $vettingInfo->extra_owner_title2;
        $vettingAttribute['extra_owner_firstname2'] = $vettingInfo->extra_owner_firstname2;
        $vettingAttribute['extra_owner_surname2'] = $vettingInfo->extra_owner_surname2;
        $vettingAttribute['extra_owner_title3'] = $vettingInfo->extra_owner_title3;
        $vettingAttribute['extra_owner_firstname3'] = $vettingInfo->extra_owner_firstname3;
        $vettingAttribute['extra_owner_surname3'] = $vettingInfo->extra_owner_surname3;

        $vetting = new Vetting();
        $vetting->fill($vettingAttribute)->save();

        switch ($propertyAttribute['property_type']) {
            case 'Commercial':
                $comAttribute = [];
                $comAttribute['commercial_type'] = $clonedProperty->commercial->commercial_type;
                $comAttribute['parking'] = $clonedProperty->commercial->parking;
                $comAttribute['property_id'] = $propertyId;
                $commercial = new Commercial();
                $commercial->fill($comAttribute)->save();

                break;
            case 'Residential':
                $resAttribute['dwelling_type'] = $clonedProperty->residential->dwelling_type;
                $resAttribute['bedrooms'] = $clonedProperty->residential->bedrooms;
                $resAttribute['bathrooms'] = $clonedProperty->residential->bathrooms;
                $resAttribute['parking'] = $clonedProperty->residential->parking;
                $resAttribute['property_id'] = $propertyId;
                $residential = new Residential();
                $residential->fill($resAttribute)->save();

                break;
            case 'Farm':
                $farmAttribute = [];
                $farmAttribute['farm_type'] = $clonedProperty->farm->farm_type;
                $farmAttribute['property_id'] = $propertyId;
                $farm = new Farm();
                $farm->fill($farmAttribute)->save();

                break;
        }

        return redirect()->route('admin.property.edit', ['id' => $propertyId]);
    }

    public function searchBySuburb()
    {
        $input = Request::all();

        if (isset($input['suburbs'])) {
            if (count($input['suburbs']) > 1) {
                $suburb = http_build_query($input['suburbs']);
            } elseif (count($input['suburbs']) == 1) {
                $suburb = $input['suburbs'][0];
            }
        } else {
            Flash::message('Please select at least one suburb for your search');

            return redirect()->back();
        }

        $province = $this->getProvincebyCityAndSuburb($input['city'], $input['suburbs'][0]);

        $slug = new Slugify();

        $province = $slug->slugify($province);

        return redirect()->route('property.index', ['property', 'all-listings', $province, $input['city'], $suburb, 'default',
            'grid_view', ]);
    }

    public function getProvincebyCityAndSuburb($city, $suburb)
    {
        $province = DB::select('SELECT a.province FROM areas a WHERE a.area_slug = "'.$city.'" AND a.suburb_town_city_slug =
        "'.$suburb.'" LIMIT 1');

        return $province[0]->province;
    }

    public function adminPropertyAdvert($id)
    {
        //region advertising board data save
        $advert_boards['property_id'] = $id;
        $advert_boards['Property_Board_1000_1250_Quantity'] = Request::get('property_board1_quantity');
        $advert_boards['Property_Board_1000_1250_Price'] = Request::get('property_board1_price');
        $advert_boards['Property_Board_2000_800_Quantity'] = Request::get('property_board2_quantity');
        $advert_boards['Property_Board_2000_800_Price'] = Request::get('property_board2_price');
        $advert_boards['Lamppost_Board_Quantity'] = Request::get('lamppost_board_quantity');
        $advert_boards['Lamppost_Board_Price'] = Request::get('lamppost_board_price');
        $advert_boards['Door_Board_Quantity'] = Request::get('door_board_quantity');
        $advert_boards['Door_Board_Price'] = Request::get('door_board_price');
        $advert_boards['Large_Board_Quantity'] = Request::get('large_board_quantity');
        $advert_boards['Large_Board_Price'] = Request::get('large_board_price');
        $advert_boards['Banner_Quantity'] = Request::get('banner_quantity');
        $advert_boards['Banner_Price'] = Request::get('banner_price');

        if (empty($property_advert_boards = $this->property_advert_boards->whereProperty_id($id)->first())) {
            $this->property_advert_boards->fill($advert_boards)->save();
//            $property_advert_boards = $this->property_advert_boards->whereProperty_id($id)->first();
        } else {
            $property_advert_boards->fill($advert_boards)->update();
        }
        //endregion

        //region advertising print data save
        $advert_prints['property_id'] = $id;
        $advert_prints['print_1_name'] = Request::get('print_media1_name');
        $advert_prints['print_1_inserts'] = Request::get('print_media1_inserts');
        $advert_prints['print_1_size'] = Request::get('print_media1_size');
        $advert_prints['print_1_price'] = Request::get('print_media1_price');
        $advert_prints['print_2_name'] = Request::get('print_media2_name');
        $advert_prints['print_2_inserts'] = Request::get('print_media2_inserts');
        $advert_prints['print_2_size'] = Request::get('print_media2_size');
        $advert_prints['print_2_price'] = Request::get('print_media2_price');
        $advert_prints['print_3_name'] = Request::get('print_media3_name');
        $advert_prints['print_3_inserts'] = Request::get('print_media3_inserts');
        $advert_prints['print_3_size'] = Request::get('print_media3_size');
        $advert_prints['print_3_price'] = Request::get('print_media3_price');
        $advert_prints['print_4_name'] = Request::get('print_media4_name');
        $advert_prints['print_4_inserts'] = Request::get('print_media4_inserts');
        $advert_prints['print_4_size'] = Request::get('print_media4_size');
        $advert_prints['print_4_price'] = Request::get('print_media4_price');
        $advert_prints['print_5_name'] = Request::get('print_media5_name');
        $advert_prints['print_5_inserts'] = Request::get('print_media5_inserts');
        $advert_prints['print_5_size'] = Request::get('print_media5_size');
        $advert_prints['print_5_price'] = Request::get('print_media5_price');
        $advert_prints['print_6_name'] = Request::get('print_media6_name');
        $advert_prints['print_6_inserts'] = Request::get('print_media6_inserts');
        $advert_prints['print_6_size'] = Request::get('print_media6_size');
        $advert_prints['print_6_price'] = Request::get('print_media6_price');

        if (empty($property_advert_prints = $this->property_advert_prints->whereProperty_id($id)->first())) {
            $this->property_advert_prints->fill($advert_prints)->save();
        } else {
            $property_advert_prints->fill($advert_prints)->update();
        }
        //endregion

        //region advertising social media data save
        $advert_social_media['property_id'] = $id;
        $advert_social_media['portal_syndication'] = Request::get('portal_syndication');
        $advert_social_media['twitter_adverts'] = Request::get('twitter_adverts');
        $advert_social_media['tele_marketing'] = Request::get('tele_marketing');
        $advert_social_media['blog_post'] = Request::get('blog_posting');
        $advert_social_media['google_post'] = Request::get('google_posting');
        $advert_social_media['google_adwords'] = Request::get('google_adwords');
        $advert_social_media['facebook_adverts'] = Request::get('facebook_adverts');
        $advert_social_media['linkedin_post'] = Request::get('linkedin_posting');

        if (empty($property_advert_social_media = $this->property_advert_social_media->whereProperty_id($id)->first())) {
            $this->property_advert_social_media->fill($advert_social_media)->save();
        } else {
            $property_advert_social_media->fill($advert_social_media)->update();
        }
        //endregion social media save

        //region advertising photography data save

        $advert_photo_data['property_id'] = $id;
        $advert_photo_data['ground_aerial_video'] = Request::get('ground_aerial_video');
        $advert_photo_data['ground_photo_tour'] = Request::get('ground_video');
        $advert_photo_data['ground_photo'] = Request::get('ground');
        $advert_photo_data['other_photo_descrip'] = Request::get('other_photo_descrip');
        $advert_photo_data['other_price'] = Request::get('other_price');

        if (empty($advert_photos = $this->advert_photos->whereProperty_id($id)->first())) {
            $this->advert_photos->fill($advert_photo_data)->save();
        } else {
            $advert_photos->fill($advert_photo_data)->update();
        }

        //endregion photography save

        Flash::message('Property advertising information has been saved.');

        return redirect()->route('admin.property.edit', ['id' => $id]);
    }

    public function adminPropertyCheck($id)
    {
        parse_str(Request::get('checklist_inputs'), $input);

        $are_input['property_id'] = $id;
        $are_input['asset_erf'] = $input['asset_erf'];
        if (isset($input['vat_registered'])) {
            $are_input['vat_registered'] = 1;
        } else {
            $are_input['vat_registered'] = 0;
        }
        $are_input['vat_num'] = $input['vat_num'];
        $are_input['asset_gross'] = str_replace(' ', '', $input['asset_gross']);

        if (empty($asset_real_estate = $this->asset_real_estate->whereProperty_id($id)->first())) {
            $this->asset_real_estate->fill($are_input)->save();
            $asset_real_estate = $this->asset_real_estate->whereProperty_id($id)->first();
        } else {
            $asset_real_estate->fill($are_input)->update();
        }

        $checklist_input['property_id'] = $id;
        if (isset($input['deeds_office_property'])) {
            $checklist_input['deeds_office_property'] = 1;
        } else {
            $checklist_input['deeds_office_property'] = 0;
        }
        if (isset($input['cipc_search'])) {
            $checklist_input['cipc_search'] = 1;
        } else {
            $checklist_input['cipc_search'] = 0;
        }
        if (isset($input['deeds_office_person'])) {
            $checklist_input['deeds_office_person'] = 1;
        } else {
            $checklist_input['deeds_office_person'] = 0;
        }
        if (isset($input['windeed_property_report'])) {
            $checklist_input['windeed_property_report'] = 1;
        } else {
            $checklist_input['windeed_property_report'] = 0;
        }
        $checklist_input['doprop_date'] = $input['doprop_date'];
        $checklist_input['cipc_date'] = $input['cipc_date'];
        $checklist_input['doper_date'] = $input['doper_date'];
        $checklist_input['wpr_date'] = $input['wpr_date'];

        if (isset($input['google_images'])) {
            $checklist_input['google_images'] = 1;
        } else {
            $checklist_input['google_images'] = 0;
        }
        if (isset($input['gis'])) {
            $checklist_input['gis'] = 1;
        } else {
            $checklist_input['gis'] = 0;
        }
        if (isset($input['signed_mandate'])) {
            $checklist_input['signed_mandate'] = 1;
        } else {
            $checklist_input['signed_mandate'] = 0;
        }
        if (isset($input['advert_template'])) {
            $checklist_input['advert_template'] = 1;
        } else {
            $checklist_input['advert_template'] = 0;
        }

        if (isset($input['referral'])) {
            $checklist_input['referral'] = 1;
        } else {
            $checklist_input['referral'] = 0;
        }
        if (isset($input['referral_form'])) {
            $checklist_input['referral_form'] = 1;
        } else {
            $checklist_input['referral_form'] = 0;
        }
        if (isset($input['viewing_date_check'])) {
            $checklist_input['viewing_date_check'] = 1;
        } else {
            $checklist_input['viewing_date_check'] = 0;
        }
        if (isset($input['intro_email'])) {
            $checklist_input['intro_email'] = 1;
        } else {
            $checklist_input['intro_email'] = 0;
        }
        $checklist_input['intro_email_date'] = $input['intro_email_date'];
        if (isset($input['vat_status_from_sars'])) {
            $checklist_input['vat_status_from_sars'] = 1;
        } else {
            $checklist_input['vat_status_from_sars'] = 0;
        }
        if (isset($input['vat_certificate'])) {
            $checklist_input['vat_certificate'] = 1;
        } else {
            $checklist_input['vat_certificate'] = 0;
        }
        if (isset($input['title_deed'])) {
            $checklist_input['title_deed'] = 1;
        } else {
            $checklist_input['title_deed'] = 0;
        }
        $checklist_input['title_deed_date'] = date('Y-m-d', strtotime($input['title_deed_date']));
        $checklist_input['no_title_reason'] = $input['no_title_reason'];
        if (isset($input['sg_diagram'])) {
            $checklist_input['sg_diagram'] = 1;
        } else {
            $checklist_input['sg_diagram'] = 0;
        }
        $checklist_input['sg_diagram_date'] = date('Y-m-d', strtotime($input['sg_diagram_date']));
        $checklist_input['arrears'] = str_replace(' ', '', $input['arrears']);
        if (isset($input['water_and_elec'])) {
            $checklist_input['water_and_elec'] = 1;
        } else {
            $checklist_input['water_and_elec'] = 0;
        }
        if (isset($input['individual_elec'])) {
            $checklist_input['individual_elec'] = 1;
        } else {
            $checklist_input['individual_elec'] = 0;
        }
        $checklist_input['who_pays_elec'] = $input['who_pays_elec'];

        if (isset($input['zoning_cert'])) {
            $checklist_input['zoning_cert'] = 1;
        } else {
            $checklist_input['zoning_cert'] = 0;
        }
        if (isset($input['building_plans'])) {
            $checklist_input['building_plans'] = 1;
        } else {
            $checklist_input['building_plans'] = 0;
        }
        if (isset($input['land_claims'])) {
            $checklist_input['land_claims'] = 1;
        } else {
            $checklist_input['land_claims'] = 0;
        }
        if (isset($input['liquor_licence'])) {
            $checklist_input['liquor_licence'] = 1;
        } else {
            $checklist_input['liquor_licence'] = 0;
        }
        if (isset($input['vacant'])) {
            $checklist_input['vacant'] = 1;
        } else {
            $checklist_input['vacant'] = 0;
        }
        if (isset($input['lease_agreements'])) {
            $checklist_input['lease_agreements'] = 1;
        } else {
            $checklist_input['lease_agreements'] = 0;
        }
        if (isset($input['rental_schedule'])) {
            $checklist_input['rental_schedule'] = 1;
        } else {
            $checklist_input['rental_schedule'] = 0;
        }
        if (isset($input['rental_invoices'])) {
            $checklist_input['rental_invoices'] = 1;
        } else {
            $checklist_input['rental_invoices'] = 0;
        }
        if (isset($input['first_right_refusal'])) {
            $checklist_input['first_right_refusal'] = 1;
        } else {
            $checklist_input['first_right_refusal'] = 0;
        }
        if (isset($input['with_vacant_occupation'])) {
            $checklist_input['with_vacant_occupation'] = 1;
        } else {
            $checklist_input['with_vacant_occupation'] = 0;
        }
        if (isset($input['movables'])) {
            $checklist_input['movables'] = 1;
        } else {
            $checklist_input['movables'] = 0;
        }
        if (isset($input['employees'])) {
            $checklist_input['employees'] = 1;
        } else {
            $checklist_input['employees'] = 0;
        }
        if (isset($input['livestock'])) {
            $checklist_input['livestock'] = 1;
        } else {
            $checklist_input['livestock'] = 0;
        }
        if (isset($input['inventory'])) {
            $checklist_input['inventory'] = 1;
        } else {
            $checklist_input['inventory'] = 0;
        }
        if (isset($input['interdict'])) {
            $checklist_input['interdict'] = 1;
        } else {
            $checklist_input['interdict'] = 0;
        }
        if (isset($input['servitudes'])) {
            $checklist_input['servitudes'] = 1;
        } else {
            $checklist_input['servitudes'] = 0;
        }
        if (isset($input['dmoss'])) {
            $checklist_input['dmoss'] = 1;
        } else {
            $checklist_input['dmoss'] = 0;
        }
        if (isset($input['encroachments'])) {
            $checklist_input['encroachments'] = 1;
        } else {
            $checklist_input['encroachments'] = 0;
        }
        if (isset($input['expropriations'])) {
            $checklist_input['expropriations'] = 1;
        } else {
            $checklist_input['expropriations'] = 0;
        }
        if (isset($input['attachments'])) {
            $checklist_input['attachments'] = 1;
        } else {
            $checklist_input['attachments'] = 0;
        }
        if (isset($input['listed_building'])) {
            $checklist_input['listed_building'] = 1;
        } else {
            $checklist_input['listed_building'] = 0;
        }
        $checklist_input['inspected_by'] = $input['inspected_by'];
        $checklist_input['inspection_date'] = date('Y-m-d', strtotime($input['inspection_date']));
        $checklist_input['defects'] = $input['defects'];
        if (isset($input['trans_att_details'])) {
            $checklist_input['trans_att_details'] = 1;
        } else {
            $checklist_input['trans_att_details'] = 0;
        }
        if (isset($input['accountant_auditors_details'])) {
            $checklist_input['accountant_auditors_details'] = 1;
        } else {
            $checklist_input['accountant_auditors_details'] = 0;
        }
        if (isset($input['entity'])) {
            $checklist_input['entity'] = 1;
        } else {
            $checklist_input['entity'] = 0;
        }
        if (isset($input['persons'])) {
            $checklist_input['persons'] = 1;
        } else {
            $checklist_input['persons'] = 0;
        }
        if (isset($input['citizen'])) {
            $checklist_input['citizen'] = 1;
        } else {
            $checklist_input['citizen'] = 0;
        }
        $checklist_input['letter_sent_date'] = date('Y-m-d', strtotime($input['letter_sent_date']));
        if (isset($input['update_letter'])) {
            $checklist_input['update_letter'] = 1;
        } else {
            $checklist_input['update_letter'] = 0;
        }
        if (isset($input['advertising_costs'])) {
            $checklist_input['advertising_costs'] = 1;
        } else {
            $checklist_input['advertising_costs'] = 0;
        }
        if (isset($input['advert_paid'])) {
            $checklist_input['advert_paid'] = 1;
        } else {
            $checklist_input['advert_paid'] = 0;
        }
        $checklist_input['advert_cost'] = $input['advert_cost'];
        if (isset($input['advert_paid_on_success'])) {
            $checklist_input['advert_paid_on_success'] = 1;
        } else {
            $checklist_input['advert_paid_on_success'] = 0;
        }
        if (isset($input['section_112'])) {
            $checklist_input['section_112'] = 1;
        } else {
            $checklist_input['section_112'] = 0;
        }
        if (isset($input['adverts_for_file'])) {
            $checklist_input['adverts_for_file'] = 1;
        } else {
            $checklist_input['adverts_for_file'] = 0;
        }

        $checklist_input['advert_comment'] = $input['advert_comment'];

        if (empty($checklist = $this->checklist->whereProperty_id($id)->first())) {
            $this->checklist->fill($checklist_input)->save();
            $checklist = $this->checklist->whereProperty_id($id)->first();
        } else {
            $checklist->fill($checklist_input)->update();
        }

        $property_file_input['property_id'] = $id;
        if (isset($input['same_as_address'])) {
            $property_file_input['same_as_address'] = 1;
        } else {
            $property_file_input['same_as_address'] = 0;
        }

        $property_file_input['file_number'] = $input['file_number'];
        $property_file_input['matter'] = $input['matter'];
        $property_file_input['mandate_value'] = str_replace(' ', '', $input['mandate_value']);
        $property_file_input['auction_date'] = date('Y-m-d', strtotime($input['auction_date']));
        $property_file_input['viewing_date'] = date('Y-m-d', strtotime($input['viewing_date']));
        $property_file_input['asset_id'] = $asset_real_estate->id;
        $property_file_input['checklist_id'] = $checklist->id;

        if (empty($property_file = $this->property_file->whereProperty_id($id)->first())) {
            $this->property_file->fill($property_file_input)->save();
        } else {
            $property_file->fill($property_file_input)->update();
        }

        $numeric_features = App::make(\App\Http\Controllers\AttributesController::class)->getAttributesByType(2);
        foreach ($numeric_features as $numeric_feature) {
            if (Request::get($numeric_feature->name) > 0 && ! $this->property->hasAttribute($numeric_feature->name)) {
                if ($numeric_feature->name == 'land_size') {
                    $this->property->assignAttribute($numeric_feature->id, $input['land_size']);
                } elseif ($numeric_feature->name == 'rates' || $numeric_feature->name == 'levies') {
                    $this->property->assignAttribute($numeric_feature->id, str_replace(' ', '', Request::get($numeric_feature->name)));
                } else {
                    $this->property->assignAttribute($numeric_feature->id, Request::get($numeric_feature->name));
                }
            } elseif (Request::get($numeric_feature->name) > 0 && $this->property->hasAttribute($numeric_feature->name)) {
                if ($numeric_feature->name == 'land_size') {
                    $this->property->ammendAttribute($numeric_feature->id, $this->property->id, $input['land_size']);
                } elseif ($numeric_feature->name == 'rates' || $numeric_feature->name == 'levies') {
                    $this->property->assignAttribute($numeric_feature->id, str_replace(' ', '', Request::get($numeric_feature->name)));
                } else {
                    $this->property->ammendAttribute($numeric_feature->id, $id, Request::get($numeric_feature->name));
                }
            } elseif (! Request::get($numeric_feature->name) > 0 && $this->property->hasAttribute($numeric_feature->name)) {
                $this->property->removeAttribute($numeric_feature->name);
            }
        }

        echo 'Done';
    }

    public function adminPrint_file($id)
    {
        $property_file = DB::select('SELECT *, zt.type AS zone_type, are.vat_num AS vat_number FROM property_file pf
                                      LEFT JOIN asset_real_estate are ON pf.asset_id = are.id
                                      LEFT JOIN checklist c ON pf.checklist_id = c.id
                                      LEFT JOIN property p ON pf.property_id = p.id
                                      LEFT JOIN vetting v ON pf.property_id = v.property_id
                                      LEFT JOIN zone_types zt ON p.zoning = zt.id
                                      WHERE pf.property_id = '.$id.' LIMIT 1');

        $property = $this->property->whereId($id)->first();

        if ($rates_value = $property->getFeatureValue('rates', $id)) {
            $rates = $rates_value[0]->value;
        } else {
            $rates = '';
        }
        if ($levies_value = $property->getFeatureValue('levies', $id)) {
            $levies = $levies_value[0]->value;
        } else {
            $levies = '';
        }
        if ($gla_value = $property->getFeatureValue('gla', $id)) {
            $gla = $gla_value[0]->value;
        } else {
            $gla = '';
        }

        if ($land_measurement_value = $property->getFeatureValue('land_measurement', $id)) {
            $land_measurement = $land_measurement_value[0]->value;
        } else {
            $land_measurement = '';
        }

        if ($land_size_value = $property->getFeatureValue('land_size', $id)) {
            $land_size = $land_size_value[0]->value;
            if ($land_measurement == 'HA') {
                $land_size /= 10000;
            }
        } else {
            $land_size = '';
        }
        $owner_ids = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id WHERE
                              t.name = 'OWNER' AND pu.property_id = ".$id.' LIMIT 3');
        $owners = null;
        if (! empty($owner_ids)) {
            foreach ($owner_ids as $owner_id) {
                $owners[] = User::find($owner_id->user_id);
            }
        }
        $seller_ids = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id WHERE
                                t.name = 'SELLER' AND pu.property_id = ".$id.' LIMIT 3');

        $sellers = null;
        if (! empty($seller_ids)) {
            $hasSeller = true;
            foreach ($seller_ids as $seller_id) {
                $seller[] = User::find($seller_id->user_id);
            }
        } else {
            $hasSeller = false;
        }

        $managing_agent_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'MANAGING AGENT' AND pu.property_id = ".$id);
        if (! empty($managing_agent_id)) {
            $managing_agent = User::find($managing_agent_id[0]->user_id);
        } else {
            $managing_agent = new User;
        }

        $attorney_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'ATTORNEY' AND pu.property_id = ".$id);
        if (! empty($attorney_id)) {
            $attorney = User::find($attorney_id[0]->user_id);
        } else {
            $attorney = new User;
        }

        if ($property_file[0]->title_deed_date == '0000-00-00') {
            $property_file[0]->title_deed_date = '';
        }

        if ($property_file[0]->sg_diagram_date == '0000-00-00') {
            $property_file[0]->sg_diagram_date = '';
        }

        if ($property_file[0]->inspection_date == '0000-00-00') {
            $property_file[0]->inspection_date = '';
        }

        if ($property_file[0]->auction_date == '0000-00-00') {
            $property_file[0]->auction_date = '';
        }

        if ($property_file[0]->viewing_date == '0000-00-00') {
            $property_file[0]->viewing_date = '';
        }

        if ($property_file[0]->letter_sent_date == '0000-00-00') {
            $property_file[0]->letter_sent_date = '';
        }

        return view('property.print.file_check', ['property_file' => $property_file, 'rates' => $rates,
            'levies' => $levies, 'owners' => $owners, 'property' => $property, 'managing_agent' => $managing_agent,
            'sellers' => $sellers, 'hasSeller' => $hasSeller, 'attorney' => $attorney, 'gla' => $gla,
            'land_size' => $land_size, 'land_measurement' => $land_measurement, ]);
    }

    public function newProperty_file($id)
    {
        $property_file = DB::select('SELECT p.reference, s.*, are.*, ta.*, c.*  FROM property_file pf
                                      LEFT JOIN seller s ON pf.seller_id = s.id
                                      LEFT JOIN transfer_attorney ta ON pf.attorney_id = ta.id
                                      LEFT JOIN asset_real_estate are ON pf.asset_id = are.id
                                      LEFT JOIN checklist c ON pf.checklist_id = c.id
                                      LEFT JOIN property p ON pf.property_id = p.id
                                      LEFT JOIN vetting v ON pf.property_id = v.property_id
                                      WHERE pf.property_id = '.$id.' LIMIT 1');
        $property_file_array = json_decode(json_encode($property_file), true);
        $property_file_array_actual = $property_file_array[0];
        //owner_is_seller_check
        //complex_number etc
        $property = $this->property->whereId($id)->first();

        if ($rates_value = $property->getFeatureValue('rates', $id)) {
            $property_file_array_actual['rates'] = $rates_value[0]->value;
        } else {
            $property_file_array_actual['rates'] = '';
        }
        if ($levies_value = $property->getFeatureValue('levies', $id)) {
            $property_file_array_actual['levies'] = $levies_value[0]->value;
        } else {
            $property_file_array_actual['levies'] = '';
        }

        $property_file_array_actual = array_diff_key($property_file_array_actual, ['seller_id' => 'xy', 'attorney_id' => 'xy',
            'asset_id' => 'xy', 'checklist_id' => 'xy', 'owner_is_seller_check' => 'xy', ]);
        $this->closed_file->fill($property_file_array_actual)->save();

        Flash::message('File has been stored you may now enter info for new file');

        return redirect()->back();
    }

    public function adminAuctioneerNotes($id)
    {
        parse_str(Request::get('auctioneer_inputs'), $input);

        $input['combined_annual'] = str_replace(' ', '', $input['combined_annual']);
        $input['gross_annual'] = str_replace(' ', '', $input['gross_annual']);
        $input['combined_annual'] = str_replace(' ', '', $input['combined_annual']);
        $input['kick_off'] = str_replace(' ', '', $input['kick_off']);
        $input['vendor'] = str_replace(' ', '', $input['vendor']);
        $input['aim'] = str_replace(' ', '', $input['aim']);

        if (isset($input['seller_comm_highlight'])) {
            $highlight_list['seller_comm_highlight'] = 1;
            unset($input['seller_comm_highlight']);
        } else {
            $highlight_list['seller_comm_highlight'] = 0;
        }
        if (isset($input['buyers_comm_highlight'])) {
            $highlight_list['buyers_comm_highlight'] = 1;
            unset($input['buyers_comm_highlight']);
        } else {
            $highlight_list['buyers_comm_highlight'] = 0;
        }
        if (isset($input['deposit_highlight'])) {
            $highlight_list['deposit_highlight'] = 1;
            unset($input['deposit_highlight']);
        } else {
            $highlight_list['deposit_highlight'] = 0;
        }
        if (isset($input['vat_payable_highlight'])) {
            $highlight_list['vat_payable_highlight'] = 1;
            unset($input['vat_payable_highlight']);
        } else {
            $highlight_list['vat_payable_highlight'] = 0;
        }
        if (isset($input['confirm_date_highlight'])) {
            $highlight_list['confirm_date_highlight'] = 1;
            unset($input['confirm_date_highlight']);
        } else {
            $highlight_list['confirm_date_highlight'] = 0;
        }
        if (isset($input['possession_highlight'])) {
            $highlight_list['possession_highlight'] = 1;
            unset($input['possession_highlight']);
        } else {
            $highlight_list['possession_highlight'] = 0;
        }
        if (isset($input['occ_interest_highlight'])) {
            $highlight_list['occ_interest_highlight'] = 1;
            unset($input['occ_interest_highlight']);
        } else {
            $highlight_list['occ_interest_highlight'] = 0;
        }
        if (isset($input['extent_highlight'])) {
            $highlight_list['extent_highlight'] = 1;
            unset($input['extent_highlight']);
        } else {
            $highlight_list['extent_highlight'] = 0;
        }
        if (isset($input['gba_highlight'])) {
            $highlight_list['gba_highlight'] = 1;
            unset($input['gba_highlight']);
        } else {
            $highlight_list['gba_highlight'] = 0;
        }
        if (isset($input['zoning_highlight'])) {
            $highlight_list['zoning_highlight'] = 1;
            unset($input['zoning_highlight']);
        } else {
            $highlight_list['zoning_highlight'] = 0;
        }
        if (isset($input['combined_annual_highlight'])) {
            $highlight_list['combined_annual_highlight'] = 1;
            unset($input['combined_annual_highlight']);
        } else {
            $highlight_list['combined_annual_highlight'] = 0;
        }
        if (isset($input['gross_annual_highlight'])) {
            $highlight_list['gross_annual_highlight'] = 1;
            unset($input['gross_annual_highlight']);
        } else {
            $highlight_list['gross_annual_highlight'] = 0;
        }

        if (isset($input['monthly_levy_highlight'])) {
            $highlight_list['monthly_levy_highlight'] = 1;
            unset($input['monthly_levy_highlight']);
        } else {
            $highlight_list['monthly_levy_highlight'] = 0;
        }
        if (isset($input['yield_highlight'])) {
            $highlight_list['yield_highlight'] = 1;
            unset($input['yield_highlight']);
        } else {
            $highlight_list['yield_highlight'] = 0;
        }
        if (isset($input['excl_comm_highlight'])) {
            $highlight_list['excl_comm_highlight'] = 1;
            unset($input['excl_comm_highlight']);
        } else {
            $highlight_list['excl_comm_highlight'] = 0;
        }
        if (isset($input['incl_comm_highlight'])) {
            $highlight_list['incl_comm_highlight'] = 1;
            unset($input['incl_comm_highlight']);
        } else {
            $highlight_list['incl_comm_highlight'] = 0;
        }

        if ($this->highlight = $this->highlight->whereProperty_id($id)->first()) {
            $this->highlight->fill($highlight_list)->update();
        } else {
            $highlight_list['property_id'] = $id;
            $highlight = new Highlight();
            $highlight->fill($highlight_list)->save();
        }

        $rule_changes = DB::table('auction_rule_change')->whereProperty_id($id)->get();

        $count = 0;
        $rule_change_ids = [];
        foreach ($rule_changes as $rule_change) {
            $rule_change_ids[$count] = $rule_change->id;
            $count++;
        }

        $count = 0;
        if (isset($input['version_comment'])) {
            foreach ($input['version_comment'] as $comment) {
                DB::table('auction_rule_change')->whereId($rule_change_ids[$count])->update(['comment' => $comment]);
                $count++;
            }
            unset($input['version_comment']);
        }

        if ($auctioneer_notes = $this->auctioneer_notes->whereProperty_id($id)->first()) {
            $auctioneer_notes->fill($input)->update();
        } else {
            $input['property_id'] = $id;
            $this->auctioneer_notes->fill($input)->save();
        }

        return 'Done';
    }

    public function adminPrint_auctioneer($id)
    {
        $property = $this->property->whereId($id)->first();
        if (is_null($property)) {
            return redirect()->route('property.index');
        }

        switch ($property['property_type']):
            case 'Residential':
                $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        case 'Commercial':
                $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        case 'Farm':
                $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        default:
                return redirect()->back();

        break;
        endswitch;

        $owner_id = DB::select("SELECT user_id FROM property_user pu INNER JOIN tags t ON pu.role_id = t.id
                      WHERE t.name = 'OWNER' AND pu.property_id = ".$id);
        if (! empty($owner_id)) {
            $owner = User::find($owner_id[0]->user_id);
        } else {
            $owner = new User();
        }

        $property_file = $this->property_file->whereProperty_id($id)->first();

        $auctioneer_notes = $this->auctioneer_notes->whereProperty_id($id)->first();

        $erected_on = preg_split('/\\r\\n|\\r|\\n/', $auctioneer_notes->erected_on);

        $marketing_amendments = preg_split('/\\r\\n|\\r|\\n/', $auctioneer_notes->marketing_amend);

        $zone_types = DB::table('zone_types')->whereActive(1)->pluck('type', 'id');

        $newid = $id;
        if ($property->listing_type = 'weauction') {
            $newid = $id.'A';
        }

        $rules_of_auction = DB::table('auction_rule_change')->whereProperty_id($newid)->get();

        $highlights = $this->highlight->whereProperty_id($id)->first();

        return view('property.print.auctioneer_notes', ['property' => $property, 'owner' => $owner,
            'property_file' => $property_file, 'auctioneer_notes' => $auctioneer_notes, 'erected_on' => $erected_on,
            'zone_types' => $zone_types, 'rules_of_auction' => $rules_of_auction,
            'marketing_amendments' => $marketing_amendments, 'highlights' => $highlights, ]);
    }

    public function getPropertyEnquiries($property_id)
    {
        $query = 'SELECT u.id AS user_id, l.id AS lead_id, l.reference AS lead_reference, t.name, u.firstname, u.lastname, l.created_at
                      FROM leads l
                      INNER JOIN lead_user lu ON l.id = lu.lead_id
                      INNER JOIN tags t ON t.id = lu.role_id
                      LEFT JOIN users u ON u.id = lu.user_id
                      WHERE l.property_id = '.$property_id;

        $users = DB::select($query);

        foreach ($users as $user) {
            $note_query = 'SELECT pn.note FROM prop_notes pn WHERE pn.lead_id = '.$user->lead_id.'
            ORDER BY pn.id DESC LIMIT 1';
            $note = DB::select($note_query);
            if (isset($note[0]->note)) {
                $user->note = $note[0]->note;
            } else {
                $user->note = 'No Notes';
            }

            $user->lead_reference = "<a href='/leads/".$user->lead_id."/edit'>".$user->lead_reference.'</a>';

            $user->user = "<a href='/admin/users/edit/".$user->user_id."'>".$user->firstname.' '.$user->lastname.'</a>';
        }

        return Datatables::of($users)
            ->rawColumns(['lead_reference', 'user', 'actions'])
            ->make(true);
    }

    public function getBDMComm()
    {
        $id = Request::get('property_id');

        $commissions = $this->commission->whereLead_id($id)->whereType(1)->get();
        $total_comm = 0;
        foreach ($commissions as $commission) {
            $total_comm += $commission->commission;
        }

        return $total_comm;
    }

    public function adminComm()
    {
        $input = Request::all();

        $this->property = $this->property->whereId($input['property_id'])->first();

        $this->property->final_price = str_replace(' ', '', $input['final_price']);
        $this->property->sale_date = date('Y-m-d', strtotime($input['sale_date']));

        $this->property->update();

        $gross_type = DB::select("SELECT id FROM commission_types WHERE type = 'Gross'");
        $sla_type = DB::select("SELECT id FROM commission_types WHERE type = 'SLA'");
        $seller_type = DB::select("SELECT id FROM commission_types WHERE type = 'Seller'");
        $absorb_cost_type = DB::select("SELECT id FROM commission_types WHERE type = 'Absorbed Costs'");

        if ($input['gross_comm'] >= 0) {
            if (! $gross_id = $this->commission->whereLead_id($input['property_id'])->whereType($gross_type[0]->id)->first()) {
                DB::insert("INSERT INTO commissions (lead_id, commission, type, created_at, updated_at) VALUES
                ('".$input['property_id']."', '".str_replace(' ', '', $input['gross_comm'])."', '".$gross_type[0]->id."', NOW(), NOW())");
            } else {
                $gross_id->commission = str_replace(' ', '', $input['gross_comm']);
                $gross_id->updated_at = date('Y-m-d H:i:s');
                $gross_id->update();
            }
        }

        if ($input['sla_rebate'] > 0) {
            if (! $sla_id = $this->commission->whereLead_id($input['property_id'])->whereType($sla_type[0]->id)->first()) {
                DB::insert("INSERT INTO commissions (lead_id, name, commission, type, created_at, updated_at) VALUES
            ('".$input['property_id']."', '".$input['sla_name']."', '".str_replace(' ', '', $input['sla_rebate'])."','".
                    $sla_type[0]->id."', NOW(), NOW())");
            } else {
                $sla_id->commission = str_replace(' ', '', $input['sla_rebate']);
                $sla_id->name = $input['sla_name'];
                $sla_id->updated_at = date('Y-m-d H:i:s');
                $sla_id->update();
            }
        }

        if ($input['seller_rebate'] > 0) {
            if (! $seller_id = $this->commission->whereLead_id($input['property_id'])->whereType($seller_type[0]->id)->first()) {
                DB::insert("INSERT INTO commissions (lead_id, name, commission, type, created_at, updated_at) VALUES
            ('".$input['property_id']."', '".$input['seller_name']."', '".str_replace(' ', '', $input['seller_rebate'])."','".
                    $seller_type[0]->id."', NOW(), NOW())");
            } else {
                $seller_id->commission = str_replace(' ', '', $input['seller_rebate']);
                $seller_id->name = $input['seller_name'];
                $seller_id->updated_at = date('Y-m-d H:i:s');
                $seller_id->update();
            }
        }

        if ($input['absorb_cost'] > 0) {
            if (! $absorb_id = $this->commission->whereLead_id($input['property_id'])->whereType($absorb_cost_type[0]->id)->first()) {
                DB::insert("INSERT INTO commissions (lead_id, commission, type, created_at, updated_at) VALUES
            ('".$input['property_id']."', '".str_replace(' ', '', $input['absorb_cost'])."', '".$absorb_cost_type[0]->id."', NOW(), NOW())");
            } else {
                $absorb_id->commission = str_replace(' ', '', $input['absorb_cost']);
                $absorb_id->updated_at = date('Y-m-d H:i:s');
                $absorb_id->update();
            }
        }
    }

    public function commExcel()
    {
        $input = Request::all();

        if (! $auction = $this->auction->whereProperty_id($input['property_id'])->first()) {
            $properties = $this->property->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->whereProperty_id($input['property_id'])->get();
            $title = ' for property ref# '.$properties[0]->reference;
        } elseif ($auction->physical_auction != null) {
            $properties = $this->auction
                ->join('property', 'property.id', '=', 'auction.property_id')
                ->wherePhysical_auction($auction->physical_auction)
                ->withTrashed()
                ->get();
            $phys_auction = $this->physical_auction->whereId($auction->physical_auction)->first();
            $title = ' for '.$phys_auction->auction_name.' '.date('Y-m-d', strtotime($phys_auction->start_date));
        } else {
            $properties = $this->property->join('vetting', 'vetting.property_id', '=', 'property.id')
                ->whereProperty_id($input['property_id'])->get();
            $title = ' for property ref# '.$properties[0]->reference;
        }

        $count = 0;

        $commission = [];

        $propertyCount = count($properties);
        $purchase_price = 0;
        $gross_tot = 0;
        $rebates_tot = 0;
        $absorbed_cost_tot = 0;
        $nett_com_tot = 0;
        $bdm_comm_tot = 0;
        $after_bdm_tot = 0;

        foreach ($properties as $property) {
            $gross_type = DB::select("SELECT id FROM commission_types WHERE type = 'Gross'");
            $sla_type = DB::select("SELECT id FROM commission_types WHERE type = 'SLA'");
            $seller_type = DB::select("SELECT id FROM commission_types WHERE type = 'Seller'");
            $absorb_cost_type = DB::select("SELECT id FROM commission_types WHERE type = 'Absorbed Costs'");

            if ($property->final_price > 1) {
                $commission[$count]['Matter'] = $property->street_number.' '.$property->street_address;

                $commission[$count]['Purchase price'] = $property->final_price;
                $purchase_price += $property->final_price;
                if (! $gross = $this->commission->whereLead_id($property->property_id)->whereType($gross_type[0]->id)->first()) {
                    $commission[$count]['Gross'] = 0;
                } else {
                    $commission[$count]['Gross'] = $gross->commission;
                    $gross_tot += $commission[$count]['Gross'];
                }

                if (! $sla = $this->commission->whereLead_id($property->property_id)->whereType($sla_type[0]->id)->first()) {
                    $commission[$count]['Rebates/Referrals'] = 0;
                } else {
                    $commission[$count]['Rebates/Referrals'] = $sla->commission;
                    $rebates_tot += $commission[$count]['Rebates/Referrals'];
                }

                if (! $seller = $this->commission->whereLead_id($property->property_id)->whereType($seller_type[0]->id)->first()) {
                    $commission[$count]['Rebates/Referrals'] = 0;
                } else {
                    $commission[$count]['Rebates/Referrals'] = $seller->commission;
                    $rebates_tot += $commission[$count]['Rebates/Referrals'];
                }

                if (! $absorb_cost = $this->commission->whereLead_id($property->property_id)->whereType($absorb_cost_type[0]->id)->first()) {
                    $commission[$count]['Absorbed Costs'] = 0;
                } else {
                    $commission[$count]['Absorbed Costs'] = $absorb_cost->commission;
                    $absorbed_cost_tot += $commission[$count]['Absorbed Costs'];
                }

                $commission[$count]['Nett Commission'] = $commission[$count]['Gross'] - $commission[$count]['Rebates/Referrals'] - $commission[$count]['Absorbed Costs'];
                $nett_com_tot += $commission[$count]['Nett Commission'];

                $BDM_commissions = $this->commission->whereLead_id($property->property_id)->whereType(1)->get();

                $total_comm = 0;
                foreach ($BDM_commissions as $BDM_commission) {
                    $total_comm += $BDM_commission->commission;
                }

                $commission[$count]['BDM Commission'] = $commission[$count]['Nett Commission'] * $total_comm / 100;
                $bdm_comm_tot += $commission[$count]['BDM Commission'];

                $commission[$count]['Nett Comm after BDM Comm'] = $commission[$count]['Nett Commission'] - $commission[$count]['BDM Commission'];
                $after_bdm_tot += $commission[$count]['Nett Comm after BDM Comm'];
            }
            $count++;
        }

        $commission[$propertyCount]['Matter'] = 'Totals: ';
        $commission[$propertyCount]['Purchase price'] = $purchase_price;
        $commission[$propertyCount]['Purchase price'] = $purchase_price;
        $commission[$propertyCount]['Gross'] = $gross_tot;
        $commission[$propertyCount]['Rebates/Referrals'] = $rebates_tot;
        $commission[$propertyCount]['Absorbed Costs'] = $absorbed_cost_tot;
        $commission[$propertyCount]['Nett Commission'] = $nett_com_tot;
        $commission[$propertyCount]['BDM Commission'] = $bdm_comm_tot;
        $commission[$propertyCount]['Nett Comm after BDM Comm'] = $after_bdm_tot;

        Excel::create('Commission '.$title, function ($excel) use ($commission) {
            // Set the title
            $excel->setTitle('Commission Break Down');

            // Chain the setters
            $excel->setCreator('In2Assets')
                ->setCompany('In2Assets');

            // Call them separately
            $excel->setDescription('Commission Break Down');

            $excel->sheet('Sheetname', function ($sheet) use ($commission) {

                // Sheet manipulation
                $sheet->setOrientation('landscape');

                $sheet->fromArray($commission);
            });
        })->export('xls');

        return Redirect::back();
    }

    public function updateDigitalMarketing()
    {
        $selectQuery = 'SELECT * FROM social_advertising WHERE property_id = '.Request::get('property_id');

        $current_values = DB::select($selectQuery);

        $digi_date = date('Y-m-d', strtotime(Request::get('digital_date')));
        if ($digi_date == '1970-01-01') {
            $digi_date = 'null';
        }
        $upQuery = 'UPDATE social_advertising SET facebook = '.Request::get('facebook').', display = '
            .Request::get('display_val').', search = '.Request::get('search_val').', start_date = '.$digi_date.
            ' WHERE property_id = '.Request::get('property_id');

        DB::update($upQuery);

        $property = $this->property->whereId(Request::get('property_id'))->first();

        $socialAds = DB::select('SELECT * FROM social_advertising WHERE property_id = '.$property->id);

        $date_info = '';

        if ($socialAds[0]->start_date != null) {
            $date_info = '<p>This marketing campaign should begin on '.date('d F Y', strtotime($socialAds[0]->start_date)).'</p>';
        }

        $face_inc = Request::get('facebook') - $current_values[0]->facebook;
        $display_inc = Request::get('search_val') - $current_values[0]->search;
        $search_inc = Request::get('display_val') - $current_values[0]->display;

        //Notify concerned parties.
        $addressLine = 'Hi Kerrilee';
        $subject = 'Update to Digital marketing property ref '.$property->reference;
        $adminMail = 'kerrilees@brabys.co.za';
        $emailData = "
<p>Please note the increase spend on property ref number: $property->reference by the following amount</p>
                              <p><a href='https://www.in2assets.co.za/property/$property->id/$property->slug'>https://www.in2assets.co.za/property/$property->id/$property->slug</a></p>
						<ul>
						<li>Facebook amount: R".$face_inc.'</li>
						<li>Google display amount: R'.$display_inc.'</li>
						<li>Google search amount: R'.$search_inc."</li>
						</ul>
						$date_info
						<br>
						<p>Kind regards</p>
						<p>The In2assets team</p>";

        $data = ['name' => $addressLine, 'subject' => $subject, 'messages' => $emailData];
        // Queue email
        Mail::queue('emails.registration.confirm', $data, function ($message) use ($adminMail, $addressLine, $subject) {
            $message->to($adminMail, $addressLine)
                ->cc('neeraj@in2assets.com', 'Neeraj Ramautar')
                ->cc('rstenzhorn@in2assets.com', 'Rainer Stenzhorn')
                ->cc('revarshang@brabysmedia.com', 'Revarshan Govender')
                ->subject($subject);
        });

        $this->prop_notes->note = 'Digital marketing updated, Facebook amount: R'.$socialAds[0]->facebook.'
						Google display amount: R'.$socialAds[0]->display.'
						Google search amount: R'.$socialAds[0]->search;
        $this->prop_notes->property_id = $property->id;
        $this->prop_notes->users_id = Auth::user()->id;
        $this->prop_notes->save();
    }
}
