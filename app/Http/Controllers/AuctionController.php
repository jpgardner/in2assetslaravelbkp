<?php

namespace App\Http\Controllers;

use App\In2Assets\Users\User;
use App\Mail\newLead;
use App\Models\Auction;
use App\Models\Auctionbids;
use App\Models\AuctioneerNotes;
use App\Models\AuctionRegister;
use App\Models\EmailAlert;
use App\Models\PhysicalAuction;
use App\Models\PhysicalAuctionRegister;
use App\Models\Property;
use App\Models\Vetting;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;

class AuctionController extends Controller
{
    protected $auction;

    protected $auction_bids;

    public function __construct(Auction $auction, Property $property, Vetting $vetting, Auctionbids $auction_bids,
                                AuctionRegister $auction_register, PhysicalAuction $physical_auction,
                                PhysicalAuctionRegister $physical_auction_register, EmailAlert $email_alert,
                                AuctioneerNotes $auctioneer_notes)
    {
        $this->property = $property;
        $this->auction = $auction;
        $this->vetting = $vetting;
        $this->auction_bids = $auction_bids;
        $this->auction_register = $auction_register;
        $this->physical_auction = $physical_auction;
        $this->physical_auction_register = $physical_auction_register;
        $this->email_alert = $email_alert;
        $this->auctioneer_notes = $auctioneer_notes;
    }

    /**
     * Display a listing of the resource.
     * GET /auction.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /auction/create.
     *
     * @return Response
     */
    public function create($property_id)
    {
        $this->auction = $this->auction->wherePropertyId($property_id)->first();
        if (is_null($this->auction)) {
            $this->auction = new Auction();
            $this->auction->property_id = $property_id;
            $this->auction->status = 'created';
            $this->auction->save();
        } else {
            $this->auction->status = 'created';
            $this->auction->save();
        }

        if ($this->auction->auction_type == 5 && $this->auction->auction_name) {
            $property = $this->property->whereId($property_id)->first();
            $this->auction->auction_name = $property->street_number.' '.$property->street_name;
        }

        return redirect()->route('admin.auctions.setup', ['id' => $property_id]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /auction.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * @param  int $id
     * @return Response
     */
    public function getCurrentPrice($id)
    {
        //
        $this->property = $this->property->whereId($id)->first();

        return $this->property->price;
    }

    /**
     * Show the form for editing the specified resource.
     * GET /auction/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function showbids($id)
    {
        //
        $this->property = $this->property->join('auction', 'auction.property_id', '=', 'property.id')->where('property.id', '=', $id)->first();

        $auction = $this->auction->whereProperty_id($id)->first();

        $this->auction_bids = $this->auction_bids->join('users', 'users.id', '=', 'auction_bids.user_id')
            ->select('users.title', 'users.firstname', 'users.lastname', 'auction_bids.bid', 'auction_bids.created_at', 'users.id')
            ->where('auction_id', '=', $auction->id)
            ->orderBy('auction_bids.id', 'asc')
            ->get();

        return view('auctions.admin.showbids', ['property' => $this->property, 'bids' => $this->auction_bids]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /auction/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $input = Request::all();

        if ($input['auction_type'] == '1' || $input['auction_type'] == '4' || $input['auction_type'] == '5') {
            $input['start_date'] = $input['start_date_phys'];
        }

        $dtStart_array = explode(' ', $input['start_date']);

        $input['start_date'] = $dtStart_array[0];

        $dtEnd_array = explode(' ', $input['end_date']);
        $input['end_date'] = $dtEnd_array[0];

        if (! isset($dtStart_array[2])) {
            $dtStart_array[2] = '00:00:00';
        }

        if (! isset($dtEnd_array[2])) {
            $dtEnd_array[2] = '00:00:00';
        }

        if (Request::get('start_date') !== null) {
            $startDate1 = str_replace('/', '-', $input['start_date']);
            $startDate2 = strtotime($startDate1);
            $start_date = date('Y-m-d', $startDate2);

            $endDate1 = str_replace('/', '-', Request::get('end_date'));
            $endDate2 = strtotime($endDate1);

            $datediff = $endDate2 - $startDate2;
            $input['period_days'] = floor($datediff / (60 * 60 * 24));

            $input['start_date'] = $start_date.' '.$dtStart_array[2];
            $end_date = date('Y-m-d', strtotime($start_date.' + '.$input['period_days'].' days'));
            $input['end_date'] = $end_date.' '.$dtEnd_array[2];
        }

        $this->auction = $this->auction->find($id);

        if ($input['auction_type'] == '1' || $input['auction_type'] == '4' || $input['auction_type'] == '5') {
            $input['period_days'] = 1;
        }

        if ($input['auction_type'] == '4') {
            $input['period_days'] = 1;
            $input['reserve'] = 1;
            $input['click_to_url'] = $this->remove_http($input['click_to_url']);
        }

        unset($input['start_date_phys']);

        $this->auction->fill($input);

        $today = strtotime(date('Y-m-d'));

        $this->vetting = $this->vetting->whereProperty_id($this->auction->property_id)->first();

        $terms = DB::table('uploads')->select('id')->whereLink($this->auction->property_id.'A')->whereType('rules_of_auction_conditions_of_sale')->first();

        $this->property = $this->property->whereId($this->auction->property_id)->first();

        if ($this->auction->status != 'live') {
            $this->property->price = Request::get('guideline');
        }

        if ($this->auction->status == 'live') {
            $this->auction->save();

            return redirect()->route('admin.auctions', ['status' => 'all']);
        } elseif (($this->auction->start_price > 0 && $this->auction->reserve > 0 && $this->auction->increment > 0 &&
                $this->auction->period_days > 0 && $this->auction->guideline > 0 && $startDate2 >= $today &&
                $this->auction->auction_type != '' && $terms != null) ||
            ($this->auction->auction_type == 1 && $this->auction->reserve > 0 && $this->auction->guideline > 0 && $terms != null &&
                $startDate2 >= $today) ||
            ($this->auction->auction_type == 4 && $this->auction->reserve > 0 && $this->auction->guideline > 0 && $terms != null &&
                $startDate2 >= $today && $this->auction->click_to_url != '') ||
            ($this->auction->auction_type == 5 && $this->auction->reserve > 0 && $this->auction->guideline > 0
                && $terms != null && $startDate2 >= $today)
        ) {
            $this->vetting->vetting_status = 2;

            $this->vetting->save();

            $this->auction->status = 'pending';
        } else {
            $this->vetting->vetting_status = 9;

            $this->vetting->save();

            $this->auction->status = 'created';
        }

        $this->auction->save();

        return redirect()->route('admin.auctions', ['status' => 'all']);
    }

    public function remove_http($url)
    {
        $disallowed = ['http://', 'https://'];
        foreach ($disallowed as $d) {
            if (strpos($url, $d) === 0) {
                return str_replace($d, '', $url);
            }
        }

        return $url;
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /auction/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function setup($id)
    {
        $this->auction = $this->auction->whereProperty_id($id)->first();

        if (date('Y-m-d', strtotime($this->auction->start_date)) !== '1970-01-01') {
            $startDate = strtotime($this->auction->start_date);
            $start_date = date('d/m/Y  H:i', $startDate);
            $this->auction->start_date = $start_date;
        } else {
            $this->auction->start_date = date('d/m/Y H:i');
        }

        if (date('Y-m-d', strtotime($this->auction->end_date)) !== '1970-01-01') {
            $startDate = strtotime($this->auction->end_date);
            $end_date = date('d/m/Y  H:i', $startDate);
            $this->auction->end_date = $end_date;
        } else {
            $this->auction->end_date = date('d/m/Y H:i');
        }

        if ($this->auction->auction_type == 5 && $this->auction->auction_name == null) {
            $property = $this->property->whereId($id)->first();
            $this->auction->auction_name = trim($property->street_number.' '.$property->street_address).' | Onsite Auction';
        }

        $terms = DB::table('uploads')->select('id')->whereLink($id.'A')->whereType('rules_of_auction_conditions_of_sale')->first();

        $auctioneers = DB::table('auctioneers')->get();

        $lead_image = $this->property->whereId($id)->select('lead_image')->first();

        return view('auctions.admin.setup', ['auction' => $this->auction, 'terms' => $terms, 'auctioneers' => $auctioneers,
            'lead_image' => $lead_image, ]);
    }

    public function listAuction($id)
    {
        $this->auction = $this->auction->whereProperty_id($id)->first();

        $this->auction->status = 'live';

        $dtToday = date('Y-m-d');

        $this->auction->start_date = $dtToday;

        $this->auction->update();

        $this->property = $this->property->whereId($id)->first();

        $this->property->price = $this->auction->start_price;

        $this->property->update();

        return view('auctions.admin.properties', ['status' => 'all']);
    }

    public function placeBid()
    {
        $bid = Request::get('bid');

        $id = Request::get('id');

        $this->property = $this->property->whereId($id)->first();

        //Notify In2Assets Admin of Pre Registration for auction.
        $name = 'Dear In2Assets Administrator,';
        $subject = 'In2Assets - A new bid has been placed';
        $userMail = 'bidtobuy@in2assets.com';
        $emailData = '<p>'.Auth::user()->firstname.' '.Auth::user()->lastname.' placed a bid on property with
        reference: '.$this->property->reference.'</p>';
        // Queue email
        Mail::to('bidtobuy@in2assets.com')
            ->cc(['rmoodley@in2assets.com', 'erich@in2assets.com'])
            ->send(new newLead($subject, $name, $emailData, $userMail));

        // dd($this->property);

        $this->auction = $this->auction->whereProperty_id($id)->first();

        // $queries = DB::getQueryLog();
//             $query = end($queries);
//             dd($query);

        // dd($this->auction);

        $this->auction_bids['user_id'] = Auth::user()->id;
        $this->auction_bids['auction_id'] = $this->auction->id;
        $this->auction_bids['bid'] = $bid;

        $this->auction_bids->save();

        $this->property->price = $bid;

        $this->property->update();

        return 'True';
    }

    public function listAuctionProperty($status)
    {
        return view('auctions.admin.properties', ['status' => $status]);
    }

    public function getAuctionProperty($status)
    {
        switch ($status) {
            case 'created':
                $status = '%created';

                break;
            case 'pending':
                $status = '%pending';

                break;
            case 'live':
                $status = '%live';

                break;
            case 'sold':
                $status = '%sold';

                break;
            case 'all':
                $status = '%';

                break;
        }

        $properties = DB::table('property')
            ->join('auction', 'property.id', '=', 'auction.property_id')
            ->join('vetting', 'property.id', '=', 'vetting.property_id')
            ->select('reference', 'province', 'suburb_town_city', 'property.id as id', 'auction.status', 'auction.start_date',
                'auction.end_date', 'auction.id as auction_id', 'auction.auction_type')
            ->whereListing_type('weauction')
            ->where('property.deleted_at', '=', null)
            ->where('auction.deleted_at', '=', null)
            ->where('auction.status', 'LIKE', $status)
            ->where(function ($query) {
                $query->where('vetting_status', '=', '2')
                    ->orWhere('vetting_status', '=', '9')
                    ->orWhere('vetting_status', '=', '10');
            })
            ->get();

        foreach ($properties as $property) {
            switch ($property->auction_type) {
                case 1:
                    $property->type = 'Physical';

                    break;
                case 2:
                    $property->type = 'Bid to Buy\Conditional';

                    break;
                case 3:
                    $property->type = 'Bid to Buy\Unconditional';

                    break;
                case 4:
                    $property->type = 'Online';

                    break;
                case 5:
                    $property->type = 'On Site';

                    break;
            }

            $startDate = strtotime($property->start_date);
            $property->startDate = date('d/m/Y H:i', $startDate);

            $endDate = strtotime($property->end_date);
            if (date('d/m/Y', $endDate) == '01/01/1970') {
                $property->endDate = 'NA';
            } elseif (date('d/m/Y H:i', $endDate) == date('d/m/Y H:i', $startDate)) {
                $property->endDate = 'NA';
            } else {
                $property->endDate = date('d/m/Y H:i', $endDate);
            }

            if ($property->status == 'not_sold') {
                $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cog"></i></a>';
            } elseif ($property->status == 'created') {
                if ($property->auction_type == 3) {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cog"></i></a>
					<a href="/admin/auctions/propertyauctionreg/'.$property->auction_id.'" title="Check Auction Register" ><i class="i-circled i-light i-alt i-small icon-bar-chart"></i></a>';
                } else {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cog"></i></a>';
                }
            } elseif ($property->status == 'pending') {
                if ($property->auction_type == 3) {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cogs"></i></a>
					<a href="/admin/auctions/listAuction/'.$property->id.'" title="Start Auction" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>
					<a href="/admin/auctions/propertyauctionreg/'.$property->auction_id.'" title="Check Auction Register" ><i class="i-circled i-light i-alt i-small icon-bar-chart"></i></a>';
                } elseif ($property->auction_type == 2) {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cogs"></i></a>
					<a href="/admin/auctions/listAuction/'.$property->id.'" title="Start Auction" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>';
                } elseif ($property->auction_type == 1) {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cogs"></i></a>';
                } else {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cogs"></i></a>';
                }
            } elseif ($property->status == 'live') {
                if ($property->auction_type == 3) {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cogs"></i></a>
					<a href="/admin/auctions/bids/'.$property->id.'" title="Check Property Bids" ><i class="i-circled i-light i-alt i-small icon-bar-chart"></i></a>
					<a href="/admin/auctions/propertyauctionreg/'.$property->auction_id.'" title="Check Auction Register" ><i class="i-circled i-light i-alt i-small icon-bar-chart"></i></a>
					<a href="#" title="Change Increment" onClick="incrementChange('.$property->auction_id.')"><i class="i-circled i-light i-alt i-small icon-external-link"></i></a>
					<a href="/admin/property/close_property/'.$property->id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
                } elseif ($property->auction_type == 2) {
                    $property->actions = '<a href="/admin/auctions/setup/'.$property->id.'" title="Setup Auction" ><i class="i-circled i-light i-alt i-small icon-cogs"></i></a>
					<a href="/admin/auctions/bids/'.$property->id.'" title="Check Property Bids" ><i class="i-circled i-light i-alt i-small icon-bar-chart"></i></a>
					<a href="#" title="Change Increment" onClick="incrementChange('.$property->auction_id.')"><i class="i-circled i-light i-alt i-small icon-external-link"></i></a>
					<a href="/admin/property/close_property/'.$property->id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
                } else {
                    $property->actions = '<a href="/admin/property/close_property/'.$property->id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
                }
            } elseif ($property->status == 'sold') {
                $property->actions = '<a href="/admin/auctions/bids/'.$property->id.'" title="Check Property Bids" ><i class="i-circled i-light i-alt i-small icon-bar-chart"></i></a>
				<a href="#" onClick="finalPrice('.$property->id.');" title="Enter Final Price" ><i class="i-circled i-light i-alt i-small icon-book"></i></a>
				<a  href="#" onClick="delete_prop('.$property->id.');" title="Complete" ><i class="i-circled i-light i-alt i-small icon-ok"></i></a>';
            }
        }

        return Datatables::of($properties)
            ->rawColumns(['type', 'startDate', 'endDate', 'actions'])
            ->make(true);
    }

    public function changeIncrement()
    {
        $auction_id = Request::get('auction_id');

        $increment = Request::get('increment');

        $this->auction = $this->auction->whereId($auction_id)->first();

        $this->auction->increment = $increment;

        $this->auction->update();

        return 'True';
    }

    public function setfinalprice()
    {
        $property_id = Request::get('property_id');

        $finalPrice = Request::get('finalPrice');

        $this->property = $this->property->whereId($property_id)->first();

        $this->property->price = $finalPrice;

        $this->property->update();

        return 'True';
    }

    public function physicalRegister()
    {
        $input = Request::all();
        $this->physical_auction = $this->physical_auction->whereId($input['id'])->first();

        $this->physical_auction_register->property_auction_id = 0;
        $this->physical_auction_register->auction_id = $input['id'];
        $this->physical_auction_register->user_id = Auth::user()->id;
        $this->physical_auction_register->status = '1';

        $this->physical_auction_register->save();

        $user = User::whereId(Auth::user()->id)->first();
        $userName = $user->firstname.' '.$user->lastname;

        //Notify User of Pre Registration for auction.
        $name = 'Dear '.$userName.',';
        $subject = 'In2Assets - Registration for Auction';
        $userMail = $user->email;
        $emailData = '<p>Thank you! We have received your pre-registration application and our team will soon be in
        contact with you to complete the process.</p>
        <p>Please note, to finalise your pre-registration we will require a copy of your barcoded South African ID, proof
        of residence (not older than 3 months) and a refundable registration deposit of R50 000.00 (fifty thousand rand).</p>
        <p>Your allocated bidder’s card will be prepared and ready for you to collect at the respective auction.</p>
        <p>We look forward to seeing you at the auction and remember our team is always available to assist with
        any queries.</p>
        <p>Sincerely</p>
        <p>The In2assets Team</p>';
        $data = ['name' => $userName, 'subject' => $subject, 'messages' => $emailData];
        // Queue email
        Mail::to($userMail)
            ->send(new newLead($subject, $name, $emailData, $userMail));

        //Notify In2Assets Admin of Pre Registration for auction.
        $name = 'Dear In2Assets Administrator,';
        $subject = 'In2Assets - New Registration for Auction';
        $emailData = '<p>A client has registered for an auction, details as follows:-</p>
						<ul>
						<li>Client Name: '.$userName.'</li>
						<li>Client Cell/Tel: '.Auth::user()->phonenumber.'</li>
						<li>Client Email: '.Auth::user()->email.'</li>
						<li>'.$this->physical_auction->auction_name.': '.
            date('Y-m-d', strtotime($this->physical_auction->start_date)).'</li></ul>';

        // Queue email
        Mail::to('auctions@in2assets.com')
            ->cc(['rmoodley@in2assets.com', 'erich@in2assets.com'])
            ->send(new newLead($subject, $name, $emailData, 'auctions@in2assets.com'));

        Flash::message('Thank you for your registration, one of our team will contact you shortly.');

        return redirect()->back();
    }

    public function register()
    {
        $input = Request::all();

        $this->auction_register->auction_id = $input['id'];
        $this->auction_register->user_id = Auth::user()->id;
        $this->auction_register->status = '1';

        $this->auction_register->save();

        $this->auction = $this->auction->whereId($input['id'])->first();

        $property = $this->property->whereId($this->auction->property_id)->first();

        if ($this->auction->physical_auction != null) {
            $this->physical_auction_register->property_auction_id = $input['id'];
            $this->physical_auction_register->auction_id = $this->auction->physical_auction;
            $this->physical_auction_register->user_id = Auth::user()->id;
            $this->physical_auction_register->status = '1';

            $this->physical_auction_register->save();
        }

        $user = User::whereId(Auth::user()->id)->first();
        $userName = $user->firstname.' '.$user->lastname;

        //Notify User of Pre Registration for auction.
        $name = 'Dear '.$userName.',';
        $subject = 'In2Assets - Registration for Auction';
        $userMail = $user->email;
        $emailData = '<p>Thank you! We have received your pre-registration application and our team will soon be in contact with you to 
        complete the process.</p>
        <p>Please note, to finalise your pre-registration we will require a copy of your barcoded South African ID, proof of residence 
        (not older than 3 months) and a refundable registration deposit of R50 000.00 (fifty thousand rand).</p>
        <p>Your allocated bidder’s card will be prepared and ready for you to collect at the respective auction.</p>
        <p>Did you know that In2assets offers 4 different auction platforms?  Once you have completed the pre-registration process, 
        you will have the benefit of taking part in any auction conducted by In2assets;</p>
        <ul>
        <li>Combined Property Auction – held every month with a range of properties available managed by our auctioneer Andrew Miller.</li>
        <li>On Site Property Auction – Auctions conducted at the property being auctioned, managed by Andrew Miller.</li>
        <li>Online Property Auction – Real time online live auctions are conducted by our in-house auctioneer.  Auctions start on a preset 
        date and time.</li>
        <li>Bid to Buy – Timed auctions online allowing the bidder to bid anytime. Bidding is open until the preset closing date and time.</li>
        </ul>
        <p>We look forward to seeing you at the next auction and remember our team is always available to assist with any queries.</p>
        <p>Sincerely</p>
        <p>The In2assets Team</p>';
        // Queue email
        Mail::to($user->email)
            ->send(new newLead($subject, $name, $emailData, $userMail));

        //Notify In2Assets Admin of Pre Registration for auction.
        $name = 'Dear In2Assets Administrator,';
        $subject = 'In2Assets - New Registration for Auction';
        $emailData = '<p>A client has registered for an auction, details as follows:-</p>
						<ul>
						<li>Client Name: '.$userName.'</li>
						<li>Client Cell/Tel: '.Auth::user()->phonenumber.'</li>
						<li>Client Email: '.Auth::user()->email.'</li>
						<li>Property Reference: '.$property->reference.'</li>
						</ul>';

        // Queue email
        Mail::to('auctions@in2assets.com')
            ->cc(['rmoodley@in2assets.com', 'erich@in2assets.com'])
            ->send(new newLead($subject, $name, $emailData, 'auctions@in2assets.com'));

        Flash::message('Thank you for your registration, one of our team will contact you shortly.');

        return redirect()->back();
    }

    public function emailAlerts()
    {
        $date = date('m/d/Y');
        $yesterday = date('Y-m-d', strtotime($date.'-1 days'));

        $onSiteAuctions = $this->auction->select('id')
            ->where('start_date', '>', $yesterday)
            ->where('auction.auction_type', '=', 5)
            ->get();

        $physicalAuctions = DB::table('physical_auctions')->where('start_date', '>', $yesterday.' 23:59')->whereDeleted_at(null)
            ->orderBy('start_date')->get();

        $onsiteArray = [];
        foreach ($onSiteAuctions as $onSiteAuction) {
            if (Request::get('onsite'.$onSiteAuction->id)) {
                $onsiteArray[] = $onSiteAuction->id;
            }
        }

        $physArray = [];
        foreach ($physicalAuctions as $physicalAuction) {
            if (Request::get('phys'.$physicalAuction->id)) {
                $physArray[] = $physicalAuction->id;
            }
        }
        $type_id = DB::table('email_alert_type')->select('id')->whereType('auction')->first();
        $input['email'] = Request::get('email');
        foreach ($onsiteArray as $alert) {
            $input['type'] = $type_id->id;
            $input['sub_type'] = 5;
            $input['type_id'] = $alert;
            $email_alert = new EmailAlert();
            $email_alert->fill($input)->save();
        }

        foreach ($physArray as $alert) {
            $input['type'] = $type_id->id;
            $input['sub_type'] = 1;
            $input['type_id'] = $alert;
            $email_alert = new EmailAlert();
            $email_alert->fill($input)->save();
        }

        Flash::message('Thank you, we will alert you closer to the time of the auction/s you have shown interested in');

        return redirect()->back();
    }

    public function newAuction()
    {
        $citiesDB = DB::table('areas')->selectRaw('province, area, suburb_town_city, count(*)')
            ->groupBy('province', 'area', 'suburb_town_city')->get();
        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        return view('auctions.admin.new-auction', ['cities' => $cities]);
    }

    public function editAuction($id)
    {
        $citiesDB = DB::table('areas')->groupBy('province', 'area', 'suburb_town_city')->get();
        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $this->physical_auction = $this->physical_auction->whereId($id)->first();

        return view('auctions.admin.edit-auction', ['cities' => $cities, 'physical_auction' => $this->physical_auction]);
    }

    public function storeAuction()
    {
        $input = Request::all();

        unset($input['Suburb']);
        $dtStart_array = explode(' ', $input['start_date']);
        $input['start_date'] = $dtStart_array[0];

        $startDate = strtotime($input['start_date']);
        $start_date = date('Y-m-d', $startDate);
        $input['start_date'] = $start_date.' '.$dtStart_array[1];

        if (! $this->physical_auction->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->physical_auction->errors);
        }

        $this->physical_auction->save();

        return redirect()->route('admin.index');
    }

    public function updateAuction($id)
    {
        $input = Request::all();

        unset($input['Suburb']);
        $dtStart_array = explode(' ', $input['start_date']);
        $input['start_date'] = $dtStart_array[0];

        $startDate = strtotime($input['start_date']);
        $start_date = date('Y-m-d', $startDate);
        $input['start_date'] = $start_date.' '.$dtStart_array[1];

        $this->physical_auction = $this->physical_auction->whereId($id)->first();

        if (! $this->physical_auction->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->physical_auction->errors);
        }

        $this->physical_auction->update();

        return redirect()->route('admin.index');
    }

    public function physicalAuctions()
    {
        return view('auctions.admin.live-auctions');
    }

    public function getPhysicalAuctions()
    {
        $auctions = DB::table('physical_auctions')
            ->select('physical_auctions.auction_name', 'physical_auctions.street_address', 'physical_auctions.start_date', 'physical_auctions.id')
            ->whereDeleted_at(null)
            ->get();

        foreach ($auctions as $auction) {
            $auctionCount = $this->auction->where('physical_auction', '=', $auction->id)->count();

            $auction->physical_auction_count = $auctionCount;
        }

        return Datatable::collection(new Collection($auctions))
            ->showColumns('start_date', 'auction_name', 'street_address', 'physical_auction_count')
            ->addColumn('action', function ($Auction) {
                return '<a href="/admin/physical_auctions/add_property/'.$Auction->id.'" title="Add Properties to Auction" ><i class="i-circled i-light i-alt i-small icon-home"></i></a>
		<a href="/admin/auctions/physicalauctionreg/'.$Auction->id.'" title="Check Auction Register" ><i class="i-circled i-light i-alt i-small icon-bar-chart"></i></a>
		<a href="/admin/auctions/'.$Auction->id.'/editauction" title="Edit Physical Auction" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
		<a href="/admin/physical-auctions/'.$Auction->id.'/delete" title="Delete Physical Auction" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
            })
            ->searchColumns('start_date', 'auction_name', 'street_address')
            ->orderColumns('start_date', 'auction_name', 'street_address')
            ->make();
    }

    public function addProperties($id)
    {
        return view('auctions.admin.add-properties', ['id' => $id]);
    }

    public function getAddPropertyTable($id)
    {
        $properties = DB::table('property')
            ->join('auction', 'property.id', '=', 'auction.property_id')
            ->leftJoin('auctioneer_notes', 'property.id', '=', 'auctioneer_notes.property_id')
            ->whereListing_type('weauction')
            ->where('auction.auction_type', '=', 1)
            ->where('auction.physical_auction', '=', null)
            ->where('property.deleted_at', '=', null)
            ->orWhere('auction.physical_auction', '=', $id)
            ->select('reference', 'property.street_address', 'auction.physical_auction', 'auction.property_id', 'auctioneer_notes.lot')
            ->get();

        foreach ($properties as $property) {
            $property->auction_id = $id;
        }

        return Datatable::collection(new Collection($properties))
            ->showColumns('reference', 'street_address')
            ->addColumn('action', function ($Property) {
                if ($Property->physical_auction === null) {
                    return '<a href="/admin/physical_auctions/add_to_auction/'.$Property->property_id.'/'.$Property->auction_id.'" title="Add Property to Auction" >
                    <i class="i-circled i-light i-alt i-small icon-check"></i>
                    </a>';
                } else {
                    return '<a href="/admin/physical_auctions/remove_from_auction/'.$Property->property_id.'/'.$Property->auction_id.'" title="Remove Property from Auction" >
                    <i class="i-circled i-light i-alt i-small icon-remove"></i>
                    </a>
                    <input type="number" class="lot" id="Lot-'.$Property->property_id.'" value="'.$Property->lot.'" >';
                }
            })
            ->searchColumns('reference', 'street_address')
            ->orderColumns('reference', 'street_address')
            ->make();
    }

    public function addProperty($property_id, $auction_id)
    {
        $auction = $this->auction->whereProperty_id($property_id)->first();

        $auction->physical_auction = $auction_id;

        $this->physical_auction = $this->physical_auction->whereId($auction_id)->first();

        $auction->start_date = $this->physical_auction->start_date;

        $auction->update();

        return redirect()->route('admin.physical_auctions.add_property', ['id' => $auction_id]);
    }

    public function removeProperty($property_id, $auction_id)
    {
        $auction = $this->auction->whereProperty_id($property_id)->first();

        $auction->physical_auction = null;

        $auction->update();

        return redirect()->route('admin.physical_auctions.add_property', ['id' => $auction_id]);
    }

    public function PropertyAuctionReg($id)
    {
        return view('auctions.admin.auctionRegister', ['id' => $id]);
    }

    public function getPropAuctRegTable($id)
    {
        $auction_reg = DB::table('auction_register')
            ->join('users', 'auction_register.user_id', '=', 'users.id')
            ->whereAuction_id($id)
            ->get();

        return Datatable::collection(new Collection($auction_reg))
            ->addColumn('name', function ($Auction_reg) {
                return $Auction_reg->title.' '.$Auction_reg->firstname.' '.$Auction_reg->lastname;
            })
            ->addColumn('status', function ($Auction_reg) {
                switch ($Auction_reg->status) {
                    case 1:
                        return 'Pending';

                        break;
                    case 2:
                        return 'Approved';

                        break;
                    case 3:
                        return 'Rejected';

                        break;
                }
            })
            ->showColumns('updated_at')
            ->addColumn('action ', function ($Auction_reg) {
                if ($Auction_reg->status == 1) {
                    return '<a href="/admin/users/edit/'.$Auction_reg->user_id.'" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a>
			<a href="/admin/auctions/approve_user/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Approve User" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>
			<a href="/admin/auctions/reject_user/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Reject User" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                } elseif ($Auction_reg->status == 2) {
                    return '<a href="/admin/users/edit/'.$Auction_reg->user_id.'" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a>
			<a href="/admin/auctions/reject_user/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Remove User from Auction" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                } else {
                    return '<a href="/admin/users/edit/'.$Auction_reg->user_id.'" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a>
			<a href="/admin/auctions/approve_user/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Approve User" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>';
                }
            })
            ->searchColumns('name', 'updated_at')
            ->orderColumns('name', 'updated_at')
            ->make();
    }

    public function approveUser($user_id, $auction_id)
    {
        $auction_register = $this->auction_register->whereUser_id($user_id)->whereAuction_id($auction_id)->first();

        $auction_register->status = 2;

        $auction_register->update();

        return redirect()->route('admin.auctions.propertyauctionreg', ['id' => $auction_id]);
    }

    public function rejectUser($user_id, $auction_id)
    {
        $auction_register = $this->auction_register->whereUser_id($user_id)->whereAuction_id($auction_id)->first();

        $auction_register->status = 3;

        $auction_register->update();

        return redirect()->route('admin.auctions.propertyauctionreg', ['id' => $auction_id]);
    }

    public function PhysicalAuctionReg($id)
    {
        return view('auctions.admin.physicalRegister', ['id' => $id]);
    }

    public function getPhysAuctRegTable($id)
    {
        $physical_auction_reg = DB::table('physical_auction_register')
            ->join('users', 'physical_auction_register.user_id', '=', 'users.id')
            ->whereAuction_id($id)
            ->get();

        return Datatable::collection(new Collection($physical_auction_reg))
            ->addColumn('name', function ($Auction_reg) {
                return $Auction_reg->title.' '.$Auction_reg->firstname.' '.$Auction_reg->lastname;
            })
            ->addColumn('status', function ($Auction_reg) {
                switch ($Auction_reg->status) {
                    case 1:
                        return 'Pending';

                        break;
                    case 2:
                        return 'Approved';

                        break;
                    case 3:
                        return 'Rejected';

                        break;
                }
            })
            ->showColumns('updated_at')
            ->addColumn('action ', function ($Auction_reg) {
                if ($Auction_reg->status == 1) {
                    return '<a href="/admin/users/edit/'.$Auction_reg->user_id.'" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a>
			<a href="/admin/auctions/approve_user_physical/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Approve User" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>
			<a href="/admin/auctions/reject_user_physical/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Reject User" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                } elseif ($Auction_reg->status == 2) {
                    return '<a href="/admin/users/edit/'.$Auction_reg->user_id.'" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a>
			<a href="/admin/auctions/reject_user_physical/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Remove User from Auction" ><i class="i-circled i-light i-alt i-small icon-remove"></i></a>';
                } else {
                    return '<a href="/admin/users/edit/'.$Auction_reg->user_id.'" title="View User" ><i class="i-circled i-light i-small icon-laptop"></i></a>
			<a href="/admin/auctions/approve_user_physical/'.$Auction_reg->user_id.'/'.$Auction_reg->auction_id.'" title="Approve User" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>';
                }
            })
            ->searchColumns('name', 'updated_at')
            ->orderColumns('name', 'updated_at')
            ->make();
    }

    public function approveUserPhysical($user_id, $auction_id)
    {
        $physical_auction_register = $this->physical_auction_register->whereUser_id($user_id)->whereAuction_id($auction_id)->first();

        $physical_auction_register->status = 2;

        $physical_auction_register->update();

        return redirect()->route('admin.auctions.physicalauctionreg', ['id' => $auction_id]);
    }

    public function rejectUserPhysical($user_id, $auction_id)
    {
        $physical_auction_register = $this->physical_auction_register->whereUser_id($user_id)->whereAuction_id($auction_id)->first();

        $physical_auction_register->status = 3;

        $physical_auction_register->update();

        return redirect()->route('admin.auctions.physicalauctionreg', ['id' => $auction_id]);
    }

    public function convertToAuction()
    {
        $input = Request::get('id');

        $auction_ref = DB::select('SELECT `auction_ref` FROM `reference` WHERE `id` = ?', [1]);
        $auction_ref = $auction_ref[0]->auction_ref;
        $reference = 'AUCT-'.str_pad($auction_ref, 6, '0', STR_PAD_LEFT);
        $auction_ref++;
        DB::update('UPDATE `reference` SET `auction_ref` = ?, `updated_at` = NOW()', [$auction_ref]);

        $this->property = $this->property->whereId($input)->first();
        $this->vetting = $this->vetting->whereProperty_id($input)->first();

        $reference_from = $this->property->reference;

        DB::update('INSERT INTO `reference_change` SET `property_id` = ?, `reference_from` = ?, `reference_to` = ?, `updated_at` = NOW()',
            [$input, $reference_from, $reference]);

        $this->property->reference = $reference;
        $this->property->listing_type = 'weauction';

        $this->property->save();

        $this->vetting->vetting_status = 3;

        $this->vetting->save();

        return 'true';
    }

    public function convertToSale()
    {
        $input = Request::get('id');

        $sale_ref = DB::select('SELECT `sale_ref` FROM `reference` WHERE `id` = ?', [1]);
        $sale_ref = $sale_ref[0]->sale_ref;
        $reference = 'SALE-'.str_pad($sale_ref, 6, '0', STR_PAD_LEFT);
        $sale_ref++;
        DB::update('UPDATE `reference` SET `sale_ref` = ?, `updated_at` = NOW()', [$sale_ref]);

        $this->property = $this->property->whereId($input)->first();
        $this->vetting = $this->vetting->whereProperty_id($input)->first();

        $this->auction->whereProperty_id($input)->delete();

        $reference_from = $this->property->reference;

        DB::update('INSERT INTO `reference_change` SET `property_id` = ?, `reference_from` = ?, `reference_to` = ?, `updated_at` = NOW()',
            [$input, $reference_from, $reference]);

        $this->property->reference = $reference;
        $this->property->listing_type = 'in2assets';

        $this->property->update();

        $this->vetting->vetting_status = 3;

        $this->vetting->update();

        return 'true';
    }

    public function deletePhysicalAuction($id)
    {
        $this->physical_auction->whereId($id)->delete();

        $this->auction->wherePhysical_auction($id)->update(['physical_auction' => null]);

        return redirect()->back();
    }

    public function setLotNum($id, $lot_num)
    {
        if (! $auctioneer_notes = $this->auctioneer_notes->whereProperty_id($id)->first()) {
            $auctioneer_notes = new AuctioneerNotes();
            $auctioneer_notes->property_id = $id;
        }

        $auctioneer_notes->lot = $lot_num;

        $auctioneer_notes->save();

        return 'Done';
    }

    public function printOrderOfSale($id)
    {
        $physical = $this->physical_auction->whereId($id)->first();

        $property_list = $this->auction
            ->join('auctioneer_notes', 'auctioneer_notes.property_id', '=', 'auction.property_id')
            ->join('property', 'property.id', '=', 'auction.property_id')
            ->select('auction.property_id', 'auctioneer_notes.lot', 'property.street_number', 'property.street_address', 'property.area')
            ->orderBy('auctioneer_notes.lot')
            ->wherePhysical_auction($id)->get();

        return view('auctions.print.order_of_sale', ['property_list' => $property_list, 'physical_auction' => $physical]);
    }

    public function getNextAuctionDate()
    {
        $date = date('m/d/Y');
        $yesterday = date('Y-m-d', strtotime($date.'-1 days'));
        if (! $physicalAuction = DB::table('physical_auctions')->where('start_date', '>', $yesterday.' 23:59')->whereDeleted_at(null)
            ->orderBy('start_date')->first()
        ) {
            $returnStr = 'None';
        } else {
            $returnStr = date('d F', strtotime($physicalAuction->start_date)).' '.$physicalAuction->id;
        }

        return $returnStr;
    }
}
