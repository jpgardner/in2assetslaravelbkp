<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class LeaseAgreementsController extends Controller
{
    protected $lease_agreement;

    public function __construct(LeaseAgreement $lease_agreement, Property $property, LeaseAmendment $lease_amendment)
    {
        $this->lease_agreement = $lease_agreement;
        $this->lease_amendment = $lease_amendment;
        $this->property = $property;
    }

    public function newLease($id, $tenant_id)
    {
        //
        $result = $this->lease_agreement->whereProperty_id($id)->whereLandlord_id(Auth::user()->id)->whereTenant_id($tenant_id)->with('amendments')->first();

        if (! $result) {
            $datetime = date('Y-m-d H:i:s');
            $leaseId = DB::table('lease_agreements')->insertGetId(['landlord_id' => Auth::user()->id, 'tenant_id' => $tenant_id, 'property_id' => $id, 'lease_status' => 'tenant', 'created_at' => $datetime, 'updated_at' => $datetime]);

            $lease = $this->lease_agreement->whereProperty_id($id)->whereLandlord_id(Auth::user()->id)->whereTenant_id('0')->with('amendments')->first();

            unset($lease->amendments['id']);
            unset($lease->amendments['lease_agreement_id']);
            unset($lease->amendments['deleted_at']);
            unset($lease->amendments['created_at']);
            unset($lease->amendments['updated_at']);

            $this->lease_amendment->fill($lease->amendments->toArray());

            $this->lease_amendment->lease_agreement_id = $leaseId;

            $this->lease_amendment->save();

            return view::make('makecontact.show', ['id' => $id]);
        } else {
            return view::make('makecontact.show', ['id' => $id]);
        }
    }

    public function show($id, $tenant_id)
    {
        //

        $property = $this->property->whereId($id)->with('commercial')->with('vetting')->first();

        $lease = $this->lease_agreement->whereProperty_id($id)->whereLandlord_id($property->user_id)->whereTenant_id($tenant_id)->with('amendments')->first();

        return view::make('lease_agreements.show', ['lease' => $lease, 'property' => $property]);
    }

    public function convert_number_to_words($number)
    {
        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = [
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion',

        ];

        if (! is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -'.PHP_INT_MAX.' and '.PHP_INT_MAX,
                E_USER_WARNING
            );

            return false;
        }

        if ($number < 0) {
            return $negative.$this->convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];

                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen.$dictionary[$units];
                }

                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds].' '.$dictionary[100];
                if ($remainder) {
                    $string .= $conjunction.$this->convert_number_to_words($remainder);
                }

                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits).' '.$dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }

                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = [];
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    public function edit($id, $tenant_id)
    {
        //
        $lease = $this->lease_agreement->whereProperty_id($id)->whereLandlord_id(Auth::user()->id)->whereTenant_id($tenant_id)->with('amendments')->first();
        $property = $this->property->whereId($id)->with('commercial')->with('vetting')->first();

        return view::make('lease_agreements.edit', ['lease' => $lease, 'property' => $property]);
    }

    public function update($id, $tenant_id)
    {
        //
        $lease = $this->lease_agreement->whereProperty_id($id)->whereLandlord_id(Auth::user()->id)->whereTenant_id($tenant_id)->first();
        $leaseId = $lease->id;

        $this->lease_amendment = $this->lease_amendment->whereLease_agreement_id($leaseId)->first();

        $input = Request::all();

        if ($input['commencement_date'] != '') {
            $commencement_date = DateTime::createFromFormat('d/m/Y', $input['commencement_date']);
            $newComencement_date = $commencement_date->format('Y-m-d');
            $input['commencement_date'] = $newComencement_date;
        }

        if ($tenant_id !== 0) {
            if (! $this->lease_amendment->fill($input)->isValid()) {
                return Redirect::back()->withInput()->withErrors($this->lease_amendment->errors);
            }
            $this->lease_amendment->update();
        } else {
            $this->lease_amendment->fill($input)->update();
        }

        return Redirect::route('lease_agreements.show', ['id' => $id, 'tenant_id' => $tenant_id]);

        // return Redirect::route('lease_agreement.show', ['id' => $id]);
    }

    // marks lease agreement as in2assets for in2assets approval

    public function approve($id, $tenant_id, $status)
    {
        $lease = $this->lease_agreement->whereProperty_id($id)->whereLandlord_id(Auth::user()->id)->whereTenant_id($tenant_id)->first();
        $lease->lease_status = $status;
        $lease->save();
        if (Auth::user()->hasRole('Admin')) {
            return view::make('lease_agreements.admin.index');
        }

        return view::make('makecontact.show', ['id' => $id]);
    }

    // admin lease agreement page

    public function adminLease()
    {
        return view::make('lease_agreements.admin.index');
    }

    // builds admin lease agreement table

    public function getAdminLease()
    {
        $lease_agreements = $this->lease_agreement
            ->join('users as landlord', 'landlord.id', '=', 'lease_agreements.landlord_id')
            ->join('users as tenant', 'tenant.id', '=', 'lease_agreements.tenant_id')
            ->join('property', 'property.id', '=', 'lease_agreements.property_id')
            ->select('lease_agreements.updated_at', 'landlord.id as llId', 'landlord.title as llTitle', 'landlord.firstname as llFirst', 'landlord.lastname as llLast', 'tenant.id as tId', 'tenant.title as tTitle', 'tenant.firstname as tFirst', 'tenant.lastname as tLast', 'property.street_address', 'property.id as property_id', 'lease_agreements.lease_status')
            ->where('lease_agreements.lease_status', '=', 'in2assets')
            ->orWhere('lease_agreements.lease_status', '=', 'approved')
            ->get();

        return Datatable::collection($lease_agreements)
            ->showColumns('updated_at', 'street_address')
            ->addColumn('landlord', function ($Lease) {
                return $Lease->llTitle.' '.$Lease->llFirst.' '.$Lease->llLast;
            })
            ->addColumn('tenant', function ($Lease) {
                return $Lease->tTitle.' '.$Lease->tFirst.' '.$Lease->tLast;
            })
            ->showColumns('lease_status')
            ->addColumn('action', function ($Lease) {
                if ($Lease->lease_status === 'in2assets') {
                    return '<a href="/lease_agreements/'.$Lease->property_id.'/'.$Lease->tId.'" title="Edit Lease" ><i class="i-circled i-light i-small icon-edit"></i></a>&nbsp
            				<a href="/property/'.$Lease->llId.'/message" title="Message Landlord" ><i class="i-circled i-light i-small icon-comments"></i></a>';
                }
                if ($Lease->lease_status === 'approved') {
                    return '<a href="/property/'.$Lease->llId.'/message" title="Message Landlord" ><i class="i-circled i-light i-small icon-comments"></i></a>';
                }
            })
            ->searchColumns('street_address', 'landlord', 'tenant')
            ->orderColumns('street_address', 'landlord', 'tenant')
            ->make();
    }
}
