

namespace App\Http\Controllers;

<?php

/**
 * Created by PhpStorm.
 * User: Erich
 * Date: 2016/12/07
 * Time: 2:29 PM.
 */
class GoogleController extends Controller
{
    public function setupGoogle()
    {
        return view::make('google.index');
    }
}
