<?php

namespace App\Http\Controllers;

use App\In2Assets\Forms\SignInForm;
use App\In2Assets\Users\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;

class SessionsController extends Controller
{
    /**
     * @var SignInForm
     */
    private $signInForm;

    /**
     * Show the form for creating a new resource.
     * GET /sessions/create.
     *
     * @return Response
     */
    public function __construct(SignInForm $signInForm)
    {
        $this->signInForm = $signInForm;
        $this->middleware('auth');
    }

//    public function create()
//    {
//        return View::make('sessions.create');
//    }

    /**
     * Store a newly created resource in storage.
     * POST /sessions.
     *
     * @return Response
     */
//    public function store()
//    {
//        Session::put('url.intended', URL::previous());
    ////        Validate
//        $formData = Request::only('email', 'password');
//        $this->signInForm->validate($formData);
//        if (Auth::attempt($formData)) {
//            if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('LettingAgent') || Auth::user()->hasRole('Agent')) {
//                return redirect()->route('admin.index');
//            }
//            $actual_link = "$_SERVER[HTTP_REFERER]";
//
//            if ($actual_link == URL::to('marketing/sell-commercial-package')) {
//                return redirect()->route('marketing.packages');
//            }
//
//            $check = DB::table('user_pass')->whereUser_id(Auth::user()->id)->first();
//            if (empty($check)) {
//                Flash::message('Welcome back');
//                return redirect()->intended('/');
//            }
//            Flash::message('Feel free to change the password we have given you.');
//            DB::table('user_pass')->whereUser_id(Auth::user()->id)->delete();
//            return redirect()->to('/users/edit#password');
//        } else {
//            Flash::message('Username or Password Incorrect');
//            return redirect()->route('login_path');
//        }
//    }

    public function loginFB()
    {
        try {
            // get data from input
            $code = Request::get('code');

            // get fb service
            $fb = OAuth::consumer('Facebook');

            // if code is provided get user data and sign in
            if (! empty($code)) {
                if (! isset($_SESSION['token'])) {
                    // This was a callback request from facebook, get the token
                    $token = $fb->requestAccessToken($code);
                    $_SESSION['token'] = $token->getAccessToken();
                }
                // Send a request with it
                $result = json_decode($fb->request('/me'), true);

                //Find user with matching socialid
                $user = User::wheresocialid($result['id'])->first();

                if (! $user) {
                    Flash::message('Please login to your account and allow facebook authentication');

                    return redirect()->home();
                } else {
                    Auth::login($user);

                    return redirect()->home();
                }
            } // if not ask for permission first
            else {

                // get fb authorization
                $url = $fb->getAuthorizationUri();
                // return to facebook login url

                return redirect()->to((string) $url);
            }
        } catch (Exception $e) {
            Flash::message('There was an error communicating with Facebook');

            return redirect()->home();
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /sessions/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {
        DB::table('google_session')->whereUser_id(Auth::user()->id)->delete();
        Auth::logout();
        Flash::message('You have been logged out');

        return redirect()->to('/');
    }
}
