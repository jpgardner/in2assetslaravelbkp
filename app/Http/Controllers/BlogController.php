<?php

namespace App\Http\Controllers;

use App\In2Assets\Blogs\Blog;
use App\In2Assets\Blogs\BlogRepository;
use App\In2Assets\Blogs\PublishBlogCommand;
use App\In2Assets\Core\CommandBus;
use App\In2Assets\Forms\PublishBlogForm;
use App\In2Assets\Uploads\Upload;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class BlogController extends Controller
{
    use CommandBus;

    protected $blogRepository;

    protected $publishBlogForm;

    public function __construct(BlogRepository $blogRepository, PublishBlogForm $publishBlogForm)
    {
        $this->blogRepository = $blogRepository;
        $this->publishBlogForm = $publishBlogForm;
    }

    // Returns Blog and Academy page
    public function index()
    {
        $blogs = $this->blogRepository->getPaginated();

        $catCount = DB::table('blogs')->whereStatus('published')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.index', ['category_title' => 0, 'category' => ''])->withBlogs($blogs)->withCategories($catCount);
    }

    // List Blogs and Return Blog Category Sidebar
    public function index_list()
    {
        $blogs = $this->blogRepository->getPaginated();

        $catCount = DB::table('blogs')->whereStatus('published')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.index_list', ['category_title' => 0, 'category' => ''])->withBlogs($blogs)->withCategories($catCount);
    }

    // Return Blogs for the Home Page
    public function homeSnippet()
    {
        $blogs = $this->blogRepository->getHome();

        return $blogs;
    }

    // Returns blogs with categories
    public function blogCategory($category)
    {
        $blogs = $this->blogRepository->getCategoryPaginated($category, 5);

        $catCount = DB::table('blogs')->whereStatus('published')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.index_list', ['category_title' => 1, 'category' => $category])->withBlogs($blogs)->withCategories($catCount);
    }

    // List blogs in Admin
    public function listBlog()
    {
        return view('blog.adminlist');
    }

    // Create Blogs in Admin
    public function createBlog()
    {
        $catCount = DB::table('blogs')->whereStatus('published')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.create')->withCategories($catCount);
    }

    // Save blogs in admin
    public function storeBlog()
    {
        $this->publishBlogForm->validate(Request::only('title', 'body'));

        //Check if Unique Slug, If not append _x to slug
        $slug = Slugify::slugify(Request::get('title'));
        $rowCount = DB::table('blogs')->where('slug', $slug)->count();
        $count = $rowCount + 1;
        if ($rowCount > 0) {
            $slug = $slug.'_'.$count;
        }

        $site = 0;
        if (Request::get('in2assets')) {
            $site += 1;
        }
        if (Request::get('wesell')) {
            $site += 2;
        }

        $this->execute(
            new PublishBlogCommand(Request::get('gallery_id'), Request::get('title'), Request::get('body'), Request::get('short_body'), Request::get('lead_image'), Request::get('category'), Request::get('status'), $slug, Auth::user()->id, $site)
        );

        Flash::message('Your Blog has been saved.');

        return view('blog.create');
    }

    // Show indirvidual blog witbh categories and images
    public function showBlog($slug)
    {
        $blog = $this->blogRepository->findBySlug($slug);
        if ($blog == null) {
            Flash::message('This Blog has been removed from our site');

            return redirect()->route('blog_path');
        }
        $blog_gallery = Upload::where('type', '=', 'blog_image')->whereLink($blog->gallery_id)->get();
        $catCount = DB::table('blogs')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })
            ->where('status', '=', 'published')
            ->selectRaw('category, count(*) as count')->groupBy('category')
            ->get();

        return view('blog.show')->withBlog($blog)->withGallery($blog_gallery)->withCategories($catCount);
    }

    // Edit blog in admin
    public function edit($slug)
    {
        $blog = $this->blogRepository->findBySlug($slug);
        $blog_gallery = Upload::where('type', '=', 'blog_image')->whereLink($blog->gallery_id)->get();
        $catCount = DB::table('blogs')->whereStatus('published')
            ->where(function ($q) {
                $q->where('site', '=', '1')->orWhere('site', '=', '3');
            })->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.edit')->withBlog($blog)->withGallery($blog_gallery)->withCategories($catCount);
    }

    // Update Blog in Admin
    public function update($id)
    {
        $input = Request::all();
        $this->publishBlogForm->validate(Request::only('title', 'body'));
        $site = 0;
        if (Request::get('in2assets')) {
            $site += 1;
        }
        if (Request::get('wesell')) {
            $site += 2;
        }
        $input['site'] = $site;
        $blog = $this->blogRepository->findBySlug($id);
        $blog->update($input);

        Flash::message('Your blog has been updated');

        return redirect()->route('list_blog_path');
    }

    // Admin List blogs datatable
    public function getDatatable()
    {
        $blogList = $this->blogRepository->findAll()
            ->get();

        foreach ($blogList as $blog) {
            $blog->actions = '<a href="/admin/blog/close/'.$blog->id.'" title="Close" ><i class="i-circled i-light i-small icon-minus-sign"></i></a>
            <a href="/admin/blog/edit/'.$blog->slug.'" title="Edit" ><i class="i-circled i-light i-small icon-edit"></i></a>';
        }

        return Datatables::collection($blogList)
            ->rawColumns(['actions'])
            ->make(true);
    }

    // Blog status change
    public function close($id)
    {
        $blog = Blog::whereId($id)->first();
        $blog->status = 'closed';
        $blog->save();
        Flash::message('This blog post has been closed!');

        return redirect()->route('list_blog_path');
    }

    public function postStart()
    {
        return view('blog.search');
    }

    public function postSearch()
    {
        $query = Request::get('q');
        $posts = $this->blogRepository->getSearch($query)->get();
        $catCount = DB::table('blogs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.searchresults')->withBlogs($posts)->withCategories($catCount)->withSearch($query);
    }
}
