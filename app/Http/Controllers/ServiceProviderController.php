<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

class ServiceProviderController extends Controller
{
    public function __construct(ServiceProvider $ServiceProvider)
    {
        $this->service_provider = $ServiceProvider;
    }

    /**
     * Display a listing of the resource.
     * GET /serviceprovider.
     *
     * @return Response
     */
    public function index($status)
    {
        //

        return view('ServiceProvider.index', ['status' => $status]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /serviceprovider/create.
     *
     * @return Response
     */
    public function create()
    {

        //
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $categories = DB::table('service_provider_categories')->get();

        return view('ServiceProvider.create', ['cities' => $cities, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /serviceprovider.
     *
     * @return Response
     */
    public function store()
    {
        //
        $input = Request::all();

        if ($input['active'] === 'on') {
            $input['active'] = 1;
        } else {
            $input['active'] = 0;
        }

        if (! $this->service_provider->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->service_provider->errors);
        }

        $this->service_provider->province_slug = Slugify::slugify($this->service_provider->province);
        $this->service_provider->area_slug = Slugify::slugify($this->service_provider->area);
        $this->service_provider->suburb_town_city_slug = Slugify::slugify($this->service_provider->suburb_town_city);

        $this->service_provider->save();

        return Redirect::route('ServiceProvider.index', ['status' => 'all']);
    }

    /**
     * Display the specified resource.
     * GET /serviceprovider/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /serviceprovider/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $this->service_provider = $this->service_provider->where('service_provider.id', '=', $id)->first();

        $provinces = DB::table('areas')->groupBy('province')->get();
        $areas = DB::table('areas')->groupBy('area')->get();
        $cities = DB::table('areas')->groupBy('suburb_town_city')->get();
        $categories = DB::table('service_provider_categories')->get();

        Request::merge(['province' => $this->service_provider->province, 'area' => $this->service_provider->area, 'suburb_town_city' => $this->service_provider->suburb_town_city]);

        return view('ServiceProvider.edit', ['ServiceProvider' => $this->service_provider, 'provinces' => $provinces, 'cities' => $cities, 'categories' => $categories, 'areas' => $areas]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /serviceprovider/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $input = Request::all();

        $this->service_provider = $this->service_provider->whereId($id)->first();

        if (isset($input['active'])) {
            $input['active'] = 1;
        } else {
            $input['active'] = 0;
        }

        if (! $this->service_provider->fill($input)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->service_provider->errors);
        }

        $this->service_provider->province_slug = Slugify::slugify($this->service_provider->province);
        $this->service_provider->area_slug = Slugify::slugify($this->service_provider->area);
        $this->service_provider->suburb_town_city_slug = Slugify::slugify($this->service_provider->suburb_town_city);

        $this->service_provider->update();

        return Redirect::route('ServiceProvider.index', ['status' => 'all']);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /serviceprovider/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $this->service_provider = $this->service_provider->whereId($id)->first();
        $this->service_provider->delete();

        return Redirect::route('ServiceProvider.index', ['status' => 'all']);
    }

    /**
     * makes database table for service providers
     * used for admin side.
     */
    public function getDatatable($status)
    {
        switch ($status):
            case 'all':
                $active = '%';

        break;
        case 'active':
                $active = '%1%';

        break;
        case 'unused':
                $status = '%0%';

        break;
        endswitch;

        $serviceProviders = $this->service_provider->where('active', 'LIKE', $active)
            ->select('company', 'contact_firstname', 'contact_lastname', 'division', 'acc_number', 'cell_number', 'office_number', 'email', 'active', 'id')
            ->get();

        return Datatable::collection($serviceProviders)
            ->showColumns('company', 'contact_firstname', 'contact_lastname', 'division', 'acc_number', 'cell_number', 'office_number', 'email')
            ->addColumn('active', function ($ServiceProvider) {
                if ($ServiceProvider->active == 1) {
                    return 'Active';
                } else {
                    return 'Unused';
                }
            })
            ->addColumn('action', function ($ServiceProvider) {
                return '<a href="../ServiceProvider/'.$ServiceProvider->id.'/edit" title="Edit" ><i class="i-circled i-light i-small icon-edit"></i></a>';
            })
            ->searchColumns('company', 'contact_firstname', 'contact_lastname', 'division', 'acc_number', 'email', 'active')
            ->orderColumns('company', 'contact_firstname', 'contact_lastname', 'division', 'acc_number', 'email', 'active')
            ->make();
    }

    public function getRegisteredServiceProviders($province, $area, $city, $category)
    {
        if ($province == 'south-africa') {
            $province = '%';
        } else {
            $provinceDB = DB::table('areas')->whereProvince_slug($province)->select('province')->first();
            $province = $provinceDB->province;
            $province = '%'.$province;
        }

        if ($area == 'all-cities') {
            $area = '%';
        } else {
            $areaDB = DB::table('areas')->whereArea_slug($area)->select('area')->first();
            $area = $areaDB->area;
            $area = '%'.$area;
        }

        if ($city == 'all-suburbs') {
            $city = '%';
        } else {
            $cityDB = DB::table('areas')->whereSuburb_town_city_slug($city)->select('suburb_town_city')->first();
            $city = $cityDB->suburb_town_city;
            $city = '%'.$city;
        }

        if ($category == 'property') {
            $category = '%';
        } else {
            $category = '%'.$category;
        }

        $serviceProviders = $this->service_provider->where('active', '=', '1')
            ->where('province', 'LIKE', $province)
            ->where('area', 'LIKE', $area)
            ->where('suburb_town_city', 'LIKE', $city)
            ->where('category', 'LIKE', $category)
            ->select('company', 'division', 'id', 'province', 'area', 'suburb_town_city')
            ->get();

        return Datatable::collection($serviceProviders)
            ->showColumns('company', 'division', 'province', 'area', 'suburb_town_city')
            ->addColumn('action', function ($ServiceProvider) {
            })
            ->searchColumns('company', 'division', 'province', 'area', 'suburb_town_city')
            ->orderColumns('company', 'division', 'province', 'area', 'suburb_town_city')
            ->make();
    }

    public function categoryAdd($category)
    {
        try {
            $id = DB::table('service_provider_categories')->insertGetId(
                ['category' => $category]
            );
        } catch (exception $e) {
            return Response::json('error', 400);
        }

        return $id;
    }

    public function categoryDelete($category)
    {
        $service_providers = $this->service_provider->whereCategory($category)->get();

        if (count($service_providers) == 0) {
            DB::table('service_provider_categories')->where('id', '=', $category)->delete();

            return 'complete';
        } else {
            return 'currently_used';
        }
    }

    public function searchServiceProviders()
    {
        $input = Request::all();

        if ($input['category'] == 'Service Provider Category:') {
            $input['category'] = 'all';
        }

        if ($input['Suburb'] == '') {
            $input['province'] = 'south-africa';
            $input['suburb_town_city'] = 'all-cities';
            $input['area'] = 'all-suburbs';
        } else {
            $areaArray = explode(',', Request::get('Suburb'));
            $input['province'] = $areaArray[0];
            $input['area'] = $areaArray[1];
            $input['suburb_town_city'] = $areaArray[2];
        }

        $province = Slugify::slugify($input['province']);
        $city = Slugify::slugify($input['suburb_town_city']);
        $area = Slugify::slugify($input['area']);
        $category = Slugify::slugify($input['category']);

        return Redirect::route('portalRegisteredServiceProvider_path', ['province' => $province, 'city' => $city, 'area' => $area, 'category' => $category]);
    }
}
