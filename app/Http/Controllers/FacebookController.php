<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use In2Assets\Core\CommandBus;
use In2Assets\Registration\RegisterUserCommand;
use In2Assets\Users\User;
use In2Assets\Users\UserRepository;

/**
 * Created by PhpStorm.
 * User: Neeraj
 * Date: 2015/11/03
 * Time: 12:40 PM.
 */
class FacebookController extends Controller
{
    use CommandBus;

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function setupFacebook()
    {
        $facebook = new Facebook(config('facebook'));
        $params = [
            'redirect_uri' => url('/loginfb/fb/callback'),
            'scope' => 'email',
        ];
        //$facebook->getLoginUrl(array('req_perms' => 'email'))
        return redirect($facebook->getLoginUrl($params));
    }

    public function loginFacebook()
    {
        $code = Request::get('code');
        if (strlen($code) == 0) {
            return redirect('/')->with('message', 'There was an error communicating with Facebook');
        }

        $facebook = new Facebook(config('facebook'));
        $uid = $facebook->getUser();

        if ($uid == 0) {
            return redirect('/')->with('message', 'There was an error');
        }

        $me = $facebook->api('/me', ['fields' => 'id,email,name,bio,education']);

//        $profile = Profile::whereUid($uid)->first();

        //Find user with matching socialid
        $socialid = User::wheresocialid($uid)->first();

        if (is_null($socialid)) {
            $names = explode(' ', $me['name']);
            //Save user
            $allow_contact = 1;
            $firstname = $names[0];
            $lastname = $names[1];
            if ((! array_key_exists('email', $me))) {
                $email = $me['id'].'@facebook.com';
            } else {
                $email = $me['email'];
            }

            $password = $uid;
            $phonenumber = '';
            $cellnumber = '';

            //check if email exists
            //else register them below
            $userCheck = User::whereEmail($email)->first();
            if (is_null($userCheck)) {
                $user = $this->execute(
                    new RegisterUserCommand('', $firstname, $lastname, '', '', '', '', '', '', '', $email, $password, $phonenumber, $cellnumber, '', '', '', $allow_contact)
                );
                //Save facebook uid
                $user->socialid = $uid;
                $user->save();

                //Authenticate User
                Auth::login($user);

                Auth::user()->roles()->attach(11);

                return Redirect::route('home');
            } else {
                //Save facebook uid
                $userCheck->socialid = $uid;
                $userCheck->save();

                //Authenticate User
                Auth::login($userCheck);

                return Redirect::route('home');
            }

//            $user->photo = 'https://graph.facebook.com/'.$me['username'].'/picture?type=large';
        } else {
            $user = User::wheresocialid($uid)->first();

            Auth::login($user);

            return Redirect::route('home');
        }

        return redirect('/')->with('message', 'Logged in with Facebook');
    }
}
