<?php

namespace App\Http\Controllers;

use App\In2Assets\Blogs\BlogRepository;
use App\In2Assets\Core\CommandBus;
use App\In2Assets\Forms\PublishBlogForm;
use App\Models\Auction;
use App\Models\AuctionRegister;
use App\Models\BrokerLogos;
use App\Models\Lead;
use App\Models\PhysicalAuctionRegister;
use App\Models\Property;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BlogRepository $blogRepository, PublishBlogForm $publishBlogForm, Property $property,
                                Auction $auction, BrokerLogos $broker_logos, Lead $lead, AuctionRegister $auction_register,
                                PhysicalAuctionRegister $physical_auction_register)
    {
        $this->middleware('auth');
        $this->blogRepository = $blogRepository;
        $this->publishBlogForm = $publishBlogForm;
        $this->property = $property;
        $this->auction = $auction;
        $this->broker_logos = $broker_logos;
        $this->lead = $lead;
        $this->auction_register = $auction_register;
        $this->physical_auction_register = $physical_auction_register;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provincesDB = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areasDB = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $cities = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $provinces = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas = implode('", "', $areas);

        $featured = App::make(\App\Http\Controllers\PropertyController::class)->getFeaturedProperties('property');

        $blogs = $this->blogRepository->getPaginated();
        $references = [];
        $propertiesDB = $this->property->select('reference')->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])->get();
        foreach ($propertiesDB as $reference) {
            $references[] = $reference->reference;
        }
        $references = implode('", "', $references);

        $propertiesProvince = $this->property
            ->selectRaw('trim(province) as province_trim, count(province) as count, count(*)')
            ->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
            ->orderBy('count', 'desc')
            ->groupBy('province_trim')
            ->get();

        $propertiesCity = $this->property
            ->selectRaw('trim(area) as area_trim, trim(province) as province_trim, count(area) as count, count(*)')
            ->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->whereIn('vetting_status', [2, 4, 5, 10, 12, 13, 15, 16])
            ->orderBy('count', 'desc')
            ->groupBy('area_trim', 'province_trim')
            ->get();

        $slug = new Slugify();

        return view('pages.home', ['references' => $references, 'provinces' => $provinces, 'areas' => $areas,
            'cities' => $cities, 'blogs' => $blogs, 'featured' => $featured, 'propertiesProvince' => $propertiesProvince,
            'slug' => $slug, 'propertiesCity' => $propertiesCity, ]);
    }
}
