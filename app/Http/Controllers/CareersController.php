<?php

namespace App\Http\Controllers;

use App\In2Assets\Forms\careerForm;
use App\Mail\newLead;
use App\Models\CareerApplicant;
use App\Models\Careers;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class CareersController extends Controller
{
    protected $careers;

    protected $uploadForm;

    public function __construct(Careers $careers, CareerApplicant $career_applicant, careerForm $careerForm)
    {
        $this->careers = $careers;
        $this->careerForm = $careerForm;
        $this->career_applicant = $career_applicant;
    }

    public function create()
    {
        $oldJobs = $this->careers->withTrashed()->pluck('title', 'id');
        $categories = DB::table('career_category')->pluck('category', 'id');

        return view::make('careers.admin.create', ['oldJobs' => $oldJobs, 'categories' => $categories]);
    }

    public function getJobinfo()
    {
        $title = Request::get('title');
        $jobInfo = $this->careers->whereTitle($title)->orderby('id', 'asc')->first();

        return $jobInfo;
    }

    public function store()
    {
        $input = Request::only('title', 'city', 'description', 'category_id');

        if (! $this->careers->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->careers->errors);
        }

        $this->careers->save();

        return redirect()->route('admin.index');
    }

    public function show($id)
    {
        $category = DB::table('career_category')->select('category')->whereId($id)->first();
        $jobs = $this->careers->whereCategory_id($id)->get();

        return view::make('careers.show', ['jobs' => $jobs, 'category' => $category->category]);
    }

    public function showAdmin($id)
    {
        $job = $this->careers->whereId($id)->first();
        $applicants = DB::table('career_applicant')->whereCareer_id($id)->get();

        return view::make('careers.showadmin', ['job' => $job, 'applicants' => $applicants]);
    }

    public function uploadCV($id)
    {
        return view::make('careers.uploadCV', ['id' => $id]);
    }

    public function saveCV()
    {
        $validate = Validator::make(Request::all(), [
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validate->fails()) {
            foreach ($validate->errors()->all() as $error) {
                Flash::message('Please check the "I am not a robot" field.');
            }

            return redirect()->back()->withInput(Request::all());
        }

        $this->careerForm->validate(Request::all());
        $file = Request::file('file');

        $jobId = Request::get('jobid');
        $jobFirstname = Request::get('firstname');
        $jobLastname = Request::get('lastname');
        $jobEmail = Request::get('email');
        $jobContact = Request::get('contact');
        $extension = File::extension($file->getClientOriginalName());
        $directory = 'uploads/careers/'.$jobId.'/';
        $filename = 'careerFile_'.Str::random(12).'.'.$extension;

        $upload_success = Request::file('file')->move($directory, $filename);
        $file_location = $directory.$filename;
        $file_type = Request::get('type');

        $career = $this->careers->whereId($jobId)->first();

        if ($upload_success) {
            $this->career_applicant->career_id = $jobId;
            $this->career_applicant->firstname = $jobFirstname;
            $this->career_applicant->lastname = $jobLastname;
            $this->career_applicant->email = $jobEmail;
            $this->career_applicant->contact = $jobContact;
            $this->career_applicant->file = $filename;
            $this->career_applicant->save();

            $email = 'auctions@in2assets.com';
            $subject = 'A CV has been uploaded for the following position: '.$career->title;
            $name = 'Dear Administration,<br>';
            $emailData = '<p>An applicant by the name of '.$jobFirstname.' '.$jobLastname.
                ' has applied for the position of '.$career->title.'</p>
                <p>You can contact them on '.$jobContact.' or email them at '.$jobEmail.'</p>
                <p>View their email <a href="https://www.in2assets.co.za/'.$file_location.'" target="_blank">'.$jobFirstname.' '.$jobLastname.
                ' CV</a></p>';

            // Queue email
            Mail::to('auctions@in2assets.com')
                ->cc(['rmoodley@in2assets.com', 'erich@in2assets.com'])
                ->send(new newLead($subject, $name, $emailData, $email));

            Flash::message('Thank you, your CV has been submitted');

            return redirect()->route('about');
        } else {
            Flash::danger('There was an error uploading your file');

            return redirect()->route('about');
        }
    }

    // List blogs in Admin
    public function listAdmin()
    {
        return view('careers.indexadmin');
    }

    // Admin List Careers datatable
    public function getDatatable()
    {
        $careerList = DB::select('select careers.id,title,description,city,deleted_at,career_category.category, careers.created_at from careers left join career_category ON careers.category_id = career_category.id where deleted_at is null');

        foreach ($careerList as $career) {
            if ($career->id == 0) {
                $career->actions = '<a href="/admin/career/'.$career->id.'/edit" title="Edit" ><i class="i-circled i-light i-small icon-edit"></i></a>
                    <a href="/admin/career/'.$career->id.'" title="View CVs" ><i class="i-circled i-light i-small icon-file"></i></a>';
            } else {
                $career->actions = '<a href="/admin/career/delete/'.$career->id.'" title="Delete" ><i class="i-circled i-light i-small icon-minus-sign"></i></a>
                    <a href="/admin/career/'.$career->id.'/edit" title="Edit" ><i class="i-circled i-light i-small icon-edit"></i></a>
                    <a href="/admin/career/'.$career->id.'" title="View CVs" ><i class="i-circled i-light i-small icon-file"></i></a>';
            }
        }

        return Datatables::of($careerList)
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function edit($id)
    {
        $career = $this->careers->whereId($id)->first();
        $categories = DB::table('career_category')->pluck('category', 'id');

        return view::make('careers.admin.edit', ['career' => $career, 'categories' => $categories]);
    }

    public function update($id)
    {
        $career = $this->careers->whereId($id)->first();

        $input = Request::only('title', 'city', 'description', 'category_id');

        if (! $career->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->careers->errors);
        }

        $career->update();

        return redirect()->route('admin.career');
    }

    public function destroy($id)
    {
        $this->careers->whereId($id)->first()->delete();

        return redirect()->back();
    }
}
