<?php

namespace App\Http\Controllers;

use App\Models\PhysicalAuction;
use App\Models\Property;
use App\Models\Report;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class ReportsController extends Controller
{
    protected $report;

    public function __construct(Report $report, PhysicalAuction $physical_auction, Property $property)
    {
        $this->report = $report;
        $this->physical = $physical_auction;
        $this->property = $property;
    }

    public function usersByMonth()
    {
        return view('users.reports.usersByMonth');
    }

    public function PropertyViews()
    {
        return view('property.reports.PropertyViews');
    }

    public function printUsersByMonth()
    {
        $input = Request::all();
        $checkYear = date('Y', strtotime($input['month_year']));
        $checkMonth = date('m', strtotime($input['month_year']));

        $userDets = DB::select('select title, firstname, lastname, companyname, street_address, suburb_town_city, area, province, country,
        postcode, email, allow_contact, phonenumber, cellnumber, communicationpref, newsletter, created_at, 
        updated_at, socialprovider, interest_res, interest_com, interest_farm, interest_auc from `users` inner join `user_interest` on
        `users`.`id` = `user_interest`.`users_id` where YEAR(created_at) = '.$checkYear.' AND MONTH(created_at) = '.$checkMonth);

        function convert(&$item, $key)
        {
            $item = (array) $item;
        }

        array_walk($userDets, 'convert');

        Excel::create('Filename', function ($excel) use ($input, $userDets) {
            // Set the title
            $excel->setTitle('Users for'.$input['month_year']);

            // Chain the setters
            $excel->setCreator('In2Assets')
                ->setCompany('In2Assets');

            // Call them separately
            $excel->setDescription('Users for'.$input['month_year']);

            $excel->sheet('Sheetname', function ($sheet) use ($userDets) {

                // Sheet manipulation
                $sheet->setOrientation('landscape');

                $sheet->fromArray($userDets);
            });
        })->export('xls');
    }

    public function printPropertyViews()
    {
        $input = Request::all();
        $start_date = date('Y-m-d 00:00:00', strtotime($input['start_date']));
        $end_date = date('Y-m-d 23:59:59', strtotime($input['end_date']));

        $query = "SELECT count(property_id) AS number_of_views, property.street_number, property.street_address,
                                             users.firstname, users.lastname, users.email
                                      FROM property_views
                                      LEFT JOIN property ON property.id = property_views.property_id
                                      LEFT JOIN users ON users.id = property_views.user_id
                                      WHERE viewed_on >= '".$start_date."' AND viewed_on <= '".$end_date."'
                                      AND property_views.user_id <> 1
                                      GROUP BY property_id, property_views.user_id";

        $property_views = DB::select($query);

        function convert(&$item, $key)
        {
            $item = (array) $item;
        }

        array_walk($property_views, 'convert');

        Excel::create('Filename', function ($excel) use ($input, $property_views) {
            // Set the title
            $excel->setTitle('Property Views From'.$input['start_date'].' until '.$input['end_date']);

            // Chain the setters
            $excel->setCreator('In2Assets')
                ->setCompany('In2Assets');

            // Call them separately
            $excel->setDescription('Property Views From'.$input['start_date'].' until '.$input['end_date']);

            $excel->sheet('Sheetname', function ($sheet) use ($property_views) {

                // Sheet manipulation
                $sheet->setOrientation('portrait');

                $sheet->fromArray($property_views);
            });
        })->export('xls');

        return view('property.reports.PropertyViews');
    }

    public function reportHome()
    {
        $reports = $this->report->orderBy('name')->pluck('name', 'id');

        return view('reports.home', ['reports' => $reports]);
    }

    public function reportRun()
    {
        $input = Request::all();
        $reports = $this->report->orderBy('name')->pluck('name', 'id');
        $actual_report = $this->report->whereId($input['report'])->first();
        $query = $actual_report->script;
        $query .= ' HAVING start_date >= "'.date('Y-m-d 00:00:00', strtotime($input['start_date'])).'"
        AND end_date <= "'.date('Y-m-d 00:00:00', strtotime($input['end_date'])).'"';
        $query .= ' '.$actual_report->end_script;

        $report_details = DB::select($query);

        $headings = [];
        if (! empty($report_details)) {
            $single_instance[] = get_object_vars($report_details[0]);
            array_walk_recursive($single_instance, function ($item, $key) use (&$headings) {
                if ($key != 'start_date' && $key != 'end_date') {
                    $headings[] = ucfirst($key);
                }
            });

            if (isset($_POST['excel-report'])) {
                function convert(&$item, $key)
                {
                    $item = (array) $item;
                }

                array_walk($report_details, 'convert');

                Excel::create('Filename', function ($excel) use ($input, $report_details) {
                    // Set the title
                    $excel->setTitle($input['report'].' from '.$input['start_date'].' until '.$input['end_date']);

                    // Chain the setters
                    $excel->setCreator('In2Assets')
                        ->setCompany('In2Assets');

                    // Call them separately
                    $excel->setDescription('Property Views From'.$input['start_date'].' until '.$input['end_date']);

                    $excel->sheet('Sheetname', function ($sheet) use ($report_details) {

                        // Sheet manipulation
                        $sheet->setOrientation('portrait');

                        $sheet->fromArray($report_details);
                    });
                })->export('xls');
            }
        } else {
            Flash::message('Your Date selection has returned no results');

            return redirect()->route('admin.reports')->withInput();
        }

        return view('reports.run', ['input' => $input, 'reports' => $reports, 'report_details' => $report_details,
            'headings' => $headings, 'actual_report' => $actual_report, ]);
    }

    public function getReport()
    {
        $input = Request::all();
        $report = $this->report->whereId($input['report'])->first();
        $query = $report->script;
        $query .= ' HAVING start_date >= "'.date('Y-m-d 00:00:00', strtotime($input['start_date'])).'"
        AND end_date <= "'.date('Y-m-d 00:00:00', strtotime($input['end_date'])).'"';
        $query .= $report->end_script;

        $report_details = DB::select($query);

        $headings = [];
        $single_instance[] = get_object_vars($report_details[0]);
        array_walk_recursive($single_instance, function ($item, $key) use (&$headings) {
            if ($key != 'start_date' && $key != 'end_date') {
                $headings[] = $key;
            }
        });

        return Datatable::collection(new Collection($report_details))
            ->showColumns($headings)
            ->searchColumns($headings)
            ->orderColumns($headings)
            ->make();
    }

    public function index()
    {
        return view('reports.index');
    }

    public function create()
    {
        return view('reports.create');
    }

    public function store()
    {
        $input = Request::all();
        if (! $this->report->fill($input)->isValid()) {
            Flash::message($this->report->errors);

            return redirect()->back()->withInput();
        }

        $this->report->save();

        return redirect()->route('admin.reports');
    }

    public function edit($id)
    {
        $report = $this->report->whereId($id)->first();

        return view('reports.edit', ['report' => $report]);
    }

    public function update($id)
    {
        $input = Request::all();

        $this->report = $this->report->whereId($id)->first();

        $this->report->fill($input)->save();

        return redirect()->route('reports.index');
    }

    public function delete($id)
    {
        $report = $this->report->whereId($id)->first();
        $report->delete();

        return redirect()->route('reports.index');
    }

    public function getReports()
    {
        $reports = $this->report->get();

        return Datatable::collection($reports)
            ->showColumns('name', 'description')
            ->addColumn('actions', function ($Reports) {
                return '<a href="/admin/report/'.$Reports->id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                        <a href="/admin/report/'.$Reports->id.'/delete" title="Delete" ><i class="i-circled i-light i-alt i-small icon-remove" style="background-color:#FF3100" ></i></a>';
            })
            ->searchColumns('name', 'description')
            ->orderColumns('name', 'description')
            ->make();
    }

    public function testScript()
    {
        $script = Request::get('script').' '.Request::get('end_script');

        if (stripos($script, 'UPDATE') !== false || stripos($script, 'DELETE') !== false ||
            stripos($script, 'TRUNCATE') !== false || stripos($script, 'DROP') !== false ||
            stripos($script, 'INSERT') !== false || stripos($script, 'ALTER') !== false
        ) {
            return 'Error_1';
        }

        try {
            DB::SELECT(DB::raw($script));
            //If the exception is thrown, this text will not be shown
            return 'Correct';
        } //catch exception
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getFullArea()
    {
        $report_id = Request::get('report_id');

        $report = $this->report->whereId($report_id)->first();

        return $report;
    }

    public function printLeads()
    {
        $input = Request::all();

        $search_note = Request::get('search_note');

        if ($search_note != '%') {
            $searchable_note = '%'.$search_note.'%';
        } else {
            $searchable_note = '%';
        }

        $query = 'SELECT pn.lead_id FROM prop_notes pn WHERE pn.note LIKE "'.$searchable_note.'" AND lead_id IS NOT NULL
        GROUP BY pn.lead_id';

        $db_results = DB::select($query);
        $lead_array = [];
        foreach ($db_results as $result) {
            $lead_array[] = $result->lead_id;
        }
        $agent = Request::get('agent');
        $property_type = Request::get('property_type');
        $lead_ref_type = Request::get('lead_ref_type');
        $contact_first = Request::get('contact_first');
        $contact_last = Request::get('contact_last');

        if ($agent == '%') {
            $agent_sql = ' AND (bdm_allocated LIKE "%" OR bdm_allocated IS NULL)';
        } else {
            $agent_sql = ' AND bdm_allocated LIKE "'.$agent.'"';
        }

        if ($property_type == '%') {
            $property_type_sql = ' AND (property_type LIKE "%" OR property_type IS NULL)';
        } else {
            $property_type_sql = ' AND property_type LIKE "'.$property_type.'"';
        }

        if ($lead_ref_type == '%') {
            $lead_ref_type_sql = ' AND (lead_ref_type LIKE "%" OR lead_ref_type IS NULL)';
        } else {
            $lead_ref_type_sql = ' AND lead_ref_type LIKE "'.$lead_ref_type.'"';
        }

        if ($contact_first == '%') {
            $contact_first_sql = ' AND (lc.firstname LIKE "%" OR lc.firstname IS NULL OR lc.firstname LIKE "%" OR lc.firstname IS NULL)';
            $user_first_sql = '';
        } else {
            $contact_first_sql = ' AND ((lc.firstname LIKE "'.$contact_first.'"';
            $user_first_sql = ' OR (u.firstname LIKE "'.$contact_first.'"';
        }

        if ($contact_last == '%') {
            $contact_last_sql = ' AND (lc.lastname LIKE "%" OR lc.lastname IS NULL OR u.lastname LIKE "%" OR u.lastname IS NULL)';
            $user_last_sql = '';
        } elseif ($contact_first == $contact_last) {
            $contact_last_sql = ' OR lc.lastname LIKE "'.$contact_last.'")';
            $user_last_sql = ' OR u.lastname LIKE "'.$contact_last.'"))';
        } else {
            $contact_last_sql = ' AND lc.lastname LIKE "'.$contact_last.'")';
            $user_last_sql = ' AND u.lastname LIKE "'.$contact_last.'"))';
        }

        if ($searchable_note == '%') {
            $query = 'SELECT l.*, lc.firstname, lc.lastname, u.firstname, u.lastname
                      FROM leads l
                      LEFT JOIN lead_contacts lc ON l.id = lc.lead_id
                      LEFT JOIN lead_user lu ON l.id = lu.lead_id
                      LEFT JOIN users u ON lu.user_id = u.id
                      WHERE l.deleted_at IS NULL'.$agent_sql.$property_type_sql.$lead_ref_type_sql.
                $contact_first_sql.$contact_last_sql.$user_first_sql.$user_last_sql;
        } else {
            $query = 'SELECT l.*, lc.firstname, lc.lastname, u.firstname, u.lastname
                      FROM leads l
                      LEFT JOIN lead_contacts lc ON l.id = lc.lead_id
                      LEFT JOIN lead_user lu ON l.id = lu.lead_id
                      LEFT JOIN users u ON lu.user_id = u.id
                      WHERE l.deleted_at IS NULL'.$agent_sql.$property_type_sql.$lead_ref_type_sql.
                $contact_first_sql.$contact_last_sql.$user_first_sql.$user_last_sql.
                ' AND l.id IN ('.implode(',', $lead_array).')';
        }

        $leads = DB::select($query);

        function convert(&$item, $key)
        {
            $item = (array) $item;
        }

        array_walk($leads, 'convert');

        Excel::create('Filename', function ($excel) use ($input, $leads) {
            // Set the title
            $excel->setTitle('Lead print out '.date('Y-m-d'));

            // Chain the setters
            $excel->setCreator('In2Assets')
                ->setCompany('In2Assets');

            // Call them separately
            $excel->setDescription('Lead print out '.date('Y-m-d'));

            $excel->sheet('Sheetname', function ($sheet) use ($leads) {

                // Sheet manipulation
                $sheet->setOrientation('landscape');

                $sheet->fromArray($leads);
            });
        })->export('xls');
    }

    public function auctionDetails()
    {
        return view('reports.auction-details');
    }

    public function getAuctionsTable()
    {
        $physical_auctions = $this->physical->get();

        return Datatable::collection($physical_auctions)
            ->showColumns('start_date', 'auction_name', 'street_address')
            ->addColumn('actions', function ($PhysicalAuction) {
                return '<a href="/admin/report/'.$PhysicalAuction->id.'/auction-report" title="Edit" >
                <i class="i-circled i-light i-alt i-small icon-edit"></i>
                </a>';
            })
            ->searchColumns('start_date', 'auction_name', 'street_address')
            ->orderColumns('start_date', 'auction_name', 'street_address')
            ->make();
    }

    public function auctionReport($id)
    {
        $startDate = '2016-01-01';
        $endDate = date('Y-m-d');
        $metrics = 'ga:pageviews';

        $physical_auction = $this->physical->whereId($id)->first();

        $properties = DB::table('property')
            ->join('auction', 'auction.property_id', '=', 'property.id')
            ->join('vetting', 'vetting.property_id', '=', 'property.id')
            ->where('auction.physical_auction', '=', $id)
            ->get();

        foreach ($properties as $property) {
            $others = ['filters' => 'ga:pagePath=~/property/'.$property->property_id.'/*', 'dimensions' => 'ga:pagePath', 'sort' => '-ga:pageviews'];
//            $others2 = ['dimensions' => 'ga:acquisitionTrafficChannel'];

            $adperformance = ['filters' => 'ga:adDestinationUrl=~https://www.in2assets.co.za/property/'.$property->property_id.'/*', 'dimensions' => 'ga:adDestinationUrl'];
//            $adperformance = ['dimensions' => 'ga:adDestinationUrl'];

            try {
                $analyticsData = AnalyticsReports::performQuery($startDate, $endDate, $metrics, $others);
            } catch (exception $e) {
            }

            try {
                $analyticsData = AnalyticsReports::performQuery($startDate, $endDate, $metrics, $others);

                $analyticsadperformance = AnalyticsReports::performQuery($startDate, $endDate, 'ga:impressions,ga:adclicks,ga:adCost,ga:CPC,ga:CTR', $adperformance);
//                de($analyticsadperformance['rows'][0][1]);
//                dd($analyticsadperformance['rows'][0][2]);
//                $analyticsData2 = AnalyticsReports::performQuery($startDate, $endDate, 'ga:pageviews', $others2);
//
//                dd($analyticsData2);

                $property->impressions = $analyticsadperformance['rows'][0][1];
                $property->adclicks = $analyticsadperformance['rows'][0][2];
                $property->adCost = $analyticsadperformance['rows'][0][3];
                $property->CPC = $analyticsadperformance['rows'][0][4];
                $property->CTR = $analyticsadperformance['rows'][0][5];
                $property->views = $analyticsData['rows'][0][1];

                try {
                    $property->views = $analyticsData['rows'][0][1] + $analyticsData['rows'][1][1];
                } catch (exception $e) {
                }

                try {
                    $property->views = $analyticsData['rows'][0][1] + $analyticsData['rows'][1][1] + $analyticsData['rows'][2][1];
                } catch (exception $e) {
                }
            } catch (exception $e) {
                $property->impressions = 0;
                $property->adclicks = 0;
                $property->adCost = 0;
                $property->CPC = 0;
                $property->CTR = 0;
                $property->views = 0;
            }
            $propertyInfo[$property->property_id]['views'] = $property->views;
            $propertyInfo[$property->property_id]['impressions'] = $property->impressions;
            $propertyInfo[$property->property_id]['adclicks'] = $property->adclicks;
        }

        return view('reports.auction-report', ['properties' => $properties, 'physical_auction' => $physical_auction,
            'propertyInfo' => $propertyInfo, ]);
    }

    public function getPropNotes()
    {
        $property_id = Request::get('property_id');

        $prop_notes = DB::table('prop_notes')
            ->join('users', 'users.id', '=', 'prop_notes.users_id')
            ->leftJoin('leads', 'leads.id', '=', 'prop_notes.lead_id')
            ->select('*')
            ->where('prop_notes.property_id', '=', $property_id)
            ->where(function ($q) {
                $q->whereNull('leads.deleted_at')
                    ->orWhereNotNull('leads.deleted_at');
            })
            ->orWhere('leads.property_id', '=', $property_id)
            ->get();

        return $prop_notes;
    }

    public function getEnquiries()
    {
        $property_id = Request::get('property_id');

        $prop_notes = DB::table('prop_notes')
            ->join('users', 'users.id', '=', 'prop_notes.users_id')
            ->join('leads', 'leads.id', '=', 'prop_notes.lead_id')
            ->select('*')
            ->where('leads.property_id', '=', $property_id)
            ->where(function ($q) {
                $q->whereNull('leads.deleted_at')
                    ->orWhereNotNull('leads.deleted_at');
            })
            ->whereLead_ref('Property Enquiry')
            ->orderBy('leads.id')
            ->get();

//        dd(AppHelper::getLastQuery());

        return $prop_notes;
    }

    public function getSocialMedia($properties)
    {
        $startDate = '2016-01-01';
        $endDate = date('Y-m-d');

        foreach ($properties as $property) {
            $referrers = AnalyticsReports::performQuery($startDate, $endDate, 'ga:organicSearches', ['filters' => 'ga:pagePath=~/property/'.$property->property_id.'/*', 'dimensions' => 'ga:socialNetwork']);

            $property->facebook = $referrers['rows'][1][1];
            $property->linked = $referrers['rows'][2][1];
            $property->twitter = $referrers['rows'][3][1];
        }
    }
}
