<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class CompetitionController extends Controller
{
    public function __construct(Competition $Competition)
    {
        $this->competition = $Competition;
    }

    public function show()
    {
        return view('competition.show');
    }

    public function store()
    {
        $input = Request::only('firstname', 'lastname', 'email', 'cell', 'type');

        $last_nine = '%'.substr($input['cell'], -9);

        $competition = $this->competition->where('cell', 'LIKE', $last_nine)->get();

        if (count($competition) == 0) {
            if (! $this->competition->fill($input)->isValid()) {
                return Redirect::back()->withInput()->withErrors($this->competition->errors);
            }
            $this->competition->save();

            return view('competition.home', ['type' => 'cupid', 'input' => $input]);
        } elseif (count($competition) == 1 && $input['type'] == 'cupid') {
            return view('competition.home', ['type' => 'already', 'input' => $input]);
        } elseif (count($competition) == 1 && $input['type'] != 'cupid') {
            $this->competition->fill($input)->save();

            return view('competition.home', ['type' => $input['type'], 'input' => $input]);
        } elseif (count($competition) == 2) {
            $socialCheck = $this->competition->where('cell', 'LIKE', $last_nine)->where('type', '!=', 'cupid')->get();
            if ($socialCheck[0]['type'] == $input['type']) {
                return view('competition.home', ['type' => $input['type'].'already', 'input' => $input]);
            } else {
                $this->competition->fill($input)->save();
            }
        }

        return view('competition.home', ['type' => 'both', 'input' => $input]);
    }
}
