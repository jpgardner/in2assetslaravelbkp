<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ZoneType;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class ZoneTypeController extends Controller
{
    public function __construct(ZoneType $zone_types)
    {
        $this->zone_types = $zone_types;
    }

    public function index($status)
    {
        return view('zone_type.index', ['status' => $status]);
    }

    public function create()
    {
        return view('zone_type.create');
    }

    public function store()
    {
        $input = Request::all();
        $this->zone_types->fill($input)->save();

        return view('zone_type.index', ['status' => 'all']);
    }

    public function edit($id)
    {
        $zone_type = $this->zone_types->whereId($id)->first();

        return view('zone_type.edit', ['zone_type' => $zone_type]);
    }

    public function update($id)
    {
        $input = Request::all();
        $this->zone_types = $this->zone_types->whereId($id)->first();

        $this->zone_types->fill($input)->update();

        return view('zone_type.index', ['status' => 'all']);
    }

    public function getZoneTypes($status)
    {
        if ($status == 'all') {
            $zoneTypes = $this->zone_types->get();
        } else {
            $zoneTypes = $this->zone_types->whereActive($status)->get();
        }

        return Datatable::collection($zoneTypes)
            ->showColumns('type')
            ->addColumn('active', function ($ZoneType) {
                if ($ZoneType->active == 1) {
                    return 'Yes';
                } else {
                    return 'No';
                }
            })
            ->addColumn('action', function ($ZoneType) {
                return '<a href="/admin/zone-types/'.$ZoneType->id.'/edit" title="Edit Logo" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>';
            })
            ->searchColumns('type')
            ->orderColumns('type')
            ->make();
    }
}
