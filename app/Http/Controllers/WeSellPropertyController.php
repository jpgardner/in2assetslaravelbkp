<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Commercial;
use App\Models\Farm;
use App\Models\Ownattourney;
use App\Models\Property;
use App\Models\Residential;
use App\Models\Vetting;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use In2Assets\Core\CommandBus;
use In2Assets\Uploads\FileRepository;
use In2Assets\Uploads\Upload;
use In2Assets\Users\User;
use Laracasts\Flash\Flash;

class WeSellPropertyController extends Controller
{
    protected $property;

    public function __construct(Property $property, User $user, Vetting $vetting, Auction $auction, Auctionbids $auction_bids, AuctionRegister $auction_register, PhysicalAuctionRegister $physical_auction_register,
     Featured $featured, Ownattourney $attourney, ServiceProvider $ServiceProvider, PhysicalAuction $physical_auction)
    {
        $this->property = $property;
        $this->user = $user;
        $this->vetting = $vetting;
        $this->auction = $auction;
        $this->auction_bids = $auction_bids;
        $this->auction_register = $auction_register;
        $this->physical_auction_register = $physical_auction_register;
        $this->physical_auction = $physical_auction;
        $this->featured = $featured;
        $this->attourney = $attourney;
        $this->service_provider = $ServiceProvider;
    }

    public function setc($count)
    {
        $cookie = Cookie::forever('paginatecount', $count);

        return response()->withCookie($cookie);
    }

    public function getc()
    {
        $value = Cookie::get('paginatecount');

        return $value;
    }

    public function ajax()
    {
        if (Request::ajax()) {
            $paginate = Cookie::get('paginatecount');

            $listing = 'wesell';

            $provinceInput = Request::get('province');
            if ($provinceInput != 'all') {
                $province = '%'.$provinceInput;
            } else {
                $province = '%';
            }

            $city_Input = Request::get('city');
            if ($city_Input != 'all') {
                $city = '%'.$city_Input;
            } else {
                $city = '%';
            }

            $suburb_Input = Request::get('suburb');
            if ($suburb_Input != 'all') {
                $suburb = '%'.$suburb_Input;
            } else {
                $suburb = '%';
            }

            $order = Request::get('order');
            if ($order == '0' && $listing == 'weauction') {
                $order = '4';
            }

            $bedrooms = Request::get('bedrooms');

            $dwelling_type = Request::get('dwelling_type');

            switch ($order) {
                case '1':
                $properties = $this->property->
                select('property.*')->
                with('residential')->with('commercial')->with('farm')->with('auction')->with('vetting')->
                join('vetting', 'vetting.property_id', '=', 'property.id')->
                join('residential', 'residential.property_id', '=', 'property.id')->
                where(function ($bq) use ($bedrooms) {
                    switch ($bedrooms) {
                        case 'any':
                            break;
                        case '2+':
                            $bq->where('bedrooms', '<>', '1 Bedroom');

                            break;
                        case '3+':
                            $bq->where('bedrooms', '<>', '1 Bedroom')->where('bedrooms', '<>', '2 Bedrooms');

                            break;
                        case '4+':
                            $bq->where('bedrooms', '=', '4 Bedrooms')->orWhere('bedrooms', '=', '5 Bedrooms or more');

                            break;
                        case '5+':
                            $bq->where('bedrooms', '=', '5 Bedrooms or more');

                            break;
                        default:
                            break;
                    }
                })->
                where(function ($dq) use ($dwelling_type) {
                    switch ($dwelling_type) {
                        case 'any':
                            break;
                        default:
                            $dq->where('dwelling_type', '=', $dwelling_type);

                            break;
                    }
                })->
                where(function ($qa) use ($listing) {
                    switch ($listing) {
                        case 'welet':
                            $qa->where('property.listing_type', '=', 'welet')->
                            orWhere('property.listing_type', '=', 'in2rental');

                            break;
                        case 'wesell':
                            $qa->where('property.listing_type', '=', 'wesell');

                            break;
                        case 'weauction':
                            $qa->where('property.listing_type', '=', 'weauction');

                            break;
                        case '%':
                            $qa->where('property.listing_type', 'LIKE', '%');

                            break;
                    }
                })->
                where(function ($pq) use ($city, $suburb) {
                    if ($city == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $suburb);
                    } elseif ($suburb == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $city)->
                        orWhere('property.area', 'LIKE', $city);
                    } else {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $city);
                    }
                })->
                where(function ($pq) use ($city, $suburb) {
                    if ($city == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $suburb);
                    } elseif ($suburb == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $city)->
                        orWhere('property.area', 'LIKE', $city);
                    } else {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $city);
                    }
                })->
                where('property.province', 'LIKE', $province)->
                where(function ($q) {
                    $q->where('vetting_status', '=', 2)
                    ->orWhere('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 5)
                    ->orWhere('vetting_status', '=', 10)
                    ->orWhere('vetting_status', '=', 12)
                    ->orWhere('vetting_status', '=', 13)
                    ->orWhere('vetting_status', '=', 14)
                    ->orWhere('vetting_status', '=', 15)
                    ->orWhere('vetting_status', '=', 16);
                })->
                orderBy('property.price', 'desc')->
                paginate($paginate);

                break;

                case '2':
                $properties = $this->property->
                select('property.*')->
                with('residential')->with('commercial')->with('farm')->with('auction')->with('vetting')->
                join('vetting', 'vetting.property_id', '=', 'property.id')->
                join('residential', 'residential.property_id', '=', 'property.id')->
                where(function ($bq) use ($bedrooms) {
                    switch ($bedrooms) {
                        case 'any':
                            break;
                        case '2+':
                            $bq->where('bedrooms', '<>', '1 Bedroom');

                            break;
                        case '3+':
                            $bq->where('bedrooms', '<>', '1 Bedroom')->where('bedrooms', '<>', '2 Bedrooms');

                            break;
                        case '4+':
                            $bq->where('bedrooms', '=', '4 Bedrooms')->orWhere('bedrooms', '=', '5 Bedrooms or more');

                            break;
                        case '5+':
                            $bq->where('bedrooms', '=', '5 Bedrooms or more');

                            break;
                        default:
                            break;
                    }
                })->
                where(function ($dq) use ($dwelling_type) {
                    switch ($dwelling_type) {
                        case 'any':
                            break;
                        default:
                            $dq->where('dwelling_type', '=', $dwelling_type);

                            break;
                    }
                })->
                where(function ($qa) use ($listing) {
                    switch ($listing) {
                        case 'welet':
                            $qa->where('property.listing_type', '=', 'welet')->
                            orWhere('property.listing_type', '=', 'in2rental');

                            break;
                        case 'wesell':
                            $qa->where('property.listing_type', '=', 'wesell');

                            break;
                        case 'weauction':
                            $qa->where('property.listing_type', '=', 'weauction');

                            break;
                        case '%':
                            $qa->where('property.listing_type', 'LIKE', '%');

                            break;
                    }
                })->
                where(function ($pq) use ($city, $suburb) {
                    if ($city == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $suburb);
                    } elseif ($suburb == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $city)->
                        orWhere('property.area', 'LIKE', $city);
                    } else {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $city);
                    }
                })->
                where(function ($pq) use ($city, $suburb) {
                    if ($city == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $suburb);
                    } elseif ($suburb == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $city)->
                        orWhere('property.area', 'LIKE', $city);
                    } else {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $city);
                    }
                })->
                where('property.province', 'LIKE', $province)->
                where(function ($q) {
                    $q->where('vetting_status', '=', 2)
                    ->orWhere('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 5)
                    ->orWhere('vetting_status', '=', 10)
                    ->orWhere('vetting_status', '=', 12)
                    ->orWhere('vetting_status', '=', 13)
                    ->orWhere('vetting_status', '=', 14)
                    ->orWhere('vetting_status', '=', 15)
                    ->orWhere('vetting_status', '=', 16);
                })->
                orderBy('property.price', 'asc')->
                paginate($paginate);

                break;

                case '0':

                $properties = $this->property->
                select('property.*')->
                with('residential')->with('commercial')->with('farm')->with('auction')->with('vetting')->
                join('vetting', 'vetting.property_id', '=', 'property.id')->
                join('residential', 'residential.property_id', '=', 'property.id')->
                where(function ($bq) use ($bedrooms) {
                    switch ($bedrooms) {
                        case 'any':
                            break;
                        case '2+':
                            $bq->where('bedrooms', '<>', '1 Bedroom');

                            break;
                        case '3+':
                            $bq->where('bedrooms', '<>', '1 Bedroom')->where('bedrooms', '<>', '2 Bedrooms');

                            break;
                        case '4+':
                            $bq->where('bedrooms', '=', '4 Bedrooms')->orWhere('bedrooms', '=', '5 Bedrooms or more');

                            break;
                        case '5+':
                            $bq->where('bedrooms', '=', '5 Bedrooms or more');

                            break;
                        default:
                            break;
                    }
                })->
                where(function ($dq) use ($dwelling_type) {
                    switch ($dwelling_type) {
                        case 'any':
                            break;
                        default:
                            $dq->where('dwelling_type', '=', $dwelling_type);

                            break;
                    }
                })->
                where(function ($qa) use ($listing) {
                    switch ($listing) {
                        case 'welet':
                        $qa->where('property.listing_type', '=', 'welet')->
                        orWhere('property.listing_type', '=', 'in2rental');

                        break;
                        case 'wesell':
                        $qa->where('property.listing_type', '=', 'wesell');

                        break;
                        case 'weauction':
                        $qa->where('property.listing_type', '=', 'weauction');

                        break;
                        case '%':
                        $qa->where('property.listing_type', 'LIKE', '%');

                        break;
                    }
                })->
                where(function ($pq) use ($city, $suburb) {
                    if ($city == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $suburb);
                    } elseif ($suburb == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $city)->
                        orWhere('property.area', 'LIKE', $city);
                    } else {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $city);
                    }
                })->
                where('property.province', 'LIKE', $province)->
                where(function ($q) {
                    $q->where('vetting_status', '=', 2)
                    ->orWhere('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 5)
                    ->orWhere('vetting_status', '=', 10)
                    ->orWhere('vetting_status', '=', 12)
                    ->orWhere('vetting_status', '=', 13)
                    ->orWhere('vetting_status', '=', 14)
                    ->orWhere('vetting_status', '=', 15)
                    ->orWhere('vetting_status', '=', 16);
                })->
                orderBy('property.id')->
                paginate($paginate);

                break;

                case '4':

                $properties = $this->property->
                select('property.*')->
                with('residential')->with('commercial')->with('farm')->with('auction')->with('vetting')->
                join('vetting', 'vetting.property_id', '=', 'property.id')->
                join('auction', 'auction.property_id', '=', 'property.id')->
                where(function ($qa) use ($listing) {
                    switch ($listing) {
                        case 'welet':
                        $qa->where('property.listing_type', '=', 'welet')->
                        orWhere('property.listing_type', '=', 'in2rental');

                        break;
                        case 'wesell':
                        $qa->where('property.listing_type', '=', 'wesell');

                        break;
                        case 'weauction':
                        $qa->where('property.listing_type', '=', 'weauction');

                        break;
                        case '%':
                        $qa->where('property.listing_type', 'LIKE', '%');

                        break;
                    }
                })->
                where(function ($pq) use ($city, $suburb) {
                    if ($city == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $suburb);
                    } elseif ($suburb == '%') {
                        $pq->where('property.suburb_town_city', 'LIKE', $city)->
                        orWhere('property.area', 'LIKE', $city);
                    } else {
                        $pq->where('property.suburb_town_city', 'LIKE', $suburb)->
                        orWhere('property.area', 'LIKE', $city);
                    }
                })->
                where('property.province', 'LIKE', $province)->
                where(function ($q) {
                    $q->where('vetting_status', '=', 2)
                    ->orWhere('vetting_status', '=', 4)
                    ->orWhere('vetting_status', '=', 5)
                    ->orWhere('vetting_status', '=', 10)
                    ->orWhere('vetting_status', '=', 12)
                    ->orWhere('vetting_status', '=', 13)
                    ->orWhere('vetting_status', '=', 14)
                    ->orWhere('vetting_status', '=', 15)
                    ->orWhere('vetting_status', '=', 16);
                })->
                orderBy(DB::raw('ABS(DATEDIFF(auction.start_date, NOW()))'))->
                orderBy(DB::raw('ABS(DATEDIFF(auction.end_date, NOW()))'))->
                orderBy('auction.auction_type')->
                orderBy('vetting_status')->
                paginate($paginate);

                break;
            }
            foreach ($properties as $property) {
                $property->id = $property->property_id;
            }

            return  $properties;
        }
    }

    public function getFeaturedProperties()
    {
        $featured = $this->property
        ->whereHas('featured', function ($q) {
            $dtToday = date('Y-m-d');
            $q->where('featured_property.start_date', '<=', $dtToday)->where('featured_property.end_date', '>=', $dtToday);
        })
        ->orWhere('property.featured', '=', 1)
        ->whereListing_type('wesell')
        ->with('residential')->with('commercial')->with('farm')->with('vetting')->with('auction')
        ->get();

        return $featured;
    }

    public function showMap($type)
    {
        $set_action_type = 'index';
        $set_all_type = 'all';
        $provinces = DB::table('areas')->groupBy('province')->get();
        $areas = DB::table('areas')->groupBy('area')->get();
        $cities = DB::table('areas')->groupBy('suburb_town_city')->get();
        $listing_type = Request::get('action_values');
        if ($listing_type !== 'property') {
            $listing = '%'.$listing_type;
        } else {
            $listing = '%';
        }
        $property_type = Request::get('category_values');

        if ($property_type !== 'all') {
            $property = '%'.$property_type;
        } else {
            $property = '%';
        }
        $provinceInput = Request::get('city');
        if ($provinceInput !== 'all') {
            $province = '%'.$provinceInput;
        } else {
            $province = '%';
        }
        $suburb_town_city_Input = Request::get('area');
        if ($suburb_town_city_Input !== 'all') {
            $suburb_town_city = '%'.$suburb_town_city_Input;
        } else {
            $suburb_town_city = '%';
        }

        switch ($type) {
            case 'all':
            $properties = Property::whereHas('vetting', function ($q) {
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            case 'approved':
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', 2);
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            case 'soldless60':
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', 4);
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            case 'soldover60':
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', 6);
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            case 'rentedless14':
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', 5);
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            case 'rentedover14':
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', 7);
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            case 'auction':
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', 9);
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            case 'auctionsold':
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', 10);
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();

            break;
            default:
            $properties = Property::whereHas('vetting', function ($q) {
                $q->where('vetting_status', '=', '%');
            })->with('residential')->with('commercial')->with('farm')->
            where('property.listing_type', 'LIKE', $listing)->
            where('property.property_type', 'LIKE', $property)->
            where('property.province', 'LIKE', $province)->
            where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
            orderBy('property.price', 'desc')->
            get();
        }

        return view('property.proptest', ['properties' => $properties, 'set_action_type' => $set_action_type, 'set_all_type' => $set_all_type, 'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas, 'type' => $type]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /property/create.
     *
     * @return Response
     */
    public function create($listing_type)
    {
        $this->beforefilter('guest');

        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $user = Auth::user();

        if ($listing_type == 'admin') {
            $agents = $this->user->whereHas('roles', function ($q) {
                $q->where('name', 'Agent');
            })->get();

            if (Auth::user()->hasRole('Admin')) {
                $brokers = DB::table('broker_logos')->select('id', 'broker_name')->whereDeleted_at(null)->get();

                return view('property.admin.create', ['cities' => $cities, 'agents' => $agents, 'brokers' => $brokers]);
            } else {
                return Redirect::route('home');
            }
        }

        return view('property.create', ['cities' => $cities, 'listing_type' => $listing_type, 'user' => $user]);
    }

    public function adminCreate()
    {
        $citiesDB = DB::table('areas')->groupBy('suburb_town_city')->get();
        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city;
        }
        $cities = implode('", "', $cities);

        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();

        return view('property.admin.create', ['cities' => $cities, 'agents' => $agents]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /property.
     *
     * @return Response
     */
    public function store()
    {

// check property section of form and validate

        $propertyInput = Request::only(['street_address', 'country', 'listing_type', 'property_type', 'x_coord', 'y_coord',
            'floor_space', 'land_size', 'land_measurement', 'sale_type', 'company', 'vat_num', 'reg_num', 'province',
            'area', 'suburb_town_city', ]);

        if ($propertyInput['land_measurement'] === 'HA') {
            $propertyInput['land_size'] = $propertyInput['land_size'] * 10000;
        }

        if ($propertyInput['floor_space'] == null) {
            $propertyInput['floor_space'] = 0;
        }

        //RESIDENTIAL
        //check whether amenity and security checkboxes have been checked and set input value to 1 if checked
        if (Request::get('dining_room')) {
            $dining_room = 1;
        } else {
            $dining_room = 0;
        }
        $propertyInput['dining_room'] = $dining_room;
        if (Request::get('entrance_hall')) {
            $entrance_hall = 1;
        } else {
            $entrance_hall = 0;
        }
        $propertyInput['entrance_hall'] = $entrance_hall;
        if (Request::get('tv_room')) {
            $tv_room = 1;
        } else {
            $tv_room = 0;
        }
        $propertyInput['tv_room'] = $tv_room;
        if (Request::get('study')) {
            $study = 1;
        } else {
            $study = 0;
        }
        $propertyInput['study'] = $study;
        if (Request::get('pets')) {
            $pets = 1;
        } else {
            $pets = 0;
        }
        $propertyInput['pets'] = $pets;
        if (Request::get('lounge')) {
            $lounge = 1;
        } else {
            $lounge = 0;
        }
        $propertyInput['lounge'] = $lounge;
        if (Request::get('kitchen')) {
            $kitchen = 1;
        } else {
            $kitchen = 0;
        }
        $propertyInput['kitchen'] = $kitchen;
        if (Request::get('recreation_room')) {
            $recreation_room = 1;
        } else {
            $recreation_room = 0;
        }
        $propertyInput['recreation_room'] = $recreation_room;
        if (Request::get('pantry')) {
            $pantry = 1;
        } else {
            $pantry = 0;
        }
        $propertyInput['pantry'] = $pantry;
        if (Request::get('laundry')) {
            $laundry = 1;
        } else {
            $laundry = 0;
        }
        $propertyInput['laundry'] = $laundry;
        if (Request::get('guest_toilet')) {
            $guest_toilet = 1;
        } else {
            $guest_toilet = 0;
        }
        $propertyInput['guest_toilet'] = $guest_toilet;
        if (Request::get('patio')) {
            $patio = 1;
        } else {
            $patio = 0;
        }
        $propertyInput['patio'] = $patio;
        if (Request::get('view')) {
            $view = 1;
        } else {
            $view = 0;
        }
        $propertyInput['view'] = $view;
        if (Request::get('tennis_court')) {
            $tennis_court = 1;
        } else {
            $tennis_court = 0;
        }
        $propertyInput['tennis_court'] = $tennis_court;
        if (Request::get('pool')) {
            $pool = 1;
        } else {
            $pool = 0;
        }
        $propertyInput['pool'] = $pool;
        if (Request::get('sprinklers')) {
            $sprinklers = 1;
        } else {
            $sprinklers = 0;
        }
        $propertyInput['sprinklers'] = $sprinklers;
        if (Request::get('aircon')) {
            $aircon = 1;
        } else {
            $aircon = 0;
        }
        $propertyInput['aircon'] = $aircon;
        if (Request::get('aerial')) {
            $aerial = 1;
        } else {
            $aerial = 0;
        }
        $propertyInput['aerial'] = $aerial;
        if (Request::get('dstv')) {
            $dstv = 1;
        } else {
            $dstv = 0;
        }
        $propertyInput['dstv'] = $dstv;
        if (Request::get('bic')) {
            $bic = 1;
        } else {
            $bic = 0;
        }
        $propertyInput['bic'] = $bic;
        if (Request::get('shower')) {
            $shower = 1;
        } else {
            $shower = 0;
        }
        $propertyInput['shower'] = $shower;
        if (Request::get('staff_quarters')) {
            $staff_quarters = 1;
        } else {
            $staff_quarters = 0;
        }
        $propertyInput['staff_quarters'] = $staff_quarters;
        if (Request::get('lapa_braai')) {
            $lapa_braai = 1;
        } else {
            $lapa_braai = 0;
        }
        $propertyInput['lapa_braai'] = $lapa_braai;
        if (Request::get('intercom')) {
            $intercom = 1;
        } else {
            $intercom = 0;
        }
        $propertyInput['intercom'] = $intercom;
        if (Request::get('alarm')) {
            $alarm = 1;
        } else {
            $alarm = 0;
        }
        $propertyInput['alarm'] = $alarm;
        if (Request::get('elec_gate')) {
            $elec_gate = 1;
        } else {
            $elec_gate = 0;
        }
        $propertyInput['elec_gate'] = $elec_gate;
        if (Request::get('sec_fence')) {
            $sec_fence = 1;
        } else {
            $sec_fence = 0;
        }
        $propertyInput['sec_fence'] = $sec_fence;
        if (Request::get('sec_lights')) {
            $sec_lights = 1;
        } else {
            $sec_lights = 0;
        }
        $propertyInput['sec_lights'] = $sec_lights;
        if (Request::get('stove')) {
            $stove = 1;
        } else {
            $stove = 0;
        }
        $propertyInput['stove'] = $stove;
        if (Request::get('dam')) {
            $dam = 1;
        } else {
            $dam = 0;
        }
        $propertyInput['dam'] = $dam;
        if (Request::get('borehole')) {
            $borehole = 1;
        } else {
            $borehole = 0;
        }
        $propertyInput['borehole'] = $borehole;
        if (Request::get('electric_fence')) {
            $electric_fence = 1;
        } else {
            $electric_fence = 0;
        }
        $propertyInput['electric_fence'] = $electric_fence;

        //COMMERCIAL
        //check whether amenity and security checkboxes have been checked and set input value to 1 if checked
        if (Request::get('yard_space')) {
            $yard_space = 1;
        } else {
            $yard_space = 0;
        }
        $propertyInput['yard_space'] = $yard_space;
        if (Request::get('tea_kitchen')) {
            $tea_kitchen = 1;
        } else {
            $tea_kitchen = 0;
        }
        $propertyInput['tea_kitchen'] = $tea_kitchen;
        if (Request::get('staff_parking')) {
            $staff_parking = 1;
        } else {
            $staff_parking = 0;
        }
        $propertyInput['staff_parking'] = $staff_parking;
        if (Request::get('staff_parking')) {
            $staff_parking = 1;
        } else {
            $staff_parking = 0;
        }
        $propertyInput['staff_parking'] = $staff_parking;
        if (Request::get('visitors_parking')) {
            $visitors_parking = 1;
        } else {
            $visitors_parking = 0;
        }
        $propertyInput['visitors_parking'] = $visitors_parking;
        if (Request::get('shaded_parking')) {
            $shaded_parking = 1;
        } else {
            $shaded_parking = 0;
        }
        $propertyInput['shaded_parking'] = $shaded_parking;
        if (Request::get('industrial_power')) {
            $industrial_power = 1;
        } else {
            $industrial_power = 0;
        }
        $propertyInput['industrial_power'] = $industrial_power;
        if (Request::get('admin_offices')) {
            $admin_offices = 1;
        } else {
            $admin_offices = 0;
        }
        $propertyInput['admin_offices'] = $admin_offices;
        if (Request::get('roller_shutter_gate')) {
            $roller_shutter_gate = 1;
        } else {
            $roller_shutter_gate = 0;
        }
        $propertyInput['roller_shutter_gate'] = $roller_shutter_gate;
        if (Request::get('gantry')) {
            $gantry = 1;
        } else {
            $gantry = 0;
        }
        $propertyInput['gantry'] = $gantry;
        if (Request::get('extra_heights')) {
            $extra_heights = 1;
        } else {
            $extra_heights = 0;
        }
        $propertyInput['extra_heights'] = $extra_heights;
        if (Request::get('concrete_floor')) {
            $concrete_floor = 1;
        } else {
            $concrete_floor = 0;
        }
        $propertyInput['concrete_floor'] = $concrete_floor;
        if (Request::get('tiled_floor')) {
            $tiled_floor = 1;
        } else {
            $tiled_floor = 0;
        }
        $propertyInput['tiled_floor'] = $tiled_floor;
        if (Request::get('carpet_floor')) {
            $carpet_floor = 1;
        } else {
            $carpet_floor = 0;
        }
        $propertyInput['carpet_floor'] = $carpet_floor;
        if (Request::get('security_system')) {
            $security_system = 1;
        } else {
            $security_system = 0;
        }
        $propertyInput['security_system'] = $security_system;
        if (Request::get('fully_fenced')) {
            $fully_fenced = 1;
        } else {
            $fully_fenced = 0;
        }
        $propertyInput['fully_fenced'] = $fully_fenced;
        if (Request::get('gated_estate')) {
            $gated_estate = 1;
        } else {
            $gated_estate = 0;
        }
        $propertyInput['gated_estate'] = $gated_estate;
        if (Request::get('fully_walled')) {
            $fully_walled = 1;
        } else {
            $fully_walled = 0;
        }
        $propertyInput['fully_walled'] = $fully_walled;
        if (Request::get('shop_front')) {
            $shop_front = 1;
        } else {
            $shop_front = 0;
        }
        $propertyInput['shop_front'] = $shop_front;
        if (Request::get('sprinkler_system')) {
            $sprinkler_system = 1;
        } else {
            $sprinkler_system = 0;
        }
        $propertyInput['sprinkler_system'] = $sprinkler_system;
        if (Request::get('fire_hydrant')) {
            $fire_hydrant = 1;
        } else {
            $fire_hydrant = 0;
        }
        $propertyInput['fire_hydrant'] = $fire_hydrant;
        if (Request::get('garbage_area')) {
            $garbage_area = 1;
        } else {
            $garbage_area = 0;
        }
        $propertyInput['garbage_area'] = $garbage_area;
        if (Request::get('heavy_load_floors')) {
            $heavy_load_floors = 1;
        } else {
            $heavy_load_floors = 0;
        }
        $propertyInput['heavy_load_floors'] = $heavy_load_floors;
        if (Request::get('swing_gate')) {
            $swing_gate = 1;
        } else {
            $swing_gate = 0;
        }
        $propertyInput['swing_gate'] = $swing_gate;
        if (Request::get('delivery_entrance')) {
            $delivery_entrance = 1;
        } else {
            $delivery_entrance = 0;
        }
        $propertyInput['delivery_entrance'] = $delivery_entrance;
        if (Request::get('loading_bay')) {
            $loading_bay = 1;
        } else {
            $loading_bay = 0;
        }
        $propertyInput['loading_bay'] = $loading_bay;
        if (Request::get('train_track')) {
            $train_track = 1;
        } else {
            $train_track = 0;
        }
        $propertyInput['train_track'] = $train_track;
        if (Request::get('garden_area')) {
            $garden_area = 1;
        } else {
            $garden_area = 0;
        }
        $propertyInput['garden_area'] = $garden_area;
        if (Request::get('mezzanine_level')) {
            $mezzanine_level = 1;
        } else {
            $mezzanine_level = 0;
        }
        $propertyInput['mezzanine_level'] = $mezzanine_level;
        if (Request::get('entertainment_area')) {
            $entertainment_area = 1;
        } else {
            $entertainment_area = 0;
        }
        $propertyInput['entertainment_area'] = $entertainment_area;
        if (Request::get('pre_paid_meters')) {
            $pre_paid_meters = 1;
        } else {
            $pre_paid_meters = 0;
        }
        $propertyInput['pre_paid_meters'] = $pre_paid_meters;

        // end of security and ameneties

        $user_id = Auth::user()->id;
        $propertyInput['user_id'] = $user_id;

        $datetime = date('Y-m-d H:i:s');
        //create unique reference number
        switch ($propertyInput['listing_type']):
        case 'welet':
        $rent_ref = DB::select('SELECT `rent_ref` FROM `reference` WHERE `id` = ?', [1]);
        $rent_ref = $rent_ref[0]->rent_ref;
        $reference = 'RENT-'.str_pad($rent_ref, 6, '0', STR_PAD_LEFT);
        $rent_ref++;
        DB::update('UPDATE `reference` SET `rent_ref` = ?, `updated_at` = NOW()', [$rent_ref]);
        if (Auth::user()->hasRole('Landlord')) {
        } else {
            DB::table('role_user')->insert(['role_id' => 4, 'user_id' => $user_id, 'created_at' => $datetime, 'updated_at' => $datetime]);
        }

        break;
        case 'wesell':
        $sale_ref = DB::select('SELECT `sale_ref` FROM `reference` WHERE `id` = ?', [1]);
        $sale_ref = $sale_ref[0]->sale_ref;
        $reference = 'SALE-'.str_pad($sale_ref, 6, '0', STR_PAD_LEFT);
        $sale_ref++;
        DB::update('UPDATE `reference` SET `sale_ref` = ?, `updated_at` = NOW()', [$sale_ref]);
        if (Auth::user()->hasRole('Seller')) {
        } else {
            DB::table('role_user')->insert(['role_id' => 3, 'user_id' => $user_id, 'created_at' => $datetime, 'updated_at' => $datetime]);
        }

        break;
        case 'weauction':
        $auction_ref = DB::select('SELECT `auction_ref` FROM `reference` WHERE `id` = ?', [1]);
        $auction_ref = $auction_ref[0]->auction_ref;
        $reference = 'AUCT-'.str_pad($auction_ref, 6, '0', STR_PAD_LEFT);
        $auction_ref++;
        DB::update('UPDATE `reference` SET `auction_ref` = ?, `updated_at` = NOW()', [$auction_ref]);

        break;

        endswitch;

        $propertyInput['reference'] = $reference;

        if (! $this->property->fill($propertyInput)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->property->errors);
        }

        $this->property['available_from'] = date('Y-m-d');

        $this->property['postcode'] = Request::get('postcode');

        $this->property->save();

        if ($propertyInput['property_type'] === 'Residential') {
            $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);
            $resInput['property_id'] = $this->property->id;
            $residential = new Residential();
            $residential->fill($resInput)->save();
        }

        if ($propertyInput['property_type'] === 'Commercial') {
            $comInput = Request::only(['commercial_type', 'parking']);
            $comInput['property_id'] = $this->property->id;
            $commercial = new Commercial();
            $commercial->fill($comInput)->save();
        }

        if ($propertyInput['property_type'] === 'Farm') {
            $farmInput = Request::only(['farm_type']);
            $farmInput['property_id'] = $this->property->id;
            $farm = new Farm();
            $farm->fill($farmInput)->save();
        }

        $vettingInput = Request::only(['property_id', 'owner_title', 'owner_firstname', 'owner_surname', 'ownership_type',
            'rep_title', 'rep_firstname', 'rep_surname', 'rep_id_number', 'rep_cell_num', 'rep_email', 'marriage_stat', 'mar_in_com',
            'spouse_title', 'spouse_firstname', 'spouse_surname', 'spouse_id_number', 'extra_owner_title1',
            'extra_owner_firstname1', 'extra_owner_surname1', 'extra_owner_title2', 'extra_owner_firstname2',
            'extra_owner_surname2', 'extra_owner_title3', 'extra_owner_firstname3', 'extra_owner_surname3', ]);

        if ($vettingInput['owner_firstname'] == '') {
            $vettingInput['owner_firstname'] = $propertyInput['company'];
        }

        $vettingInput['property_id'] = $this->property->id;

        $vettingInput['vetting_status'] = 1;

        // validate and save to vetting table if not valid return and delete $this->property
        $vetting = new Vetting();

        if (! $vetting->fill($vettingInput)->isValid()) {
            $this->property->forceDelete();

            return Redirect::back()->withInput()->withErrors($vetting->errors);
        }

        $vetting->save();

        return Redirect::route('property.createcont', $this->property->id);
    }

    public function getBroker($id)
    {
        $property = $this->property->select('broker')->whereId($id)->first();

        if ($property->broker != null) {
            $broker = DB::table('broker_logos')->select('broker_name')->whereId($property->broker)->first();
        } else {
            $broker = null;
        }

        return $broker;
    }

    /**
     * Display the specified resource.
     * GET /property/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if ($id == '0') {
            return view('property.show', ['property' => '0', 'images' => '0', 'contact' => '0']);
        } elseif (is_numeric($id)) {
            $images = $this->property->
            join('uploads', 'uploads.link', '=', 'property.id')->
            whereClass('property')->
            whereType('image')->
            where('property.id', $id)->
            get();

            if (! $images->isEmpty()) {
                switch ($images[0]['property_type']):
                    case 'Residential':
                        $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                            ->join('vetting', 'property.id', '=', 'vetting.property_id')
                            ->where('property.id', $id)->first();

                break;
                case 'Commercial':
                        $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                            ->join('vetting', 'property.id', '=', 'vetting.property_id')
                            ->where('property.id', $id)->first();

                break;
                case 'Farm':
                        $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                            ->join('vetting', 'property.id', '=', 'vetting.property_id')
                            ->where('property.id', $id)->first();

                break;
                default:
                        return Redirect::back();

                break;
                endswitch;
            } else {
                return Redirect::back();
            }

            $contact = DB::table('users')->whereId($property->user_id)->first();

            if ($property['attourney'] == 0) {
                $attorney = DB::table('own_attourney')->whereProperty_id($id)->first();
            } else {
                $attorney = $this->service_provider->whereId($property['attourney'])->first();
            }

            if (Auth::check()) {
                $madeContact = DB::table('make_contact')->whereUser_id(Auth::user()->id)->whereProperty_id($id)->first();
            } else {
                $madeContact = null;
            }

            if ($property->listing_type == 'in2rental' || $property->listing_type == 'in2assets') {
                $agent = $this->user->whereId($property->rep_id_number)->first();
            } else {
                $agent = null;
            }

            if ($property->listing_type == 'weauction') {
                $auctionDets = $this->auction->whereProperty_id($id)->first();

                $dtTNow = date('Y-m-d H:i:s');

                if ($auctionDets->status == 'pending' && $auctionDets->start_date <= $dtTNow) {
                    $auctionDets->status = 'live';
                    $auctionDets->save();
                }

                if ($auctionDets->status == 'live' && $auctionDets->end_date <= $dtTNow && ($property->vetting_status == 2 || $property->vetting_status == 16)) {
                    $auctionDets->status = 'not_sold';
                    $auctionDets->save();

                    if ($auctionDets->auction_type == 1) {
                        $property->vetting_status == 9;
                        $property->save();
                    }
                }

                $auctionCalendarShow = $this->property->with('auction')->with('residential')->with('commercial')->with('farm')->whereId($id)->first();

                $auctioneer = DB::table('auctioneers')->whereId($auctionDets->auctioneer)->first();

                $auctionDets->auctioneer = $auctioneer->auctioneer;

                $agent = $this->user->whereId($property->rep_id_number)->first();

                if ($agent == null) {
                    $agent = $this->user->whereId(1)->first();
                    $property->rep_firstname = 'In2assets';
                    $property->rep_surname = '';
                    $property->rep_cell_num = '0861 444 769';
                    $property->rep_email = 'marketing@in2assets.com';
                    $property->rep_id_number = '1';
                }

                if ($auctionDets != null) {
                    if (Auth::check()) {
                        $yourBids = $this->auction_bids->whereUser_id(Auth::user()->id)->orderBy('id', 'desc')->first();
                    } else {
                        $yourBids = null;
                    }

                    if ($auctionDets->auction_type == 2 || $auctionDets->auction_type == 3) {
                        if ($auctionDets->status == 'not_sold') {
                            $register = null;
                        } elseif (Auth::check()) {
                            $register = $this->auction_register->whereUser_id(Auth::user()->id)->whereAuction_id($auctionDets->id)->first();
                        } else {
                            $register = null;
                        }

                        $physicalAuction = null;
                    } elseif ($auctionDets->auction_type == 1) {
                        if ($auctionDets->status == 'not_sold') {
                            $register = null;
                        } elseif (Auth::check()) {
                            $register = $this->physical_auction_register->whereUser_id(Auth::user()->id)->whereAuction_id($auctionDets->physical_auction)->first();
                        } else {
                            $register = null;
                        }
                        $physicalAuction = $this->physical_auction->whereId($auctionDets->physical_auction)->first();
                    } elseif ($auctionDets->auction_type == 5) {
                        if ($auctionDets->status == 'not_sold') {
                            $register = null;
                        } elseif (Auth::check()) {
                            $register = $this->auction_register->whereUser_id(Auth::user()->id)->whereAuction_id($auctionDets->id)->first();
                        } else {
                            $register = null;
                        }
                        $physicalAuction = null;
                    } else {
                        $yourBids = null;
                        $register = null;
                        $physicalAuction = null;
                    }

                    $broker = $this->getBroker($property->property_id);

                    return view('wesell.property.show', ['property' => $property, 'images' => $images,
                        'contact' => $contact, 'auctionDets' => $auctionDets, 'yourBids' => $yourBids,
                        'register' => $register, 'madeContact' => $madeContact, 'auctionCalendarShow' => $auctionCalendarShow,
                        'attorney' => $attorney, 'agent' => $agent, 'physicalAuction' => $physicalAuction, 'broker' => $broker,
                        'popup' => false, ]);
                } else {
                    $broker = $this->getBroker($property->property_id);

                    return view('wesell.property.show', ['property' => $property, 'images' => $images,
                        'contact' => $contact, 'auctionDets' => $auctionDets, 'madeContact' => $madeContact,
                        'auctionCalendarShow' => $auctionCalendarShow, 'attorney' => $attorney, 'agent' => $agent, 'broker' => $broker,
                        'popup' => false, ]);
                }
            } else {
                $broker = $this->getBroker($property->property_id);

                return view('wesell.property.show', ['property' => $property, 'images' => $images,
                    'contact' => $contact, 'madeContact' => $madeContact, 'attorney' => $attorney, 'agent' => $agent, 'broker' => $broker, ]);
            }
        } else {
            $broker = $this->getBroker($property->property_id);

            return view('property.admin.history', ['property' => $property, 'images' => $images, 'contact' => $contact,
                'madeContact' => $madeContact, 'attorney' => $attorney, 'agent' => $agent, 'broker' => $broker, ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * GET /property/{id}/edit.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $property = $this->property->whereId($id)->first();
        if (is_null($property)) {
            return Redirect::route('property.index');
        }

        switch ($property['property_type']):
        case 'Residential':
        $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
        ->where('property.id', $id)->first();

        break;
        case 'Commercial':
        $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
        ->where('property.id', $id)->first();

        break;
        case 'Farm':
        $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
        ->where('property.id', $id)->first();

        break;
        default:
        return Redirect::route('property.index');

        break;
        endswitch;

        $user = $this->user->whereId($property->user_id)->first();
        $files = $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')->get();

        return view('property.edit', ['property' => $property, 'files' => $files]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /property/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $propertyInput = Request::only(['lead_image', 'short_descrip', 'long_descrip', 'price', 'floor_space', 'land_size',
            'land_measurement', 'youtube', 'province', 'area', 'suburb_town_city', 'price', 'broker', 'x_coord', 'y_coord',
            'complex_number', 'complex_name', 'street_number', 'street_address', ]);

        if ($propertyInput['land_measurement'] === 'HA') {
            $propertyInput['land_size'] = $propertyInput['land_size'] * 10000;
        }

        //RESIDENTIAL
        //check whether amenity and security checkboxes have been checked and set input value to 1 if checked
        if (Request::get('dining_room')) {
            $dining_room = 1;
        } else {
            $dining_room = 0;
        }
        $propertyInput['dining_room'] = $dining_room;
        if (Request::get('entrance_hall')) {
            $entrance_hall = 1;
        } else {
            $entrance_hall = 0;
        }
        $propertyInput['entrance_hall'] = $entrance_hall;
        if (Request::get('tv_room')) {
            $tv_room = 1;
        } else {
            $tv_room = 0;
        }
        $propertyInput['tv_room'] = $tv_room;
        if (Request::get('study')) {
            $study = 1;
        } else {
            $study = 0;
        }
        $propertyInput['study'] = $study;
        if (Request::get('pets')) {
            $pets = 1;
        } else {
            $pets = 0;
        }
        $propertyInput['pets'] = $pets;
        if (Request::get('lounge')) {
            $lounge = 1;
        } else {
            $lounge = 0;
        }
        $propertyInput['lounge'] = $lounge;
        if (Request::get('kitchen')) {
            $kitchen = 1;
        } else {
            $kitchen = 0;
        }
        $propertyInput['kitchen'] = $kitchen;
        if (Request::get('recreation_room')) {
            $recreation_room = 1;
        } else {
            $recreation_room = 0;
        }
        $propertyInput['recreation_room'] = $recreation_room;
        if (Request::get('pantry')) {
            $pantry = 1;
        } else {
            $pantry = 0;
        }
        $propertyInput['pantry'] = $pantry;
        if (Request::get('laundry')) {
            $laundry = 1;
        } else {
            $laundry = 0;
        }
        $propertyInput['laundry'] = $laundry;
        if (Request::get('guest_toilet')) {
            $guest_toilet = 1;
        } else {
            $guest_toilet = 0;
        }
        $propertyInput['guest_toilet'] = $guest_toilet;
        if (Request::get('patio')) {
            $patio = 1;
        } else {
            $patio = 0;
        }
        $propertyInput['patio'] = $patio;
        if (Request::get('view')) {
            $view = 1;
        } else {
            $view = 0;
        }
        $propertyInput['view'] = $view;
        if (Request::get('tennis_court')) {
            $tennis_court = 1;
        } else {
            $tennis_court = 0;
        }
        $propertyInput['tennis_court'] = $tennis_court;
        if (Request::get('pool')) {
            $pool = 1;
        } else {
            $pool = 0;
        }
        $propertyInput['pool'] = $pool;
        if (Request::get('sprinklers')) {
            $sprinklers = 1;
        } else {
            $sprinklers = 0;
        }
        $propertyInput['sprinklers'] = $sprinklers;
        if (Request::get('aircon')) {
            $aircon = 1;
        } else {
            $aircon = 0;
        }
        $propertyInput['aircon'] = $aircon;
        if (Request::get('aerial')) {
            $aerial = 1;
        } else {
            $aerial = 0;
        }
        $propertyInput['aerial'] = $aerial;
        if (Request::get('dstv')) {
            $dstv = 1;
        } else {
            $dstv = 0;
        }
        $propertyInput['dstv'] = $dstv;
        if (Request::get('bic')) {
            $bic = 1;
        } else {
            $bic = 0;
        }
        $propertyInput['bic'] = $bic;
        if (Request::get('shower')) {
            $shower = 1;
        } else {
            $shower = 0;
        }
        $propertyInput['shower'] = $shower;
        if (Request::get('staff_quarters')) {
            $staff_quarters = 1;
        } else {
            $staff_quarters = 0;
        }
        $propertyInput['staff_quarters'] = $staff_quarters;
        if (Request::get('lapa_braai')) {
            $lapa_braai = 1;
        } else {
            $lapa_braai = 0;
        }
        $propertyInput['lapa_braai'] = $lapa_braai;
        if (Request::get('intercom')) {
            $intercom = 1;
        } else {
            $intercom = 0;
        }
        $propertyInput['intercom'] = $intercom;
        if (Request::get('alarm')) {
            $alarm = 1;
        } else {
            $alarm = 0;
        }
        $propertyInput['alarm'] = $alarm;
        if (Request::get('elec_gate')) {
            $elec_gate = 1;
        } else {
            $elec_gate = 0;
        }
        $propertyInput['elec_gate'] = $elec_gate;
        if (Request::get('sec_fence')) {
            $sec_fence = 1;
        } else {
            $sec_fence = 0;
        }
        $propertyInput['sec_fence'] = $sec_fence;
        if (Request::get('sec_lights')) {
            $sec_lights = 1;
        } else {
            $sec_lights = 0;
        }
        $propertyInput['sec_lights'] = $sec_lights;
        if (Request::get('stove')) {
            $stove = 1;
        } else {
            $stove = 0;
        }
        $propertyInput['stove'] = $stove;
        if (Request::get('dam')) {
            $dam = 1;
        } else {
            $dam = 0;
        }
        $propertyInput['dam'] = $dam;
        if (Request::get('borehole')) {
            $borehole = 1;
        } else {
            $borehole = 0;
        }
        $propertyInput['borehole'] = $borehole;
        if (Request::get('electric_fence')) {
            $electric_fence = 1;
        } else {
            $electric_fence = 0;
        }
        $propertyInput['electric_fence'] = $electric_fence;

        //COMMERCIAL
        //check whether amenity and security checkboxes have been checked and set input value to 1 if checked
        if (Request::get('yard_space')) {
            $yard_space = 1;
        } else {
            $yard_space = 0;
        }
        $propertyInput['yard_space'] = $yard_space;
        if (Request::get('tea_kitchen')) {
            $tea_kitchen = 1;
        } else {
            $tea_kitchen = 0;
        }
        $propertyInput['tea_kitchen'] = $tea_kitchen;
        if (Request::get('staff_parking')) {
            $staff_parking = 1;
        } else {
            $staff_parking = 0;
        }
        $propertyInput['staff_parking'] = $staff_parking;
        if (Request::get('staff_parking')) {
            $staff_parking = 1;
        } else {
            $staff_parking = 0;
        }
        $propertyInput['staff_parking'] = $staff_parking;
        if (Request::get('visitors_parking')) {
            $visitors_parking = 1;
        } else {
            $visitors_parking = 0;
        }
        $propertyInput['visitors_parking'] = $visitors_parking;
        if (Request::get('shaded_parking')) {
            $shaded_parking = 1;
        } else {
            $shaded_parking = 0;
        }
        $propertyInput['shaded_parking'] = $shaded_parking;
        if (Request::get('industrial_power')) {
            $industrial_power = 1;
        } else {
            $industrial_power = 0;
        }
        $propertyInput['industrial_power'] = $industrial_power;
        if (Request::get('admin_offices')) {
            $admin_offices = 1;
        } else {
            $admin_offices = 0;
        }
        $propertyInput['admin_offices'] = $admin_offices;
        if (Request::get('roller_shutter_gate')) {
            $roller_shutter_gate = 1;
        } else {
            $roller_shutter_gate = 0;
        }
        $propertyInput['roller_shutter_gate'] = $roller_shutter_gate;
        if (Request::get('gantry')) {
            $gantry = 1;
        } else {
            $gantry = 0;
        }
        $propertyInput['gantry'] = $gantry;
        if (Request::get('extra_heights')) {
            $extra_heights = 1;
        } else {
            $extra_heights = 0;
        }
        $propertyInput['extra_heights'] = $extra_heights;
        if (Request::get('concrete_floor')) {
            $concrete_floor = 1;
        } else {
            $concrete_floor = 0;
        }
        $propertyInput['concrete_floor'] = $concrete_floor;
        if (Request::get('tiled_floor')) {
            $tiled_floor = 1;
        } else {
            $tiled_floor = 0;
        }
        $propertyInput['tiled_floor'] = $tiled_floor;
        if (Request::get('carpet_floor')) {
            $carpet_floor = 1;
        } else {
            $carpet_floor = 0;
        }
        $propertyInput['carpet_floor'] = $carpet_floor;
        if (Request::get('security_system')) {
            $security_system = 1;
        } else {
            $security_system = 0;
        }
        $propertyInput['security_system'] = $security_system;
        if (Request::get('fully_fenced')) {
            $fully_fenced = 1;
        } else {
            $fully_fenced = 0;
        }
        $propertyInput['fully_fenced'] = $fully_fenced;
        if (Request::get('gated_estate')) {
            $gated_estate = 1;
        } else {
            $gated_estate = 0;
        }
        $propertyInput['gated_estate'] = $gated_estate;
        if (Request::get('fully_walled')) {
            $fully_walled = 1;
        } else {
            $fully_walled = 0;
        }
        $propertyInput['fully_walled'] = $fully_walled;
        if (Request::get('shop_front')) {
            $shop_front = 1;
        } else {
            $shop_front = 0;
        }
        $propertyInput['shop_front'] = $shop_front;
        if (Request::get('sprinkler_system')) {
            $sprinkler_system = 1;
        } else {
            $sprinkler_system = 0;
        }
        $propertyInput['sprinkler_system'] = $sprinkler_system;
        if (Request::get('fire_hydrant')) {
            $fire_hydrant = 1;
        } else {
            $fire_hydrant = 0;
        }
        $propertyInput['fire_hydrant'] = $fire_hydrant;
        if (Request::get('garbage_area')) {
            $garbage_area = 1;
        } else {
            $garbage_area = 0;
        }
        $propertyInput['garbage_area'] = $garbage_area;
        if (Request::get('heavy_load_floors')) {
            $heavy_load_floors = 1;
        } else {
            $heavy_load_floors = 0;
        }
        $propertyInput['heavy_load_floors'] = $heavy_load_floors;
        if (Request::get('swing_gate')) {
            $swing_gate = 1;
        } else {
            $swing_gate = 0;
        }
        $propertyInput['swing_gate'] = $swing_gate;
        if (Request::get('delivery_entrance')) {
            $delivery_entrance = 1;
        } else {
            $delivery_entrance = 0;
        }
        $propertyInput['delivery_entrance'] = $delivery_entrance;
        if (Request::get('loading_bay')) {
            $loading_bay = 1;
        } else {
            $loading_bay = 0;
        }
        $propertyInput['loading_bay'] = $loading_bay;
        if (Request::get('train_track')) {
            $train_track = 1;
        } else {
            $train_track = 0;
        }
        $propertyInput['train_track'] = $train_track;
        if (Request::get('garden_area')) {
            $garden_area = 1;
        } else {
            $garden_area = 0;
        }
        $propertyInput['garden_area'] = $garden_area;
        if (Request::get('mezzanine_level')) {
            $mezzanine_level = 1;
        } else {
            $mezzanine_level = 0;
        }
        $propertyInput['mezzanine_level'] = $mezzanine_level;
        if (Request::get('entertainment_area')) {
            $entertainment_area = 1;
        } else {
            $entertainment_area = 0;
        }
        $propertyInput['entertainment_area'] = $entertainment_area;
        if (Request::get('pre_paid_meters')) {
            $pre_paid_meters = 1;
        } else {
            $pre_paid_meters = 0;
        }
        $propertyInput['pre_paid_meters'] = $pre_paid_meters;

        // end of security and ameneties
        $this->property = $this->property->find($id);
        $this->property->fill($propertyInput)->update();

        // if property type is residential then validate and save or redirect back to form with residential errors
        if ($this->property->property_type === 'Residential') {
            $resInput = Request::only(['bedrooms', 'bathrooms', 'parking']);
            $resInput['property_id'] = $this->property->id;

            // validate and save to residential table if not valid return and delete $this->property
            $residential = new Residential();
            $residential = $residential->whereProperty_id($id)->first();
            $residential->fill($resInput)->update();
        }

        // if property type is commercial then validate and save or redirect back to form with commercial errors
        if ($this->property->property_type === 'Commercial') {
            $comInput = Request::only(['parking']);
            $comInput['property_id'] = $this->property->id;

            // validate and save to residential table if not valid return and delete $this->property
            $commercial = new Commercial();
            $commercial = $commercial->whereProperty_id($id)->first();
            $commercial->fill($comInput)->update();
        }

        $vettingInput = Request::only(['rep_email', 'rep_cell_num', 'rep_firstname', 'rep_surname']);
        $vettingInput['property_id'] = $this->property->id;
        $vettingInput['rep_id_number'] = Request::get('agents');

        // validate and save to vetting table if not valid return and delete $this->property
        $vetting = $this->vetting->whereProperty_id($id)->first();
        $vetting->fill($vettingInput)->save();

        if (Auth::user()->hasRole('Admin')) {
            Flash::message('Property has been updated.');

            return Redirect::route('property.admin', ['status' => 'all']);
        } elseif ($this->property->listing_type == 'wesell') {
            Flash::message('Your property has been updated.');

            return Redirect::route('portalSellers_path');
        } elseif ($this->property->listing_type == 'welet') {
            Flash::message('Your property has been updated.');

            return Redirect::route('portalLandlords_path');
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /property/{id}.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $property = $this->property->find($id);

        if ($property->residential) {
            $property->residential->delete();
        }
        if ($property->commercial) {
            $property->commercial->delete();
        }
        if ($property->farm) {
            $property->farm->delete();
        }

        if ($property->listing_type = 'weauction') {
            $auction = $this->auction->whereProperty_id($id)->first();
            $auction->status = 'complete';
            $auction->update();
        }

        $property->vetting->delete();
        $property->delete();
        if (Auth::user()->hasRole('Admin')) {
            return Redirect::route('property.admin', ['status' => 'all']);
        }

        Session::start();

        return Redirect::route('property.manage_properties');
    }

    /** lists property by listing type and property type*/
    public function listType($listing_type)
    {

// $listingtype in each case becomes the title of the page
        switch ($listing_type) {
        case 'RentRes':
        $listing_type = 'Residential Rental';
        $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.vetting_status', '=', 2)->whereListing_type('Rent')->whereProperty_type('Residential')->get();

        break;
        case 'RentCom':
        $listing_type = 'Commercial Rental';
        $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.vetting_status', '=', 2)->whereListing_type('Rent')->whereProperty_type('Commercial')->get();

        break;
        case 'RentFarm':
        $listing_type = 'Farm Rental';
        $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.vetting_status', '=', 2)->whereListing_type('Rent')->whereProperty_type('Farm')->get();

        break;
        case 'SaleRes':
        $listing_type = 'Residential Sale';
        $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.vetting_status', '=', 2)->whereListing_type('Sale')->whereProperty_type('Residential')->
        orWhere('listing_type', 'Auction')->whereProperty_type('Residential')->get();

        break;
        case 'SaleCom':
        $listing_type = 'Commercial Sale';
        $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.vetting_status', '=', 2)->whereListing_type('Sale')->whereProperty_type('Commercial')->
        orWhere('listing_type', 'Auction')->whereProperty_type('Commercial')->get();

        break;
        case 'SaleFarm':
        $listing_type = 'Farm Sale';
        $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.vetting_status', '=', 2)->whereListing_type('Sale')->whereProperty_type('Farm')->
        orWhere('listing_type', 'Auction')->whereProperty_type('Farm')->get();

        break;
        default:
        $properties = $this->property->join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting.vetting_status', '=', 2)->whereListing_type($listing_type)->get();

        break;
    }

        return view('property.index', ['properties' => $properties, 'listing' => $listing_type]);
    }

    public function get_adv_areas()
    {
        $provincesDB = DB::table('areas')->groupBy('province')->get();
        $areasDB = DB::table('areas')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->groupBy('province', 'area', 'suburb_town_city')->get();
        //suburbs
        foreach ($citiesDB as $city) {
            $cities[] = $city->province.', '.$city->area.', '.$city->suburb_town_city;
        }
        $areas['cities'] = implode('", "', $cities);
        //provinces
        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $areas['provinces'] = implode('", "', $provinces);
        //cities
        foreach ($areasDB as $area) {
            $areas[] = $area->province.', '.$area->area;
        }
        $areas['areas'] = implode('", "', $areas);

        return $areas;
    }

    /**
     * Display a listing of the resource.
     * GET /property.
     *
     * @return Response
     */
    public function index()
    {
        $set_action_type = 'index';
        $set_all_type = 'all';

        $areas = $this->get_adv_areas();

        $featured = $this->getFeaturedProperties();

        $first_prop = rand(1, count($featured));

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'provinces_adv' => $areas['provinces'], 'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'featured' => $featured,
            'first_prop' => $first_prop, ]);
    }

    /** lists property by listing type and property type*/
    public function weSell($property_type)
    {
        $set_action_type = 'wesell';
        $set_all_type = $property_type;
        $provinces = DB::table('areas')->groupBy('province')->get();
        $areas = DB::table('areas')->groupBy('area')->get();
        $cities = DB::table('areas')->groupBy('suburb_town_city')->get();
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));
        $adv_search_set['listing_type'] = 'wesell';
        $adv_search_set['property_type'] = ucfirst($property_type);
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas, 'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'], 'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'],
            'adv_search_set' => $adv_search_set, ]);
    }

    public function weRent($property_type)
    {
        $set_action_type = 'welet';
        $set_all_type = $property_type;
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $adv_search_set['listing_type'] = 'welet';
        $adv_search_set['property_type'] = $property_type;
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type, 'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'], 'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'],
            'adv_search_set' => $adv_search_set, ]);
    }

    public function weAuction($property_type)
    {
        $set_action_type = 'weauction';
        $set_all_type = $property_type;
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $adv_search_set['listing_type'] = 'weauction';
        $adv_search_set['property_type'] = $property_type;
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'],
            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set, ]);
    }

    public function weSellAll()
    {
        $set_action_type = 'wesell';
        $set_all_type = 'all';
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $adv_search_set['listing_type'] = 'wesell';
        $adv_search_set['property_type'] = 'all';
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'provinces_adv' => $areas['provinces'], 'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'featured' => $featured, 'first_prop' => $first_prop,
            'areas_adv' => $areas, 'adv_search_set' => $adv_search_set, ]);
    }

    public function weRentAll()
    {
        $set_action_type = 'welet';
        $set_all_type = 'all';
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $adv_search_set['listing_type'] = 'welet';
        $adv_search_set['property_type'] = 'all';
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'],
            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set, ]);
    }

    public function weAuctionAll()
    {
        $set_action_type = 'weauction';
        $set_all_type = 'all';
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $adv_search_set['listing_type'] = 'weauction';
        $adv_search_set['property_type'] = 'all';
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'],
            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set, ]);
    }

    public function residentialAll()
    {
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        return view('wesell.property.index', ['featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'],
                            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], ]);
    }

    public function commercialAll()
    {
        $set_action_type = 'all';
        $set_all_type = 'commercial';
        $featured = $this->getFeaturedProperties();

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $adv_search_set['listing_type'] = 'all';
        $adv_search_set['property_type'] = 'Commercial';
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        return view('property.index', ['set_action_type' => $set_action_type, 'set_all_type' => $set_all_type,
            'featured' => $featured, 'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'],
            'cities_adv' => $areas['cities'], 'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set, ]);
    }

    public function vettingInfo()
    {
        return view('property.vettingInfo');
    }

    public function archive()
    {
        return view('property.admin.archive');
    }

    public function restore($id)
    {
        $this->property->whereId($id)->restore();

        $property = $this->property->find($id);

        $this->vetting->whereProperty_id($id)->restore();

        if ($property->residential) {
            $property->residential->restore();
        }
        if ($property->commercial) {
            $property->commercial->restore();
        }
        if ($property->farm) {
            $property->farm->restore();
        }

        return view('property.admin.archive');
    }

    public function getArchivedTable()
    {
        $properties = DB::table('property')
        ->join('vetting', 'vetting.property_id', '=', 'property.id')
        ->whereNotNull('property.deleted_at')
        ->join('users', 'users.id', '=', 'property.user_id')
        ->select('reference', 'property.complex_number', 'property.complex_name', 'property.street_number', 'property.street_address', 'property.province', 'property.suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname', 'property_id', 'user_id', 'listing_type')
        ->get();

        return Datatable::collection(new Collection($properties))
        ->showColumns('reference', 'listing_type')
        ->addColumn('street_address', function ($Property) {
            return $Property->complex_number.' '.$Property->complex_name.' '.$Property->street_number.' '.$Property->street_address;
        })
        ->showColumns('province', 'suburb_town_city', 'property_type', 'firstname', 'lastname')
        ->addColumn('action', function ($Property) {
            return '<a href="/admin/property/history/'.$Property->property_id.'" title="View Property History" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
            <a href="/admin/property/restore/'.$Property->property_id.'" title="Restore Property" ><i class="i-circled i-light i-alt i-small icon-ok" style="background-color: #2ecc71"></i></a>';
        })
        ->searchColumns('reference', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname')
        ->orderColumns('reference', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname')
        ->make();
    }

    public function getArea()
    {
        $province = Request::get('option');

        $areas = DB::table('areas')->where('province', $province)->groupBy('area')->pluck('area');

        return response($areas);
    }

    public function getSuburbTown()
    {
        $area = Request::get('option');
        $suburb_town = DB::table('areas')->where('area', $area)->groupBy('suburb_town_city')->pluck('suburb_town_city');

        return response($suburb_town);
    }

    public function createcont($id)
    {
        $property = $this->getPropertyDetsbyId($id);
        $availDate = strtotime($property['available_from']);
        $available_from = date('m/d/Y', $availDate);
        $property->available_from = $available_from;
        $user = Auth::user();

        $files = $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')->get();

        $catcode = DB::table('service_provider_categories')->whereCategory('Conveyancing Attorney')->select('id')->first();

        $provinces = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('province')->get();
        $areas = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('area')->get();
        $cities = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('suburb_town_city')->get();
        $attorneys = DB::table('service_provider')->whereCategory($catcode->id)->groupBy('company')->get();

        return view('property.createcont', ['property' => $property, 'files' => $files, 'provinces' => $provinces, 'cities' => $cities, 'areas' => $areas, 'attorneys' => $attorneys]);
    }

    public function createEdit()
    {

//saves details without validating
        if (Request::get('save')) {
            $id = Request::get('id');
            $propertyInput = Request::only(['lead_image', 'short_descrip', 'long_descrip', 'image_gallery', 'price', 'youtube', 'attourney']);
            $propertyInput['attourney'] = $propertyInput['attourney'][0];

            $this->property = $this->property->find($id);

            if ($propertyInput['attourney'] == 0) {
                $attourneyInput = Request::only(['company_name', 'contact_name', 'contact_number', 'email']);
                $this->attourney = $this->attourney->whereProperty_id($id)->first();
                if ($this->attourney == null) {
                    $attourneyId = DB::table('own_attourney')->insertGetId(['property_id' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                    $this->attourney = new Ownattourney();
                    $this->attourney = $this->attourney->whereId($attourneyId)->first();
                }

                $this->attourney->save();
            } else {
                $propertyInput['attourney'] = Request::get('company');
            }

            if ($propertyInput['image_gallery'] !== '' || $propertyInput['image_gallery'] !== null || $propertyInput['image_gallery'] !== 0) {
                $propertyInput['image_gallery'] = 1;
            }
            $availDate = strtotime(Request::get('available_from'));
            $available_from = date('Y-m-d', $availDate);
            $this->property->available_from = $available_from;
            $this->property->fill($propertyInput)->update();
            if ($this->property->property_type === 'Residential') {
                $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);

                $resInput['property_id'] = $this->property->id;
                // save to residential table
                $residential = new Residential();
                $residential = $residential->whereProperty_id($id)->first();
                $residential->fill($resInput)->update();
            }

            if ($this->property->property_type === 'Commercial') {
                $comInput = Request::only(['commercial_type', 'parking']);
                $comInput['property_id'] = $this->property->id;
                // save to commercial table
                $commercial = new Commercial();
                $commercial = $commercial->whereProperty_id($id)->first();
                $commercial->fill($comInput)->update();
            }

            if ($this->property->property_type === 'Farm') {
                $farmInput = Request::only(['farm_type']);
                $farmInput['property_id'] = $this->property->id;
                // save to farm table
                $farm = new Farm();
                $farm = $farm->whereProperty_id($id)->first();
                $farm->fill($farmInput)->update();
            }
            $vetting = new Vetting();
            $vetting = $vetting->whereProperty_id($id)->first();
            $id_photos = json_encode(Request::get('id_photos'));
            if ($id_photos === '[""]') {
                $vetting->id_photos = null;
            } else {
                $vetting->id_photos = $id_photos;
            }
            if ($vetting->ownership_type === 'Closed Corporation' || $vetting->ownership_type === 'Company (Pty) Limited' || $vetting->ownership_type === 'Trust') {
                $vetting->resolution = Request::get('resolution');
            }

            $vetting->vetting_status = 1;

            $vetting->update();

            if ($this->property->listing_type === 'welet') {
                return Redirect::route('portalLandlords_path');
            } elseif ($this->property->listing_type === 'wesell') {
                return Redirect::route('portalSellers_path');
            } else {
                return Redirect::route('portalSellers_path');
            }

            // submit must validate outstanding data
        } elseif (Request::get('submit')) {
            $id = Request::get('id');
            $propertyInput = Request::only(['lead_image', 'short_descrip', 'long_descrip', 'image_gallery', 'price', 'youtube', 'attourney']);
            $propertyInput['attourney'] = $propertyInput['attourney'][0];

            $this->property = $this->property->find($id);

            if ($propertyInput['image_gallery'] !== '' || $propertyInput['image_gallery'] !== null || $propertyInput['image_gallery'] !== 0) {
                $propertyInput['image_gallery'] = 1;
            }

            $availDate = strtotime(Request::get('available_from'));
            $available_from = date('Y-m-d', $availDate);
            $this->property->available_from = $available_from;

            if (! $this->property->fill($propertyInput)->isValid2()) {
                return Redirect::back()->withInput()->withErrors($this->property->errors);
            }
            if ($propertyInput['attourney'] == 0) {
                $attourneyInput = Request::only(['company_name', 'contact_name', 'contact_number', 'email']);
                $this->attourney = $this->attourney->whereProperty_id($id)->first();
                if ($this->attourney == null) {
                    $attourneyId = DB::table('own_attourney')->insertGetId(['property_id' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                    $this->attourney = new Ownattourney();
                    $this->attourney = $this->attourney->whereId($attourneyId)->first();
                }

                if (! $this->attourney->fill($attourneyInput)->isValid()) {
                    return Redirect::back()->withInput()->withErrors($this->attourney->errors);
                }

                $this->attourney->save();
            } else {
                $this->property->attourney = Request::get('company');
            }

            $this->property->update();

            // if property type is residential then validate and save or redirect back to form with residential errors
            if ($this->property->property_type === 'Residential') {
                if (Request::get('dwelling_type') !== 'Vacant Land') {
                    $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);

                    $resInput['property_id'] = $this->property->id;
                    // validate and save to residential table if not valid return
                    $residential = new Residential();
                    $residential = $residential->whereProperty_id($id)->first();
                    if (! $residential->fill($resInput)->isValid()) {
                        return Redirect::back()->withInput()->withErrors($residential->errors);
                    }
                    $residential->update();
                } else {
                    $resInput = Request::only(['dwelling_type']);

                    $resInput['property_id'] = $this->property->id;
                    // validate and save to residential table if not valid return
                    $residential = new Residential();
                    $residential = $residential->whereProperty_id($id)->first();
                    if (! $residential->fill($resInput)->isValid2()) {
                        return Redirect::back()->withInput()->withErrors($residential->errors);
                    }
                    $residential->update();
                }
            }

            // if property type is commercial then validate and save or redirect back to form with commercial errors
            if ($this->property->property_type === 'Commercial') {
                $comInput = Request::only(['commercial_type', 'parking']);
                $comInput['property_id'] = $this->property->id;
                // validate and save to commercial table if not valid return
                $commercial = new Commercial();
                $commercial = $commercial->whereProperty_id($id)->first();
                if (! $commercial->fill($comInput)->isValid()) {
                    return Redirect::back()->withInput()->withErrors($commercial->errors);
                }
                $commercial->update();
            }
            // if property type is farm then validate and save or redirect back to form with farm errors
            if ($this->property->property_type === 'Farm') {
                $farmInput = Request::only(['farm_type']);
                $farmInput['property_id'] = $this->property->id;
                // validate and save to farm table if not valid return
                $farm = new Farm();
                $farm = $farm->whereProperty_id($id)->first();
                if (! $farm->fill($farmInput)->isValid()) {
                    return Redirect::back()->withInput()->withErrors($farm->errors);
                }
                $farm->update();
            }

            // validate and save to vetting table if not valid return
            $vetting = new Vetting();
            $vetting = $vetting->whereProperty_id($id)->first();
            $id_photos = json_encode(Request::get('id_photos'));
            if ($id_photos === '[""]') {
                $vetting->id_photos = null;
            } else {
                $vetting->id_photos = $id_photos;
            }
            if ($vetting->ownership_type === 'Closed Corporation' || $vetting->ownership_type === 'Company (Pty) Limited' || $vetting->ownership_type === 'Trust') {
                $vetting->resolution = Request::get('resolution');
            }
            if (! $vetting->isValid2()) {
                return Redirect::back()->withInput()->withErrors();
            }
            $vetting->vetting_status = 3;

            $vetting->update();

            if ($this->property->listing_type === 'welet') {
                return Redirect::route('portalLandlords_path');
            } elseif ($this->property->listing_type === 'wesell') {
                return Redirect::route('portalSellers_path');
            } else {
                return Redirect::route('portalSellers_path');
            }
        }
    }

    /**
     * Get all property dtails by id.
     */
    public function getPropertyDetsbyId($id)
    {
        $property = $this->property->whereId($id)->first();

        if (is_null($property)) {
            return Redirect::route('property.index');
        }
        switch ($property['property_type']):
            case 'Residential':
                $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();
        $property['id'] = $id;

        break;
        case 'Commercial':
                $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();
        $property['id'] = $id;

        break;
        case 'Farm':
                $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
                    ->join('vetting', 'property.id', '=', 'vetting.property_id')
                    ->where('property.id', $id)->first();
        $property['id'] = $id;

        break;
        default:
                return Redirect::route('property.index');

        break;
        endswitch;

        return $property;
    }

    public function manageProperties()
    {
        $userId = Auth::user()->id;
        $properties = $this->property->
            join('vetting', 'property.id', '=', 'vetting.property_id')->
            leftJoin('residential', 'property.id', '=', 'residential.property_id')->
            where('property.user_id', '=', $userId)->
            where('property.property_type', '=', 'Residential')->
            orderBy('vetting_status')->
            orderBy('listing_type')->
            get();

        $percent = [];

        foreach ($properties as $property) {
            $percent[] = $this->form_complete_percent($property['property_id']);
        }

        return view('wesell.users.manage_properties', ['properties' => $properties, 'listing' => 'My', 'percent' => $percent]);
    }

    public function portal($type)
    {
        $userId = Auth::user()->id;
        $properties = $this->property->
            join('vetting', 'property.id', '=', 'vetting.property_id')->
            where('property.user_id', '=', $userId)->orderBy('vetting_status')->whereListing_type($type)->orderBy('listing_type')->get();

        $percent = [];

        foreach ($properties as $property) {
            $percent[] = $this->form_complete_percent($property['property_id']);
        }
        if ($type === 'welet') {
            return Redirect::route('portalLandlords_path');
        } else {
            return Redirect::route('portalSellers_path');
        }
    }

    public function getCoordinates($address)
    {
        $address = urlencode($address);
        $url = 'http://maps.google.com/maps/api/geocode/json?sensor=false&address='.$address;
        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];

        return ['xcoord' => $lat, 'ycoord' => $lng];
    }

    public function form_complete_percent($id)
    {
        $property = $this->getPropertyDetsbyId($id);

        switch ($property['property_type']) {
            case 'Residential':

                $total = 11;
                $startCount = 11;
                if ($property['dwelling_type'] === 'Vacant Land') {
                    $startCount += 3;
                }
                if ($property['price'] === '' || $property['price'] === null || $property['price'] == 0) {
                    $startCount--;
                }
                if ($property['dwelling_type'] === '' || $property['dwelling_type'] === null) {
                    $startCount--;
                }
                if ($property['bedrooms'] === '' || $property['bedrooms'] === null) {
                    $startCount--;
                }
                if ($property['bathrooms'] === '' || $property['bathrooms'] === null) {
                    $startCount--;
                }
                if ($property['parking'] === '' || $property['parking'] === null) {
                    $startCount--;
                }

                break;
            case 'Commercial':
                $total = 8;
                $startCount = 8;
                if ($property['price'] === '' || $property['price'] === null || $property['price'] == 0) {
                    $startCount--;
                }
                if ($property['commercial_type'] === '' || $property['commercial_type'] === null) {
                    $startCount--;
                }

                break;
            case 'Farm':
                $total = 8;
                $startCount = 8;
                if ($property['price'] === '' || $property['price'] === null || $property['price'] == 0) {
                    $startCount--;
                }
                if ($property['farm_type'] === '' || $property['farm_type'] === null) {
                    $startCount--;
                }

                break;
            default:
                // code...
                break;
        }

        if ($property['lead_image'] === '' || $property['lead_image'] === null || $property['lead_image'] === 'images/lead_placeholder.png') {
            $startCount--;
        }
        if ($property['image_gallery'] === '' || $property['image_gallery'] === null || $property['image_gallery'] == 0) {
            $startCount--;
        }
        if ($property['ownership_type'] === 'Private Owner' || $property['ownership_type'] === 'Other') {
            $total--;
            $startCount--;
        } else {
            if ($property['resolution'] === '' || $property['resolution'] === null) {
                $startCount--;
            }
        }
//        if($property['id_photos'] === '' || $property['id_photos'] === null){
//            $startCount--;
//        }
        if ($property['short_descrip'] === '' || $property['short_descrip'] === null) {
            $startCount--;
        }
        if ($property['long_descrip'] === '' || $property['long_descrip'] === null) {
            $startCount--;
        }
//             dd($total . ' ' . $startCount);
        return number_format($startCount / $total * 100, 0);
    }

    public function admin($type, $vetting)
    {
        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('vetting_status', '=', $vetting)->whereListing_type($type)->get();

        return view('vetting.index', ['properties' => $properties, 'title' => $type.' to vet']);
    }

    public function adminProperty($status)
    {
        return view('property.admin.properties', ['status' => $status]);
    }

    public function getAdminPropertyTable($status)
    {
        switch ($status):
        case 'all':
        $status = '%';

        break;
        case 1:
        $status = '%1%';

        break;
        case 2:
        $status = '%2%';

        break;
        case 3:
        $status = '%3%';

        break;
        case 4:
        $status = '%4%';

        break;
        case 5:
        $status = '%5%';

        break;
        case 6:
        $status = '%6%';

        break;
        case 7:
        $status = '%7%';

        break;
        case 8:
        $status = '%8%';

        break;
        endswitch;

        $properties = DB::table('property')
        ->join('vetting', 'vetting.property_id', '=', 'property.id')
        ->where('vetting_status', 'LIKE', $status)
        ->where('property.deleted_at', '=', null)
        ->join('users', 'users.id', '=', 'property.user_id')
        ->select('reference', 'property.complex_number', 'property.complex_name', 'property.street_number', 'property.street_address', 'property.province', 'property.suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname', 'property_id', 'user_id', 'listing_type', 'featured')
        ->get();

        return Datatable::collection(new Collection($properties))
        ->showColumns('reference', 'listing_type')
        ->addColumn('street_address', function ($Property) {
            return $Property->complex_number.' '.$Property->complex_name.' '.$Property->street_number.' '.$Property->street_address;
        })
        ->showColumns('province', 'suburb_town_city', 'property_type')
        ->addColumn('name', function ($Property) {
            return $Property->firstname.' '.$Property->lastname;
        })
        ->addColumn('vetting_status', function ($Property) {
            switch ($Property->vetting_status) {
                case 1:
                return 'Pending';

                break;
                case 2:
                return 'Approved';

                break;
                case 3:
                return 'Vetting';

                break;
                case 4:
                return 'Sold < 60 Days';

                break;
                case 5:
                return 'Rented < 14 Days';

                break;
                case 6:
                return 'Sold > 60 Days';

                break;
                case 7:
                return 'Rented > 14 Days';

                break;
                case 8:
                return 'Rejected';

                break;
                case 9:
                return 'Ready For Auction';

                break;
                case 10:
                return 'Sold on Auction';

                break;
                case 11:
                return 'Not Sold';

                break;
                case 12:
                return 'Sold Private Treaty Pre Auction';

                break;
                case 13:
                return 'Sold Private Treaty Post Auction';

                break;
                case 14:
                return 'Withdrawn';

                break;
                case 15:
                return 'Postponed';

                break;
                case 16:
                return 'S.T.C.';

                break;
            }
        })
->addColumn('action', function ($Property) {
    if ($Property->vetting_status == 1) {
        return '<a href="/admin/property/cont/'.$Property->property_id.'" title="Complete Property" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
        <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>';
    } elseif ($Property->vetting_status == 2) {
        if ($Property->listing_type == 'welet') {
            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
            <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
            <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
            <a href="/admin/property/close_property/'.$Property->property_id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
        } else {
            if ($Property->featured != 1) {
                return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>
                <a href="/admin/property/close_property/'.$Property->property_id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
            } else {
                return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
                <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
                <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>
                <a href="/admin/property/close_property/'.$Property->property_id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
            }
        }
    } elseif ($Property->vetting_status == 3) {
        return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
        <a href="/property/'.$Property->property_id.'" title="Preview Property" ><i class="i-circled i-light i-alt i-small icon-laptop"></i></a>
        <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
        <a href="/admin/vetting/'.$Property->property_id.'" title="Vet Property" ><i class="i-circled i-light i-alt i-small icon-check"></i></a>';
    } elseif ($Property->vetting_status == 9) {
        if ($Property->featured != 1) {
            return '<a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>
          <a href="/admin/property/close_property/'.$Property->property_id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
        } else {
            return '<a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>
        <a href="/admin/property/close_property/'.$Property->property_id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
        }
    } else {
        if ($Property->featured != 1) {
            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
        <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
        <a href="#" title="Feature Property" onclick="mark_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond"></i></a>
        <a href="/admin/property/close_property/'.$Property->property_id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
        } else {
            return '<a href="/admin/property/'.$Property->property_id.'/edit" title="Edit" ><i class="i-circled i-light i-alt i-small icon-edit"></i></a>
      <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
      <a href="#" title="Feature Property" onclick="mark_not_featured('.$Property->property_id.')" ><i class="i-circled i-light i-alt i-small icon-diamond" style="background-color:#008080"></i></a>
      <a href="/admin/property/close_property/'.$Property->property_id.'" title="Change Property Status" ><i class="i-circled i-light i-alt i-small icon-cog" style="background-color:#008080"></i></a>';
        }
    }
})
->searchColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'name')
->orderColumns('reference', 'listing_type', 'street_address', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'name')
->make();
    }

    public function adminPropertyEdit($id)
    {
        $property = $this->property->whereId($id)->first();
        if (is_null($property)) {
            return Redirect::route('property.index');
        }

        switch ($property['property_type']):
    case 'Residential':
    $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        case 'Commercial':
    $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        case 'Farm':
    $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
    ->join('vetting', 'property.id', '=', 'vetting.property_id')->where('property.id', $id)->first();

        break;
        default:
    return Redirect::back();

        break;
        endswitch;

        $user = $this->user->whereId($property->user_id)->first();
        $files = $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')->get();

        $agents = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        })->get();

        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city.', '.$city->area.', '.$city->province;
        }
        $cities = implode('", "', $cities);

        $brokers = DB::table('broker_logos')->select('id', 'broker_name')->whereDeleted_at(null)->get();

        return view('property.admin.edit', ['property' => $property, 'files' => $files, 'agents' => $agents, 'cities' => $cities, 'brokers' => $brokers]);
    }

    public function adminPropertyStore()
    {

    // check property section of form and validate
        $propertyInput = Request::only(['complex_number', 'complex_name', 'street_number', 'street_address', 'country', 'listing_type',
        'property_type', 'price', 'x_coord', 'y_coord', 'floor_space', 'land_size', 'land_measurement', 'sale_type', 'vat_num',
        'reg_num', 'company', 'province', 'area', 'suburb_town_city', 'owner_id', 'broker', '', ]);

        $propertyInput['user_id'] = Auth::user()->id;

        if ($propertyInput['land_measurement'] === 'HA') {
            $propertyInput['land_size'] = $propertyInput['land_size'] * 10000;
        }

        if ($propertyInput['floor_space'] == null) {
            $propertyInput['floor_space'] = 0;
        }

        //create unique reference number
        switch ($propertyInput['listing_type']):
    case 'wesell':
    $sale_ref = DB::select('SELECT `sale_ref` FROM `reference` WHERE `id` = ?', [1]);
        $sale_ref = $sale_ref[0]->sale_ref;
        $reference = 'SALE-'.str_pad($sale_ref, 6, '0', STR_PAD_LEFT);
        $sale_ref++;
        DB::update('UPDATE `reference` SET `sale_ref` = ?, `updated_at` = NOW()', [$sale_ref]);
        $propertyInput['listing_type'] = 'in2assets';

        break;
        case 'weauction':
    $auction_ref = DB::select('SELECT `auction_ref` FROM `reference` WHERE `id` = ?', [1]);
        $auction_ref = $auction_ref[0]->auction_ref;
        $reference = 'AUCT-'.str_pad($auction_ref, 6, '0', STR_PAD_LEFT);
        $auction_ref++;
        DB::update('UPDATE `reference` SET `auction_ref` = ?, `updated_at` = NOW()', [$auction_ref]);

        break;
        case 'in2rental':
    $rent_ref = DB::select('SELECT `rent_ref` FROM `reference` WHERE `id` = ?', [1]);
        $rent_ref = $rent_ref[0]->rent_ref;
        $reference = 'RENT-'.str_pad($rent_ref, 6, '0', STR_PAD_LEFT);
        $rent_ref++;
        DB::update('UPDATE `reference` SET `rent_ref` = ?, `updated_at` = NOW()', [$rent_ref]);
        $propertyInput['listing_type'] = 'in2rental';

        break;
        default:
    return Redirect::back()->withInput()->withErrors('Please select a Listing Type');

        break;
        endswitch;

        $propertyInput['reference'] = $reference;

        if (! $this->property->fill($propertyInput)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->property->errors);
        }

        $this->property['postcode'] = Request::get('postcode');

        $this->property->save();

        if ($propertyInput['property_type'] === 'Residential') {
            $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);
            $resInput['property_id'] = $this->property->id;
            $residential = new Residential();
            if ($resInput['dwelling_type'] == 'Vacant Land') {
                $resInput = Request::only(['dwelling_type']);
                $resInput['property_id'] = $this->property->id;

                if (! $residential->fill($resInput)->isValid2()) {
                    return Redirect::back()->withInput()->withErrors($residential->errors);
                }
            } else {
                if (! $residential->fill($resInput)->isValid()) {
                    return Redirect::back()->withInput()->withErrors($residential->errors);
                }
            }

            $residential->save();
        }

        if ($propertyInput['property_type'] === 'Commercial') {
            $comInput = Request::only(['commercial_type', 'parking']);
            $comInput['property_id'] = $this->property->id;
            $commercial = new Commercial();
            if (! $commercial->fill($comInput)->isValid()) {
                return Redirect::back()->withInput()->withErrors($commercial->errors);
            }
            $commercial->save();
        }

        if ($propertyInput['property_type'] === 'Farm') {
            $farmInput = Request::only(['farm_type']);
            $farmInput['property_id'] = $this->property->id;
            $farm = new Farm();
            if (! $farm->fill($farmInput)->isValid()) {
                return Redirect::back()->withInput()->withErrors($farm->errors);
            }
            $farm->save();
        }

        $vettingInput = Request::only(['ownership_type', 'rep_email', 'rep_cell_num', 'rep_firstname', 'rep_surname']);

        $vettingInput['property_id'] = $this->property->id;

        $vettingInput['vetting_status'] = 1;
        $vettingInput['owner_title'] = 'NA';
        $vettingInput['owner_firstname'] = $propertyInput['company'];
        $vettingInput['owner_surname'] = 'In2Assets';
        $vettingInput['marriage_stat'] = 'NA';
        $vettingInput['rep_id_number'] = Request::get('agents');

        // validate and save to vetting table if not valid return and delete $this->property
        $vetting = new Vetting();

        $vetting->fill($vettingInput)->save();

        return Redirect::route('admin.property.createcont', $this->property->id);
    }

    public function adminCreatecont($id)
    {
        $property = $this->getPropertyDetsbyId($id);

        $user = $this->user->whereId($property->user_id)->first();

        $files = $user->files()->where('link', '=', $id)->where('type', '=', 'image')->with('user')->get();

        return view::make('property.admin.createcont', ['property' => $property, 'files' => $files]);
    }

    public function adminContStore()
    {
        $property_id = Request::get('id');

        $property = $this->property->whereId($property_id)->first();
        $this->vetting = $this->vetting->whereProperty_id($property_id)->first();
        $propertyInput = Request::only('long_descrip', 'short_descrip', 'lead_image', 'image_gallery', 'youtube');
        $vettingInput = Request::only('id_photos', 'resolution');

        if (! $property->fill($propertyInput)->isValid2()) {
            return Redirect::back()->withInput()->withErrors($property->errors);
        }

        $this->vetting->vetting_status = 3;

        $property->save();

        $this->vetting->fill($vettingInput)->save();

        return Redirect::route('property.admin', ['status' => 'all']);
    }

    public function adminCloseProp($id)
    {
        $property = $this->getPropertyDetsbyId($id);

        return view('property.admin.closeProp', ['property' => $property]);
    }

    public function adminClose()
    {
        $input = Request::all();

        $id = $input['property_id'];
        $vetting_status = $input['vetting_status'];

        unset($input['property_id']);
        unset($input['vetting_status']);

        $vetting = $this->vetting->whereProperty_id($id)->first();

        $vetting->vetting_status = $vetting_status;

        $vetting->save();

        $property = $this->property->whereId($id)->first();

        $property->fill($input)->save();

        if ($vetting_status == '6' || $vetting_status == '7' || $vetting_status == '11') {
            $property->delete();
        }

        return Redirect::route('property.admin', ['status' => 'all']);
    }

    public function searchFavorites()
    {
        $user = Auth::user()->id; //Get Authed User
        $favorites = DB::table('property_user')->select('property_id')->whereUser_id($user)->get(); // Get IDs of all favorited properties for user that are live
        $resultArray = json_decode(json_encode($favorites), true); // Convert to array

        $properties = $this->property->
        select('property.*', 'residential.*', 'commercial.*', 'farm.*', 'vetting.*')->
        join('vetting', 'property.id', '=', 'vetting.property_id')->leftJoin('farm', 'property.id', '=', 'farm.property_id')->leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        whereIn('property.id', $resultArray)->
        where(function ($q) {
            $q->where('vetting_status', '=', 2)
            ->orWhere('vetting_status', '=', 4)
            ->orWhere('vetting_status', '=', 5)
            ->orWhere('vetting_status', '=', 10)
            ->orWhere('vetting_status', '=', 12)
            ->orWhere('vetting_status', '=', 13)
            ->orWhere('vetting_status', '=', 14)
            ->orWhere('vetting_status', '=', 15)
            ->orWhere('vetting_status', '=', 16);
        })->
        get();

        foreach ($properties as $property) {
            $property['smallImage'] = substr_replace($property['lead_image'], '_265_163', -4, 0);
        }

        return view('property.favorites', ['properties' => $properties, 'listing' => 'Search']);
    }

    public function viewOffers($listing_type)
    {
        return view('property.admin.offers', ['listing_type' => $listing_type]);
    }

    public function getAdminOffersTable($listing_type)
    {
        switch ($listing_type):
        case 'all':
        $listing_type = '%';

        break;
        case 'Auction':
        $listing_type = '%weauction%';

        break;
        case 'In2assets':
        $listing_type = '%in2assets%';

        break;
        case 'WeSell':
        $listing_type = '%wesell%';

        break;
        endswitch;

        $properties = DB::table('property')
        ->join('make_contact', 'make_contact.property_id', '=', 'property.id')
        ->where('listing_type', 'LIKE', $listing_type)
        ->where('property.deleted_at', '=', null)
        ->where('make_contact.deleted_at', '=', null)
        ->join('users', 'users.id', '=', 'property.user_id')
        ->select('reference', 'property.province', 'property.suburb_town_city', 'property_type', 'firstname', 'lastname', 'make_contact.offer', 'property_id', 'listing_type', 'make_contact.id', 'property.user_id', 'make_contact.id')
        ->get();

        foreach ($properties as $property) {
            $offer = DB::table('property')
            ->join('make_contact', 'make_contact.property_id', '=', 'property.id')
            ->where('make_contact.id', '=', $property->id)
            ->join('users', 'users.id', '=', 'make_contact.user_id')
            ->select('firstname', 'lastname', 'make_contact.user_id')
            ->first();

            $property->offer_firstname = $offer->firstname;
            $property->offer_lastname = $offer->lastname;
            $property->offer_user_id = $offer->user_id;
        }

        return Datatable::collection(new Collection($properties))
        ->showColumns('reference', 'province', 'suburb_town_city', 'property_type')
        ->addColumn('owner', function ($Property) {
            return $Property->firstname.' '.$Property->lastname;
        })
        ->addColumn('madeOffer', function ($Property) {
            return $Property->offer_firstname.' '.$Property->offer_lastname;
        })
        ->showColumns('offer')
        ->addColumn('listing_type', function ($Property) {
            switch ($Property->listing_type):
            case 'weauction':
            return 'Auction';

            break;
            case 'in2assets':
            return 'In2Assets';

            break;
            case 'wesell':
            return 'WeSell';

            break;
            endswitch;
        })
        ->addColumn('action', function ($Property) {
            if ($Property->listing_type == 'wesell') {
                return '<a href="/admin/users/edit/'.$Property->user_id.'" title="View Seller" ><i class="i-circled i-light i-small icon-laptop"></i></a>
                <a href="/admin/users/edit/'.$Property->offer_user_id.'" title="View Buyer" ><i class="i-circled i-light i-small icon-diamond"></i></a>
                <a href="/admin/messages/'.$Property->offer_user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                <a href="/portal/makecontact/delete/'.$Property->id.'" title="Delete Offer" ><i class="i-circled i-light i-small icon-remove" style="background-color: #ff0000"></i></a>';
            } else {
                return '<a href="/admin/users/edit/'.$Property->offer_user_id.'" title="View Buyer" ><i class="i-circled i-light i-small icon-diamond"></i></a>
                <a href="/admin/messages/'.$Property->user_id.'/'.$Property->property_id.'/property/create" title="Message User" ><i class="i-circled i-light i-alt i-small icon-comments"></i></a>
                <a href="/portal/makecontact/delete/'.$Property->id.'" title="Delete Offer" ><i class="i-circled i-light i-small icon-remove" style="background-color: #ff0000"></i></a>';
            }
        })
->searchColumns('reference', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname', 'listing_type')
->orderColumns('reference', 'province', 'suburb_town_city', 'property_type', 'vetting_status', 'firstname', 'lastname', 'listing_type')
->make();
    }

    public function featureProperty()
    {
        $input = Request::all();

        $dtStart = new DateTime('today');
        $dtStart->modify('+1 day');
        $dtStart = $dtStart->format('Y-m-d');

        $dtEnd = new DateTime($dtStart);
        $dtEnd->modify('+'.$input['days'].'day');
        $dtEnd = $dtEnd->format('Y-m-d');

        $property_id = $input['property_id'];
        $amount = $input['amount'];

        $this->featured->property_id = $property_id;
        $this->featured->amount_paid = $amount;
        $this->featured->start_date = $dtStart;
        $this->featured->end_date = $dtEnd;

        $this->featured->save();

        return $this->featured->id;
    }

    public function calendar($property_id)
    {
        $property = $this->property->whereId($property_id)->first();
        if ($property->listing_type == 'weauction') {
            $auctionDets = $this->auction->whereProperty_id($property_id)->first();
            $yourBids = $this->auction_bids->whereUser_id(Auth::user()->id)->orderBy('id', 'desc')->first();
            if ($auctionDets->auction_type == 2 || $auctionDets->auction_type == 3) {
                $register = $this->auction_register->whereUser_id(Auth::user()->id)->whereAuction_id($auctionDets->id)->first();
            } else {
                $yourBids = null;
                $register = null;
            }
            $auctionCalendarShow = $this->property->with('auction')->with('residential')->with('commercial')->with('farm')->whereId($property_id)->first();
        } else {
            $auctionDets = null;
            $yourBids = null;
            $auctionCalendarShow = null;
        }

        return view('wesell.property.calendar', ['property' => $property, 'auctionDets' => $auctionDets, 'yourBids' => $yourBids, 'auctionCalendarShow' => $auctionCalendarShow]);
    }

    public function history($id)
    {
        $images = $this->property->
    join('uploads', 'uploads.link', '=', 'property.id')->whereClass('property')->whereType('image')->where('property.id', $id)->withTrashed()->get();

        switch ($images[0]['property_type']):
    case 'Residential':
    $property = $this->property->join('residential', 'property.id', '=', 'residential.property_id')
    ->join('vetting', 'property.id', '=', 'vetting.property_id')
    ->where('property.id', $id)->withTrashed()->first();

        break;
        case 'Commercial':
    $property = $this->property->join('commercial', 'property.id', '=', 'commercial.property_id')
    ->join('vetting', 'property.id', '=', 'vetting.property_id')
    ->where('property.id', $id)->withTrashed()->first();

        break;
        case 'Farm':
    $property = $this->property->join('farm', 'property.id', '=', 'farm.property_id')
    ->join('vetting', 'property.id', '=', 'vetting.property_id')
    ->where('property.id', $id)->withTrashed()->first();

        break;
        default:
    return Redirect::route('property.index');

        break;
        endswitch;

        $contact = DB::table('users')->whereId($property->user_id)->first();

        return view('property.admin.history', ['property' => $property, 'images' => $images,
        'contact' => $contact, ]);
    }

    public function trimAreas()
    {
        $provinces = DB::table('areas')->groupBy('province')->pluck('province');
        foreach ($provinces as $province) {
            DB::table('areas')->where('province', '=', $province)->update(['province'=> trim($province)]);
        }
        $areas = DB::table('areas')->groupBy('area')->pluck('area');
        foreach ($areas as $area) {
            DB::table('areas')->where('area', '=', $area)->update(['area'=> trim($area)]);
        }
        $suburb_town_citys = DB::table('areas')->groupBy('suburb_town_city')->pluck('suburb_town_city');
        foreach ($suburb_town_citys as $suburb_town_city) {
            DB::table('areas')->where('suburb_town_city', '=', $suburb_town_city)->update(['suburb_town_city'=> trim($suburb_town_city)]);
        }
    }

    public function getAgentDetails()
    {
        $id = Request::get('agent_id');

        $agent_dets = $this->user->whereId($id)->first();

        return ['agent_dets' => $agent_dets];
    }

    public function getAgentProperties($id)
    {
        $adv_search_set['property_type'] = '';
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        $property_types[0] = 'Commercial';
        $property_types[1] = 'Farm';
        $property_types[2] = 'Residential';

        return $this->searchResult('', '', $adv_search_set, $property_types, $id);
    }

    public function searchRes()
    {
        $searchfield = '';
        $listing_type = 'wesell';

        $adv_search_set['property_type'] = 'Residential';
        $adv_search_set['province'] = '';
        $adv_search_set['area'] = '';
        $adv_search_set['suburb_town_city'] = '';
        $adv_search_set['sale_type'] = 'all';
        $adv_search_set['floor_space_range'] = '0 - 2000+';
        $adv_search_set['land_size_range'] = '0 - 20000+';
        $adv_search_set['price_range'] = '0 - 15000000+';
        $adv_search_set['price_range_rent'] = '0 - 30000+';

        $property_types[0] = 'Residential';
        $property_types[1] = null;
        $property_types[2] = null;

        return $this->searchResult($searchfield, $listing_type, $adv_search_set, $property_types);
    }

    public function searchCom()
    {
        $input = Request::all();

        $searchfield = $input['CommercialSearch'];

        $listing_type = $input['listing_type'];

        if (! $id = $this->searchReference($searchfield)) {
            $adv_search_set['property_type'] = 'Commercial';
            $adv_search_set['province'] = '';
            $adv_search_set['area'] = '';
            $adv_search_set['suburb_town_city'] = '';
            $adv_search_set['sale_type'] = 'all';
            $adv_search_set['floor_space_range'] = '0 - 2000+';
            $adv_search_set['land_size_range'] = '0 - 20000+';
            $adv_search_set['price_range'] = '0 - 15000000+';
            $adv_search_set['price_range_rent'] = '0 - 30000+';

            $property_types[0] = 'Commercial';
            $property_types[1] = 'Farm';
            $property_types[2] = null;

            return $this->searchResult($searchfield, $listing_type, $adv_search_set, $property_types);
        } else {
            return Redirect::route('property.show', ['id' => $id]);
        }
    }

    public function searchReference($searchField)
    {
        $refCheck = strtoupper(substr($searchField, 0, 5));

        if ($refCheck == 'SALE-' || $refCheck == 'RENT-' || $refCheck == 'AUCT-') {
            if ($property = $this->property->
            join('vetting', 'property.id', '=', 'vetting.property_id')->
            with('residential')->
            with('commercial')->
            with('farm')->
            with('auction')->
            where('property.reference', '=', $searchField)->
            where(function ($q) {
                $q->where('vetting_status', '=', 2)
                ->orWhere('vetting_status', '=', 4)
                ->orWhere('vetting_status', '=', 5)
                ->orWhere('vetting_status', '=', 10)
                ->orWhere('vetting_status', '=', 12)
                ->orWhere('vetting_status', '=', 13)
                ->orWhere('vetting_status', '=', 14)
                ->orWhere('vetting_status', '=', 15)
                ->orWhere('vetting_status', '=', 16);
            })->first()) {
                return $property->property_id;
            } else {
                return false;
            }
        }
    }

    public function searchResult($place, $listing_type, $adv_search_set, $property_types, $agent = '%')
    {
        $featured = $this->getFeaturedProperties();

        switch ($listing_type) {
     case 'wesell':
     $adv_search_set['listing_type'] = 'wesell';

     break;
     case 'welet':
     $adv_search_set['listing_type'] = 'welet';

     break;
     case 'weauction':
     $adv_search_set['listing_type'] = 'weauction';

     break;
     default:
     $adv_search_set['listing_type'] = 'all';

     break;
 }

        $areas = $this->get_adv_areas();

        $first_prop = rand(1, count($featured));

        $properties = $this->property->
 join('vetting', 'property.id', '=', 'vetting.property_id')->
 leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
 leftJoin('residential', 'property.id', '=', 'residential.property_id')->
 leftJoin('farm', 'property.id', '=', 'farm.property_id')->
 leftJoin('auction', 'property.id', '=', 'auction.property_id')->
 with('commercial')->
 with('residential')->
 with('farm')->
 with('auction')->
 with('vetting')->
 where('vetting.rep_id_number', 'LIKE', $agent)->
 where(function ($lq) use ($listing_type) {
     switch ($listing_type) {
     case 'wesell':
     $lq->where('listing_type', '=', 'wesell');

     break;
     case 'welet':
     $lq->where('listing_type', '=', 'welet')
     ->orWhere('listing_type', '=', 'in2rental');

     break;
     case 'weauction':
     $lq->where('listing_type', '=', 'weauction');

     break;
 }
 })->
 where(function ($q) {
     $q->where('vetting_status', '=', 2)
    ->orWhere('vetting_status', '=', 4)
    ->orWhere('vetting_status', '=', 5)
    ->orWhere('vetting_status', '=', 10)
    ->orWhere('vetting_status', '=', 12)
    ->orWhere('vetting_status', '=', 13)
    ->orWhere('vetting_status', '=', 14)
    ->orWhere('vetting_status', '=', 15)
    ->orWhere('vetting_status', '=', 16);
 })->
 get();

        foreach ($properties as $property) {
            $property['id'] = $this->getIdbyReference($property['reference']);

            $property['smallImage'] = substr_replace($property['lead_image'], '_265_163', -4, 0);
        }

        return view('wesell.property.searchResult', ['properties' => $properties, 'listing' => 'Search', 'featured' => $featured,
    'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'], 'cities_adv' => $areas['cities'],
    'areas_adv' => $areas['areas'], 'adv_search_set' => $adv_search_set, ]);
    }

    public function getIdbyReference($reference)
    {
        $this->property = $this->property->select('id')->whereReference($reference)->first();

        return $this->property->id;
    }

    public function search()
    {
        $input = Request::all();

        // gets max min price from slider range
        // ****************************************
        if ($input['listing_type'] === 'welet') {
            $MinMaxPrices = explode('-', $input['price_range_rent']);
            $minPrice = substr(trim($MinMaxPrices[0]), 1);
            $maxPrice = substr(trim($MinMaxPrices[1]), 1);
        } else {
            $MinMaxPrices = explode('-', $input['price_range']);
            $minPrice = substr(trim($MinMaxPrices[0]), 1);
            $maxPrice = substr(trim($MinMaxPrices[1]), 1);
        }

        //********************************************

        // gets max min land size and floor space from slider range
        // ****************************************
        $MinMaxLand = explode('-', $input['land_size_range']);
        $MinMaxFloor = explode('-', $input['floor_space_range']);

        $minLand = trim($MinMaxLand[0]);
        $maxLand = trim($MinMaxLand[1]);

        $minFloor = trim($MinMaxFloor[0]);
        $maxFloor = trim($MinMaxFloor[1]);

        //********************************************

        // filter through general search criteria

        if ($input['province'] != '') {
            $province = '%'.trim($input['province']);
        } else {
            $province = '%';
        }
        if ($input['suburb_town_city'] != '') {
            $suburb_town_city = '%'.trim($input['suburb_town_city']);
        } else {
            $suburb_town_city = '%';
        }

        if ($input['area'] != '') {
            $area = '%'.trim($input['area']);
        } else {
            $area = '%';
        }
        if ($input['listing_type'] != 'all') {
            $listing_type = '%'.$input['listing_type'];
        } else {
            $listing_type = '%';
        }

        if ($input['sale_type'] != 'all') {
            $sale_type = '%'.$input['sale_type'];
        } else {
            $sale_type = '%';
        }

        $price_min = $minPrice;

        if (substr($maxPrice, -1) !== '+') {
            $price_max = $maxPrice;
        } else {
            $price_max = '1.79E+308';
        }

        $land_min = $minLand;

        if (substr($maxLand, -1) !== '+') {
            $land_max = $maxLand;
        } else {
            $land_max = '1.79E+308';
        }

        $floor_min = $minFloor;

        if (substr($maxFloor, -1) !== '+') {
            $floor_max = $maxFloor;
        } else {
            $floor_max = '1.79E+308';
        }

        //switch through property types

        switch ($input['property_type']):
   // dd($listing_type);
    case 'Residential':

//filter residential search criteria

    if (Request::get('tennis_court')) {
        $tennis = '1';
    } else {
        $tennis = '0';
    }
        if (Request::get('pets')) {
            $pets = '1';
        } else {
            $pets = '0';
        }
        if (Request::get('aircon')) {
            $aircon = '1';
        } else {
            $aircon = '0';
        }
        if (Request::get('bic')) {
            $bic = '1';
        } else {
            $bic = '0';
        }
        if (Request::get('pool')) {
            $pool = '1';
        } else {
            $pool = '0';
        }

        if (Request::get('security')) {
            $electric_fence = '1';
            $sec_lights = '1';
            $sec_fence = '1';
            $elec_gate = '1';
            $alarm = '1';
            $intercom = '1';
        } else {
            $electric_fence = '0';
            $sec_lights = '0';
            $sec_fence = '0';
            $elec_gate = '0';
            $alarm = '0';
            $intercom = '0';
        }

        //get properties fulfilling search criteria
        $properties = $this->property->with('residential')->
    join('vetting', 'property.id', '=', 'vetting.property_id')->
    leftJoin('residential', 'property.id', '=', 'residential.property_id')->
    leftJoin('auction', 'property.id', '=', 'auction.property_id')->
    with('residential')->
    with('auction')->
    with('vetting')->
    where(function ($q) {
        $q->where('vetting_status', '=', 2)
        ->orWhere('vetting_status', '=', 4)
        ->orWhere('vetting_status', '=', 5)
        ->orWhere('vetting_status', '=', 10)
        ->orWhere('vetting_status', '=', 12)
        ->orWhere('vetting_status', '=', 13)
        ->orWhere('vetting_status', '=', 14)
        ->orWhere('vetting_status', '=', 15)
        ->orWhere('vetting_status', '=', 16);
    })->
    where(function ($qa) use ($listing_type) {
        switch ($listing_type) {
            case '%welet':
            $qa->where('property.listing_type', '=', 'welet')->
            orWhere('property.listing_type', '=', 'in2rental');

            break;
            case '%wesell':
            $qa->where('property.listing_type', '=', 'wesell')->
            orWhere('property.listing_type', '=', 'in2assets');

            break;
            case '%weauction':
            $qa->where('property.listing_type', '=', 'weauction');

            break;
            case '%':
            $qa->where('property.listing_type', 'LIKE', '%');

            break;
        }
    })->
    where('property.sale_type', 'LIKE', $sale_type)->
    where('property.province', 'LIKE', $province)->
    where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
    where('property.area', 'LIKE', $area)->
    where('property.land_size', '>=', $land_min)->
    where('property.floor_space', '>=', $floor_min)->
    where('property.tennis_court', '<=', $tennis)->
    where('property.pets', '<=', $pets)->
    where('property.aircon', '<=', $aircon)->
    where('property.bic', '<=', $bic)->
    where('property.pool', '<=', $pool)->
    where('property.electric_fence', '<=', $electric_fence)->
    where('property.sec_lights', '<=', $sec_lights)->
    where('property.sec_fence', '<=', $sec_fence)->
    where('property.elec_gate', '<=', $elec_gate)->
    where('property.alarm', '<=', $alarm)->
    where('property.intercom', '<=', $intercom)->
    where('property.price', '>=', $price_min)->
    where('property.price', '<=', $price_max)->
    whereHas('residential', function ($q) {
        $input = Request::all();

        if ($input['bedrooms'] != '') {
            $bedrooms = '%'.$input['bedrooms'];
        } else {
            $bedrooms = '%';
        }

        if ($input['bathrooms'] != '') {
            $bathrooms = '%'.$input['bathrooms'];
        } else {
            $bathrooms = '%';
        }

        if ($input['dwelling_type'] != '') {
            $dwelling_type = '%'.$input['dwelling_type'];
        } else {
            $dwelling_type = '%';
        }

        $q->where('residential.bedrooms', 'LIKE', $bedrooms)->
        where('residential.bathrooms', 'LIKE', $bathrooms)->
        where('residential.dwelling_type', 'LIKE', $dwelling_type);
    })->
    get();

        break;

        case 'Commercial':
//filter commercial search criteria
    if ($input['commercial_type'] != '') {
        $commercial_type = '%'.$input['commercial_type'];
    } else {
        $commercial_type = '%';
    }
        //get properties fulfilling search criteria
        $properties = $this->property->
    join('vetting', 'property.id', '=', 'vetting.property_id')->
    leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
    leftJoin('auction', 'property.id', '=', 'auction.property_id')->
    with('commercial')->
    with('auction')->
    with('vetting')->
    where(function ($q) {
        $q->where('vetting_status', '=', 2)
        ->orWhere('vetting_status', '=', 4)
        ->orWhere('vetting_status', '=', 5)
        ->orWhere('vetting_status', '=', 10)
        ->orWhere('vetting_status', '=', 12)
        ->orWhere('vetting_status', '=', 13)
        ->orWhere('vetting_status', '=', 14)
        ->orWhere('vetting_status', '=', 15)
        ->orWhere('vetting_status', '=', 16);
    })->
    where(function ($qa) use ($listing_type) {
        switch ($listing_type) {
            case '%welet':
            $qa->where('property.listing_type', '=', 'welet')->
            orWhere('property.listing_type', '=', 'in2rental');

            break;
            case '%wesell':
            $qa->where('property.listing_type', '=', 'wesell')->
            orWhere('property.listing_type', '=', 'in2assets');

            break;
            case '%weauction':
            $qa->where('property.listing_type', '=', 'weauction');

            break;
            case '%':
            $qa->where('property.listing_type', 'LIKE', '%');

            break;
        }
    })->
    where('property.sale_type', 'LIKE', $sale_type)->
    where('property.province', 'LIKE', $province)->
    where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
    where('property.area', 'LIKE', $area)->
    where('property.land_size', '>=', $land_min)->
    where('property.land_size', '<=', $land_max)->
    where('property.floor_space', '>=', $floor_min)->
    where('property.floor_space', '<=', $floor_max)->
    where('property.price', '>=', $price_min)->
    where('property.price', '<=', $price_max)->
    where('commercial.commercial_type', 'LIKE', $commercial_type)->
    get();

        break;

        case 'Farm':
//filter farm search criteria

//get properties fulfilling search criteria
    $properties = $this->property->
    join('vetting', 'property.id', '=', 'vetting.property_id')->
    leftJoin('farm', 'property.id', '=', 'farm.property_id')->
    leftJoin('auction', 'property.id', '=', 'auction.property_id')->
    with('farm')->
    with('auction')->
    with('vetting')->
    where(function ($q) {
        $q->where('vetting_status', '=', 2)
        ->orWhere('vetting_status', '=', 4)
        ->orWhere('vetting_status', '=', 5)
        ->orWhere('vetting_status', '=', 10)
        ->orWhere('vetting_status', '=', 12)
        ->orWhere('vetting_status', '=', 13)
        ->orWhere('vetting_status', '=', 14)
        ->orWhere('vetting_status', '=', 15)
        ->orWhere('vetting_status', '=', 16);
    })->
    where(function ($qa) use ($listing_type) {
        switch ($listing_type) {
            case '%welet':
            $qa->where('property.listing_type', '=', 'welet')->
            orWhere('property.listing_type', '=', 'in2rental');

            break;
            case '%wesell':
            $qa->where('property.listing_type', '=', 'wesell')->
            orWhere('property.listing_type', '=', 'in2assets');

            break;
            case '%weauction':
            $qa->where('property.listing_type', '=', 'weauction');

            break;
            case '%':
            $qa->where('property.listing_type', 'LIKE', '%');

            break;
        }
    })->
    where('property.sale_type', 'LIKE', $sale_type)->
    where('property.province', 'LIKE', $province)->
    where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
    where('property.area', 'LIKE', $area)->
    where('property.land_size', '>=', $land_min)->
    where('property.land_size', '<=', $land_max)->
    where('property.floor_space', '>=', $floor_min)->
    where('property.floor_space', '<=', $floor_max)->
    where('property.price', '>=', $price_min)->
    where('property.price', '<=', $price_max)->
    whereHas('farm', function ($fq) {
        $input = Request::all();
        if ($input['farm_type'] != 'null') {
            $farm_type = '%'.$input['farm_type'];
        } else {
            $farm_type = '%';
        }
        $fq->where('farm.farm_type', 'LIKE', $farm_type);
    })->
    get();

        break;

        default:
// dd(here);
//get properties if property_type is not selected
    $properties = $this->property->
    join('vetting', 'property.id', '=', 'vetting.property_id')->
    leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
    leftJoin('residential', 'property.id', '=', 'residential.property_id')->
    leftJoin('farm', 'property.id', '=', 'farm.property_id')->
    leftJoin('auction', 'property.id', '=', 'auction.property_id')->
    with('commercial')->
    with('residential')->
    with('farm')->
    with('auction')->
    with('vetting')->
    where(function ($q) {
        $q->where('vetting_status', '=', 2)
        ->orWhere('vetting_status', '=', 4)
        ->orWhere('vetting_status', '=', 5)
        ->orWhere('vetting_status', '=', 10)
        ->orWhere('vetting_status', '=', 12)
        ->orWhere('vetting_status', '=', 13)
        ->orWhere('vetting_status', '=', 14)
        ->orWhere('vetting_status', '=', 15)
        ->orWhere('vetting_status', '=', 16);
    })->
    where(function ($qa) use ($listing_type) {
        switch ($listing_type) {
            case '%welet':
            $qa->where('property.listing_type', '=', 'welet')->
            orWhere('property.listing_type', '=', 'in2rental');

            break;
            case '%wesell':
            $qa->where('property.listing_type', '=', 'wesell')->
            orWhere('property.listing_type', '=', 'in2assets');

            break;
            case '%weauction':
            $qa->where('property.listing_type', '=', 'weauction');

            break;
            case '%':
            $qa->where('property.listing_type', 'LIKE', '%');

            break;
        }
    })->
    where('property.sale_type', 'LIKE', $sale_type)->
    where('property.province', 'LIKE', $province)->
    where('property.suburb_town_city', 'LIKE', $suburb_town_city)->
    where('property.area', 'LIKE', $area)->
    where('property.land_size', '>=', $land_min)->
    where('property.land_size', '<=', $land_max)->
    where('property.floor_space', '>=', $floor_min)->
    where('property.floor_space', '<=', $floor_max)->
    get();

        break;
        endswitch;

        foreach ($properties as $property) {
            $property['id'] = $this->getIdbyReference($property['reference']);

            $prop_price = $this->property->whereId($property['id'])->first();

            $property->price = $prop_price['price'];
        }

        $featured = $this->getFeaturedProperties();
        $first_prop = rand(1, count($featured));

        $areas = $this->get_adv_areas();

        foreach ($properties as $property) {
            $property['smallImage'] = substr_replace($property['lead_image'], '_265_163', -4, 0);
        }

        return view('property.searchResult', ['properties' => $properties, 'listing' => 'Search', 'featured' => $featured,
        'first_prop' => $first_prop, 'provinces_adv' => $areas['provinces'], 'cities_adv' => $areas['cities'],
        'areas_adv' => $areas['areas'], 'adv_search_set' => $input, ]);
    }

    public function AddWeSellProperty()
    {
        $input = Request::all();

        $input['user_id'] = Auth::user()->id;
        $input['country'] = 'South Africa';
        $input['listing_type'] = 'wesell';

        $input['property_type'] = 'Residential';

        if ($input['street_address'] != '') {
            $address = $input['street_address'].' ';
        } else {
            $address = '';
        }

        if ($input['street_number'] != '') {
            $street_number = $input['street_number'].' ';
        } else {
            $street_number = '';
        }

        if ($input['suburb_town_city'] != '') {
            $suburb_town_city = $input['suburb_town_city'].' ';
        } else {
            $suburb_town_city = '';
        }

        if ($input['area'] != '') {
            $area = $input['area'].' ';
        } else {
            $area = '';
        }

        if ($input['province'] != '') {
            $province = $input['province'].' ';
        } else {
            $province = '';
        }

        if ($suburb_town_city == $area) {
            $area = '';
        }
        $country = 'South Africa';

        $address = $street_number.$address.$suburb_town_city.' '.$area.' '.$province.' '.$country; // Google HQ
        $prepAddr = str_replace(' ', '+', $address);
        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output = json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;

        $input['x_coord'] = $latitude;
        $input['y_coord'] = $longitude;

        $sale_ref = DB::select('SELECT `sale_ref` FROM `reference` WHERE `id` = ?', [1]);
        $sale_ref = $sale_ref[0]->sale_ref;
        $reference = 'SALE-'.str_pad($sale_ref, 6, '0', STR_PAD_LEFT);
        $sale_ref++;
        DB::update('UPDATE `reference` SET `sale_ref` = ?, `updated_at` = NOW()', [$sale_ref]);

        $input['reference'] = $reference;

        $this->property->fill($input);

        $this->property->save();

        DB::table('residential')->insert(['property_id' => $this->property->id, 'created_at' => date('Y-m-d H:i:s')]);

        DB::table('vetting')->insert(['property_id' => $this->property->id, 'ownership_type' => $input['ownership_type'],
            'vetting_status' => 'Pending', 'created_at' => date('Y-m-d H:i:s'), ]);

        return Redirect::route('wesell-congratulations', ['id' => $this->property->id]);
    }

    public function ManageWesell()
    {
        $id = Request::get('id');
        $propertyInput = Request::only(['lead_image', 'image_gallery', 'short_descrip', 'long_descrip', 'price', 'floor_space', 'land_size',
            'land_measurement', 'province', 'area', 'suburb_town_city', 'price', 'x_coord', 'y_coord',
            'complex_number', 'complex_name', 'street_number', 'street_address', 'company', 'owner_id', 'reg_num', ]);

        if ($propertyInput['land_measurement'] == 'HA') {
            $propertyInput['land_size'] = $propertyInput['land_size'] * 10000;
        }

        //RESIDENTIAL
        //check whether amenity and security checkboxes have been checked and set input value to 1 if checked

        if (Request::get('pets')) {
            $pets = 1;
        } else {
            $pets = 0;
        }
        $propertyInput['pets'] = $pets;
        if (Request::get('granny_flat')) {
            $granny_flat = 1;
        } else {
            $granny_flat = 0;
        }
        $propertyInput['granny_flat'] = $granny_flat;
        if (Request::get('view')) {
            $view = 1;
        } else {
            $view = 0;
        }
        $propertyInput['view'] = $view;
        if (Request::get('pool')) {
            $pool = 1;
        } else {
            $pool = 0;
        }
        $propertyInput['pool'] = $pool;
        if (Request::get('staff_quarters')) {
            $staff_quarters = 1;
        } else {
            $staff_quarters = 0;
        }
        $propertyInput['staff_quarters'] = $staff_quarters;
        if (Request::get('security_system')) {
            $security_system = 1;
        } else {
            $security_system = 0;
        }
        $propertyInput['security_system'] = $security_system;
        if (Request::get('gated_estate')) {
            $gated_estate = 1;
        } else {
            $gated_estate = 0;
        }
        $propertyInput['gated_estate'] = $gated_estate;

        $this->property = $this->property->find($id);
        $this->property->fill($propertyInput)->update();

        $resInput = Request::only(['dwelling_type', 'bedrooms', 'bathrooms', 'parking']);
        $resInput['property_id'] = $this->property->id;

        // validate and save to residential table if not valid return and delete $this->property
        $residential = new Residential();
        $residential = $residential->whereProperty_id($id)->first();
        $residential->fill($resInput)->update();

        $tab = Request::get('open_tab');

        $vetting = new Vetting();
        $vetting = $vetting->whereProperty_id($id)->first();

        if (Request::get('submit')) {
            $vetting->vetting_status = 3;
            $vetting->update();
            Flash::message('Your property has been submitted');
        } else {
            Flash::message('Your property has been updated');
        }

        return Redirect::route('wesell-property_management', ['id' => $id, 'tab' => $tab]);
    }
}
