<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use In2Assets\Users\User;

class FunctionController extends Controller
{
    public function __construct(Property $property, Vetting $vetting, User $user)
    {
        $this->property = $property;
        $this->vetting = $vetting;
        $this->user = $user;
    }

    public function addOldData()
    {
        $old_data = DB::select('SELECT * FROM old_data');

        foreach ($old_data as $update_data) {
            $this->property = new Property();
            $this->property->agent_id = $update_data->agent_id;
            $this->property->reference = $update_data->reference;
            $this->property->x_coord = $update_data->y_coord;
            $this->property->y_coord = $update_data->x_coord;
            $this->property->street_address = $update_data->street_address;
            $this->property->area = $update_data->area;
            $this->property->suburb_town_city = $update_data->suburb_town_city;
            $this->property->province = $update_data->province;
            if ($update_data->selling_price > '999999999') {
                $price = 0;
            } else {
                $price = $update_data->selling_price;
            }
            $this->property->price = $price;
            $this->property->listing_type = $update_data->listing_type;
            $this->property->property_type = $update_data->property_type;
            $this->property->final_price = $update_data->final_price;
            $this->property->user_id = '1';

            $email = str_replace(' ', '-', strtolower($update_data->owner_firstname.'_'.$update_data->owner_surname)).'noreply@in2assets.com';
            if ($update_data->owner_firstname != '') {
                if (! $owner_id = DB::table('users')->select('id')->where('email', '=', $email)->first()) {
                    $owner_id = DB::table('users')->insertGetId(['firstname' => $update_data->owner_firstname,
                        'lastname' => $update_data->owner_surname, 'email' => $email, 'cellnumber' => $update_data->owner_cell, ]);
                    $this->property->owner_id = $owner_id;
                    $this->user->find($owner_id)->assignRole(4);
                } else {
                    if (! $this->user->find($owner_id->id)->hasRole(4)) {
                        $this->user->find($owner_id->id)->assignRole(4);
                    }
                    $this->user->find($owner_id->id)->update(['firstname' => $update_data->owner_firstname,
                        'lastname' => $update_data->owner_surname, 'email' => $email, 'cellnumber' => $update_data->owner_cell, ]);
                    $this->property->owner_id = $owner_id->id;
                }
            }

            $this->property->save();

            switch ($update_data->property_type) {
                case 'Residential':
                    DB::insert('INSERT INTO residential SET property_id = '.$this->property->id);

                    break;
                case 'Commercial':
                    DB::insert('INSERT INTO commercial SET property_id = '.$this->property->id);

                    break;
                case 'Farm':
                    DB::insert('INSERT INTO farm SET property_id = '.$this->property->id);

                    break;
            }

            DB::insert('INSERT INTO vetting SET property_id = '.$this->property->id.',vetting_status = '.$update_data->vetting_status
                .',rep_id_number ='.$update_data->agent_id);
        }
    }

    public function getActualCoords($xcoord)
    {
        $properties = $this->property->whereX_coord($xcoord)->get();

        foreach ($properties as $property) {
            if ($property->suburb_town_city != $property->area) {
                $address = $property->street_address.', '.$property->suburb_town_city.', '.$property->area.', '.
                    $property->province.', '.$property->country;
            } else {
                $address = $property->street_address.', '.$property->suburb_town_city.', '.
                    $property->province.', '.$property->country;
            }

            $address = urlencode($address);
            $url = 'https://maps.google.com/maps/api/geocode/json?sensor=false&address='.$address;
            $response = file_get_contents($url);
            $json = json_decode($response, true);

            if ($json['status'] != 'ZERO_RESULTS') {
                $lat = $json['results'][0]['geometry']['location']['lat'];
                $lng = $json['results'][0]['geometry']['location']['lng'];

                if (is_float($lat) && is_float($lng)) {
                    $property->x_coord = $lat;
                    $property->y_coord = $lng;

                    $property->save();
                }
            } else {
                echo $address.'<br>';
            }
        }

        return 'Done';
    }

    public function runFunction()
    {
        $properties = Property::all();

        foreach ($properties as $property) {
            if ($property->id != null) {
                $this->attributise($property->id);
            }
        }
    }

    public function slugForUrl()
    {
        $properties = $this->property->whereSlug('')->with('Residential')->with('Commercial')->with('Farm')->get();

        foreach ($properties as $property) {
            $slug = new Slugify();
            switch ($property->property_type) {
                case 'Commercial':
                    if (! is_object($property->commercial)) {
                        break;
                    }
                    $property_type = $property->commercial->commercial_type;

                    break;
                case 'Residential':
                    if (! is_object($property->residential)) {
                        break;
                    }
                    $property_type = $property->residential->dwelling_type;

                    break;
                case 'Farm':
                    $property_type = $property->farm->farm_type;

                    break;
            }

            switch ($property->listing_type) {
                case 'wesell':
                case 'in2assets':
                    $listing_type = 'for sale';

                    break;
                case 'welet':
                case 'in2rental':
                    if ($property->property_type == 'Commercial') {
                        $listing_type = 'to let';
                    } else {
                        $listing_type = 'to rent';
                    }

                    break;
                case 'weauction':
                    $listing_type = 'auction';

                    break;

            }

            if ($property->area == $property->suburb_town_city) {
                $property->slug = $slug->slugify($property->property_type.' '.$property_type.' '.$listing_type
                    .' '.$property->province.' '.$property->area.' '.$property->street_number
                    .' '.$property->street_address);
            } else {
                $property->slug = $slug->slugify($property->property_type.' '.$property_type.' '.$listing_type
                    .' '.$property->province.' '.$property->area.' '.$property->suburb_town_city.' '.$property->street_number
                    .' '.$property->street_address);
            }

            $property->save();
        }
    }

    public function attributise($id)
    {
        $property = App::make('PropertyController')->getPropertyDetsbyId($id);

        if (is_object($property)) {
            $bedrooms = substr($property->bedrooms, 0, 1);
            if ($bedrooms == 'S') {
                $bedrooms = 0;
            }
            $bathrooms = substr($property->bathrooms, 0, 1);
            $floor_space = $property->floor_space;
            $land_size = $property->land_size;
            $garages = substr($property->parking, 0, 1);
            if (is_nan($garages)) {
                $garages = null;
            }
            $rates = $property->rates;
            $levies = $property->levies;

            $numeric_features = App::make('AttributesController')->getAttributesByType(2);
            foreach ($numeric_features as $numeric_feature) {
//            dd($numeric_feature->id);
                switch ($numeric_feature->name) {
                    case 'bedrooms':
                        if ($bedrooms != null && $bedrooms != '') {
                            $property->assignAttribute($numeric_feature->id, $bedrooms);
                        }

                        break;
                    case 'bathrooms':
                        if ($bathrooms != null && $bathrooms != '') {
                            $property->assignAttribute($numeric_feature->id, $bathrooms);
                        }

                        break;
                    case 'floor_space':
                        if ($floor_space != null && $floor_space != '' && $floor_space != '0') {
                            $property->assignAttribute($numeric_feature->id, $floor_space);
                        }

                        break;
                    case 'land_size':
                        if ($land_size != null && $land_size != '' && $land_size != '0') {
                            $property->assignAttribute($numeric_feature->id, $land_size);
                        }

                        break;
                    case 'garages':
                        if ($garages != null && $garages != '') {
                            $property->assignAttribute($numeric_feature->id, $garages);
                        }

                        break;
                    case 'rates':
                        if ($rates != null && $rates != '') {
                            $property->assignAttribute($numeric_feature->id, $rates);
                        }

                        break;
                    case 'levies':
                        if ($levies != null && $levies != '') {
                            $property->assignAttribute($numeric_feature->id, $levies);
                        }

                        break;
                }
            }

            $text_features = App::make('AttributesController')->getAttributesByType(3);
            $land_measurement = $property->land_measurement;
            foreach ($text_features as $text_feature) {
//            dd($text_feature->id);
                switch ($text_feature->name) {
                    case 'land_measurement':
                        if ($land_measurement != null && $land_measurement != '') {
                            $property->assignAttribute($text_feature->id, $land_measurement);
                        }

                        break;
                }
            }

            $boolean_features = App::make('AttributesController')->getAttributesByType(1);

            foreach ($boolean_features as $boolean_feature) {
                $name = $boolean_feature->name;
                if ($property->$name == 1) {
                    $property->assignAttribute($boolean_feature->id, 1);
                }
            }
        }
    }

    public function addPropertyUsers()
    {
        $properties = $this->property->select('id', 'tenants_id', 'managing_agent_id', 'keyholder_id')->get();

        foreach ($properties as $property) {
            $role_tenants_id = DB::select("SELECT id FROM roles WHERE name = 'Tenant'");
            if ($property->tenants_id != null || $property->tenants_id != 0 || $property->tenants_id != '') {
                try {
                    DB::insert("INSERT INTO property_user (property_id, user_id, role_id) VALUES ($property->id ,
                                $property->tenants_id, ".$role_tenants_id[0]->id.')');
                } catch (exception $e) {
                    echo $e->getMessage();
                    echo '<br>error with tenant property id '.$property->id;
                }
            }

            $role_managing_agent_id = DB::select("SELECT id FROM roles WHERE name = 'ManagingAgent'");
            if ($property->managing_agent_id != null || $property->managing_agent_id != 0 || $property->managing_agent_id != '') {
                try {
                    DB::insert("INSERT INTO property_user (property_id, user_id, role_id) VALUES ($property->id ,
                                $property->managing_agent_id, ".$role_managing_agent_id[0]->id.')');
                } catch (exception $e) {
                    echo $e->getMessage();
                    echo '<br>error with managing_agent property id '.$property->id;
                }
            }

            $role_keyholder_id = DB::select("SELECT id FROM roles WHERE name = 'KeyHolder'");
            if ($property->keyholder_id != null || $property->keyholder_id != 0 || $property->keyholder_id != '') {
                try {
                    DB::insert("INSERT INTO property_user (property_id, user_id, role_id) VALUES ($property->id ,
                                $property->keyholder_id, ".$role_keyholder_id[0]->id.')');
                } catch (exception $e) {
                    echo $e->getMessage();
                    echo '<br>error with keyholder property id '.$property->id;
                }
            }
        }
        dd('Done');
    }

    public function propertyUserAttach()
    {

//        owner
        $owners = $this->property
            ->select('id', 'owner_id')
            ->get();

//        keyholder
        $keyholders = $this->property
            ->select('id', 'keyholder_id')
            ->get();
//        managing agent
        $managing_agents = $this->property
            ->select('id', 'managing_agent_id')
            ->get();
//        tenants id
        $tenants = $this->property
            ->select('id', 'tenants_id')
            ->get();

        foreach ($owners as $owner) {
            try {
                if (! $alreadyConnected = DB::table('property_user')->whereProperty_id($owner->id)->whereUser_id($owner->owner_id)->whereRole_id(4)) {
                    DB::insert('INSERT INTO property_user (property_id, user_id, role_id) VALUES ('.$owner->id.', '.$owner->owner_id.', 6)');
                } else {
                    if (is_numeric($owner->owner_id) && $owner->owner_id != '' && $owner->owner_id != null) {
                        DB::update('UPDATE property_user SET role_id = 6 WHERE property_id = '.$owner->id.' AND user_id = '.$owner->owner_id.' AND role_id = 4');
                    }
                }
                Log::alert('Owner id: '.$owner->id.' SUCCESS');
            } catch (exception $e) {
                Log::error('Owner id: '.$owner->id.' FAIL');
                de($e->getMessage());
            }
        }

        foreach ($tenants as $tenant) {
            try {
                if (! $alreadyConnected = DB::table('property_user')->whereProperty_id($tenant->id)->whereUser_id($tenant->tenants_id)->whereRole_id(7)) {
                    DB::insert('INSERT INTO property_user (property_id, user_id, role_id) VALUES ('.$tenant->id.', '.$tenant->tenants_id.', 5)');
                } else {
                    if (is_numeric($tenant->tenant_id) && $tenant->tenant_id != '' && $tenant->tenant_id != null) {
                        DB::update('UPDATE property_user SET role_id = 5 WHERE property_id = '.$tenant->id.' AND user_id = '.$tenant->tenants_id.' AND role_id = 7');
                    }
                }
                Log::alert('Tenant id: '.$tenant->id.' SUCCESS');
            } catch (exception $e) {
                Log::error('Tenant id: '.$tenant->id.' FAIL');
                de($e->getMessage());
            }
        }

        foreach ($managing_agents as $managing_agent) {
            try {
                if (! $alreadyConnected = DB::table('property_user')->whereProperty_id($managing_agent->id)->whereUser_id($managing_agent->managing_agent_id)->whereRole_id(13)) {
                    DB::insert('INSERT INTO property_user (property_id, user_id, role_id) VALUES ('.$managing_agent->id.', '.$managing_agent->managing_agent_id.', 7)');
                } else {
                    if (is_numeric($managing_agent->managing_agent_id) && $managing_agent->managing_agent_id != '' && $managing_agent->managing_agent_id != null) {
                        DB::update('UPDATE property_user SET role_id = 7 WHERE property_id = '.$managing_agent->id.' AND user_id = '.$managing_agent->managing_agent_id.' AND role_id = 13');
                    }
                }
                Log::alert('Managing Agent id: '.$managing_agent->id.' SUCCESS');
            } catch (exception $e) {
                Log::error('Managing Agent id: '.$managing_agent->id.' FAIL');
                de($e->getMessage());
            }
        }

        foreach ($keyholders as $keyholder) {
            try {
                if (! $alreadyConnected = DB::table('property_user')->whereProperty_id($keyholder->id)->whereUser_id($keyholder->keyholder_id)->whereRole_id(14)) {
                    DB::insert('INSERT INTO property_user (property_id, user_id, role_id) VALUES ('.$keyholder->id.', '.$keyholder->owner_id.', 8)');
                } else {
                    if (is_numeric($keyholder->keyholder_id) && $keyholder->keyholder_id != '' && $keyholder->keyholder_id != null) {
                        DB::update('UPDATE property_user SET role_id = 8 WHERE property_id = '.$keyholder->id.' AND user_id = '.$keyholder->keyholder_id.' AND role_id = 14');
                    }
                }
                Log::alert('Keyholder id: '.$keyholder->id.' SUCCESS');
            } catch (exception $e) {
                Log::error('Keyholder id: '.$keyholder->id.' FAIL');
                de($e->getMessage());
            }
        }
        dd('Fin');
    }

    public function rolesToTags()
    {
        $user_roles = DB::select('SELECT * FROM role_user ru JOIN roles r ON ru.role_id = r.id');
        foreach ($user_roles as $user_role) {
            if ($user_role->name == 'Owner') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'OWNER')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'Seller') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'SELLER')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'Buyer') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'BUYER')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'Tenant') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'TENANT')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'ManagingAgent') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'MANAGING AGENT')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'KeyHolder') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'KEY HOLDER')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'BuildingManager') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'BUILDING MANAGER')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'Director') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'DIRECTOR')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'Trustee') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'TRUSTEE')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
            if ($user_role->name == 'AuctionBidder') {
                $user = User::find($user_role->user_id);
                $tag = DB::table('tags')->where('name', '=', 'AUCTION BIDDER')->first();
                $tagName = DB::table('tags')->whereId($tag->id)->first();
                if ($user->hasTag($tagName->name) == false) {
                    $user->tags()->attach($tag->id);
                }
                DB::delete('DELETE FROM role_user WHERE user_id = '.$user_role->user_id.' AND role_id = '.$user_role->role_id);
            }
        }
    }
}
