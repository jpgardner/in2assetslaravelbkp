<?php

namespace App\Http\Controllers;

use App\Models\Preferences;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class PaymentsController extends Controller
{
    public function __construct(Preferences $payment)
    {
        $this->payment = $payment;
    }

    public function index()
    {
        $featured = DB::select("select * from payment_products where product_group = 'featured'");
        $properties = Property::whereHas('vetting', function ($q) {
            $q->where('vetting_status', '=', 2);
        })->where('property.user_id', '=', Auth::user()->id)->get();
        $socials = DB::select("select * from payment_products where product_group in ('facebook','twitter','linkedin','googleplus')");
        $googleppc = DB::select("select * from payment_products where product_group = 'googleppc'");
        $marketing = DB::select("select * from payment_products where product_group = 'marketing'");

        return view('payments.index', ['featured' => $featured, 'properties' => $properties, 'socials' => $socials,
            'googleppcs' => $googleppc, 'marketings' => $marketing, ]);
    }

    //AJAX Add product to cart and persist in Database payment_sessions tabl e
    public function addCart()
    {
        if (Request::ajax()) {
            $user = Auth::user()->id;
            $item = Request::get('productId');
            $property = Request::get('propertyId');

            $results = DB::select("select * from payment_sessions where user_id = '$user' and product_id = '$item' and property_id = '$property'");

            if ($results) {
                $response = [];
                $response['type'] = 'warning';
                $response['text'] = 'This product is already in your cart';

                return $response;
            } else {
                DB::insert('insert into payment_sessions (user_id, product_id, property_id) values (?, ?, ?)', [$user, $item, $property]);
                $response['type'] = 'success';
                $response['text'] = 'This product has been added to your cart';

                return $response;
            }
        }
    }

    //Remove product from cart
    public function deleteCart()
    {
        if (Request::ajax()) {
            $item = Request::get('cartItem');
            DB::delete("delete from payment_sessions where id = $item");
            $response = [];
            $response['type'] = 'success';
            $response['text'] = 'This product has been removed from your cart';
            $response['id'] = $item;

            return $response;
        }
    }

    //Get customers cart from DB
    public function getCart()
    {
        if (Request::ajax()) {
            $user = Auth::user()->id;
            $results = DB::select("select payment_sessions.id, payment_sessions.user_id, payment_sessions.product_id, property_id, product_name, product_description, product_price, reference, street_address from payment_sessions
                                    left join payment_products on payment_sessions.product_id = payment_products.product_id
                                    left join property on payment_sessions.property_id = property.id
                                    where payment_sessions.user_id = $user");

            return $results;
        }
    }

    //Keep track of cart totals and build purchase form when cart is updated
    public function updateCart()
    {
        if (Request::ajax()) {

            //Fetch cart items from DB
            $user = Auth::user()->id;
            $products = DB::select("select product_price, reference, product_name, property_id, payment_sessions.product_id, product_description from payment_sessions
                                    left join payment_products on payment_sessions.product_id = payment_products.product_id
                                    left join property on payment_sessions.property_id = property.id
                                    where payment_sessions.user_id = $user");

            $cartTotal = 0;
            $cartCount = 0;
            $description = [];
            $productId = [];
            $propertyId = [];

            //Calculate totals
            foreach ($products as $product) {
                $cartTotal = $cartTotal + $product->product_price;
                $cartCount = $cartCount + 1;
                array_push($description, $product->product_description.' ['.$product->reference.']');
                array_push($productId, $product->product_id);
                array_push($propertyId, $product->property_id);
            }

            if ($cartCount == 0) {
                $cartCountText = 'You have no items in your Cart';
            } elseif ($cartCount == 1) {
                $cartCountText = 'You have 1 item in your Cart';
            } else {
                $cartCountText = 'You have '.$cartCount.' items in your Cart';
            }

            $description = implode(', ', $description);
            $productId = implode(',', $productId);
            $propertyId = implode(',', $propertyId);

            //Build form for current cart
            $passPhrase = 'miketest';
            $pfOutput = '';
            // Construct variables
            $data = [
                // Merchant details
                'merchant_id' => '10004083',
                'merchant_key' => 'pe35y798qcc4y',
                'return_url' => 'http://in2assetsdev.co.za.dedi840.jnb1.host-h.net/portal/payment/invoices',
                'cancel_url' => 'http://in2assetsdev.co.za.dedi840.jnb1.host-h.net/portal/payment/cancelled-transaction',
                'notify_url' => 'http://in2assetsdev.co.za.dedi840.jnb1.host-h.net/portal/payment/itn',
                'name_first' => Auth::user()->firstname,
                'name_last' => Auth::user()->lastname,
                'email_address' => Auth::user()->email,
                'amount' => number_format(sprintf('%.2f', $cartTotal), 2, '.', ''), //Amount needs to be in ZAR, if you have a multicurrency system, the conversion needs to place before building this array
                'item_name' => 'WeSell Marketing Package',
                'item_description' => $description,
                'custom_str1' => $productId,
                'custom_str2' => $propertyId,
                'custom_str3' => Auth::user()->id,
            ];

            // Create GET string
            foreach ($data as $key => $val) {
                if (! empty($val)) {
                    $pfOutput .= $key.'='.urlencode(trim($val)).'&';
                }
            }

            // Remove last ampersand
            $getString = substr($pfOutput, 0, -1);
            if (isset($passPhrase)) {
                $getString .= '&passphrase='.$passPhrase;
            }
            $data['signature'] = md5($getString);

            // If in testing mode use the sandbox domain ?  sandbox.payfast.co.za else www.payfast.co.za
            $testingMode = true;
            $pfHost = $testingMode ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
            $htmlForm = '<form action="https://'.$pfHost.'/eng/process" id="paymentForm" method="post">';
            foreach ($data as $name => $value) {
                $htmlForm .= '<input type="hidden" name="'.$name.'" value="'.$value.'">';
            }
            $htmlForm .= '<input class="button button-mini button-3d button-color nomargin" id="payNow" style="width:100%" type="submit" value="Pay Now"></form>';

            if ($cartCount == 0) {
                $htmlForm = '';
            }

            $result = [];
            $result['total'] = $cartTotal;
            $result['itemcount'] = $cartCountText;
            $result['form'] = $htmlForm;

            return $result;
        }
    }

    public function cancel()
    {
        $featured = DB::select("select * from payment_products where product_group = 'featured'");
        $properties = Property::whereHas('vetting', function ($q) {
            $q->where('vetting_status', '=', 2);
        })->where('property.user_id', '=', Auth::user()->id)->get();
        $socials = DB::select("select * from payment_products where product_group in ('facebook','twitter','linkedin','googleplus')");
        $googleppc = DB::select("select * from payment_products where product_group = 'googleppc'");
        $marketing = DB::select("select * from payment_products where product_group = 'marketing'");

        Flash::message('You have cancelled your order. Feel free to come back and order at another time');

        return view('payments.index', ['featured' => $featured, 'properties' => $properties, 'socials' => $socials,
            'googleppcs' => $googleppc, 'marketings' => $marketing, ]);
    }

    public function payfastResponse()
    {
        //$results = DB::select("select payment_sessions.user_id = $user");
        return view('payments.thankyou');
    }

    public function orderList($id)
    {
        //$results = DB::select("select payment_sessions.user_id = $user");
        return view('payments.orderlist')->withId($id);
    }

    public function getDatatableOrder($id)
    {
        $user = Auth::user()->id;
        $userList = DB::select("select order_reference, property, status, payment_products.product_name, property.reference, property.street_address from payment_lineitems
                                left join property on payment_lineitems.property = property.id
                                left join payment_products on payment_lineitems.product_name = payment_products.product_id
                                where order_reference = $id");
        $userList = new Illuminate\Support\Collection($userList);

        return Datatable::collection($userList)
            ->showColumns('product_name', 'status', 'reference', 'street_address')
            ->searchColumns('product_name', 'status', 'reference', 'street_address')
            ->orderColumns('product_name', 'status', 'reference', 'street_address')
            ->make();
    }

    public function getDatatable()
    {
        $user = Auth::user()->id;
        $userList = DB::select("select * from payment_invoice where user_id = $user");
        $userList = new Illuminate\Support\Collection($userList);

        return Datatable::collection($userList)
            ->showColumns('payment_id', 'total_price', 'payment_status', 'created_at')
            ->addColumn('action', function ($userList) {
                return '<a href="/portal/payment/order/'.$userList->id.'" title="View Order" >
                <i class="i-circled i-light i-small icon-laptop"></i>
                </a>';
            })
            ->searchColumns('payment_id', 'total_price', 'payment_status', 'created_at')
            ->orderColumns('payment_id', 'total_price', 'payment_status', 'created_at')
            ->make();
    }

    //Fetches Products from Product Table and Active Properties for the User
    public function payfastSubmit()
    {
        $user = Auth::user()->id;
        $social = DB::select("select * from payment_products where product_group in ('facebook','twitter','linkedin','googleplus')");
        $googleppc = DB::select("select * from payment_products where product_group = 'googleppc'");
        $marketing = DB::select("select * from payment_products where product_group = 'marketing'");
        $featured = DB::select("select * from payment_products where product_group = 'featured'");

        $properties = Property::whereHas('vetting', function ($q) {
            $q->where('vetting_status', '=', 2);
        })->where('property.user_id', '=', $user)->get();

        return view('payments.index')->withSocials($social)->withGoogleppcs($googleppc)->withMarketings($marketing)->withProperties($properties)->withFeatured($featured);
    }

    public function payfastProcess()
    {
    }

    //Callback from Payfast once payment has completed. This is the success url for payfast.
    public function payfastITN()
    {
        Log::info('Hit ITN Page');

        /*
         * Notes:
         * - All lines with the suffix "// DEBUG" are for debugging purposes and
         *   can safely be removed from live code.
         * - Remember to set PAYFAST_SERVER to LIVE for production/live site
         */
        // General defines
        define('PAYFAST_SERVER', 'TEST');
        // Whether to use "sandbox" test server or live server
        define('USER_AGENT', 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        // User Agent for cURL

        // Messages
        // Error
        define('PF_ERR_AMOUNT_MISMATCH', 'Amount mismatch');
        define('PF_ERR_BAD_SOURCE_IP', 'Bad source IP address');
        define('PF_ERR_CONNECT_FAILED', 'Failed to connect to PayFast');
        define('PF_ERR_BAD_ACCESS', 'Bad access of page');
        define('PF_ERR_INVALID_SIGNATURE', 'Security signature mismatch');
        define('PF_ERR_CURL_ERROR', 'An error occurred executing cURL');
        define('PF_ERR_INVALID_DATA', 'The data received is invalid');
        define('PF_ERR_UKNOWN', 'Unkown error occurred');

        // General
        define('PF_MSG_OK', 'Payment was successful');
        define('PF_MSG_FAILED', 'Payment has failed');

        // Notify PayFast that information has been received
        header('HTTP/1.0 200 OK');
        flush();

        // Variable initialization
        $pfError = false;
        $pfErrMsg = '';
        $filename = 'notify.txt'; // DEBUG
        $output = ''; // DEBUG
        $pfParamString = '';
        $pfHost = (PAYFAST_SERVER == 'LIVE') ?
            'www.payfast.co.za' : 'sandbox.payfast.co.za';

        //// Dump the submitted variables and calculate security signature
        if (! $pfError) {
            $output = "Posted Variables:\n\n"; // DEBUG

            Log::info($_POST);

            // Strip any slashes in data
            foreach ($_POST as $key => $val) {
                $pfData[$key] = stripslashes($val);
            }

            // Dump the submitted variables and calculate security signature
            foreach ($pfData as $key => $val) {
                if ($key != 'signature') {
                    $pfParamString .= $key.'='.urlencode($val).'&';
                }
            }

            // Remove the last '&' from the parameter string
            $pfParamString = substr($pfParamString, 0, -1);
            $pfTempParamString = $pfParamString;

            // If a passphrase has been set in the PayFast Settings, then it needs to be included in the signature string.
            $passPhrase = 'miketest'; //You need to get this from a constant or stored in you website
            if (! empty($passPhrase)) {
                $pfTempParamString .= '&passphrase='.urlencode($passPhrase);
            }
            $signature = md5($pfTempParamString);

            $result = ($_POST['signature'] == $signature);

            $output .= "Security Signature:\n\n"; // DEBUG
            $output .= '- posted     = '.$_POST['signature']."\n"; // DEBUG
            $output .= '- calculated = '.$signature."\n"; // DEBUG
            $output .= '- result     = '.($result ? 'SUCCESS' : 'FAILURE')."\n"; // DEBUG
        }

        //// Verify source IP
        if (! $pfError) {
            $validHosts = [
                'www.payfast.co.za',
                'sandbox.payfast.co.za',
                'w1w.payfast.co.za',
                'w2w.payfast.co.za',
            ];

            $validIps = [];

            foreach ($validHosts as $pfHostname) {
                $ips = gethostbynamel($pfHostname);

                if ($ips !== false) {
                    $validIps = array_merge($validIps, $ips);
                }
            }

            // Remove duplicates
            $validIps = array_unique($validIps);

            if (! in_array($_SERVER['REMOTE_ADDR'], $validIps)) {
                $pfError = true;
                $pfErrMsg = PF_ERR_BAD_SOURCE_IP;
            }
        }

        //// Connect to server to validate data received
        if (! $pfError) {
            // Use cURL (If it's available)
            if (function_exists('curl_init')) {
                $output .= "\n\nUsing cURL\n\n"; // DEBUG

                // Create default cURL object
                $ch = curl_init();

                // Base settings
                $curlOpts = [
                    // Base options
                    CURLOPT_USERAGENT => USER_AGENT, // Set user agent
                    CURLOPT_RETURNTRANSFER => true,  // Return output as string rather than outputting it
                    CURLOPT_HEADER => false,         // Don't include header in output
                    CURLOPT_SSL_VERIFYHOST => 2,
                    CURLOPT_SSL_VERIFYPEER => false,

                    // Standard settings
                    CURLOPT_URL => 'https://'.$pfHost.'/eng/query/validate',
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $pfParamString,
                ];
                curl_setopt_array($ch, $curlOpts);

                // Execute CURL
                $res = curl_exec($ch);
                curl_close($ch);

                if ($res === false) {
                    $pfError = true;
                    $pfErrMsg = PF_ERR_CURL_ERROR;
                }
            } // Use fsockopen
            else {
                $output .= "\n\nUsing fsockopen\n\n"; // DEBUG

                // Construct Header
                $header = "POST /eng/query/validate HTTP/1.0\r\n";
                $header .= 'Host: '.$pfHost."\r\n";
                $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                $header .= 'Content-Length: '.strlen($pfParamString)."\r\n\r\n";

                // Connect to server
                $socket = fsockopen('ssl://'.$pfHost, 443, $errno, $errstr, 10);

                // Send command to server
                fwrite($socket, $header.$pfParamString);

                // Read the response from the server
                $res = '';
                $headerDone = false;

                while (! feof($socket)) {
                    $line = fgets($socket, 1024);

                    // Check if we are finished reading the header yet
                    if (strcmp($line, "\r\n") == 0) {
                        // read the header
                        $headerDone = true;
                    } // If header has been processed
                    elseif ($headerDone) {
                        // Read the main response
                        $res .= $line;
                    }
                }
            }
        }

        //// Get data from server
        if (! $pfError) {
            // Parse the returned data
            $lines = explode("\n", $res);

            $output .= "\n\nValidate response from server:\n\n"; // DEBUG

            foreach ($lines as $line) { // DEBUG
                $output .= $line."\n";
            } // DEBUG
        }

        //// Interpret the response from server
        if (! $pfError) {
            // Get the response from PayFast (VALID or INVALID)
            $result = trim($lines[0]);

            $output .= "\nResult = ".$result; // DEBUG

            // If the transaction was valid
            if (strcmp($result, 'VALID') == 0) {
                Log::info('Transaction Valid');
                Log::info($output);

                $paymentID = $_POST['pf_payment_id'];
                $paymentStatus = $_POST['payment_status'];
                $paymentAmount = $_POST['amount_gross'];
                $paymentProducts = $_POST['custom_str1'];
                $paymentProperties = $_POST['custom_str2'];
                $paymentUserid = $_POST['custom_str3'];
                $currentTime = Carbon\Carbon::now();

                //Insert Invoice into Table
                DB::insert('insert into payment_invoice (user_id, payment_id, payment_products, total_price, payment_status, created_at) values (?, ?, ?, ?, ?, ?)',
                    [$paymentUserid, $paymentID, $paymentProducts, $paymentAmount, $paymentStatus, $currentTime]);
                $reference_id = DB::getPdo()->lastInsertId();

                Log::info('Properties'.$paymentProperties);

                //Create individual products related to above invoice
                $productArray = explode(',', $paymentProducts);
                $propertyArray = explode(',', $paymentProperties);
                $prodprop = array_combine($productArray, $propertyArray);

                foreach ($prodprop as $product_name => $property_name) {
                    DB::insert('insert into payment_lineitems (user_id, product_name, order_reference, property, status, created_at) values (?, ?, ?, ?, ?, ?)',
                        [$paymentUserid, $product_name, $reference_id, $property_name, 'Pending', $currentTime]);
                }

                //Clear users Shopping Cart
                DB::delete("delete from payment_sessions where user_id = $paymentUserid");
            } // If the transaction was NOT valid
            else {
                // Log for investigation
                $pfError = true;
                $pfErrMsg = PF_ERR_INVALID_DATA;
            }
        }

        // If an error occurred
        if ($pfError) {
            $output .= "\n\nAn error occurred!";
            $output .= "\nError = ".$pfErrMsg;
        }

        //// Write output to file // DEBUG
        file_put_contents($filename, $output); // DEBUG
    }

    //----> Admin Functions -------

    public function adminInvoices()
    {
        //$results = DB::select("select payment_sessions.user_id = $user");
        return view('payments.adminInvoice');
    }

    public function adminListpackages()
    {
        //$results = DB::select("select payment_sessions.user_id = $user");
        return view('payments.adminOrders');
    }

    public function adminInvoiceItems($id)
    {
        return view('payments.adminItems')->withId($id);
    }

    public function getadminDatatableOrder()
    {
        $userList = DB::select('select payment_lineitems.id, users.firstname, users.lastname, order_reference, property, status, payment_products.product_name, property.reference, property.street_address from payment_lineitems
                                left join property on payment_lineitems.property = property.id
                                left join payment_products on payment_lineitems.product_name = payment_products.product_id
                                left join users on payment_lineitems.user_id = users.id');
        $userList = new Illuminate\Support\Collection($userList);

        return Datatable::collection($userList)
            ->showColumns('firstname', 'lastname', 'product_name', 'status', 'reference', 'street_address')
            ->addColumn('action', function ($userList) {
                if ($userList->status == 'Pending') {
                    return '<a href="/admin/payment/order/inprogress/'.$userList->id.'" title="In Progress" ><i class="i-circled i-light i-small icon-laptop"></i></a>
                        <a href="/admin/payment/order/complete/'.$userList->id.'" title="Completed" ><i class="i-circled i-light i-alt i-small icon-thumbs-up" style="background-color:#2ecc71"></i></a>';
                } elseif ($userList->status == 'In Progress') {
                    return '<a href="/admin/payment/order/complete/'.$userList->id.'" title="Completed" ><i class="i-circled i-light i-alt i-small icon-thumbs-up" style="background-color:#2ecc71"></i></a>';
                }
            })
            ->searchColumns('firstname', 'lastname', 'product_name', 'status', 'reference', 'street_address')
            ->orderColumns('firstname', 'lastname', 'product_name', 'status', 'reference', 'street_address')
            ->make();
    }

    public function getadminDatatable()
    {
        $userList = DB::select('select * from payment_invoice
            left join users on payment_invoice.user_id = users.id');
        $userList = new Illuminate\Support\Collection($userList);

        return Datatable::collection($userList)
            ->showColumns('firstname', 'lastname', 'payment_id', 'total_price', 'payment_status', 'created_at')
            ->addColumn('action', function ($userList) {
                return '<a href="/admin/invoices/items/'.$userList->id.'" title="View Order" ><i class="i-circled i-light i-small icon-laptop"></i></a>';
            })
            ->searchColumns('firstname', 'lastname', 'payment_status', 'created_at')
            ->orderColumns('firstname', 'lastname', 'payment_status', 'created_at')
            ->make();
    }

    public function adminSetProgress($id)
    {
        DB::table('payment_lineitems')->where('id', $id)->update(['status' => 'In Progress']);

        return redirect()->to('/admin/payment/');
    }

    public function adminSetCompleted($id)
    {
        DB::table('payment_lineitems')->where('id', $id)->update(['status' => 'Complete']);

        return redirect()->to('/admin/payment/');
    }
}
