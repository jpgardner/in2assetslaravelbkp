<?php

namespace App\Http\Controllers;

use App\Models\ViewingSessionsBooked;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ViewingSessionBookingController extends Controller
{
    protected $ViewingSessions;

    public function __construct(ViewingSessionsBooked $ViewingSessions, Viewing $viewing, Property $property)
    {
        $this->ViewingSessions = $ViewingSessions;
        $this->viewing = $viewing;
        $this->property = $property;
    }

    public function getViewingSessionsForViewing($id)
    {
        $viewings = $this->ViewingSessions->
        where('viewing_id', '=', $id)->get();

        $count = 0;
        foreach ($viewings as $viewing) {
            $viewingarr[$count]['id'] = $viewing['id'];
            $viewingarr[$count]['name'] = $viewing['name'];
            $viewingarr[$count]['email'] = $viewing['email'];
            $viewingarr[$count]['phone'] = $viewing['phone'];
            $viewingarr[$count]['time'] = $viewing['time'];
            $viewingarr[$count]['viewing_id'] = $viewing['viewing_id'];
            $viewingarr[$count]['alert'] = $viewing['alert'];

            $count++;
        }

        $viewing_array = [];

        for ($i = 0; $i < $count; $i++) {
            $viewing_array[] = [
                'id' => $viewingarr[$i]['id'],
                'name' => $viewingarr[$i]['name'],
                'email' => $viewingarr[$i]['email'],
                'phone' => $viewingarr[$i]['phone'],
                'time' => $viewingarr[$i]['time'],
                'viewing_id' => $viewingarr[$i]['viewing_id'],
                'alert' => $viewingarr[$i]['alert'],
            ];
        }

        echo json_encode($viewing_array);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the viewing sessions
        $ViewingSessions = $this->ViewingSessions->all();

        // load the view and pass the viewing sessions
        return view('ViewingSessionBooked.index')
            ->with('ViewingSessions', $ViewingSessions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('ViewingSessionBooked.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'time' => 'required',
        ];

        $validator = Validator::make(Request::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return redirect('ViewingSessionBooked/create')
                ->withErrors($validator);
        } else {
            // store
            $ViewingSessions = new ViewingSessionsBooked;
            $ViewingSessions->name = Request::get('name');
            $ViewingSessions->email = Request::get('email');
            $ViewingSessions->phone = Request::get('phone');
            $ViewingSessions->time = Request::get('time');
            $ViewingSessions->alert = Request::get('alert');
            $ViewingSessions->viewing_id = Request::get('viewing_id');

            $ViewingSessions->save();

            $viewing = $this->viewing->select('property_id')->whereId(Request::get('viewing_id'))->first();
            $property = $this->property->where('property.id', '=', $viewing->property_id)->
            join('vetting', 'vetting.property_id', '=', 'property.id')
                ->first();
            $property_seller = DB::table('users')->whereId($property->user_id)->first();

            if (Request::get('alert')) {
                //buyer detais
                $buyer['name'] = Request::get('name');
                $buyer['email'] = Request::get('email');
                $buyer['contact'] = Request::get('phone');

                //seller details
                $seller['name'] = $property_seller->firstname.' '.$property_seller->lastname;
                $seller['email'] = $property_seller->email;
                $seller['contact'] = $property_seller->phonenumber;

                //booking date and time
                $datetimeArray = explode(' ', Request::get('time'));
                $booking['date'] = $datetimeArray[0];
                $booking['time'] = $datetimeArray[1];

                //agent details
                $agent['name'] = $property->rep_firstname.' '.$property->rep_surname;
                $agent['email'] = $property->rep_email;
                if (trim($agent['name']) == '') {
                    $agent['name'] = 'The WeSell team';
                }
                if (trim($agent['email']) == '') {
                    $agent['email'] = 'viewing@wesell.co.za';
                }
                //address
                $address = $property->street_number.', '.$property->street_address.', '.$property->area;

                $this->sendEmails(1, $buyer, $seller, $booking, $agent, $address);
            }

            // redirect
            Session::flash('message', 'Successfully created viewing session!');

            return 'True';
        }
    }

    /**function to send emails
     * first parameter defines the type of email
     * 1. create
     * 2. edit
     * 3. delete
     */

    public function sendEmails($email_type, $buyer, $seller, $booking, $agent, $address)
    {

        //switches through different email types
        switch ($email_type) {
            case 1:
                //Notify Seller of booking CC agent

                $addressedTo = 'Dear '.$seller['name'].',';
                $subject = 'New Viewing Session';
                $email = $seller['email'];
                $emailData = '<p>A buyer viewing session has been booked for you:<br>
							<strong>Property:</strong> '.$address.'<br>
							<strong>Buyer:</strong> '.$buyer['name'].'<br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$buyer['email'].'<br>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$buyer['contact'].'<br>
						    <strong>Date:</strong> '.$booking['date'].'<br>
						    <strong>Time:</strong> '.$booking['time'].'</p>
						    <p>Please contact me if you need any assistance.</p>
						    <p>Yours Sincerely</p>
						    <p>'.$agent['name'].'</p>';

                $data = ['name' => $addressedTo, 'subject' => $subject, 'messages' => $emailData];

                // Queue email
                Mail::queue('emails.registration.confirm', $data, function ($message) use ($email, $addressedTo, $subject, $agent) {
                    $message->to($email, $addressedTo)
                        ->cc($agent['email'], $agent['name'])
                        ->subject($subject);
                });

                //Notify Buyer of booking

                $addressedTo = 'Dear '.$buyer['name'].',';
                $subject = 'New Viewing Session';
                $email = $buyer['email'];
                $emailData = '<p>A buyer viewing session has been booked for you:<br>
							<strong>Property:</strong> '.$address.'<br>
							<strong>Seller:</strong> '.$seller['name'].'<br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$seller['email'].'<br>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$seller['contact'].'<br>
						    <strong>Date:</strong> '.$booking['date'].'<br>
						    <strong>Time:</strong> '.$booking['time'].'</p>
						    <p>Please contact me if you need any assistance.</p>
						    <p>Yours Sincerely</p>
						    <p>'.$agent['name'].'</p>';

                $data = ['name' => $addressedTo, 'subject' => $subject, 'messages' => $emailData];

                // Queue email
                Mail::queue('emails.registration.confirm', $data, function ($message) use ($email, $addressedTo, $subject) {
                    $message->to($email, $addressedTo)
                        ->subject($subject);
                });

                break;
            case 2:
                //Notify Seller of change to booking CC agent
                $addressedTo = 'Dear '.$seller['name'].',';
                $subject = 'Booking Changed '.$booking['date'].' '.$booking['time'];
                $email = $seller['email'];
                $emailData = '<p>The details of a booked buyer viewing session have changed - see updated booking below:<br>
							<strong>Property:</strong> '.$address.'<br>
							<strong>Buyer:</strong> '.$buyer['name'].'<br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$buyer['email'].'<br>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$buyer['contact'].'<br>
						    <strong>Date:</strong> '.$booking['date'].'<br>
						    <strong>Time:</strong> '.$booking['time'].'</p>
						    <p>Please contact me if you need any assistance.</p>
						    <p>Yours Sincerely</p>
						    <p>'.$agent['name'].'</p>';

                $data = ['name' => $addressedTo, 'subject' => $subject, 'messages' => $emailData];

                // Queue email
                Mail::queue('emails.registration.confirm', $data, function ($message) use ($email, $addressedTo, $subject, $agent) {
                    $message->to($email, $addressedTo)
                        ->cc($agent['email'], $agent['name'])
                        ->subject($subject);
                });

                //Notify Buyer of change to booking

                $addressedTo = 'Dear '.$buyer['name'].',';
                $subject = 'Booking Changed '.$booking['date'].' '.$booking['time'];
                $email = $buyer['email'];
                $emailData = '<p>A viewing booked on your behalf has been changed - see updated booking below:<br>
							<strong>Property:</strong> '.$address.'<br>
							<strong>Seller:</strong> '.$seller['name'].'<br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$seller['email'].'<br>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$seller['contact'].'<br>
						    <strong>Date:</strong> '.$booking['date'].'<br>
						    <strong>Time:</strong> '.$booking['time'].'</p>
						    <p>Please contact me if you need any assistance.</p>
						    <p>Yours Sincerely</p>
						    <p>'.$agent['name'].'</p>';

                $data = ['name' => $addressedTo, 'subject' => $subject, 'messages' => $emailData];

                // Queue email
                Mail::queue('emails.registration.confirm', $data, function ($message) use ($email, $addressedTo, $subject) {
                    $message->to($email, $addressedTo)
                        ->subject($subject);
                });

                break;
            case 3:
                //Notify Seller of booking cancellation CC agent

                $addressedTo = 'Dear '.$seller['name'].',';
                $subject = 'Booking Cancelled '.$booking['date'].' '.$booking['time'];
                $email = $seller['email'];
                $emailData = '<p>The following viewing booking has been cancelled:<br>
							<strong>Property:</strong> '.$address.'<br>
							<strong>Buyer:</strong> '.$buyer['name'].'<br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$buyer['email'].'<br>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$buyer['contact'].'<br>
						    <strong>Date:</strong> '.$booking['date'].'<br>
						    <strong>Time:</strong> '.$booking['time'].'</p>
						    <p>Please contact me if you need any assistance.</p>
						    <p>Yours Sincerely</p>
						    <p>'.$agent['name'].'</p>';

                $data = ['name' => $addressedTo, 'subject' => $subject, 'messages' => $emailData];

                // Queue email
                Mail::queue('emails.registration.confirm', $data, function ($message) use ($email, $addressedTo, $subject, $agent) {
                    $message->to($email, $addressedTo)
                        ->cc($agent['email'], $agent['name'])
                        ->subject($subject);
                });

                //Notify Buyer of booking cancellation

                $addressedTo = 'Dear '.$buyer['name'].',';
                $subject = 'Booking Cancelled '.$booking['date'].' '.$booking['time'];
                $email = $buyer['email'];
                $emailData = '<p>A viewing booked on your behalf has been cancelled:<br>
							<strong>Property:</strong> '.$address.'<br>
							<strong>Seller:</strong> '.$seller['name'].'<br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$seller['email'].'<br>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$seller['contact'].'<br>
						    <strong>Date:</strong> '.$booking['date'].'<br>
						    <strong>Time:</strong> '.$booking['time'].'</p>
						    <p>If the booking was cancelled by the seller, I will be in contact with you to arrange an alternative time.</p>
						    <p>Yours Sincerely</p>
						    <p>'.$agent['name'].'</p>';

                $data = ['name' => $addressedTo, 'subject' => $subject, 'messages' => $emailData];

                // Queue email
                Mail::queue('emails.registration.confirm', $data, function ($message) use ($email, $addressedTo, $subject) {
                    $message->to($email, $addressedTo)
                        ->subject($subject);
                });

                break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // get the ViewingSession
        $ViewingSession = ViewingSessionsBooked::find($id);

        // show the view and pass the nerd to it
        return view('ViewingSessionBooked.show')
            ->with('ViewingSession', $ViewingSession);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        // get the ViewingSession
        $ViewingSession = ViewingSessionsBooked::find($id);

        // show the edit form and pass the ViewingSession
        return view('ViewingSessionBooked.edit')
            ->with('ViewingSession', $ViewingSession);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'time' => 'required',
        ];
        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return 'FALSE';
        } else {

            // update
            $ViewingSessions = ViewingSessionsBooked::find($id);
            $ViewingSessions->name = Request::get('name');
            $ViewingSessions->email = Request::get('email');
            $ViewingSessions->phone = Request::get('phone');
            $ViewingSessions->time = Request::get('time');
            $ViewingSessions->viewing_id = Request::get('viewing_id');

            $ViewingSessions->save();

            //send changed if alert.$id is checked

            if (Request::get('alerted') == true) {
                $viewing = $this->viewing->select('property_id')->whereId(Request::get('viewing_id'))->first();
                $property = $this->property->where('property.id', '=', $viewing->property_id)->
                join('vetting', 'vetting.property_id', '=', 'property.id')
                    ->first();
                $property_seller = DB::table('users')->whereId($property->user_id)->first();
                //buyer detais
                $buyer['name'] = Request::get('name');
                $buyer['email'] = Request::get('email');
                $buyer['contact'] = Request::get('phone');

                //seller details
                $seller['name'] = $property_seller->firstname.' '.$property_seller->lastname;
                $seller['email'] = $property_seller->email;
                $seller['contact'] = $property_seller->phonenumber;

                //booking date and time
                $datetimeArray = explode(' ', Request::get('time'));
                $booking['date'] = $datetimeArray[0];
                $booking['time'] = $datetimeArray[1];

                //agent details
                $agent['name'] = $property->rep_firstname.' '.$property->rep_surname;
                $agent['email'] = $property->rep_email;
                if (trim($agent['name']) == '') {
                    $agent['name'] = 'The WeSell team';
                }
                if (trim($agent['email']) == '') {
                    $agent['email'] = 'viewing@wesell.co.za';
                }
                //address
                $address = $property->street_number.', '.$property->street_address.', '.$property->area;

                $this->sendEmails(2, $buyer, $seller, $booking, $agent, $address);
            }

            // redirect
            Session::flash('message', 'Successfully updated session!');

            return 'True';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $ViewingSessions = ViewingSessionsBooked::find($id);

        $viewing = $this->viewing->select('property_id')->whereId($ViewingSessions->viewing_id)->first();
        $property = $this->property->where('property.id', '=', $viewing->property_id)->
        join('vetting', 'vetting.property_id', '=', 'property.id')
            ->first();
        $property_seller = DB::table('users')->whereId($property->user_id)->first();

        //buyer detais
        $buyer['name'] = $ViewingSessions->name;
        $buyer['email'] = $ViewingSessions->email;
        $buyer['contact'] = $ViewingSessions->phone;

        //seller details
        $seller['name'] = $property_seller->firstname.' '.$property_seller->lastname;
        $seller['email'] = $property_seller->email;
        $seller['contact'] = $property_seller->phonenumber;

        //booking date and time
        $datetimeArray = explode(' ', $ViewingSessions->time);
        $booking['date'] = $datetimeArray[0];
        $booking['time'] = $datetimeArray[1];

        //agent details
        $agent['name'] = $property->rep_firstname.' '.$property->rep_surname;
        $agent['email'] = $property->rep_email;
        if (trim($agent['name']) == '') {
            $agent['name'] = 'The WeSell team';
        }
        if (trim($agent['email']) == '') {
            $agent['email'] = 'viewing@wesell.co.za';
        }
        //address
        $address = $property->street_number.', '.$property->street_address.', '.$property->area;

        $this->sendEmails(3, $buyer, $seller, $booking, $agent, $address);

        $ViewingSessions->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the viewing session!');

        return 'success';
    }
}
