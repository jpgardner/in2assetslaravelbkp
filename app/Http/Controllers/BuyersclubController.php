<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use In2Assets\Buyersclub\Buyersclub;
use In2Assets\Buyersclub\BuyersclubRepository;
use In2Assets\Buyersclub\PublishBuyersclubCommand;
use In2Assets\Core\CommandBus;
use In2Assets\Forms\PublishBuyersclubForm;
use In2Assets\Uploads\Upload;
use Laracasts\Flash\Flash;

class BuyersclubController extends Controller
{
    use CommandBus;

    protected $buyersclubepository;

    protected $publishBuyersclubForm;

    public function __construct(BuyersclubRepository $buyersclubRepository, PublishBuyersclubForm $publishBuyersclubForm)
    {
        $this->buyersclubRepository = $buyersclubRepository;
        $this->publishBuyersclubForm = $publishBuyersclubForm;
    }

    // List Blogs and Return Blog Category Sidebar
    public function index()
    {
        if (Auth::guest()) {
            return Redirect::route('login_path');
        } else {
            $user = Auth::user();
            if ($user->hasRole('Buyersclub')) {
                $buyersclub = $this->buyersclubRepository->getPaginated();
                $catCount = DB::table('buyersclubs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

                return view('buyersclub.index')->withBuyersclub($buyersclub)->withCategories($catCount);
            } else {
                return view('buyersclub.register');
            }
        }
    }

    public function register()
    {
        $user = Auth::user();
        if ($user->hasRole('Buyersclub')) {
            return Redirect::route('buyersclub_path');
        } else {
            $user->roles()->attach(12);

            return Redirect::route('buyersclub_path');
        }
    }

    // Return Blogs for the Home Page
    public function homeSnippet()
    {
        $buyersclub = $this->buyersclubRepository->getHome();

        return $buyersclub;
    }

    // Returns blogs with categories
    public function buyersclubCategory($category)
    {
        $buyersclub = $this->buyersclubRepository->getCategoryPaginated($category);
        $catCount = DB::table('buyersclubs')->selectRaw('category, count(*) as count')->groupBy('category')->get();
        //dd($buyersclub)
        return view('buyersclub.index')->withBuyersclub($buyersclub)->withCategories($catCount);
    }

    // List blogs in Admin
    public function listBuyersclub()
    {
        return view('buyersclub.adminlist');
    }

    // Create Blogs in Admin
    public function createBuyersclub()
    {
        $catCount = DB::table('buyersclubs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('buyersclub.create')->withCategories($catCount);
    }

    // Save blogs in admin
    public function storeBuyersclub()
    {
        $this->publishBuyersclubForm->validate(Request::only('title', 'body'));

        //Check if Unique Slug, If not append _x to slug
        $slug = Slugify::slugify(Request::get('title'));
        $rowCount = DB::table('buyersclubs')->where('slug', $slug)->count();
        $count = $rowCount + 1;
        if ($rowCount > 0) {
            $slug = $slug.'_'.$count;
        }

        $file = Request::file('file');
        $extension = File::extension($file->getClientOriginalName());
        $directory = 'uploads/buyersclub/';
        $filename = 'buyersClub_'.Str::random(12).'.'.$extension;

        $upload_success = Request::file('file')->move($directory, $filename);
        $file_location = $directory.$filename;
        $file_type = Request::get('type');

        if ($upload_success) {
            $this->execute(
                new PublishBuyersclubCommand(Request::get('gallery_id'), Request::get('title'), Request::get('body'), Request::get('short_body'), Request::get('lead_image'), Request::get('category'), Request::get('status'), $file_location, $slug, Auth::user()->id)
            );
            Flash::message('Your Buyers Club post has been saved.');

            return Redirect::route('list_buyersclub_path');
        }
    }

    // Show indirvidual blog witbh categories and images
    public function showBuyersclub($slug)
    {
        $buyersclub = $this->buyersclubRepository->findBySlug($slug);
        $buyersclub_gallery = Upload::where('type', '=', 'buyersclub_image')->whereLink($buyersclub->gallery_id)->get();
        $catCount = DB::table('buyersclubs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('buyersclub.show')->withBuyersclub($buyersclub)->withGallery($buyersclub_gallery)->withCategories($catCount);
    }

    // Edit blog in admin
    public function edit($slug)
    {
        $buyersclub = $this->buyersclubRepository->findBySlug($slug);
        $buyersclub_gallery = Upload::where('type', '=', 'buyersclub_image')->whereLink($buyersclub->gallery_id)->get();
        $catCount = DB::table('buyersclubs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('buyersclub.edit')->withBuyersclub($buyersclub)->withGallery($buyersclub_gallery)->withCategories($catCount);
    }

    // Update Blog in Admin
    public function update($id)
    {
        $input = Request::all();
        $this->publishBuyersclubForm->validate(Request::only('title', 'body'));
        $buyersclub = $this->buyersclubRepository->findBySlug($id);
        $buyersclub->update($input);

        if (Request::file('file')) {
            $file = Request::file('file');
            de($file);
            $extension = File::extension($file->getClientOriginalName());
            $directory = 'uploads/buyersclub/';
            $filename = 'buyersClub_'.Str::random(12).'.'.$extension;

            $upload_success = Request::file('file')->move($directory, $filename);
            $file_location = $directory.$filename;
            $buyersclub->file = $file_location;
            $buyersclub->save();
        }

        Flash::message('Your Buyers Club blog has been updated');

        return Redirect::route('list_buyersclub_path');
    }

    // Admin List blogs datatable
    public function getDatatable()
    {
        $buyersclubList = $this->buyersclubRepository->findAll()
            ->get();

        return Datatable::collection($buyersclubList)
            ->showColumns('title', 'category', 'created_at', 'status', 'action')
            ->addColumn('action', function ($buyersclubList) {
                return '<a href="/admin/buyersclub/close/'.$buyersclubList->id.'" title="Close" ><i class="i-circled i-light i-small icon-minus-sign"></i></a><a href="/admin/buyersclub/edit/'.$buyersclubList->slug.'" title="Edit" ><i class="i-circled i-light i-small icon-edit"></i></a>';
            })
            ->searchColumns('title', 'category', 'created_at', 'status')
            ->orderColumns('title', 'category', 'created_at', 'status')
            ->make();
    }

    // Blog status change
    public function close($id)
    {
        $buyersclub = Buyersclub::whereId($id)->first();
        $buyersclub->status = 'closed';
        $buyersclub->save();
        Flash::message('This Buyers Club blog post has been closed!');

        return Redirect::route('list_buyersclub_path');
    }

    public function postStart()
    {
        return view('buyersclub.search');
    }

    public function postSearch()
    {
        $query = Request::get('query');
        $blogs = $this->buyersclubRepository->getSearch($query)->get();
        $catCount = DB::table('buyersclubs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('buyersclub.searchresults')->withBlogs($blogs)->withCategories($catCount)->withSearch($query);
    }
}
