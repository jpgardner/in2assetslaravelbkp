<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use In2Assets\Academys\Academy;
use In2Assets\Academys\AcademyRepository;
use In2Assets\Academys\PublishAcademyCommand;
use In2Assets\Core\CommandBus;
use In2Assets\Forms\PublishAcademyForm;
use In2Assets\Uploads\Upload;
use Laracasts\Flash\Flash;

class AcademyController extends Controller
{
    use CommandBus;

    protected $academyRepository;

    protected $publishAcademyForm;

    public function __construct(AcademyRepository $academyRepository, PublishAcademyForm $publishAcademyForm)
    {
        $this->academyRepository = $academyRepository;
        $this->publishAcademyForm = $publishAcademyForm;
    }

    // List Blogs and Return Blog Category Sidebar
    public function index()
    {
        $blogs = $this->blogRepository->getPaginated();
        $catCount = DB::table('blogs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.index')->withBlogs($blogs)->withCategories($catCount);
    }

    // Return Blogs for the Home Page
    public function homeSnippet()
    {
        $blogs = $this->blogRepository->getHome();

        return $blogs;
    }

    // Returns blogs with categories
    public function academyCategory($category)
    {
        $academys = $this->academyRepository->getCategoryPaginated($category);

        return view('academy.index')->withAcademys($academys)->withCategory($category);
    }

    // List blogs in Admin
    public function listAcademy()
    {
        return view('academy.adminlist');
    }

    // Create Blogs in Admin
    public function createAcademy()
    {
        return view('academy.create');
    }

    // Save blogs in admin
    public function storeAcademy()
    {
        $this->publishAcademyForm->validate(Request::only('title', 'body'));

        //Check if Unique Slug, If not append _x to slug
        $slug = Slugify::slugify(Request::get('title'));

        $rowCount = DB::table('academies')->where('slug', 'LIKE', '%'.$slug.'%')->count();

        $count = $rowCount + 1;
        if ($rowCount > 0) {
            $slug = $slug.'_'.$count;
        }

        $this->execute(
            new PublishAcademyCommand(Request::get('title'), Request::get('body'), Request::get('category'), Request::get('status'), $slug)
        );

        Flash::message('Your Academy Article has been saved.');

        return view('academy.adminlist');
    }

    // Show indirvidual blog witbh categories and images
    public function showBlog($slug)
    {
        $blog = $this->blogRepository->findBySlug($slug);
        $blog_gallery = Upload::where('type', '=', 'blog_image')->whereLink($blog->gallery_id)->get();
        $catCount = DB::table('blogs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('blog.show')->withBlog($blog)->withGallery($blog_gallery)->withCategories($catCount);
    }

    // Edit blog in admin
    public function edit($slug)
    {
        $academy = $this->academyRepository->findBySlug($slug);

        return view('academy.edit')->withAcademy($academy);
    }

    // Update Blog in Admin
    public function update($id)
    {
        $input = Request::all();
        $this->publishAcademyForm->validate(Request::only('title', 'body'));
        $blog = $this->academyRepository->findBySlug($id);
        $blog->update($input);
        Flash::message('Your academy article has been updated');

        return Redirect::route('list_academy_path');
    }

    // Admin List blogs datatable
    public function getDatatable()
    {
        $academyList = $this->academyRepository->findAll()
            ->get();

        return Datatable::collection($academyList)
            ->showColumns('title', 'category', 'created_at', 'status', 'action')
            ->addColumn('action', function ($academyList) {
                return '<a href="/admin/academy/close/'.$academyList->id.'" title="Close" >
            <i class="i-circled i-light i-small icon-minus-sign"></i>
            </a>
            <a href="/admin/academy/edit/'.$academyList->slug.'" title="Edit" >
            <i class="i-circled i-light i-small icon-edit"></i>
            </a>';
            })
            ->searchColumns('title', 'category', 'created_at', 'status')
            ->orderColumns('title', 'category', 'created_at', 'status')
            ->make();
    }

    // Blog status change
    public function close($id)
    {
        $academy = Academy::whereId($id)->first();
        $academy->status = 'closed';
        $academy->save();
        Flash::message('This academy article has been closed!');

        return Redirect::route('list_academy_path');
    }

    public function postStart()
    {
        return view('blog.search');
    }

    public function articleSearch($category)
    {
        $query = Request::get('q');
        $articles = $this->academyRepository->getSearch($query, $category)->get();
        $catCount = DB::table('blogs')->selectRaw('category, count(*) as count')->groupBy('category')->get();

        return view('academy.searchresults')->withAcademys($articles)->withSearch($query)->withCategory($category);
    }
}
