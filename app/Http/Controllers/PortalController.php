<?php

namespace  App\Http\Controllers;

use App\Models\Preferences;
use App\Models\Property;
use App\Models\Viewing;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class PortalController extends Controller
{
    public function __construct(Property $property, Viewing $viewing, Preferences $preferences)
    {
        $this->property = $property;
        $this->viewing = $viewing;
        $this->preferences = $preferences;
    }

    public function hasDeedorLease()
    {
        //check to see if user/tenant has lease agreement in process
        if ($result = DB::table('lease_agreements')->whereTenant_id(Auth::user()->id)->get()) {
            $hasLease = 1;
        } else {
            $hasLease = 0;
        }

        //check to see if user/buyer has deed of sale in process
        if ($result = DB::table('deed_of_sale')->whereBuyer_id(Auth::user()->id)->get()) {
            $hasDeed = 1;
        } else {
            $hasDeed = 0;
        }

        return [$hasLease, $hasDeed];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('portal.index')->withUser($user);
    }

    public function userMailbox()
    {
        $user = Auth::user();

        return view('portal.mailbox')->withUser($user);
    }

    public function userCalendar()
    {
        $user = Auth::user();

        $userId = $user->id;
        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        where('property.user_id', '=', $userId)->orderBy('vetting_status')->orderBy('listing_type')->get();

//        dd($properties);
        return view('wesell.calendar', ['properties' => $properties, 'userId' => $userId]);
    }

    public function adminPropertyCalendar($id)
    {
        $property = $this->property->whereId($id)->first();

        $userId = $property->user_id;

        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        where('property.user_id', '=', $userId)->
        where('property.listing_type', '=', 'wesell')->
        orderBy('vetting_status')->orderBy('listing_type')->get();

        return view('wesell.admin.calendar', ['properties' => $properties, 'userId' => $userId]);
    }

    public function userCalendarbyProperty($id)
    {
        $property = $this->property->select('user_id')->whereId($id)->first();
        $userId = $property['user_id'];
        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        where('property.user_id', '=', $userId)->orderBy('vetting_status')->orderBy('listing_type')->get();

//        dd($properties);
        return view('wesell.calendar', ['properties' => $properties, 'userId' => $userId]);
    }

    public function buyerClub()
    {
        $user = Auth::user();

        return view('portal.buyersClub')->withUser($user);
    }

    public function buyer()
    {
        $user = Auth::user();

        if (Auth::user()->hasRole('Buyer')) {
            return view('portal.buyers')->withUser($user);
        } elseif (Auth::user()->hasRole('BuyerPending')) {
            return view('portal.buyersPending')->withUser($user);
        } elseif ($buyer = DB::table('buyers')->whereUser_id($user->id)->first()) {
            return view('portal.buyersrejected')->withUser($user);
        } else {
            return view('portal.buyersNo_dets')->withUser($user);
        }
    }

    public function tenant()
    {
        $user = Auth::user();

        if (Auth::user()->hasRole('Tenant')) {
            return view('portal.buyers')->withUser($user);
        } elseif (Auth::user()->hasRole('TenantPending')) {
            return view('portal.tenantsPending')->withUser($user);
        } elseif ($tenant = DB::table('tenants')->whereUser_id($user->id)->first()) {
            if ($tenant->itc_agree == 1) {
                return view('portal.tenantsrejected', ['itc' => 1])->withUser($user);
            } else {
                return view('portal.tenantsrejected', ['itc' => 0])->withUser($user);
            }
        } else {
            return view('portal.tenantsNo_dets')->withUser($user);
        }
    }

    public function seller()
    {
        $user = Auth::user();

        $userId = $user->id;

        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
        leftJoin('farm', 'property.id', '=', 'farm.property_id')->
        where('property.user_id', '=', $userId)->whereListing_type('wesell')->orderBy('vetting_status')->orderBy('property.id')->
        get();

        $propertyIds = $this->property->select('property.id')->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('property.user_id', '=', $userId)->whereListing_type('wesell')->orderBy('vetting_status')->orderBy('property.id')->
        get();

        $percent = [];
        $count = 0;
        foreach ($properties as $property) {
            $property['id'] = $propertyIds[$count]->id;

            $propPrice = DB::table('property')->select('price')->whereId($property->id)->first();

            $property['price'] = $propPrice->price;
            $property['property_id'] = $propertyIds[$count]->id;
            $percent[] = App::make(\App\Http\Controllers\PropertyController::class)->form_complete_percent($property['id']);

            // checks to see if any potential buyers have checked cotact details

            $result = DB::table('make_contact')->select()->whereProperty_id($property->property_id)->get();

            if (! $result) {
                $property['makecontact'] = 0;
            } else {
                $property['makecontact'] = 1;
            }

            $count++;
        }

        return view('portal.sellers', ['properties' => $properties, 'percent' => $percent])->withUser($user);
    }

    public function registeredServiceProvider($province, $area, $city, $category)
    {
        $user = Auth::user();
        $citiesDB = DB::table('areas')->orderBy('suburb_town_city', 'ASC')->get();

        foreach ($citiesDB as $cityDB) {
            $cities[] = $cityDB->suburb_town_city.', '.$cityDB->area.', '.$cityDB->province;
        }
        $cities = implode('", "', $cities);
        $categories = DB::table('service_provider_categories')->get();

        return view('portal.registeredServiceProviders', ['cities' => $cities, 'categories' => $categories, 'province' => $province, 'city' => $city, 'area' => $area, 'category' => $category])->withUser($user);
    }

    public function propertyRaceTrack()
    {
        $user = Auth::user();

        return view('portal.property_race_track')->withUser($user);
    }

    public function HowToGuide()
    {
        $user = Auth::user();

        return view('portal.howToGuide')->withUser($user);
    }

    public function ServiceProvider()
    {
        $user = Auth::user();

        return view('portal.ServiceProvider')->withUser($user);
    }

    public function Landlords()
    {
        $user = Auth::user();

        $userId = $user->id;

        $properties = $this->property->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        leftJoin('residential', 'property.id', '=', 'residential.property_id')->
        leftJoin('commercial', 'property.id', '=', 'commercial.property_id')->
        leftJoin('farm', 'property.id', '=', 'farm.property_id')->
        where('property.user_id', '=', $userId)->whereListing_type('welet')->orderBy('vetting_status')->orderBy('property.id')->get();

        $propertyIds = $this->property->select('property.id')->
        join('vetting', 'property.id', '=', 'vetting.property_id')->
        where('property.user_id', '=', $userId)->whereListing_type('welet')->orderBy('vetting_status')->orderBy('property.id')->get();

        $percent = [];
        $count = 0;
        foreach ($properties as $property) {
            $property['id'] = $propertyIds[$count]->id;
            $property['property_id'] = $propertyIds[$count]->id;
            $percent[] = App::make(\App\Http\Controllers\PropertyController::class)->form_complete_percent($property['id']);

            // checks to see if any potential tenants have checked cotact details

            $result = DB::table('make_contact')->select()->whereProperty_id($property->property_id)->get();

            if (! $result) {
                $property['makecontact'] = 0;
            } else {
                $property['makecontact'] = 1;
            }
            $count++;
        }
        // dd($properties->toArray());

        return view('portal.landlords', ['properties' => $properties, 'percent' => $percent])->withUser($user);
    }

    public function property_needs($role)
    {
        $provincesDB = DB::table('areas')->selectRaw('areas.province, count(*)')->groupBy('province')->get();
        $areasDB = DB::table('areas')->selectRaw('areas.area, areas.province, count(*)')->groupBy('province', 'area')->get();
        $citiesDB = DB::table('areas')->selectRaw('areas.suburb_town_city, areas.area, areas.province, count(*)')->groupBy('province', 'area', 'suburb_town_city')->get();

        foreach ($citiesDB as $city) {
            $cities[] = $city->suburb_town_city;
        }
        $cities = implode('", "', $cities);

        foreach ($provincesDB as $province) {
            $provinces[] = $province->province;
        }
        $provinces = implode('", "', $provinces);

        foreach ($areasDB as $area) {
            $areas[] = $area->area;
        }
        $areas = implode('", "', $areas);

        $user = Auth::user();

        return view('portal.property_needs', ['provinces' => $provinces, 'cities' => $cities, 'areas' => $areas,
            'role' => $role, 'dwelling_type' => '', 'bedrooms' => '', 'bathrooms' => '', 'garages' => '', ])->withUser($user);
    }

    public function Deeds()
    {
        $deeds = DB::table('deed_of_sale')->join('property', 'property.id', '=', 'deed_of_sale.property_id')->whereBuyer_id(Auth::user()->id)->get();

        return view::make('portal.deeds', ['deeds' => $deeds]);
    }

    public function Leases()
    {
        $leases = DB::table('lease_agreements')->join('property', 'property.id', '=', 'lease_agreements.property_id')->whereTenant_id(Auth::user()->id)->get();

        return view::make('portal.leases', ['leases' => $leases]);
    }

    public function getFullArea()
    {
        $suburb = Request::get('suburb');
        $city = Request::get('city');
        $city = Request::get('province');
        $group = Request::get('group');
        switch ($group) {
            case 'Province':
                $column = 'province';

                break;
            case 'City':
                $column = 'area';

                break;
            case 'Suburb':
                $column = 'suburb_town_city';

                break;
            default:
                break;
        }

        $area_row = DB::table('areas')->where($column, '=', $place)->first();

        switch ($group) {
            case 'Province':
                $area_row->area = '';
                $area_row->area_slug = '';
                $area_row->suburb_town_city = '';
                $area_row->suburb_town_city_slug = '';

                break;
            case 'City':
                $area_row->suburb_town_city = '';
                $area_row->suburb_town_city_slug = '';

                break;
            default:
                break;
        }

        return ['area_row' => $area_row];
    }

    public function getSavedSearches($user_id)
    {
        $preferences = $this->preferences->whereUser_id($user_id)->get();

        foreach ($preferences as $preference) {
            if ($preference['listing_type'] == 'to-let') {
                $preference['price1'] = '0-15000000+';
                $preference['price2'] = str_replace('R', '', str_replace(' ', '', $preference['price']));
            } else {
                $preference['price1'] = str_replace('R', '', str_replace(' ', '', $preference['price']));
                $preference['price2'] = '0-30000+';
            }
        }

        $slug = new Slugify();

        foreach ($preferences as $preference) {
            $preference['province'] = $slug->slugify($preference['province']);
            $preference['area'] = $slug->slugify($preference['area']);
            $preference['suburb_town_city'] = $slug->slugify($preference['suburb_town_city']);
        }

        return Datatable::collection($preferences)
            ->addColumn('title', function ($Preference) {
                switch ($Preference->listing_type) {
                    case 'welet':
                        $listing_type = ' properties to rent';

                        break;
                    case 'weauction':
                        $listing_type = ' properties on auction';

                        break;
                    case 'wesell':
                        $listing_type = ' properties on sale';

                        break;
                    default:
                        $listing_type = '';

                        break;
                }
                if ($Preference->suburb_town_city == 'all-suburbs') {
                    if ($Preference->area == 'all-cities') {
                        if ($Preference->province == 'south-africa') {
                            $place = ' in South Africa';
                        } else {
                            $place = ' in '.ucfirst($Preference->province);
                        }
                    } else {
                        $place = ' in '.ucfirst($Preference->area);
                    }
                } else {
                    $place = ' in '.ucfirst($Preference->suburb_town_city);
                }
                $description = ucfirst($Preference->property_type).$listing_type.$place;

                return $description;
            })
//            ->showColumns('province')
            ->addColumn('actions', function ($Preference) {
                switch ($Preference->property_type) {
                    case 'residential':
                        if ($Preference['dwelling_type'] == 'None') {
                            $sub_type = '';
                        } else {
                            $sub_type = '&st='.$Preference['dwelling_type'];
                        }
                        if ($Preference['bedrooms'] == 'None') {
                            $bedrooms = '';
                        } else {
                            $bedrooms = '&b1='.$Preference['bedrooms'];
                        }
                        if ($Preference['bathrooms'] == 'None') {
                            $bathrooms = '';
                        } else {
                            $bathrooms = '&b2='.$Preference['bathrooms'];
                        }
                        $append = $sub_type.$bedrooms.$bathrooms;

                        break;
                    case 'commercial':
                        if ($Preference['commercial_type'] == 'None') {
                            $sub_type = '';
                        } else {
                            $sub_type = '&st='.$Preference['commercial_type'];
                        }
                        $append = $sub_type;

                        break;
                    case 'farm':
                        if ($Preference['farm_type'] == 'None') {
                            $sub_type = '';
                        } else {
                            $sub_type = '&st='.$Preference['farm_type'];
                        }
                        $append = $sub_type;

                        break;
                    default:
                        $append = '';

                        break;
                }
                switch ($Preference->listing_type) {
                    case 'wesell':
                        $listing_type = 'for-sale';

                        break;
                    case 'welet':
                        if ($Preference->property_type == 'residential') {
                            $listing_type = 'to-rent';
                        } else {
                            $listing_type = 'to-let';
                        }

                        break;
                    case 'weauction':
                        $listing_type = 'on-auction';

                        break;
                    default:
                        $listing_type = 'all-listings';

                        break;
                }

                if ($Preference['price1'] == '0-15000000+') {
                    $price1 = '';
                } else {
                    $price1 = '&pr='.$Preference['price1'];
                }

                if ($Preference['price2'] == '0-30000+') {
                    $price2 = '';
                } else {
                    $price2 = '&rpr='.$Preference['price2'];
                }

                if (str_replace(' ', '', $Preference['floor_space']) == '0-2000+') {
                    $floor_space = '';
                } else {
                    $floor_space = '&fr='.str_replace(' ', '', $Preference['floor_space']);
                }

                if (str_replace(' ', '', $Preference['land_size']) == '0-20000+') {
                    $land_size = '';
                } else {
                    $land_size = '&lr='.str_replace(' ', '', $Preference['land_size']);
                }

                return '<a href="/'.$Preference['property_type'].'/property/'.$listing_type.'/'.
                $Preference['province'].'/'.$Preference['area'].'/'.$Preference['suburb_town_city'].
                '?o=default&v=grid_view'.$price1.$price2.$floor_space.$land_size.$append.'
                " title="View Results" >
                <i class="i-circled i-light i-small fa fa-laptop"></i></a>
                <a href="/property-search/edit/'.$Preference['id'].'" title="Edit Property Search" >
                <i class="i-circled i-light i-small fa fa-edit"></i></a>
                <a href="/property-search/delete/'.$Preference['id'].'" title="Delete Property Search" >
                <i class="i-circled i-light i-small fa fa-times" style="background-color: #f04f23"></i></a>';
            })
            ->searchColumns('title')
            ->orderColumns('title')
            ->make();
    }
}
