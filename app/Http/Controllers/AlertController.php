<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AlertController extends Controller
{
    public function __construct(EmailAlert $email_alert, AuctionRegister $auction_register, PhysicalAuction $physical_auction,
                                Property $property, Auction $auction, Preferences $preferences)
    {
        $this->email_alert = $email_alert;
        $this->auction_register = $auction_register;
        $this->physical_auction = $physical_auction;
        $this->property = $property;
        $this->auction = $auction;
        $this->preferences = $preferences;
    }

    public function checkAuctionEmailAlerts()
    {
        try {
            $physical_auctions = DB::select('SELECT a.physical_auction
                                            FROM auction a
                                            WHERE a.start_date
                                            BETWEEN
                                            DATE_ADD(CURDATE(), INTERVAL 1 day)
                                            AND DATE_ADD(CURDATE(), INTERVAL 2 day)
                                            AND a.auction_type = 1
                                            GROUP BY a.physical_auction');
            $physical_auctions = json_decode(json_encode($physical_auctions), true);

            $onsite_auctions = DB::select('SELECT a.id
                                        FROM auction a
                                        WHERE a.start_date BETWEEN DATE_ADD(CURDATE(), INTERVAL 1 day)
                                        AND DATE_ADD(CURDATE(), INTERVAL 2 day)
                                        AND a.auction_type = 5');

            foreach ($physical_auctions as $auction) {
                $this->getAuctionEmailAlerts($auction['physical_auction'], 1);
            }

            foreach ($onsite_auctions as $auction) {
                $this->getAuctionEmailAlerts($auction['id'], 5);
            }
            if (empty($physical_auctions) && empty($onsite_auctions)) {
                return 'There are no emails to be sent out';
            } else {
                return 'All emails sent successfully';
            }
        } catch (Exception $e) {
            return "Error Details: $e";
        }
    }

    public function getAuctionEmailAlerts($auction_id, $type)
    {
        $type_id = DB::table('email_alert_type')->select('id')->whereType('auction')->first();

        $alerts = $this->email_alert->whereType_id($auction_id)->whereSub_type($type)->whereType($type_id->id)->get();

        $this->sendAuctionAlerts($alerts);
    }

    public function sendAuctionAlerts($alerts)
    {
        foreach ($alerts as $alert) {
            $alert_type = DB::table('email_alert_type')->select('type')->whereId($alert->type)->first();

            switch ($alert_type->type) {
                case 'auction':
                    if ($alert->sub_type == 1) {
                        $auction_dets = $this->physical_auction->whereId($alert->type_id)->first();
                        $property_list = $this->property->select('property_type', 'property.id AS property_id', 'slug',
                            'lead_image', 'listing_title', 'short_descrip')->
                        join('auction', 'auction.property_id', '=', 'property.id')->
                        join('vetting', 'vetting.property_id', '=', 'property.id')->
                        leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
                        leftJoin('residential', 'residential.property_id', '=', 'property.id')->
                        leftJoin('farm', 'farm.property_id', '=', 'property.id')->
                        where('auction.physical_auction', '=', $alert->type_id)->
                        get();
                        $auction_type = 1;
                    } elseif ($alert->sub_type == 5) {
                        $auction_dets = $this->auction->whereId($alert->type_id)->first();
                        $property_list = $this->property->select('property_type', 'property.id AS property_id', 'slug',
                            'lead_image', 'listing_title', 'short_descrip')->
                        join('auction', 'auction.property_id', '=', 'property.id')->
                        join('vetting', 'vetting.property_id', '=', 'property.id')->
                        leftJoin('commercial', 'commercial.property_id', '=', 'property.id')->
                        leftJoin('residential', 'residential.property_id', '=', 'property.id')->
                        leftJoin('farm', 'farm.property_id', '=', 'property.id')->
                        where('auction.id', '=', $alert->type_id)->
                        get();
                        $auction_type = 5;
                    }

                    $userMail = $alert->email;
                    $subject = 'This is a reminder of our Auction on '.date('Y-m-d', strtotime($auction_dets->start_date));
                    $name = 'Dear Sir/Madam,<br>';

                    $data = ['name' => $name, 'subject' => $subject, 'auction_dets' => $auction_dets, 'property_list' => $property_list,
                        'auction_type' => $auction_type, ];
                    // Queue email
                    Mail::queue('emails.auction_alert.auction_alert', $data, function ($message) use ($userMail, $name, $subject) {
                        $message->to($userMail, $name)
                            ->subject($subject);
                    });

                    break;
            }
            $alert->delete();
        }
    }

    public function addPreferenceAlert($preference_id)
    {
        $type = DB::table('email_alert_type')->select('id')->whereType('preference')->first();

        $preference = $this->preferences->
        join('users', 'users.id', '=', 'preferences.user_id')->
        select('users.email')->where('preferences.id', '=', $preference_id)->first();

        $input['email'] = $preference->email;
        $input['type'] = $type->id;
        $input['type_id'] = $preference_id;

        $today = date('Y-m-d');
        $input['send_date'] = date('Y-m-t', strtotime($today));
        if ($input['send_date'] == $today) {
            $input['send_date'] = date('Y-m-t', strtotime('+1 month'));
        }

        if ($this->email_alert->whereEmail($input['email'])->whereType_id($input['type_id'])->whereType($input['type'])->first()) {
            return 'Complete';
        }

        $this->email_alert->fill($input)->save();

        return 'Complete';
    }

    public function checkPreferenceAlerts()
    {
        $today = date('Y-m-d');
        //get next month
        $next_month_date = date('Y-m-t', strtotime('+1 month'));
        //get type id
        $type = DB::table('email_alert_type')->select('id')->whereType('preference')->first();

        $alerts = $this->email_alert->whereSend_date($today)->whereType($type->id)->get();

        $count = 0;
        $alert_ids = [];
        foreach ($alerts as $alert) {
            $properties = App::make('PreferencesController')->getPreferencesResults($alert->type_id);
            if ($properties->isEmpty()) {
                //set send_date to last day of next month
                $alert->send_date = $next_month_date;
                //update database
                $alert->update();
            } else {
                //should send email,
                $this->sendPreferenceAlert($properties, $alert);
                // add alert id to array of ids to add to batch
                $alert_ids[] = $alert->id;
                $count++;
            }
        }
        //create new batch email,
        if ($count > 0) {
            $batch_id = DB::table('batch_emails')->insertGetId(
                ['email_count' => $count, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );

            // update all email alerts in array with batch id,
            foreach ($alert_ids as $alert_id) {
                //get alert
                $alert = $this->email_alert->whereId($alert_id)->first();
                //get preferences id
                $preference_id = $alert->type_id;
                //set sub_type = batch id
                $alert->sub_type = $batch_id;
                $alert->update();
                // delete email alert and
                $alert->delete();
                // create new email alert for following month.
                $input['email'] = Auth::user()->email;
                $input['type'] = $type->id;
                $input['type_id'] = $preference_id;

                $input['send_date'] = date('Y-m-t', strtotime($next_month_date));

                if ($this->email_alert->whereEmail($input['email'])->whereType_id($input['type_id'])->whereType($input['type'])->first()) {
                    return 'Complete';
                }
                $this->email_alert->fill($input)->save();
            }
        }

        return 'Complete';
    }

    public function sendPreferenceAlert($properties, $alert)
    {
        $userMail = $alert->email;
        $subject = 'The following property(ies) were added in the last month that match your search criteria';
        $name = 'Dear Sir/Madam';

        $data = ['name' => $name, 'subject' => $subject, 'properties' => $properties];
        // Queue email
        Mail::queue('emails.preference.monthly_preference_email', $data, function ($message) use ($userMail, $name, $subject) {
            $message->to($userMail, $name)
                ->subject($subject);
        });
    }
}
