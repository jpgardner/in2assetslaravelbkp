
<?php

   if (isset($_FILES['image'])) {
       $errors = [];
       $file_name = $_FILES['image']['name'];
       $file_size = $_FILES['image']['size'];
       $file_tmp = $_FILES['image']['tmp_name'];
       $file_type = $_FILES['image']['type'];
       $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));

       $expensions = ['pdf', 'jpg', 'png'];

       if (in_array($file_ext, $expensions) === false) {
           $errors[] = 'Please select a file or make sure the selected file is in PDF format.';
       }

       if ($file_size > 2097152000) {
           $errors[] = 'File size must be excately 2 Gigabytes';
       }

       if (empty($errors) == true) {
           move_uploaded_file($file_tmp, 'brochure/'.$file_name);
           echo 'Success';
       } else {
           print_r($errors);
       }
   }

?>
<script>
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction2(x) {
  var copyText = document.getElementById("myInput"+x);
  copyText.select();
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
</script>
<html>
<head>
<meta name="robots" content="noindex, nofollow"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
   <body>
      
<div class="navbar navbar-default navbar-fixed-top navbar-inverse nav"  style="opacity:0.9; box-shadow: 0 5px 8px 0 rgba(0,0,0,.25);marging">
  <div class="container-fluid">
     <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

                <a href="https://www.in2assets.co.za/upload.php"><img height="70" alt="In2assets Logo" src="/images/in2assets-logo-light.png"></a>

        </div>
    
  </div>
</div>
<div class="container body-content" style="margin-top:120px">
<div>
<h3>Instructions:</h3>
<p>Please choose the file you wish to upload and generate a link for by clicking on on the browse button on the far left.Once the file has been selected and you can see it's name on the text box, click on "Generate Link" button to get rolling.The document will be uploaded, the link will also be generate and displayed on the text box below the "Generate Link" button.Now you can use the "Copy Link" button next to any text box to copy the link or can just copy it like normal text.</p>
<p>To get previously genrate file links, just scroll down the list of "previously Uploaded" files.</p>
</div>
<hr />
         <form  action = "" method = "POST" enctype = "multipart/form-data">
		 
		 <div class="form-group">  
			<label class="control-label col-sm-2" for="file">Choose File:</label>
			<div class="col-sm-6">
			   <input type = "file" class="form-control" name = "image" />
			</div>
		 </div>
			<br /><br /><br />
		<div class="form-group">
			<label class="control-label col-sm-2" for="file"></label>
			<div class="col-sm-6">
				 <input class="btn btn-default" value="Generate Link" type ="submit"/>
			</div>
		</div>
			<br /><br />
	
			   <?php $res = 'https://www.in2assets.co.za/brochure/'.strstr($_FILES['image']['name'], '.', true);
               echo "
		<div class='form-group'>	
			<label class='control-label col-sm-2' for='sentFile'>Your File Link:</label>
			<div class='col-sm-6'>
				<input type='text' class='form-control' value='$res' id='myInput'/>
			</div>
		</div>
		<br /><br />
		<div class='form-group'>
			<label class='control-label col-sm-2' for='filez'></label>
		<div class='col-sm-6'>
				 <button class='btn btn-default' onclick='myFunction()'>Copy Link</button>
			</div> 
		</div>"?>
			
      </form>


<div class="container" style="margin-top:50px">

<h3 style="margin-top:30px">Previously uploaded:</h3>
</div>

     <?php
     $dir = 'brochure/';
$x = 1;
// Open a directory, and read its contents
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            $resu = 'https://in2assets.co.za/brochure/'.strstr($file, '.', true);
            echo "<div class='container' >	  
	<div class='row'>
		<div class='col-sm-8'><input type='text' class='form-control' value='$resu' id='myInput$x'/></div>
		<div class='col-sm-4' ><button class='btn btn-default' onclick='myFunction2($x)'>Copy Link</button></div>
    </div><br>
	  </div>
	";
            $x++;
        }
        closedir($dh);
    }
}
     ?>
</div>
   </body>
</html>