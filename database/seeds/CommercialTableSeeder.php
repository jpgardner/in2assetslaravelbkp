<?php

class CommercialTableSeeder extends Seeder
{
    public function run()
    {
        Eloquent::unguard();

        $price = range(5000, 20000);

        $comtype_array = ['Office Space', 'Retail Space', 'Hospitality', 'Industrial Space', 'Vacant Land'];
        $commercial_type = $comtype_array[array_rand($comtype_array)];
        $parking_array = ['Garage', 'Covered', 'Street', 'None'];
        $parking_type = $parking_array[array_rand($parking_array)];

        Commercial::create([
            'price' => $price[array_rand($price)],
            'commercial_type' => $commercial_type,
            'parking' => $parking_type,
        ]);
    }
}
