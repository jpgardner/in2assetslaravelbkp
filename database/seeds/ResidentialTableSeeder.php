<?php

class ResidentialTableSeeder extends Seeder
{
    public function run()
    {
        Eloquent::unguard();
        $restype_array = ['House', 'Flat', 'Cottage', 'Industrial Space', 'Holiday Home', 'Vacant Land', 'B&Bs'];
        $residential_type = $restype_array[array_rand($restype_array)];
        $parking_array = ['Garage', 'Covered', 'Street', 'None'];
        $parking_type = $parking_array[array_rand($parking_array)];
        $bedrooms_array = ['Studio', '1 Bedroom', '2 Bedrooms', '3 Bedrooms', '4 Bedrooms', '5 Bedrooms or more'];
        $bedrooms = $bedrooms_array[array_rand($bedrooms_array)];
        $bathrooms_array = ['1 Bathroom', '2 Bathrooms', '3 Bathrooms', '4 Bathrooms or more'];
        $bathrooms = $bathrooms_array[array_rand($bathrooms_array)];
        $price = range(5000, 20000);
        Residential::create([
                'price' => $price[array_rand($price)],
                'dwelling_type' => $residential_type,
                'parking' => $parking_type,
                'bedrooms' => $bedrooms,
                'bathrooms' => $bathrooms,
                'pet' => 0,
            ]);
    }
}
