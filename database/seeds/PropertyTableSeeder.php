<?php

class PropertyTableSeeder extends Seeder
{
    public function run()
    {
        Eloquent::unguard();

        $faker = Faker\Factory::create('en_ZA');

        foreach (range(1, 20) as $index) {
            $listing_array = ['Rent', 'Sale', 'Auction'];
            $proptype_array = ['Residential', 'Commercial', 'Farm'];
            $listing_type = $listing_array[array_rand($listing_array)];
            $property_type = $proptype_array[array_rand($proptype_array)];
            Property::create([
                'id' => $index,
                'reference' => $index,
                'short_descrip' => $faker->sentence(3),
                'long_descrip' => $faker->paragraph(3),
                'street_address' => $faker->streetAddress,
                'suburb_town_city' => $faker->city,
                'area' => $faker->city,
                'province' => $faker->province,
                'listing_type' => $listing_type,
                'property_type' => $property_type,
            ]);
            $this->call('VettingTableSeeder');
            if ($property_type === 'Residential') {
                $this->call('ResidentialTableSeeder');
            }
            if ($property_type === 'Commercial') {
                $this->call('CommercialTableSeeder');
            }
            if ($property_type === 'Farm') {
                $this->call('FarmTableSeeder');
            }
        }
    }
}
