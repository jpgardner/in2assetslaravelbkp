<?php

class FarmTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create('en_ZA');

        Eloquent::unguard();

        $farmtype_array = ['Agricultural Farm', 'Small Holding', 'Game Farm', 'Farm Land'];
        $farm_type = $farmtype_array[array_rand($farmtype_array)];
        $price_range = range(5000, 20000);

        Farm::create([
            'price' => $price_range[array_rand($price_range)],
            'farm_type' => $farm_type,
            'farm_name' => $faker->sentence(2),
            ]);
    }
}
