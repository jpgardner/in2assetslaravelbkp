<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class VettingTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create('en_ZA');
        $status_array = ['pending', 'approved', 'rejected', 'vetting'];
        $status = $status_array[array_rand($status_array)];
        Eloquent::unguard();
        Vetting::create([
                        'owner_title' => $faker->title($gender = null | 'male' | 'female'),
                        'owner_firstname' => $faker->firstName($gender = null | 'male' | 'female'),
                        'owner_surname' => $faker->lastName,
                        'rep_title' => $faker->title($gender = null | 'male' | 'female'),
                        'rep_firstname' => $faker->firstName($gender = null | 'male' | 'female'),
                        'rep_surname' => $faker->lastName,
                        'marriage_stat' => 'Single',
                        'ownership_type' => 'Private Owner',
                        'vetting_status' => $status,
                        ]);
    }
}
