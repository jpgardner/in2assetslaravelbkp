<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnsToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property', function (Blueprint $table) {
            $table->unsignedInteger('final_price')->after('land_measurement');
            $table->string('purchaser_name')->after('final_price');
            $table->string('purchaser_email')->after('purchaser_name');
            $table->string('purchaser_cell')->after('purchaser_email');
            $table->string('notes')->after('purchaser_cell');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property', function (Blueprint $table) {
            $table->dropColumn('final_price');
            $table->dropColumn('purchaser_name');
            $table->dropColumn('purchaser_email');
            $table->dropColumn('purchaser_cell');
            $table->dropColumn('notes');
        });
    }
}
