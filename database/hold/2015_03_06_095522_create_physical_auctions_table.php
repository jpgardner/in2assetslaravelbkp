<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhysicalAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_auctions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('auction_name');
            $table->string('province');
            $table->string('area');
            $table->string('suburb_town_city');
            $table->string('street_address');
            $table->date('start_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('physical_auctions');
    }
}
