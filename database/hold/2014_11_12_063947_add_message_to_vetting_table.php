<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMessageToVettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vetting', function (Blueprint $table) {
            $table->string('message')->after('vetting_status');
            $table->unsignedInteger('vetted_by')->after('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vetting', function (Blueprint $table) {
            $table->dropColumn('message');
            $table->dropColumn('vetted_by');
        });
    }
}
