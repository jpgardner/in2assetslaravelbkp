<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeaseAmendmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lease_amendments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lease_agreement_id');
            $table->string('tenant_business');
            $table->string('tenant_business_number');
            $table->tinyInteger('business_type');
            $table->string('vat_number');
            $table->string('trade_names');
            $table->string('business_nature');
            $table->string('tenant_registered_business_address');
            $table->string('tenant_physical_business_address');
            $table->string('tenant_postal_business_address');
            $table->string('tenant_business_telephone');
            $table->string('tenant_business_fax');
            $table->string('tenant_business_email');
            $table->string('tenant_contact_person');
            $table->string('surety_fullname');
            $table->string('surety_id');
            $table->string('surety_fullname_2nd');
            $table->string('surety_id_2nd');
            $table->string('landlord_business');
            $table->string('landlord_business_number');
            $table->string('landlord_physical_business_address');
            $table->string('landlord_postal_business_address');
            $table->string('landlord_business_telephone');
            $table->string('landlord_business_fax');
            $table->string('landlord_business_email');
            $table->string('contact_person');
            $table->date('commencement_date');
            $table->string('lease_term');
            $table->string('renewal_period');
            $table->tinyInteger('tenant_prorata_share');
            $table->unsignedInteger('current_rental');
            $table->float('base_rental');
            $table->float('operating_costs');
            $table->float('rates_taxes');
            $table->float('parking_rental');
            $table->float('storeroom_rental');
            $table->float('annual_increase');
            $table->unsignedInteger('deposit');
            $table->string('agent_business');
            $table->string('agent_business_telephone');
            $table->string('agent_business_fax');
            $table->string('agent_business_email');
            $table->string('agent_contact_person');
            $table->string('lease_business');
            $table->string('lease_business_telephone');
            $table->string('lease_business_email');
            $table->string('lease_contact_person');
            $table->string('property_description');
            $table->string('property_address');
            $table->string('leased_property_description');
            $table->string('unit_no');
            $table->unsignedInteger('unit_size');
            $table->unsignedInteger('parking_basement');
            $table->unsignedInteger('parking_covered');
            $table->string('premises_use');
            $table->string('annexure_a');
            $table->string('annexure_b');
            $table->string('annexure_c');
            $table->string('annexure_d');
            $table->string('annexure_e');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lease_amendments');
    }
}
