<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeedAmendmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deed_amendment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('deed_of_sale_id');
            $table->string('business_name');
            $table->string('business_reg_num');
            $table->string('seller_id');
            $table->string('purchaser_name');
            $table->string('purchaser_id_num');
            $table->string('purchaser_address');
            $table->string('purchaser_tel_num');
            $table->string('purchaser_fax_num');
            $table->string('purchaser_email');
            $table->string('purchaser_cell_num');
            $table->string('purchaser_marital_status');
            $table->tinyInteger('purchaser_married_in_com')->nullable();
            $table->string('purchaser_spouse');
            $table->string('purchaser_spouse_id');
            $table->string('entity_type');
            $table->string('entity_name');
            $table->string('entity_reg_num');
            $table->string('entity_vat_num');
            $table->string('entity_address');
            $table->string('entity_tel_num');
            $table->string('entity_fax_num');
            $table->string('entity_email');
            $table->string('entity_cell_num');
            $table->string('title_description');
            $table->string('title_deed');
            $table->unsignedInteger('in_extent');
            $table->string('street_address_known_as');
            $table->unsignedInteger('purchase_price');
            $table->tinyInteger('deposit');
            $table->string('deposit_payment');
            $table->date('deposit_payment_date');
            $table->string('guarantee_period');
            $table->date('guarantee_period_date');
            $table->string('confirmation_period');
            $table->date('confirmation_date');
            $table->unsignedInteger('occupational_interest');
            $table->string('conveyancer');
            $table->string('conveyancer_address');
            $table->unsignedInteger('subject_to_bond');
            $table->tinyInteger('tenancy')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deed_amendment');
    }
}
