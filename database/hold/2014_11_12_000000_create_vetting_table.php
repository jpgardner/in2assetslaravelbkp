<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vetting', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('property_id');
            $table->string('owner_title');
            $table->string('owner_firstname');
            $table->string('owner_surname');
            $table->string('ownership_type');
            $table->string('rep_title');
            $table->string('rep_firstname');
            $table->string('rep_surname');
            $table->bigInteger('rep_id_number')->unsigned();
            $table->string('rep_cell_num');
            $table->string('rep_email');
            $table->string('marriage_stat');
            $table->string('mar_in_com')->nullable();
            $table->string('spouse_title')->nullable();
            $table->string('spouse_firstname')->nullable();
            $table->string('spouse_surname')->nullable();
            $table->bigInteger('spouse_id_number')->unsigned()->nullable();
            $table->string('extra_owner_title1')->nullable();
            $table->string('extra_owner_firstname1')->nullable();
            $table->string('extra_owner_surname1')->nullable();
            $table->string('extra_owner_title2')->nullable();
            $table->string('extra_owner_firstname2')->nullable();
            $table->string('extra_owner_surname2')->nullable();
            $table->string('extra_owner_title3')->nullable();
            $table->string('extra_owner_firstname3')->nullable();
            $table->string('extra_owner_surname3')->nullable();
            $table->tinyInteger('vetting_status');
            $table->string('resolution')->nullable();
            $table->string('id_photos')->nullable();
            $table->softDeletes();
            $table->timestamp('approved_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vetting');
    }
}
