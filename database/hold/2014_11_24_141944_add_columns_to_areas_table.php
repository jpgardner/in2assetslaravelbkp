<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnsToAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->string('province_slug')->after('province');
            $table->string('area_slug')->after('area');
            $table->string('suburb_town_city_slug')->after('suburb_town_city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->dropColumn('province_slug');
            $table->dropColumn('area_slug');
            $table->dropColumn('suburb_town_city_slug');
        });
    }
}
