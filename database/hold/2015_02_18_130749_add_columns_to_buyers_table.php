<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnsToBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buyers', function (Blueprint $table) {
            $table->tinyInteger('private_buyer')->after('user_id');
            $table->tinyInteger('multiple_buyer')->after('private_buyer');
            $table->string('ownership_type')->after('multiple_buyer');
            $table->string('rep_full_name')->after('ownership_type');
            $table->string('rep_id_number')->after('rep_full_name');
            $table->string('contact_name')->after('rep_id_number');
            $table->string('contact_cell')->after('contact_name');
            $table->string('contact_email')->after('contact_cell');
            $table->tinyInteger('married')->after('contact_email');
            $table->string('id_number')->after('married');
            $table->tinyInteger('married_in_com')->after('id_number');
            $table->string('spouse_id_number')->after('married_in_com');
            $table->string('spouse_full_name')->after('spouse_id_number');
            $table->tinyInteger('self_employed')->after('spouse_full_name');
            $table->tinyInteger('pre_approval')->after('self_employed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buyers', function (Blueprint $table) {
            $table->dropColumn('private_buyer');
            $table->dropColumn('multiple_buyer');
            $table->dropColumn('ownership_type');
            $table->dropColumn('rep_full_name');
            $table->dropColumn('rep_id_number');
            $table->dropColumn('contact_name');
            $table->dropColumn('contact_cell');
            $table->dropColumn('contact_email');
            $table->dropColumn('married');
            $table->dropColumn('id_number');
            $table->dropColumn('married_in_com');
            $table->dropColumn('spouse_id_number');
            $table->dropColumn('spouse_full_name');
            $table->dropColumn('self_employed');
            $table->dropColumn('pre_approval');
        });
    }
}
