<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserInterestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_interest', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('users_id');
            $table->tinyInteger('interest_res');
            $table->tinyInteger('interest_com');
            $table->tinyInteger('interest_farm');
            $table->tinyInteger('interest_auc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_interest');
    }
}
