<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAmenetiesToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property', function (Blueprint $table) {
            $table->tinyInteger('dining_room')->after('price');
            $table->tinyInteger('entrance_hall')->after('dining_room');
            $table->tinyInteger('tv_room')->after('entrance_hall');
            $table->tinyInteger('study')->after('tv_room');
            $table->tinyInteger('lounge')->after('study');
            $table->tinyInteger('kitchen')->after('lounge');
            $table->tinyInteger('recreation_room')->after('kitchen');
            $table->tinyInteger('pantry')->after('recreation_room');
            $table->tinyInteger('laundry')->after('pantry');
            $table->tinyInteger('guest_toilet')->after('laundry');
            $table->tinyInteger('patio')->after('guest_toilet');
            $table->tinyInteger('view')->after('patio');
            $table->tinyInteger('tennis_court')->after('view');
            $table->tinyInteger('pool')->after('tennis_court');
            $table->tinyInteger('aircon')->after('pool');
            $table->tinyInteger('aerial')->after('aircon');
            $table->tinyInteger('dstv')->after('aerial');
            $table->tinyInteger('bic')->after('dstv');
            $table->tinyInteger('stove')->after('bic');
            $table->tinyInteger('shower')->after('stove');
            $table->tinyInteger('staff_quarters')->after('shower');
            $table->tinyInteger('lapa_braai')->after('staff_quarters');
            $table->tinyInteger('intercom')->after('lapa_braai');
            $table->tinyInteger('alarm')->after('intercom');
            $table->tinyInteger('elec_gate')->after('alarm');
            $table->tinyInteger('sec_fence')->after('elec_gate');
            $table->tinyInteger('sec_lights')->after('sec_fence');
            $table->tinyInteger('pets')->after('sec_lights');
            $table->tinyInteger('sprinklers')->after('pets');
            $table->tinyInteger('dam')->after('sprinklers');
            $table->tinyInteger('borehole')->after('dam');
            $table->tinyInteger('electric_fence')->after('borehole');
            $table->unsignedInteger('floor_space')->after('electric_fence');
            $table->unsignedInteger('land_size')->after('floor_space');
            $table->string('land_measurement')->after('land_size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property', function (Blueprint $table) {
            $table->dropColumn('dining_room');
            $table->dropColumn('entrance_hall');
            $table->dropColumn('tv_room');
            $table->dropColumn('study');
            $table->dropColumn('lounge');
            $table->dropColumn('kitchen');
            $table->dropColumn('recreation_room');
            $table->dropColumn('pantry');
            $table->dropColumn('laundry');
            $table->dropColumn('guest_toilet');
            $table->dropColumn('patio');
            $table->dropColumn('view');
            $table->dropColumn('tennis_court');
            $table->dropColumn('pool');
            $table->dropColumn('sprinklers');
            $table->dropColumn('aircon');
            $table->dropColumn('aerial');
            $table->dropColumn('dstv');
            $table->dropColumn('bic');
            $table->dropColumn('stove');
            $table->dropColumn('shower');
            $table->dropColumn('staff_quarters');
            $table->dropColumn('lapa_braai');
            $table->dropColumn('intercom');
            $table->dropColumn('alarm');
            $table->dropColumn('elec_gate');
            $table->dropColumn('sec_fence');
            $table->dropColumn('sec_lights');
            $table->dropColumn('pets');
            $table->dropColumn('dam');
            $table->dropColumn('borehole');
            $table->dropColumn('electric_fence');
            $table->dropColumn('floor_space');
            $table->dropColumn('land_size');
            $table->dropColumn('land_measurement');
        });
    }
}
