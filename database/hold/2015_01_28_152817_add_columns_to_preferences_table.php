<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnsToPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preferences', function (Blueprint $table) {
            $table->tinyInteger('security')->after('parking');
            $table->tinyInteger('aircon')->after('pets');
            $table->tinyInteger('bic')->after('aircon');
            $table->tinyInteger('pool')->after('bic');
            $table->tinyInteger('tennis_court')->after('pool');
            $table->string('sale_type')->after('tennis_court');
            $table->integer('floor_min')->after('farm_type');
            $table->integer('floor_max')->after('floor_min');
            $table->integer('land_min')->after('floor_max');
            $table->integer('land_max')->after('land_min');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preferences', function (Blueprint $table) {
            $table->dropColumn('security');
            $table->dropColumn('aircon');
            $table->dropColumn('bic');
            $table->dropColumn('pool');
            $table->dropColumn('tennis_court');
            $table->dropColumn('sale_type');
            $table->dropColumn('floor_min');
            $table->dropColumn('floor_max');
            $table->dropColumn('land_min');
            $table->dropColumn('land_max');
        });
    }
}
