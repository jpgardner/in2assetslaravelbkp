<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('companyname');
            $table->string('street_address');
            $table->string('suburb_town_city');
            $table->string('area');
            $table->string('province');
            $table->string('country');
            $table->string('postcode');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phonenumber');
            $table->string('cellnumber');
            $table->string('communicationpref');
            $table->string('newsletter');
            $table->string('avatar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
