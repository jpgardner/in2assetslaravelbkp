<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDetailsToClassifiedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classifieds', function (Blueprint $table) {
            $table->tinyInteger('det_cell')->after('lead_image');
            $table->tinyInteger('det_email')->after('det_cell');
            $table->tinyInteger('det_company')->after('det_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classifieds', function (Blueprint $table) {
            $table->dropColumn('det_cell');
            $table->dropColumn('det_email');
            $table->dropColumn('det_company');
        });
    }
}
