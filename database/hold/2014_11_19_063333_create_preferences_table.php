<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('suburb_town_city');
            $table->string('area');
            $table->string('province');
            $table->string('country');
            $table->unsignedInteger('min_price');
            $table->unsignedInteger('max_price');
            $table->string('dwelling_type');
            $table->string('bedrooms');
            $table->string('bathrooms');
            $table->string('parking');
            $table->tinyInteger('pets');
            $table->string('farm_type');
            $table->string('commercial_type');
            $table->string('property_type');
            $table->string('listing_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('preferences');
    }
}
