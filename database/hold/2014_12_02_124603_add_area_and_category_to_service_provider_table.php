<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAreaAndCategoryToServiceProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_provider', function (Blueprint $table) {
            $table->integer('category')->after('division');
            $table->string('province')->after('category');
            $table->string('area')->after('province');
            $table->string('suburb_town_city')->after('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_provider', function (Blueprint $table) {
            $table->dropColumn('category');
            $table->dropColumn('province');
            $table->dropColumn('area');
            $table->dropColumn('suburb_town_city');
        });
    }
}
