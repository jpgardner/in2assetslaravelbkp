<?php

return [
    'secret'  => getenv('NOCAPTCHA_SECRET') ?: '6LfqPIgUAAAAAG6R87bezSinhz65ea7ltV1i6W8L',
    'sitekey' => getenv('NOCAPTCHA_SITEKEY') ?: '6LfqPIgUAAAAAG3LtvjCcHKsBFuNNJ5uJpZiz46a',

    'lang'    => app()->getLocale(),

];
